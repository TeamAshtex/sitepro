<footer id="footer" class="site-footer" role="contentinfo">
	<div class="footer-widgets">
		<div class="container">
			<?php $class = apply_filters('dt_footer_widget_class', 'col-lg-3')?>
			<?php if(FOOTER_SIDEBAR_COUNT > 0) { ?>
				<div class="<?php echo $class; ?>">
					<?php dynamic_sidebar('sidebar-footer1');?>
				</div>
			<?php } ?>
			<?php if(FOOTER_SIDEBAR_COUNT > 1) { ?>
				<div class="<?php echo $class; ?>">
					<?php dynamic_sidebar('sidebar-footer2');?>
				</div>
			<?php } ?>
			<?php if(FOOTER_SIDEBAR_COUNT > 2) { ?>
				<div class="<?php echo $class; ?>">
					<?php dynamic_sidebar('sidebar-footer3');?>
				</div>
			<?php } ?>
			<?php if(FOOTER_SIDEBAR_COUNT > 3) { ?>
				<div class="<?php echo $class; ?>">
					<?php dynamic_sidebar('sidebar-footer4');?>
				</div>
			<?php } ?>
		</div>
	</div>
	<div class="copyright <?php echo (has_nav_menu( 'footer-links' )) ? 'footer-links-active' : ''; ?>">
		<div class="container">
			<p class="copyright-text">
			<?php do_action('dt_footer_copyright'); ?>
			</p>
			<?php
			if (has_nav_menu( 'footer-links' )) {
				wp_nav_menu( array(
						'theme_location' => 'footer-links',
						'container_class' => 'ds_footer_links'
				) );
			}
			?>
		</div>
	</div>

</footer>
<?php wp_footer(); ?>
<script>
$=jQuery;
title="<li class='menu-logo'> <h1 class='site-title <?php echo $class; ?>'><a href='<?php echo esc_url( home_url( '/' ) ); ?>' title='<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>' rel='home'><img src='<?php echo esc_url( get_theme_mod( 'logo', false ) ); ?>' alt='<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>'></a></h1></li>";
$( ".last-menu" ).after( title );
$(window).scroll(function() {    
    var scroll = $(window).scrollTop();
    if (scroll >= 72) {
        $(".site-header").addClass("fix");
    }
	 else {
    $(".site-header").removeClass("fix");
	}
});
$.fn.preload = function() {
    this.each(function(){
        $('<img/>')[0].src = this;
    });
}
$(['/wp-content/uploads/2017/03/small-logo.png']).preload();
</script>
<script>
jQuery(document).ready( function(){
    jQuery('#primary-nav').fadeIn(1000);
} );
</script>
</div>
</body>
</html>