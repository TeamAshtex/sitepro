<?php

add_action('widgets_init', create_function('', 'return register_widget("Geodir_Events_AYI_Widget");'));

class Geodir_Events_AYI_Widget extends WP_Widget
{

    /**
     * Class constructor.
     */
    function __construct()
    {
        $widget_ops = array(
            'description' => __('Displays "Are you interested?" widget in event pages', GEODIRECTORY_FRAMEWORK),
            'classname' => 'widget_are_you_interested',
        );
        parent::__construct(false, $name = _x('Whoop > Are you interested?', 'widget name', GEODIRECTORY_FRAMEWORK), $widget_ops);

    }

    /**
     * Display the widget.
     *
     * @param array $args Widget arguments.
     * @param array $instance The widget settings, as saved by the user.
     */
    function widget($args, $instance)
    {
        extract($args);
        if (!get_current_user_id()) {
            return;
        }
        if (get_query_var('post_type') != 'gd_event') {
            return;
        }
        global $post;
        $current_date = date_i18n( 'Y-m-d', time() );
        $event_details = maybe_unserialize($post->recurring_dates);
        $event_start_date = $event_details['event_start'] ? date_i18n('Y-m-d', strtotime($event_details['event_start'])) : '';
        $buttons = false;
        if (strtotime($event_start_date) > strtotime($current_date)) {
            $buttons = true;
        }
        extract($args, EXTR_SKIP);

        $title = empty($instance['title']) ? __('Are You Interested?', GEODIRECTORY_FRAMEWORK) : apply_filters('ayi_widget_title', __($instance['title'], GEODIRECTORY_FRAMEWORK));
        echo $before_widget;
        ?>
        <?php if ($title && $buttons) {
        echo $before_title . $title . $after_title;
    } ?>
        <div class="ayi-html-wrap">
            <?php whoop_ayi_widget_html($post, $buttons); ?>
        </div>
        <?php echo $after_widget; ?>
    <?php
    }

    function update($new_instance, $old_instance)
    {
        //save the widget
        $instance = $old_instance;
        $instance['title'] = strip_tags($new_instance['title']);
        return $instance;
    }

    function form($instance)
    {
        //widgetform in backend
        $instance = wp_parse_args((array)$instance, array('title' => __('Are You Interested?', GEODIRECTORY_FRAMEWORK)));
        $title = strip_tags($instance['title']);
        ?>
        <p><label for="<?php echo $this->get_field_id('title'); ?>"><?php echo __("Widget Title:", GEODIRECTORY_FRAMEWORK); ?> <input class="widefat"
                                                                                         id="<?php echo $this->get_field_id('title'); ?>"
                                                                                         name="<?php echo $this->get_field_name('title'); ?>"
                                                                                         type="text"
                                                                                         value="<?php echo esc_attr($title); ?>"/></label>
        </p>
    <?php
    }

}

function whoop_ayi_widget_html($post, $buttons = false)
{

    $cur_user_interested = event_is_current_user_interested($post->ID);
    if ($buttons) {
    ?>
    <div class="ayi-buttons ayi-border-bottom">
    <?php
            if ($cur_user_interested) {
                if ($cur_user_interested == 'event_rsvp_yes') {
                    ?>
                    <p class="event-cur-user-interested">
                        <?php echo __('You Replied:', GEODIRECTORY_FRAMEWORK); ?>
                        <strong><?php echo __("I'm in!", GEODIRECTORY_FRAMEWORK); ?></strong>
                        <a href="#" data-action="remove" data-type="event_rsvp_yes" data-postid="<?php echo $post->ID; ?>"
                           class="whoop-btn-rsvp"><?php echo __('Cancel', GEODIRECTORY_FRAMEWORK); ?></a>
                    </p>
                <?php
                } elseif ($cur_user_interested == 'event_rsvp_maybe') {
                    ?>
                    <p class="event-cur-user-interested">
                        <?php echo __('You Replied:', GEODIRECTORY_FRAMEWORK); ?>
                        <strong><?php echo __('Sounds Cool', GEODIRECTORY_FRAMEWORK); ?></strong>
                        <a href="#" data-action="remove" data-type="event_rsvp_maybe" data-postid="<?php echo $post->ID; ?>"
                           class="whoop-btn-rsvp"><?php echo __('Cancel', GEODIRECTORY_FRAMEWORK); ?></a>
                    </p>
                <?php
                }
            } else { ?>
                <ul class="inline-layout">
                    <li>
                        <a href="#" data-action="add" data-type="event_rsvp_yes" data-postid="<?php echo $post->ID; ?>"
                           class="whoop-btn whoop-btn-small whoop-btn-full whoop-btn-rsvp whoop-btn-rsvp-yes"><?php echo __("I'm in!", GEODIRECTORY_FRAMEWORK); ?></a>
                    </li>
                    <li>
                        <a href="#" data-action="add" data-type="event_rsvp_maybe" data-postid="<?php echo $post->ID; ?>"
                           class="whoop-btn whoop-btn-small whoop-btn-full whoop-btn-rsvp whoop-btn-rsvp-maybe"><?php echo __("Sounds Cool", GEODIRECTORY_FRAMEWORK); ?></a>
                    </li>
                </ul>
            <?php } ?>
    </div>
    <?php } ?>
    <?php $count = event_interested_people_count($post->ID); ?>
    <div class="widget_bp_whoop_friends_widget ayi-whois-in ayi-border-bottom">
        <h3 class="ayi-section-title"><?php echo __('Who\'s in?', GEODIRECTORY_FRAMEWORK); ?>
            <span><?php $text = sprintf(_n('1 response', '%s responses', $count['yes'], GEODIRECTORY_FRAMEWORK), $count['yes']);
                echo $text; ?></span></h3>
        <?php if ($count['yes'] > 0) { ?>
            <ul class="item-list ayi-list-users">
                <?php get_rsvp_users_for_a_post($post->ID, "event_rsvp_yes"); ?>
            </ul>
        <?php } else { ?>
            <p class="ayi-noone">
                <?php echo __('No one is in yet.', GEODIRECTORY_FRAMEWORK); ?>
            </p>
        <?php } ?>
    </div>
    <div class="widget_bp_whoop_friends_widget ayi-sounds-cool">
        <h3 class="ayi-section-title"><?php echo __('Sounds Cool', GEODIRECTORY_FRAMEWORK); ?>
            <span><?php $text = sprintf(_n('1 response', '%s responses', $count['maybe'], GEODIRECTORY_FRAMEWORK), $count['maybe']);
                echo $text; ?></span></h3>
        <?php if ($count['maybe'] > 0) { ?>
            <ul class="item-list ayi-list-users">
                <?php get_rsvp_users_for_a_post($post->ID, "event_rsvp_maybe"); ?>
            </ul>
        <?php } else { ?>
            <p class="ayi-noone">
                <?php echo __('No one is in yet.', GEODIRECTORY_FRAMEWORK); ?>
            </p>
        <?php } ?>
    </div>
    <?php
    whoop_are_you_interested_js();
}

function whoop_are_you_interested_ajax()
{
    check_ajax_referer('whoop-ayi-nonce', 'whoop_ayi_nonce');
    //set variables
    $action = strip_tags(esc_sql($_POST['btnaction']));
    $type = strip_tags(esc_sql($_POST['type']));
    $post_id = strip_tags(esc_sql($_POST['postid']));

    $rsvp_args = array();
    $rsvp_args['action'] = $action;
    $rsvp_args['type'] = $type;
    $rsvp_args['post_id'] = $post_id;

    geodir_rsvp_add_or_remove($rsvp_args);
    $post = geodir_get_post_info($post_id);
    $current_date = date_i18n( 'Y-m-d', time() );
    $event_details = maybe_unserialize($post->recurring_dates);
    $event_start_date = $event_details['event_start'] ? date_i18n('Y-m-d', strtotime($event_details['event_start'])) : '';
    $buttons = false;
    if (strtotime($event_start_date) > strtotime($current_date)) {
        $buttons = true;
    }
    whoop_ayi_widget_html($post, $buttons);
    wp_die();
}

//Ajax functions
add_action('wp_ajax_whoop_are_you_interested', 'whoop_are_you_interested_ajax');

//Javascript
//add_action('wp_footer', 'whoop_are_you_interested_js');
function whoop_are_you_interested_js()
{
    $ajax_nonce = wp_create_nonce("whoop-ayi-nonce");
    ?>
    <script type="text/javascript">
        jQuery(document).ready(function () {
            jQuery('a.whoop-btn-rsvp').click(function (e) {
                e.preventDefault();
                var container = jQuery('.ayi-html-wrap');
                var btnaction = jQuery(this).attr('data-action');
                var type = jQuery(this).attr('data-type');
                var postid = jQuery(this).attr('data-postid');
                var data = {
                    'action': 'whoop_are_you_interested',
                    'whoop_ayi_nonce': '<?php echo $ajax_nonce; ?>',
                    'btnaction': btnaction,
                    'type': type,
                    'postid': postid
                };

                jQuery.post('<?php echo admin_url('admin-ajax.php'); ?>', data, function (response) {
                    container.html(response);
                });
            })
        });
    </script>
<?php
}