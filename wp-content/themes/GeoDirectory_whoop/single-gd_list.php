<?php get_header(); ?>

<div id="geodir_wrapper" class="geodir-single">
    <?php //geodir_breadcrumb();?>
    <div class="clearfix geodir-common">
        <div id="geodir_content" class="whoop-full-width" role="main">
            <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
                <article id="post-<?php the_ID(); ?>" <?php post_class('cf'); ?> role="article" itemscope
                         itemtype="http://schema.org/BlogPosting">
                    <header class="article-header">
                        <h3 class="entry-title single-title" itemprop="headline">
                            <?php the_title(); ?>
                            <?php
                            global $post;
                            $pid = $post->ID;
                            if (current_user_can('edit_post', $pid)) { ?>
                                <a style="font-size: 10px;" href="<?php echo esc_url(add_query_arg(array('pid' => $pid), home_url('/add-list/'))); ?>">
                                    <?php echo __('Edit Title and Desc', GEODIRECTORY_FRAMEWORK); ?>
                                </a>
                                <a href="<?php echo esc_url(add_query_arg(array('list_id' => $post->ID), bp_get_loggedin_user_link().'lists/')); ?>" class="whoop-btn whoop-btn-small gd-list-view-btn"><?php echo __('View List', GEODIRECTORY_FRAMEWORK); ?></a>
                            <?php } ?>
                        </h3>

                        <p class="gd-list-item-desc">
                            <?php echo stripcslashes(get_the_content()); ?>
                        </p>
                    </header>
                    <?php // end article header ?>
                    <?php
                global $post;
                $cur_post_id = $post->ID;
                if ( current_user_can('edit_post', $cur_post_id) ) { ?>
                    <section class="entry-content cf" itemprop="articleBody">
                        <?php
                        // the content (pretty self explanatory huh)
                            $all_posts = gdlist_get_all_reviewed_posts();
                            $listed_posts = gdlist_get_all_listed_posts();
                            $unlisted_posts = array_diff($all_posts, $listed_posts);
                            ?>
                            <h4><?php echo __('Drag and drop items to create or re-arrange your list.', GEODIRECTORY_FRAMEWORK) ?></h4>
                            <div class="add-items-to-list">
                                <h4><?php echo __('All Your Reviews:', GEODIRECTORY_FRAMEWORK) ?></h4>
                                <ul id="whoop-listSortable-left" class="whoop-listSortable">
                                    <?php
                                    foreach ($unlisted_posts as $id => $title) {
                                        ?>
                                        <li id="post_<?php echo $id; ?>" class="whoop-li-title">
                                            <?php echo $title; ?>
                                        </li>
                                    <?php
                                    }
                                    ?>
                                </ul>
                            </div>
                            <div class="add-items-to-list">
                                <form name="addlistform" class="gd_list_form" id="propertyform" action="#" method="post">
                                    <input type='hidden' id="cur_post_id" name='cur_post_id' value='<?php echo $cur_post_id; ?>'>
                                    <h4><?php echo __('Your List:', GEODIRECTORY_FRAMEWORK) ?></h4>
                                    <button type="submit" id="gd_list_submit_btn" class="whoop-btn whoop-btn-small">
                                        <?php echo __('Done', GEODIRECTORY_FRAMEWORK) ?>
                                    </button>
                                    <ul id="whoop-listSortable-right" class="whoop-listSortable">
                                        <?php
                                        foreach ($listed_posts as $id => $title) {
                                            ?>
                                            <li id="post_<?php echo $id; ?>" class="whoop-li-title">
                                                <?php echo $title; ?>
                                            </li>
                                        <?php
                                        }
                                        ?>
                                    </ul>
                                </form>
                            </div>
                    </section>
                    <?php } ?>
                </article>
                <?php // end article ?>
            <?php endwhile; ?>
            <?php else : ?>
      <article id="post-not-found" class=">
									<header class="article-header">
        <h1>
          <?php _e( 'Oops, Post Not Found!', GEODIRECTORY_FRAMEWORK ); ?>
        </h1>
        </header>
        <section class="entry-content">
          <p>
            <?php _e( 'Uh Oh. Something is missing. Try double checking things.', GEODIRECTORY_FRAMEWORK ); ?>
          </p>
        </section>
      </article>
      <?php endif; ?>
        </div>
    </div>
</div>
<?php get_footer(); ?>
