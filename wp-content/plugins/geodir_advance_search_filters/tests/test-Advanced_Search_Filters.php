<?php
class AdvancedSearch extends WP_UnitTestCase
{
    public function setUp()
    {
        parent::setUp();
        wp_set_current_user(1);
        update_option('geodir_search_display_searched_params', 1);
    }

    public function testAdvancedSearch() {
        global $wpdb;

        $adv_se_sql = "select * from ".GEODIR_ADVANCE_SEARCH_TABLE;
        $adv_se_info = $wpdb->get_results($adv_se_sql);

        $this->assertTrue(empty($adv_se_info));

        $_REQUEST = array (
            'action' =>  'geodir_ajax_advance_search_action',
            'create_field' =>  'true',
            'field_ins_upd' =>  'submit',
            '_wpnonce' =>  wp_create_nonce('custom_advance_search_fields_gd_placecategory'),
            'listing_type' =>  'gd_place',
            'field_type' =>  'taxonomy',
            'field_id' =>  'gd_placecategory',
            'data_type' =>  'SELECT',
            'is_active' =>  '1',
            'site_field_title' =>  'Category',
            'field_data_type' =>  'VARCHAR',
            'search_condition' =>  'SINGLE',
            'site_htmlvar_name' =>  'gd_placecategory',
            'field_title' =>  'gd_placecategory',
            'expand_custom_value' =>  '',
            'search_operator' =>  'AND',
            'front_search_title' =>  'Category',
            'field_desc' =>  'Cat'
        );
        ob_start();
        geodir_advance_search_ajax_handler();
        $output = ob_get_contents();
        ob_end_clean();
        $this->assertContains('Double Click to toggle', $output);


        $adv_se_sql = "select * from ".GEODIR_ADVANCE_SEARCH_TABLE;
        $adv_se_info = $wpdb->get_results($adv_se_sql);

        $this->assertTrue($adv_se_info[0]->site_htmlvar_name == 'gd_placecategory');

        $category = get_term_by('name', 'Attractions', 'gd_placecategory');
        $this->assertTrue(is_int($category->term_id));


        $_REQUEST['geodir_search'] = 1;
        $_REQUEST['sgd_placecategory'] = array($category->term_id);
        $_GET['sgd_placecategory'] = array($category->term_id);
        $query_args = array(
            'post_status' => 'publish',
            'post_type' => 'gd_place',
            'posts_per_page' => 1,
            's' => 'Audacious'
        );

        $all_posts = new WP_Query( $query_args );

        $total_posts = $all_posts->found_posts;

        $this->assertTrue(is_int((int) $total_posts));

        $title = null;
        while ( $all_posts->have_posts() ) : $all_posts->the_post();
            $title = get_the_title();
        endwhile;

        $this->assertEquals($title, 'Audacious Freedom');

    }

    public function testAdminOptionForms() {
        ob_start();
        geodir_autocompleter_options_form('advanced_search_fields');
        $output = ob_get_contents();
        ob_end_clean();
        $this->assertContains('Search Autocompleter Settings', $output);
//        $this->assertContains('Near Autocompleter Settings', $output);
        $this->assertContains('GeoLocation Settings', $output);
    }

    public function testAdvSearchFields() {
        $fields  = array();
        $filters = array();
        // Salary
        $fields[] = array(
            'listing_type' => 'gd_place',
            'data_type' => 'INT',
            'field_type' => 'text',
            'admin_title' => __('Salary', 'directory_starter'),
            'admin_desc' => __('Enter salary.', 'directory_starter'),
            'site_title' => __('Salary', 'directory_starter'),
            'htmlvar_name' => 'job_salary',
            'default_value' => '',
            'option_values' => '',
            'is_default' => '1',
            'is_admin' => '1',
            'clabels' => __('Salary', 'directory_starter')
        );

        // Salary Filter
        $filters[] = array(
            'create_field' => 'true',
            'listing_type' => 'gd_place',
            'field_id' => '',
            'field_type' => 'text',
            'data_type' => 'RANGE',
            'is_active' => '1',
            'site_field_title' => 'Salary',
            'field_data_type' => 'INT',
            'data_type_change' => 'TEXT',
            'search_condition_select' => 'FROM',
            'search_min_value' => '',
            'search_max_value' => '',
            'search_diff_value' => '',
            'first_search_value' => '',
            'first_search_text' => '',
            'last_search_text' => '',
            'search_condition' => 'FROM',
            'site_htmlvar_name' => 'geodir_job_salary',
            'field_title' => 'geodir_job_salary',
            'expand_custom_value' => '',
            'front_search_title' => 'Salary',
            'field_desc' => ''
        );

        // Dist Filter
        $filters[] = array(
            'create_field' => 'true',
            'listing_type' => 'gd_place',
            'field_id' => 'dist',
            'field_type' => 'text',
            'data_type' => 'SINGLE',
            'is_active' => '1',
            'site_field_title' => 'Dist',
            'field_data_type' => 'FLOAT',
            'data_type_change' => 'TEXT',
            'search_condition_select' => 'SINGLE',
            'search_min_value' => '',
            'search_max_value' => '100',
            'search_diff_value' => '5',
            'first_search_value' => '',
            'first_search_text' => '',
            'last_search_text' => '',
            'search_condition' => 'SINGLE',
            'site_htmlvar_name' => 'dist',
            'field_title' => 'dist',
            'expand_custom_value' => '',
            'front_search_title' => 'Dist',
            'field_desc' => '',
        );

        // Custom Fields
        if (!empty($fields)) {
            foreach ($fields as $field_index => $field) {
                    $lastid = geodir_custom_field_save( $field );
                    $this->assertTrue(is_int($lastid));
            }
        }

        // Advance Search Filters
        if (!empty($filters) && function_exists('geodir_load_translation_geodiradvancesearch')) {
            foreach ($filters as $filter_index => $filter) {
                $lastid = geodir_custom_advance_search_field_save($filter);
                $this->assertTrue(is_int($lastid));
            }
        }

        $_REQUEST['subtab'] = 'advance_search';
        ob_start();
        geodir_manage_advace_search_selected_fields('advance_search');
        $output = ob_get_contents();
        ob_end_clean();
        $this->assertFalse(empty($output));

        ob_start();
        $_REQUEST['geodir_search'] = "1";
        $_REQUEST['stype'] = 'gd_place';
        $this->go_to( home_url('/?geodir_search=1&stype=gd_place&s=test&snear=&sgeo_lat=&sgeo_lon=') );
        $_SERVER['QUERY_STRING'] = 'geodir_search=1&stype=gd_place&s=test&snear=&sgeo_lat=&sgeo_lon=';
        $this->load_template();
        $output = ob_get_contents();
        ob_end_clean();
        $this->assertContains('Search Results for', $output);

    }

    public function testAdvSearchForm() {

        $query_args = array(
            'post_status' => 'publish',
            'post_type' => 'gd_place',
            'posts_per_page' => 1,
        );

        $all_posts = new WP_Query( $query_args );
        $post_id = null;
        while ( $all_posts->have_posts() ) : $all_posts->the_post();
            $post_id = get_the_ID();
            ob_start();
            geodir_advance_search_form();
            $output = ob_get_contents();
            ob_end_clean();
            $this->assertFalse(empty($output));
        endwhile;

        $this->assertTrue(is_int($post_id));

    }

    public function testAdvSearchOptions() {
        $default_options = geodir_autocompleter_options();
        ob_start();
        var_dump($default_options);
        $output = ob_get_contents();
        ob_end_clean();
        $this->assertContains('LEAVE BLANK FOR NO DISTANCE LIMIT', $output);
    }

    public function testSearchWhere() {
        ob_start();
        $_REQUEST['geodir_search'] = "1";
        $_REQUEST['stype'] = 'gd_place';
        $this->go_to( home_url('/?geodir_search=1&stype=gd_place&s=test&snear=&sgeo_lat=&sgeo_lon=') );
        $_SERVER['QUERY_STRING'] = 'geodir_search=1&stype=gd_place&s=test&snear=&sgeo_lat=&sgeo_lon=';
        $this->load_template();
        $output = ob_get_contents();
        ob_end_clean();
        $this->assertContains('Search Results for', $output);
    }

    private function load_template() {
        do_action( 'template_redirect' );
        $template = false;
        if	 ( is_404()			&& $template = get_404_template()			) :
        elseif ( is_search()		 && $template = get_search_template()		 ) :
        elseif ( is_front_page()	 && $template = get_front_page_template()	 ) :
        elseif ( is_home()		   && $template = get_home_template()		   ) :
        elseif ( is_post_type_archive() && $template = get_post_type_archive_template() ) :
        elseif ( is_tax()			&& $template = get_taxonomy_template()	   ) :
        elseif ( is_attachment()	 && $template = get_attachment_template()	 ) :
            remove_filter('the_content', 'prepend_attachment');
        elseif ( is_single()		 && $template = get_single_template()		 ) :
        elseif ( is_page()		   && $template = get_page_template()		   ) :
        elseif ( is_category()	   && $template = get_category_template()	   ) :
        elseif ( is_tag()			&& $template = get_tag_template()			) :
        elseif ( is_author()		 && $template = get_author_template()		 ) :
        elseif ( is_date()		   && $template = get_date_template()		   ) :
        elseif ( is_archive()		&& $template = get_archive_template()		) :
        elseif ( is_paged()		  && $template = get_paged_template()		  ) :
        else :
            $template = get_index_template();
        endif;
        /**
         * Filter the path of the current template before including it.
         *
         * @since 3.0.0
         *
         * @param string $template The path of the template to include.
         */
        if ( $template = apply_filters( 'template_include', $template ) ) {
            $template_contents = file_get_contents( $template );
            $included_header = $included_footer = false;
            if ( false !== stripos( $template_contents, 'get_header();' ) ) {
                do_action( 'get_header', null );
                locate_template( 'header.php', true, false );
                $included_header = true;
            }
            include( $template );
            if ( false !== stripos( $template_contents, 'get_footer();' ) ) {
                do_action( 'get_footer', null );
                locate_template( 'footer.php', true, false );
                $included_footer = true;
            }
            if ( $included_header && $included_footer ) {
                global $wp_scripts;
                $wp_scripts->done = array();
            }
        }
        return;
    }

    public function tearDown()
    {
        parent::tearDown();
    }
}
?>