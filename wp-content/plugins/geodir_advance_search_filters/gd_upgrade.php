<?php
global $wpdb;

if (get_option('geodiradvancesearch' . '_db_version') != GEODIRADVANCESEARCH_VERSION) {

    add_action('plugins_loaded', 'geodiradvancesearch_upgrade_all');
    update_option('geodiradvancesearch' . '_db_version', GEODIRADVANCESEARCH_VERSION);
}

function geodiradvancesearch_upgrade_all()
{
    geodir_advance_search_field();
}