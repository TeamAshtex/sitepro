jQuery(document).ready(function ($) {
    $('.timepicker').timepickerjr({ 'forceRoundTime': true, 'scrollDefault': '9:00am', 'step': 15, 'noneOption': [ GDBHDateTimePickerOptions.noTime], 'timeFormat': GDBHDateTimePickerOptions.timeFormat });

    // When a time is picked, clear the checkboxes
    $('.timepicker').on('change', function () {
        $(this).closest('td').siblings().find('input[type="checkbox"]').attr('checked', false);
        $('input#nohours').attr('checked', false);
    });

    // When "Closed all day" clear the other selections
    $('.isClosed').on('click', function () {
        $(this).closest('td').siblings().find('input').val('');
        $(this).closest('td').siblings().find('input[type="checkbox"]').attr('checked', false);
        $('input#nohours').attr('checked', false);
    });

    // When "Open 24 hours" clear the other selections
    $('.is24hours').on('click', function () {
        $(this).closest('td').siblings().find('input').val('');
        $(this).closest('td').siblings().find('input[type="checkbox"]').attr('checked', false);
        $('input#nohours').attr('checked', false);
    });

    // ToDo: Move to a function that can run on page load
    // When choosing to use no hours, clear everything & disable.
    // When choosing to now allow hours, enable everything.
    $('#nohours').on('click', function () {
        if ($('input#nohours').is(":checked")) {
            $('.is24hours').attr('checked', false).attr("disabled", true);
            $('.isClosed').attr('checked', false).attr("disabled", true);
            $('.timepicker').val('').attr("disabled", true);
        } else {
            $('.is24hours').removeAttr("disabled");
            $('.isClosed').removeAttr("disabled");
            $('.timepicker').removeAttr("disabled");
        }
    });

    $('#publish').on( 'click', validateForm );

    $('input[type="submit"]').on( 'click', validateForm );

});

function validateForm() {
    var weekdays = getWeekdays();
    var hasError = false;

    var noHours = jQuery("#nohours").is(":checked");

    if (noHours) {
        return true;
    }

    weekdays.every(function (entry) {
        var openValue = jQuery("#" + entry + "_open").val();
        var openTime = new Date('20 Aug 2000 ' + openValue);
        var is24Hours = jQuery("#" + entry + "_24hours").is(':checked');

        var closeValue = jQuery("#" + entry + "_close").val();
        var closeTime = new Date('20 Aug 2000 ' + closeValue);
        var isClosed = jQuery("#" + entry + "_closed").is(':checked');

        if (false == is24Hours) {
            if (!( isOpenSet(isClosed, openValue) )) {
                hasError = true;
                jQuery("#" + entry + "_open").focus();
                return false;
            }

            if (!( isClosedSet(isClosed, closeValue) )) {
                hasError = true;
                jQuery("#" + entry + "_close").focus();
                return false;
            }
        }

        if (openTime >= closeTime) {
            // There's an exception to allow for bars etc. If closing time is before 5:00 am, we allow it.
            if (closeTime < new Date('20 Aug 2000 5:00 am')) {
                return true;
            }
            hasError = true;
            alert(GDBHDateTimePickerOptions.closeBeforeOpen);
            jQuery("#" + entry + "_close").focus();
            return false;
        }

        return true;
    });

    if (hasError) {
        return false;
    } else {
        return true; // Reset this to false for testing.
    }
}

function isOpenSet(isClosed, openValue) {
    if (!( isClosed ) && ( openValue == '')) {
        alert(GDBHDateTimePickerOptions.missingOpenClose);
        return false;
    }
    return true;
}

function isClosedSet(isClosed, closeValue) {
    if (!( isClosed ) && ( closeValue == '')) {
        alert(GDBHDateTimePickerOptions.missingOpenClose);
        return false;
    }
    return true;
}

function getWeekdays() {
    var weekdays = [ 'monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday', 'sunday' ];

    return weekdays;
}
