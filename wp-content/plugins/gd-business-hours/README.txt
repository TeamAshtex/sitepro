=== GeoDirectory Business Hours ===
Requires at least: 4.0
Tested up to: 4.4.1
Stable tag: 1.3.7.5
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Extension for the WPGeoDirectory plugin to add business hours to an entry.

== Description ==
REQUIRES GeoDirectory v 1.3 or higher plugin
What does this plugin do? It adds the ability to add hours of operation to entries.

Business hours can be entered for each day of the week.

It comes with a widget that will display only on the detail view of a single entry. The widget can be configured to show a "We are currently open." status message.

Standard GeoDirectory widgets that show listings can now have an open/closed indicator on them. NOTE: This overrides the standard GD widget and will require you to stay up to date with both GD Business Hours and GeoDirectory core.

== Installation ==

Upload through the WordPress plugin installer.

== Screenshots ==
1. Custom open/closed images

== Changelog ==
= 1.3.7.5 3/26/2016 =
* Updated: Move to WPGeoDirectory licensing for plugin updates
* New: Introduce Schema.org content
= 1.3.7.4 2/2/2016 =
* Fixed: Remove even more left over debug code
* Fixed: Remove left over debug code
* Fixed: Missing arguments in function
= 1.3.7 2/1/2016 =
* New: Able to pre-check the "Do not use business hours" for listings to allow users to bypass instead of getting the nag
* New: Header for the hours table so you can add descriptive text/instructions - you can style the div with #gdbh_instructions
* New: Replace the existing "Timing" field from WPGeoDirectory and put the hours inline with the other details
= 1.3.6.4 1/25/2016 =
* Fixed: No having chosen a post type or price was throwing an error
= 1.3.6.3 1/24/2016 =
* Fixed: Timezone data now saves properly for posts
= 1.3.6.2 1/21/2016 =
* Fixed: Changed the action used to display "Now open" on widgets for WHOOP! compatibility
* Fixed: Fixed problem when using WHOOP! theme and times not saving from public side
* Update: Removed static http reference to a CDN. Replace with flexible uri
= 1.3.6.1 1/10/2016 =
* Update: Added more checking for missing database column and logging
* Update: Added more checking for missing timezone data
= 1.3.6. 1/9/2016 =
* Update: Added a note on Settings to visit Pricing when using the Payment plugin
* Update: Added more classes on data entry table to allow for styling
* Update: Add more checks for missing database update
* Update: More protection in case listing doesn't have a timezone
= 1.3.5.2 1/2/2016 =
* Update: Fix for listings without physical locations. WP_Error was being returned.
= 1.3.5.1 10/17/2015 =
* Update: Don't re-write the price table structure, just add the column
= 1.3.5 6/10/2015 =
* Update: Remove custom templates - GeoDirectory provides the needed filters now.
= 1.3.3 5/17/2015 =
* Fix: Widgets reverting to LIST view regardless of choosing columns
* Fix: Widget & non-Widget view now appears with listing preview
* Update: Match templates with latest GeoDirectory
= 1.3.2.1 5/15/2015 =
* Fix: Add new template filter to make Popular Locations widget work
= 1.3.2 4/15/2015 =
* Fix: Fix time calculation that was affecting open/closed status
= 1.3.1 2/18/2015 =
* FIX: Prevent 2nd time slot from displaying when no time is entered
= 1.3.0 2/13/2015 =
* NEW: 2nd set of open/closed times for each day
* NEW: Timezones - When saving a listing, the plugin gets the timezone based on the address and uses this to calculate the Open/Closed status. This will allow directories that cover larger areas to be more accurate.
* NEW: Use a custom open/closed image in the sidebar / widget instead of "We are currently open/closed" text.
* Bug fixes
= 1.2.7 1/20/2015 =
* FIX: More problems with editor not appearing on the front end
= 1.2.6 12/16/2014 =
* UPDATE: This version REQUIRES GeoDirectory version 1.3 - there are changes that will break your site if you update one without the other.
* FIX: Problems with hours editor not appearing in the front end
= 1.2.5 11/20/2014 =
* FIX: Bug when not using Price/Package plugin
= 1.2.1 11/11/2014 =
* UPDATE: Support for the default package selection. User no longer has to choose a package before seeing business hours options (if applicable).
= 1.2 11/10/2014 =
* NEW: Able to allow Business Hours per package - Package implies an associated type, so this will OVERRIDE your post type selection.
* To be clear, if the PRICE & PAYMENTS module is active, post type selections are disregarded in favor of packages.
* FIX: Re-enable widget. Not sure why it was missing.
= 1.1.7 11/7/2014 =
* Remove debug code that was outputting my name. Sorry folks.
= 1.1.5 10/29/2014 =
* Fix for "No listings found" - changed the template
= 1.1.4 10/24/2014 =
* Attempt to fix bad release version - No real changes
= 1.1.3 10/23/2014 =
* Update the widget template after change to GeoDirectory.
* Other bug fixes
= 1.1.2 10/23/2014 =
* Remove the bad language file causing XXX to display.
= 1.1.1 10/22/2014 =
* Fix: 24 hour time not being shown when selected
* Fix: Wrong text domain used for some translations
= 1.1.0 10/18/2014 =
* NEW: Small open/closed indicator on GD widgets that show listings
* NEW: Updated translation file with lots of new language (Sorry, I missed them before)
* Fix: Various small bugs
= 1.0.2 10/06/2014 =
* Added option to use 24 hour time everywhere (Entry & Display) (Thanks Thomas)
* Fixed bug where open/closed status compared English day to Translated day (Tuesday !== Dienstag) - Thanks Thomas
= 1.0.1 09/27/20114 =
* Updated language files
* Sidebar shows "Business Hours" head when "do not use business hours" is selected - FIXED
= 1.0 09/27/2014 =
* First public release
