<?php
/**
 * An Extension for the WPGeoDirectory plugin which adds a metabox for
 * adding the business hours of operation and a widget to display
 * them.
 *
 * @package   WPGeoDirectory
 * @category  Extension
 * @author    Jeff Rose
 * @license   GPL-2.0+
 * @link      http://www.solarflarecomputing.com
 * @copyright 2014 Jeff Rose
 *
 * @wordpress-plugin
 * Plugin Name:       GD Business Hours
 * Plugin URI:        http://www.solarflarecomputing.com
 * Description:       An Extension for the WPGeoDirectory plugin which adds a metabox for adding the business hours of operation and a widget to display them.
 * Version:           1.3.7.5
 * Author:            Jeff Rose
 * Author URI:        http://www.solarflarecomputing.com
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       gd_business_hours
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( !defined( 'WPINC' ) ) {
	die;
}

if(is_admin()){
	if(!class_exists('GD_Plugin_Updater')) {//only load the update file if needed
		require_once('gd_update.php'); // require update script
	}
	/*
	 * Checks for updates to this addon.
	 *
	 * @param string $textdomain The textdomain of the addon.
	 * @param string $name The name of the addon shown on update screen.
	 * @param string $version The addon version number.
	 * @param string $download_id The GD download id.
	 * @param string $slug The slug of the addon file.
	 * @param string $notes Notes, these appear under the licence text box on update screen.
	 */
	gd_add_plugin_to_update_list('gd_business_hours','GD Business Hours','1.3.7.5',132849,__FILE__,'');
}


//require 'plugin-updates/plugin-update-checker.php';
//$MyUpdateChecker = PucFactory::buildUpdateChecker( 'http://www.installedforyou.com/updates/?action=get_metadata&slug=gd-business-hours', __FILE__ );

register_activation_hook( __FILE__, 'gdbh_plugin_activation' );

register_deactivation_hook( __FILE__, array(
	'GD_Business_Hours',
	'one_time_cleanup'
) );

function gdbh_plugin_activation() {
	$is_active = get_option( 'gdbh_activated' );
	if ( empty( $is_active ) ) {
		$gdbh = new GD_BH_Init();

		add_option( 'geodir_business_hours_activation_redirect', 1 );

		return $gdbh;
	}
}

if ( !( class_exists( 'GD_BH_Init' ) ) ) {
	class GD_BH_Init{

		public function __construct() {
			GD_BUSINESS_HOURS::gdbh_check_payment_manager();

			return true;
		}
	}
}

if ( !class_exists( 'GD_Business_Hours' ) ) {

	class GD_Business_Hours{

		public $weekdays;

		public function __construct() {

			self::defineConstants();
			self::loadDependencies();

			$this->weekdays = array(
				'sunday' => __( 'Sunday', 'gd_business_hours' ),
				'monday' => __( 'Monday', 'gd_business_hours' ),
				'tuesday' => __( 'Tuesday', 'gd_business_hours' ),
				'wednesday' => __( 'Wednesday', 'gd_business_hours' ),
				'thursday' => __( 'Thursday', 'gd_business_hours' ),
				'friday' => __( 'Friday', 'gd_business_hours' ),
				'saturday' => __( 'Saturday', 'gd_business_hours' ),
			);

			// This is run more often than needed to ensure the column is added if the Payment plugin is activated AFTER this one.
			if ( false == get_option( "gdbh_price_table_updated" ) || '' == get_option( "gdbh_price_table_updated" ) ) {
				gdbh_add_hours_to_price_table();
			}

			/*
			 * Load translation. NOTE: This should be run on the init action hook because
			 * function calls for translatable strings, like __() or _e(), execute before
			 * the language files are loaded will not be loaded.
			 *
			 * NOTE: Any portion of the plugin w/ translatable strings should be bound to the init action hook or later.
			 */
			add_action( 'init', array(
				'GD_Business_Hours',
				'loadTextdomain'
			) );

			// Register CSS and JavaScript.
			add_action( 'wp_enqueue_scripts', array(
				__CLASS__,
				'registerScripts'
			) );

			if ( is_admin() ) {
				// Enqueue the admin CSS and JS
				add_action( 'gd_admin_enqueue_edit_styles', array(
					__CLASS__,
					'adminStyles'
				) );

				add_action( 'admin_enqueue_scripts', array(
					__CLASS__,
					'adminStyles'
				) );

				// Since we're using a custom field, we need to add our own sanitization method.
				add_filter( 'gd_meta_sanitize_field-business_hours', array(
					__CLASS__,
					'sanitize'
				) );

				add_action( 'admin_init', array(
					__CLASS__,
					'admin_init'
				) );

				add_action( 'geodir_before_admin_panel', array(
					__CLASS__,
					'display_messages'
				) );

				add_action( 'add_meta_boxes', array(
					__CLASS__,
					'hours_metabox_add'
				), 10 );

			}

			if ( function_exists( 'is_plugin_active' ) && is_plugin_active( 'geodir_payment_manager/geodir_payment_manager.php' ) ) {
				if ( false == get_option( "gdbh_price_table_updated" ) ) {
					gdbh_add_hours_to_price_table();
				}
				add_action( 'geodir_payment_package_extra_fields', 'gdbh_show_price_fields' );
				add_action( 'geodir_after_save_package', 'gdbh_save_hours_preference' );
			}

			add_action( 'wp_ajax_geodir_hours_ajax_action', array(
				__CLASS__,
				'hours_manager_ajax'
			) );

			add_action( 'wp_ajax_nopriv_geodir_hours_ajax_action', array(
				__CLASS__,
				'hours_manager_ajax'
			) );

			// Business Hours uses a custom field type, so let's add the action to add it.
			add_action( 'gd_meta_field-business_hours', array(
				__CLASS__,
				'field'
			), 10, 2 );

			// Add the business hours option to the admin settings page.
			add_filter( 'gd_content_blocks', array(
				__CLASS__,
				'settingsOption'
			) );

			// Enqueue the public CSS
			add_action( 'wp_enqueue_scripts', array(
				__CLASS__,
				'enqueueScripts'
			), 11 );

			// Register our Schema as required
			add_filter( 'geodir_details_schema', array(
				__CLASS__,
				'add_schema'
			), 10, 2 );

//			 Register the widget.
			$disable_widget = get_option( 'geodir_business_hours_disable_widget' );
			$replace_timing = get_option( 'geodir_business_hours_replace_timing' );

			if ( '1' != $disable_widget ) {
				add_action( 'widgets_init', array( __CLASS__, 'register_widgets' ) );
			} else {
				if ( '1' == $replace_timing ) {
					add_filter( 'geodir_filter_geodir_post_custom_fields', array( 'GD_Business_Hours', 'add_timing_field' ) );
					add_filter( 'geodir_show_geodir_timing', array( 'GD_Business_Hours', 'add_to_sidebar' ) );
				} else {
					add_action( 'geodir_detail_page_sidebar_content', array(
						__CLASS__,
						'add_to_sidebar'
					) );
				}
			}

			if ( isset( $_GET['listing_type'] ) ) {
				$post_type = esc_attr( $_GET['listing_type'] );
			} elseif ( isset( $_GET['pid'] ) ) {
				$post_type = get_post_type( absint( $_GET['pid'] ) );
			}

			add_action( 'wp', array( $this, 'before_main_content' ) );

			if ( self::can_have_hours() ) {
				add_action( 'geodir_after_main_form_fields', array(
					'GD_Business_hours',
					'hours_metabox_content'
				), 0 );
			}

			add_action( 'geodir_after_save_listing', array(
				$this,
				'gdbh_save_listing_meta'
			), 99, 2 );
		}

		public static function before_main_content() {
			if ( geodir_is_page( 'add-listing' ) ) {
				add_action( 'geodir_before_main_content', array( 'GD_BUSINESS_HOURS', 'add_listing_can_have_hours' ) );
			}
		}

		public static function register_widgets() {
			register_widget( "GDBHHoursWidget" );
		}

		public static function one_time_cleanup() {
			global $wpdb;

			$remove_custom_fields_sql = "SELECT ID FROM " . GEODIR_CUSTOM_FIELDS_TABLE . " WHERE htmlvar_name LIKE 'geodir_%day%_open' OR htmlvar_name LIKE 'geodir_%day%_close'";
			$fields                   = $wpdb->get_var( $remove_custom_fields_sql );
			if ( !( empty( $fields ) ) ) {
				foreach ( $fields as $id ) {
					geodir_custom_field_delete( $id );
				}
			}
		}

		public static function gdbh_activation_redirect() {
			if ( get_option( 'geodir_business_hours_activation_redirect', false ) ) {
				delete_option( 'geodir_business_hours_activation_redirect' );
				wp_redirect( admin_url( 'admin.php?page=geodirectory&tab=business_hours_fields&subtab=bh_options' ) );
			}
		}

		/**
		 * Define the constants.
		 *
		 * @access  private
		 * @since  1.0
		 * @return void
		 */
		private static function defineConstants() {

			define( 'GDBH_CURRENT_VERSION', '1.3.7.5' );
			define( 'GDBH_DIR_NAME', plugin_basename( dirname( __FILE__ ) ) );
			define( 'GDBH_BASE_NAME', plugin_basename( __FILE__ ) );
			define( 'GDBH_PATH', plugin_dir_path( __FILE__ ) );
			define( 'GDBH_URL', plugin_dir_url( __FILE__ ) );
		}

		private static function loadDependencies() {

//			require_once( GDBH_PATH . 'includes/class.widgets.php' );
			require_once( GDBH_PATH . 'includes/class.html.php' );
		}


		public static function activate() {


		}

		public static function deactivate() {


		}

		/**
		 * Load the plugin translation.
		 *
		 * Credit: Adapted from Ninja Forms / Easy Digital Downloads.
		 *
		 * @access private
		 * @since 0.7.9
		 * @uses apply_filters()
		 * @uses get_locale()
		 * @uses load_textdomain()
		 * @uses load_plugin_textdomain()
		 * @return (void)
		 */
		public static function loadTextdomain() {

			// Plugin's unique textdomain string.
			$textdomain = 'gd_business_hours';

			// Filter for the plugin languages folder.
			$languagesDirectory = apply_filters( 'geodirectory_lang_dir', GDBH_DIR_NAME . '/languages/' );

			// The 'plugin_locale' filter is also used by default in load_plugin_textdomain().
			$locale = apply_filters( 'plugin_locale', get_locale(), $textdomain );

			// Filter for WordPress languages directory.
			$wpLanguagesDirectory = apply_filters( 'geodirectory_wp_lang_dir', WP_LANG_DIR . '/gd-business-hours/' . sprintf( '%1$s-%2$s.mo', $textdomain, $locale ) );

			// Translations: First, look in WordPress' "languages" folder = custom & update-secure!
			load_textdomain( $textdomain, $wpLanguagesDirectory );

			// Translations: Secondly, look in plugin's "languages" folder = default.
			load_plugin_textdomain( $textdomain, false, $languagesDirectory );
		}

		public static function registerScripts() {

			// If SCRIPT_DEBUG is set and TRUE load the non-minified JS files, otherwise, load the minified files.
//			$min = defined( 'SCRIPT_DEBUG' ) && SCRIPT_DEBUG ? '' : '.min';
			$min = '';
			// Register CSS.
			wp_register_style( 'GDBH-admin', GDBH_URL . "assets/css/gdbh-admin$min.css", array(
				'geodirectory-admin-css'
			), GDBH_CURRENT_VERSION );
			wp_register_style( 'GDBH-public', GDBH_URL . "assets/css/gdbh-public$min.css", '', GDBH_CURRENT_VERSION );
			wp_register_style( 'gdbh-timepicker-css', GDBH_URL . "assets/css/jquery.timepicker.css", '', GDBH_CURRENT_VERSION );

			// Register JavaScript.
			wp_register_script( 'gdbh-timepicker', GDBH_URL . "assets/js/jquery.timepicker$min.js", array( 'jquery' ), '1.4.3' );

			wp_register_script( 'jquery-validate', '//ajax.aspnetcdn.com/ajax/jquery.validate/1.13.0/jquery.validate.min.js', array( 'jquery' ) );
			wp_register_script( 'GDBH-ui-js', GDBH_URL . "assets/js/gdbh.js", array( 'gdbh-timepicker' ), GDBH_CURRENT_VERSION, true );

			//wp_register_script( 'GDBH-ui-js', GDBH_URL . "assets/js/GDBH-common$min.js", array( 'jquery-timepicker' ), GDBH_CURRENT_VERSION, true );

			wp_localize_script( 'GDBH-ui-js', 'GDBHDateTimePickerOptions', GD_Business_Hours::dateTimePickerOptions() );

		}

		/**
		 * Enqueues the CSS on the admin pages only.
		 *
		 * @access private
		 * @since  1.0
		 *
		 * @param  string $pageHook The current admin page hook.
		 *
		 * @return void
		 */
		public static function adminStyles( $pageHook ) {
			global $post;
			self::registerScripts();
			if ( !( empty( $post ) ) && ( 'page' != $post->post_type && 'post' != $post->post_type ) ) {
				wp_enqueue_style( 'GDBH-admin' );
				wp_enqueue_style( 'gdbh-timepicker-css' );
				wp_enqueue_script( 'jquery-validate' );
				wp_enqueue_script( 'gdbh-timepicker' );
				wp_enqueue_script( 'GDBH-ui-js' );
			}
		}

		/**
		 * Enqueues the CSS.
		 *
		 * NOTE: This will only be enqueued if Form is installed and active
		 * because a CSS file registered by Form is listed as a dependancy
		 * when registering 'GDBH-public'.
		 *
		 * @access private
		 * @since  1.0
		 * @return void
		 */
		public static function enqueueScripts() {
			wp_enqueue_style( 'GDBH-public' );
			wp_enqueue_style( 'gdbh-timepicker-css' );
			wp_enqueue_script( 'GDBH-ui-js' );
		}

		public static function dateTimePickerOptions() {
			$time_format = 'g:i a';
			$is_military = get_option( 'geodir_business_hours_time_format' );
			if ( $is_military ) {
				$time_format = 'G:i';
			}

			$options = array(
				'noTime' => __( 'No time', 'gd_business_hours' ),
				'missingOpenClose' => __( 'Both open and closing times need to be specified, unless "Open all day" or "Closed all day" is checked.', 'gd_business_hours' ),
				'closeBeforeOpen' => __( 'Closing time must be AFTER opening time.', 'gd_business_hours' ),
				'timeFormat' => $time_format,
			);

			return apply_filters( 'GDBH_timepicker_options', $options );
		}

		public static function timeFormat() {

			return apply_filters( 'GDBH_time_format', get_option( 'time_format' ) );
		}

		public static function formatTime( $value, $format = null ) {

			$format = is_null( $format ) ? self::timeFormat() : $format;

			if ( strlen( $value ) > 0 ) {

				return date( $format, strtotime( $value ) );

			} else {

				return $value;
			}
		}

		public static function getWeekdays() {
			global $wp_locale;

			// Output the weekdays sorted by the start of the week
			// set in the WP General Settings. The array keys need to be
			// retained which is why array_shift and array push are not
			// being used.
			$weekStart = apply_filters( 'GDBH_start_of_week', get_option( 'start_of_week' ) );
			$weekday   = $wp_locale->weekday;

			if ( is_plugin_active( 'piglatin/piglatin.php' ) ) {
				$weekday = array(
					'Sunday',
					'Monday',
					'Tuesday',
					'Wednesday',
					'Thursday',
					'Friday',
					'Saturday',
				);
			}

			for ( $i = 0; $i < $weekStart; $i ++ ) {

				$day = array_slice( $weekday, 0, 1, true );
				unset( $weekday[$i] );

				$weekday = $weekday + $day;
			}

			return $weekday;
		}

		public static function settingsOption( $blocks ) {

			$blocks['business_hours'] = __( 'Business Hours', 'gd_business_hours' );

			return $blocks;
		}

		public static function field( $field, $value ) {

			?>

			<table name="start_of_week" id="start_of_week">

				<thead>
				<th><?php _e( 'Weekday', 'gd_business_hours' ); ?></th>
				<td><?php _e( 'Open', 'gd_business_hours' ); ?></td>
				<td><?php _e( 'Close', 'gd_business_hours' ); ?></td>
				<td><?php _e( 'Add / Remove Period', 'gd_business_hours' ); ?></td>
				</thead>

				<tfoot>
				<th><?php _e( 'Weekday', 'gd_business_hours' ); ?></th>
				<td><?php _e( 'Open', 'gd_business_hours' ); ?></td>
				<td><?php _e( 'Close', 'gd_business_hours' ); ?></td>
				<td><?php _e( 'Add / Remove Period', 'gd_business_hours' ); ?></td>
				</tfoot>

				<tbody>

				<tr id="GDBH-period" style="display: none;">
					<td>&nbsp;</td>
					<td>
						<?php

						gdHTML::field( array(
							'type' => 'text',
							'class' => '',
							'id' => $field['id'] . '[day][period][open]',
							'required' => false,
							'label' => '',
							'before' => '',
							'after' => '',
							'return' => false,
						) );

						?>
					</td>
					<td>
						<?php

						gdHTML::field( array(
							'type' => 'text',
							'class' => '',
							'id' => $field['id'] . '[day][period][close]',
							'required' => false,
							'label' => '',
							'before' => '',
							'after' => '',
							'return' => false,
						) );

						?>
					</td>
					<td><span class="button GDBH-remove-period">&ndash;</span><span class="button GDBH-add-period">+</span></td>
				</tr>

				<?php

				foreach ( self::getWeekdays() as $key => $day ) {

					// If there are no periods saved for the day,
					// add an empty period to prevent index not found errors.
					if ( !isset( $value[$key] ) ) {

						$value[$key] = array(
							0 => array(
								'open' => '',
								'close' => ''
							),
						);
					}

					foreach ( $value[$key] as $period => $data ) {

						$open = gdHTML::field( array(
							'type' => 'text',
							'class' => 'timepicker',
							'id' => $field['id'] . '[' . $key . '][' . $period . '][open]',
							'required' => false,
							'label' => '',
							'before' => '',
							'after' => '',
							'return' => true,
						), self::formatTime( $data['open'] ) );

						$close = gdHTML::field( array(
							'type' => 'text',
							'class' => 'timepicker',
							'id' => $field['id'] . '[' . $key . '][' . $period . '][close]',
							'required' => false,
							'label' => '',
							'before' => '',
							'after' => '',
							'return' => true,
						), self::formatTime( $data['close'] ) );

						if ( $period == 0 ) {

							// Display the "+" button only. This button should only be shown on the first period of the day.
							$buttons = sprintf( '<span class="button GDBH-remove-period" data-day="%1$d" data-period="%2$d" style="display: none;">–</span><span class="button GDBH-add-period" data-day="%1$d" data-period="%2$d">+</span>', $key, $period );

						} else {

							// Display both buttons. Both buttons should be shown for every period after the first.
							$buttons = sprintf( '<span class="button GDBH-remove-period" data-day="%1$d" data-period="%2$d">–</span><span class="button GDBH-add-period" data-day="%1$d" data-period="%2$d">+</span>', $key, $period );
						}

						printf( '<tr %1$s %2$s %3$s><th>%4$s</th><td>%5$s</td><td>%6$s</td><td>%7$s</td></tr>', 'class="GDBH-day-' . absint( $key ) . '"', $period == 0 ? 'id="GDBH-day-' . absint( $key ) . '"' : '', $period == 0 ? 'data-count="' . absint( count( $value[$key] ) - 1 ) . '"' : '', $period == 0 ? esc_attr( $day ) : '&nbsp;', $open, $close, $buttons );
					}

				}

				?>

				</tbody>
			</table>

			<?php

			printf( '<p>%s</p>', __( 'To create a closed day or closed period within a day, leave both the open and close hours blank.', 'gd_business_hours' ) );

			// Enqueue the JS required for the metabox.
			wp_enqueue_script( 'GDBH-ui-js' );
		}

		/**
		 * Sanitize the times as a text input using the gdSanitize class.
		 *
		 * @access  private
		 * @since  1.0
		 *
		 * @param  array $value The opening/closing hours.
		 *
		 * @return array
		 */
		public static function sanitize( $value ) {

			if ( empty( $value ) ) {
				return $value;
			}

			foreach ( $value as $key => $day ) {

				foreach ( $day as $period => $time ) {

					// Save all time values in 24hr format.
					$time['open']  = self::formatTime( $time['open'], 'H:i' );
					$time['close'] = self::formatTime( $time['close'], 'H:i' );

					$value[$key][$period]['open']  = gdSanitize::string( 'text', $time['open'] );
					$value[$key][$period]['close'] = gdSanitize::string( 'text', $time['close'] );

				}
			}

			return $value;
		}

		/**
		 * The output of the business hour data.
		 *
		 * Called by the gd_meta_output_field-GDBH action in gdOutput->getMetaBlock().
		 *
		 * @access  private
		 * @since  1.0
		 *
		 * @param  string $id The field id.
		 * @param  array $value The business hours data.
		 * @param  array $atts The shortcode atts array passed from the calling action.
		 *
		 * @return string
		 */
		public static function block( $id, $value, $object = null, $atts ) {
			global $wp_locale;

			$defaults = array(
				'header' => true,
				'footer' => false,
				'day_name' => 'full',
				// Valid options are 'full', 'abbrev' or 'initial'.
				'show_closed_day' => true,
				'show_closed_period' => false,
				'show_if_no_hours' => false,
				'show_open_status' => true,
				'highlight_open_period' => true,
				'open_close_separator' => '&ndash;'
			);

			$atts = wp_parse_args( $atts, $defaults );

			if ( !self::hasOpenHours( $value ) ) {
				return;
			}

			echo '<div class="GDBH-block">';

			// Whether or not to display the open status message.
			if ( $atts['show_open_status'] && self::openStatus( $value ) ) {
				$open_text  = esc_html( get_option( 'business_hours_currently_open_text' ) );
				$open_image = esc_html( get_option( 'business_hours_currently_open_image' ) );

				if ( empty( $open_text ) && empty( $open_image ) ) {
					$open_text = __( 'We are currently open.', 'gd_business_hours' );
				}

				if ( !( empty( $open_image ) ) ) {
					echo '<img src="' . esc_attr( $open_image ) . '" id="gdbh_currently_open" />';
				} else {
					printf( '<p class="GDBH-status GDBH-status-open">%s</p>', $open_text );
				}

			} elseif ( $atts['show_open_status'] ) {
				$closed_text  = esc_html( get_option( 'business_hours_currently_closed_text' ) );
				$closed_image = esc_html( get_option( 'business_hours_currently_closed_image' ) );

				if ( empty( $closed_text ) && empty( $closed_image ) ) {
					$closed_text = __( 'Sorry, we are currently closed.', 'gd_business_hours' );
				}

				if ( !( empty( $closed_image ) ) ) {
					echo '<img src="' . esc_attr( $closed_image ) . '"  id="gdbh_currently_closed" />';
				} else {
					printf( '<p class="GDBH-status GDBH-status-closed">%s</p>', $closed_text );
				}
			}

			?>

			<table class="GDBH">

				<?php if ( $atts['header'] ) : ?>

					<thead>
					<th>&nbsp;</th>
					<th><?php _e( 'Open', 'gd_business_hours' ); ?></th>
					<th class="GDBH-separator">&nbsp;</th>
					<th><?php _e( 'Close', 'gd_business_hours' ); ?></th>
					</thead>

				<?php endif; ?>

				<?php if ( $atts['footer'] ) : ?>

					<tfoot>
					<th>&nbsp;</th>
					<th><?php _e( 'Open', 'gd_business_hours' ); ?></th>
					<th class="GDBH-separator">&nbsp;</th>
					<th><?php _e( 'Close', 'gd_business_hours' ); ?></th>
					</tfoot>

				<?php endif; ?>

				<tbody>
				<?php
				$timeformat  = 'g:i a';
				$is_military = get_option( 'geodir_business_hours_time_format' );

				if ( '1' == $is_military ) {
					$timeformat = 'H:i';
				}
				$open_24_hours_text = esc_html( get_option( 'business_hours_open_24_hours_text' ) );
				if ( empty( $open_24_hours_text ) ) {
					$open_24_hours_text = __( 'Open 24 Hours', 'gd_business_hours' );
				}
				$closed_today_text = esc_html( get_option( 'business_hours_closed_today_text' ) );
				if ( empty( $closed_today_text ) ) {
					$closed_today_text = __( 'Closed Today', 'gd_business_hours' );
				}

				$weekdays = self::getWeekdays();

				foreach ( $weekdays as $key => $full_day ) {

					// Display the day as either its initial or abbreviation.
					switch ( $atts['day_name'] ) {

						case 'initial' :

							$day = $wp_locale->get_weekday_initial( $full_day );
							break;

						case 'abbrev' :

							$day = $wp_locale->get_weekday_abbrev( $full_day );
							break;

						case 'full' :
							$day = $full_day;
					}

					// Show the "Closed" message if there are no open and close hours recorded for the day.
					if ( $atts['show_closed_day'] && !self::openToday( $value, $full_day ) ) {

						printf( '<tr %1$s %2$s><th>%3$s</th><td class="GDBH-closed" colspan="3">%4$s</td></tr>', 'class="GDBH-day-' . absint( $key ) . '"', 'id="GDBH-day-' . absint( $key ) . '"', esc_attr( $day ), $closed_today_text );

						// Exit this loop.
						continue;
					} elseif ( !self::openToday( $value, $full_day ) ) {
						continue;
					}

					$full_day = strtolower( $full_day );
					$period   = 0;

					if ( isset( $value[$full_day . '_24hours'] ) && '24hours' == $value[$full_day . '_24hours'] ) {
						printf( '<tr %1$s %2$s><th>%3$s</th><td class="GDBH-open" colspan="3">%4$s</td></tr>', 'class="GDBH-day-' . absint( $key ) . ' GDBH-open-period"', 'id="GDBH-day-' . absint( $key ) . '"', esc_attr( $day ), $open_24_hours_text );
					} else {
						printf( '<tr %1$s %2$s %3$s><th>%4$s</th><td class="GDBH-open">%5$s</td><td class="GDBH-separator">%6$s</td><td class="GDBH-close">%7$s</td></tr>', 'class="GDBH-day-' . absint( $key ) . ( $atts['highlight_open_period'] && date( 'w', current_time( 'timestamp' ) ) == $key && self::isOpen( $value[$full_day . '_open'], $value[$full_day . '_close'] ) ? ' GDBH-open-period' : '' ) . '"', $period == 0 ? 'id="GDBH-day-' . absint( $key ) . '"' : '', "", esc_attr( $day ), date_i18n( $timeformat, strtotime( $value[$full_day . '_open'] ) ), esc_attr( $atts['open_close_separator'] ), date_i18n( $timeformat, strtotime( $value[$full_day . '_close'] ) ) );
						if ( isset( $value[$full_day . '_open_2'] ) && ( !( empty( $value[$full_day . '_open_2'] ) ) ) ) {
							printf( '<tr %1$s %2$s %3$s><th>%4$s</th><td class="GDBH-open">%5$s</td><td class="GDBH-separator">%6$s</td><td class="GDBH-close">%7$s</td></tr>', 'class="GDBH-day-' . absint( $key ) . ( $atts['highlight_open_period'] && date( 'w', current_time( 'timestamp' ) ) == $key && self::isOpen( $value[$full_day . '_open'], $value[$full_day . '_close'] ) ? ' GDBH-open-period' : '' ) . '"', $period == 0 ? 'id="GDBH-day-' . absint( $key ) . '"' : '', "", '&nbsp;', date_i18n( $timeformat, strtotime( $value[$full_day . '_open_2'] ) ), esc_attr( $atts['open_close_separator'] ), date_i18n( $timeformat, strtotime( $value[$full_day . '_close_2'] ) ) );
						}
					}
				}

				?>

				</tbody>
			</table>

			<?php

			echo '</div>';
		}

		public static function openStatus( $value ) {
			foreach ( self::getWeekdays() as $day ) {
				$day = strtolower( $day );

				$today = strtolower( date_i18n( "l", current_time( 'timestamp' ) ) );

				if ( is_plugin_active( 'piglatin/piglatin.php' ) ) {
					$today = strtolower( date( "l", current_time( 'timestamp' ) ) );
				}

				if ( $day == $today && ( isset( $value[$day . '_24hours'] ) && '24hours' == $value[$day . '_24hours'] ) ) {
					return true;
				}

				if ( $day == $today && ( isset( $value[$day . '_open'] ) && self::isOpen( $value[$day . '_open'], $value[$day . '_close'] ) ) ) {
					return true;
				}

				if ( $day == $today && ( isset( $value[$day . '_open_2'] ) && self::isOpen( $value[$day . '_open_2'], $value[$day . '_close_2'] ) ) ) {
					return true;
				}

			}

			return false;
		}

		/**
		 * Whether or not there are any open hours during the week.
		 *
		 * @access  private
		 * @since  1.0
		 *
		 * @param  array $days
		 *
		 * @return boolean
		 */
		public static function hasOpenHours( $days ) {
			$weekdays = self::getWeekdays();

			foreach ( $weekdays as $day ) {
				$day = str_replace( '"', '', ltrim( rtrim( $day ) ) );

				if ( ( isset( $days[strtolower( $day ) . '_open'] ) ) && ( !empty( $days[strtolower( $day ) . '_open'] ) ) ) {
					return true;
				}

				if ( ( isset( $days[strtolower( $day ) . '_closed'] ) ) && ( 'closed' == $days[strtolower( $day ) . '_closed'] ) ) {
					return true;
				}

				if ( ( isset( $days[strtolower( $day ) . '_24hours'] ) ) && ( '24hours' == $days[strtolower( $day ) . '_24hours'] ) ) {
					return true;
				}
			}

			return false;

		}

		/**
		 * Whether or not the day has any open periods.
		 *
		 * @access  private
		 * @since  1.0
		 *
		 * @param array $value
		 * @param  array $day
		 *
		 * @return bool
		 */
		private static function openToday( $value, $day ) {
			$day = strtolower( $day );

			if ( isset( $value[$day . '_open'] ) && !( empty( $value[$day . '_open'] ) ) ) {
				return true;
			}

			if ( isset( $value[$day . '_24hours'] ) && ( '24hours' == $value[$day . '_24hours'] ) ) {
				return true;
			}

			return false;
		}

		/**
		 * Whether or not the period is open.
		 *
		 * @access  private
		 * @since  1.0
		 *
		 * @param  array $period
		 *
		 * @return bool
		 */
		private static function openPeriod( $period ) {

			if ( empty( $period ) ) {
				return false;
			}

			if ( !empty( $period['open'] ) && !empty( $period['close'] ) ) {
				return true;
			}

			return false;
		}

		private static function isOpen( $t1, $t2, $tn = null ) {
			global $post;

			$timezone_data = get_post_meta( $post->ID, 'gdbh_timezone', true );
			if ( !empty( $timezone_data ) ) {
				$tzstring = $timezone_data->timeZoneId;

				date_default_timezone_set( $tzstring );
			}

			$current_time = time();

			$tn = is_null( $tn ) ? date( 'H:i', $current_time ) : self::formatTime( $tn, 'H:i' );

			$t1 = strtotime( $t1 );
			$t2 = strtotime( $t2 );
			$tn = strtotime( $tn );
			// If $t2 (close) is later or equal to $t1(open) ie: Close is after open
			if ( $t2 >= $t1 ) {
				// if $t1 (open) is before or equal to $tn (now), and $tn (now) is before $t2 (close) return true (we're open)
				return $t1 <= $tn && $tn < $t2;
			} else {
				// Close is before open
				// if $t2 (close) is before or equal to $tn (now), and $tn is before $t1 return NOT(true)
				return !( $t2 <= $tn && $tn < $t1 );
			}

		}

		public function gdbh_save_listing_meta( $post_id, $post_info ) {

			self::update_timezone_details( $post_id );
			$weekdays = self::getWeekdays();
			$weekdays = array_values( $weekdays );

			if ( isset( $post_info['nohours'] ) && 'nohours' == $post_info['nohours'] ) {
				$hours['nohours'] = 'nohours';
			} else {
				$hours['nohours'] = '';
				foreach ( $weekdays as $day_name ) {
					$day_name = strtolower( $day_name );
					if ( isset( $post_info[$day_name . '_closed'] ) && ( 'closed' == $post_info[$day_name . '_closed'] ) ) {
						$hours[$day_name . '_open']    = '';
						$hours[$day_name . '_close']   = '';
						$hours[$day_name . '_24hours'] = '';
						$hours[$day_name . '_closed']  = 'closed';
					} elseif ( isset( $post_info[$day_name . '_24hours'] ) ) {
						$hours[$day_name . '_open']    = '';
						$hours[$day_name . '_close']   = '';
						$hours[$day_name . '_closed']  = '';
						$hours[$day_name . '_24hours'] = '24hours';
					} elseif ( isset( $post_info[$day_name . '_open'] ) ) {
						$hours[$day_name . '_open']    = isset( $post_info[$day_name . '_open'] ) ? $post_info[$day_name . '_open'] : '';
						$hours[$day_name . '_close']   = isset( $post_info[$day_name . '_close'] ) ? $post_info[$day_name . '_close'] : '';
						$hours[$day_name . '_open_2']  = isset( $post_info[$day_name . '_open_2'] ) ? $post_info[$day_name . '_open_2'] : '';
						$hours[$day_name . '_close_2'] = isset( $post_info[$day_name . '_close_2'] ) ? $post_info[$day_name . '_close_2'] : '';
						$hours[$day_name . '_closed']  = 'open';
						$hours[$day_name . '_24hours'] = '';
					}
				}
			}

			if ( !( empty( $hours ) ) ) {
				update_post_meta( $post_id, 'business_hours', $hours );
			}
		}

		public function update_timezone_details( $post_id ) {
			// I'm using the regular WordPress post_meta here, it's not worth
			// The extra column in the other table since it's seldom used and
			// Not searched or used in other intense computations.
//			$timezone  = get_post_meta( $post_id, 'timezone', true );
			$latitude  = geodir_get_post_meta( $post_id, 'post_latitude', true );
			$longitude = geodir_get_post_meta( $post_id, 'post_longitude', true );
			$timestamp = time();

			if ( empty( $latitude ) || empty( $longitude ) ) {
				return;
			}

			$tz_query_url = 'https://maps.googleapis.com/maps/api/timezone/json?location=' . $latitude . ',' . $longitude . '&timestamp=' . $timestamp;

			$tz_data = wp_remote_get( $tz_query_url );

			if ( is_wp_error( $tz_data ) ) {
				return;
			}

			if ( is_array( $tz_data ) ) {
				if ( isset( $tz_data['response']['code'] ) && ( 200 == $tz_data['response']['code'] ) ) {
					$data = json_decode( $tz_data['body'] );

					if ( 'ok' == strtolower( $data->status ) ) {
						update_post_meta( $post_id, 'gdbh_timezone', $data );
					}
				}
			}
		}

		public static function admin_init() {
			add_filter( 'geodir_settings_tabs_array', array(
				__CLASS__,
				'geodir_admin_business_hours_tabs'
			) );

			add_action( 'geodir_admin_option_form', array(
				__CLASS__,
				'geodir_get_admin_hours_form'
			) );
		}

		public static function geodir_get_admin_hours_form() {
			if ( isset( $_GET['subtab'] ) && 'bh_options' === $_GET['subtab'] ) {
				self::gdbh_options_form();
			}
		}

		public static function geodir_admin_business_hours_tabs( $tabs ) {
			$tabs['business_hours_fields'] = array(
				'label' => __( 'Business Hours', 'gd_business_hours' ),
				'subtabs' => array(
					array(
						'subtab' => 'bh_options',
						'label' => __( 'Options', 'gd_business_hours' ),
						'form_action' => admin_url( 'admin-ajax.php?action=geodir_hours_ajax_action' )
					),
				)
			);

			return $tabs;
		}

		public static function gdbh_options_form() {
			geodir_admin_fields( self::gdbh_default_options() ); ?>

			<p class="submit">

				<input name="save" class="button-primary" type="submit" value="<?php _e( 'Save changes', 'gd_business_hours' ); ?>"/>
				<input type="hidden" name="subtab" value="bh_options" id="last_tab"/>
			</p>
			</div>

			<?php
		}

		public static function gdbh_default_options() {

			$arr[] = array(
				'name' => __( 'Options', 'gd_business_hours' ),
				'type' => 'no_tabs',
				'desc' => '',
				'id' => 'hours_options'
			);

			$arr[] = array(
				'name' => __( 'Business Hours Options', 'gd_business_hours' ),
				'type' => 'sectionstart',
				'id' => 'hours_default_options'
			);

			if ( !( is_plugin_active( 'geodir_payment_manager/geodir_payment_manager.php' ) ) ) {
				$arr[] = array(
					'name' => __( 'Choose post types that allow business hours', 'gd_business_hours' ),
					'desc' => __( 'If you are using price packages, the setting for those will override these.', 'gd_busines_hours' ),
					'tip' => '',
					'id' => 'geodir_post_types_business_hours',
					'css' => 'min-width:300px;',
					'std' => array(),
					'type' => 'multiselect',
					'placeholder_text' => __( 'Select post types', 'gd_business_hours' ),
					'class' => 'chosen_select',
					'options' => array_unique( self::geodir_business_hours_type_setting() )
				);
			} else {
				$arr[] = array(
					'name' => __( 'Choose post types that allow business hours', 'gd_business_hours' ),
					'desc' => sprintf( __( 'Visit the <a href="%s">Geodirectory settings for prices</a> to activate Vouchers on select packages.', 'gd_busines_hours' ), 'admin.php?page=geodirectory&tab=paymentmanager_fields&subtab=geodir_payment_general_options' ),
					'tip' => '',
					'type' => 'html_content',
				);
			}

			$arr[] = array(
				'name' => __( 'Use 24 hour format', 'gd_business_hours' ),
				'desc' => __( 'Show all times in 24 hour format', 'gd_business_hours' ),
				'tip' => '',
				'id' => 'geodir_business_hours_time_format',
				'css' => 'min-width:300px;',
				'std' => 'g:i a',
				'type' => 'checkbox'
			);

			$arr[] = array(
				'name' => __( 'Pre-check "Do not use" by default', 'gd_business_hours' ),
				'desc' => __( 'Pre-check the "Do not use business hours" option so users can more easily skip if needed', 'gd_business_hours' ),
				'tip' => '',
				'id' => 'geodir_business_hours_precheck_no_hours',
				'css' => 'min-width:300px;',
				'std' => 'precheck_no_hours',
				'type' => 'checkbox'
			);

			$arr[] = array(
				'name' => __( 'Instruction Text', 'gd_business_hours' ),
				'desc' => __( 'Adds a div to the top of the table. You can target it with CSS as #gdbh_instructions.', 'gd_business_hours' ),
				'id' => 'geodir_business_hours_instruction_text',
				'css' => 'min-width:300px;',
				'std' => '',
				'type' => 'textarea',
				'placeholder_text' => '',
			);

			$arr[] = array(
				'name' => __( 'Show status on GD widgets', 'gd_business_hours' ),
				'desc' => __( 'Display the open or closed status in any widget displaying a listing. Note, this replaces the normal widget', 'gd_business_hours' ),
				'tip' => '',
				'id' => 'geodir_business_hours_show_status_widget',
				'css' => 'min-width:300px;',
				'std' => 'show_status_widget',
				'type' => 'checkbox'
			);

			$arr[] = array(
				'name' => __( 'Disable widget', 'gd_business_hours' ),
				'desc' => __( 'Disable the widget & display above the address block in the sidebar', 'gd_business_hours' ),
				'tip' => '',
				'id' => 'geodir_business_hours_disable_widget',
				'css' => 'min-width:300px;',
				'std' => 'disable_widget',
				'type' => 'checkbox'
			);

			$arr[] = array(
				'name' => __( 'Replace Built-in Timing field', 'gd_business_hours' ),
				'desc' => __( 'Display in place of the regular Time block between Address and Phone. You must also choose to disable the widget.', 'gd_business_hours' ),
				'tip' => '',
				'id' => 'geodir_business_hours_replace_timing',
				'css' => 'min-width:300px;',
				'std' => 'replace_timing',
				'type' => 'checkbox'
			);

			$arr[] = array(
				'name' => '',
				'desc' => __( 'The settings below affect the non-widget version when the above option is checked.', 'gd_business_hours' ),
				'type' => 'html_content',
			);

			$arr[] = array(
				'name' => __( 'Section Header', 'gd_business_hours' ),
				'desc' => '',
				'id' => 'geodir_business_hours_section_header',
				'css' => 'min-width:300px;',
				'std' => __( 'Business Hours', 'gd_business_hours' ),
				'type' => 'text',
				'placeholder_text' => __( 'Business Hours', 'gd_business_hours' ),
			);

			$arr[] = array(
				'name' => __( 'Show open status', 'gd_business_hours' ),
				'desc' => __( 'Show the current status of the listing based on LISTING time zone(Open or closed).', 'gd_business_hours' ),
				'tip' => '',
				'id' => 'geodir_business_hours_show_open_status',
				'css' => 'min-width:300px;',
				'std' => 'show_open_status',
				'type' => 'checkbox'
			);

			$arr[] = array(
				'name' => __( 'Highlight open hours', 'gd_business_hours' ),
				'desc' => __( 'Highlight the open hours in the list when the listing is open.', 'gd_business_hours' ),
				'tip' => '',
				'id' => 'geodir_business_hours_highlight_open_hours',
				'css' => 'min-width:300px;',
				'std' => 'highlight_open_hours',
				'type' => 'checkbox'
			);

			$arr[] = array(
				'name' => __( 'Show Open/Close header', 'gd_business_hours' ),
				'desc' => __( 'Show column headers for Open and Close hours at the top', 'gd_business_hours' ),
				'tip' => '',
				'id' => 'geodir_business_hours_show_open_header',
				'css' => 'min-width:300px;',
				'std' => 'show_open_header',
				'type' => 'checkbox'
			);

			$arr[] = array(
				'name' => __( 'Show Open/Close footer', 'gd_business_hours' ),
				'desc' => __( 'Show column headers for Open and Close hours at the bottom', 'gd_business_hours' ),
				'tip' => '',
				'id' => 'geodir_business_hours_show_open_footer',
				'css' => 'min-width:300px;',
				'std' => 'show_open_footer',
				'type' => 'checkbox'
			);

			$arr[] = array(
				'name' => __( 'Show "Closed Today" message', 'gd_business_hours' ),
				'desc' => __( 'Show days that are closed with "Closed Today" or custom text.', 'gd_business_hours' ),
				'tip' => '',
				'id' => 'geodir_business_hours_show_closed_today',
				'css' => 'min-width:300px;',
				'std' => 'show_closed_today',
				'type' => 'checkbox'
			);

			$arr[] = array(
				'name' => __( 'Display day name as', 'gd_business_hours' ),
				'desc' => '',
				'tip' => '',
				'id' => 'geodir_business_hours_day_name',
				'css' => 'min-width:300px;',
				'std' => array(),
				'type' => 'select',
				'options' => array(
					'full' => __( 'Full Name', 'gd_business_hours' ),
					'abbrev' => __( 'Abbreviated', 'gd_business_hours' ),
					'initial' => __( 'Initial', 'gd_business_hours' ),
				)
			);

			$arr[] = array(
				'type' => 'sectionend',
				'id' => 'hours_default_options'
			);

			$arr[] = array(
				'name' => __( 'Widget Text', 'gd_business_hours' ),
				'type' => 'sectionstart',
				'id' => 'hours_widget_text'
			);

			$arr[] = array(
				'name' => __( '"Currently Open" text', 'gd_business_hours' ),
				'desc' => '',
				'id' => 'business_hours_currently_open_text',
				'css' => 'min-width:300px;',
				'std' => __( 'We are currently open.', 'gd_business_hours' ),
				'type' => 'text',
				'placeholder_text' => __( 'We are currently open.', 'gd_business_hours' ),
			);

			$arr[] = array(
				'name' => __( '"Currently Closed" text', 'gd_business_hours' ),
				'desc' => '',
				'id' => 'business_hours_currently_closed_text',
				'css' => 'min-width:300px;',
				'std' => __( 'Sorry, we are currently closed.', 'gd_business_hours' ),
				'type' => 'text',
				'placeholder_text' => __( 'Sorry, we are currently closed.', 'gd_business_hours' ),
			);

			$arr[] = array(
				'name' => '',
				'id' => '',
				'desc' => __( 'The text above is NOT shown in the sidebar/widget if you have an image selected below.', 'gd_business_hours' ),
				'type' => 'html_content',
			);

			$arr[] = array(
				'name' => '',
				'id' => '',
				'desc' => __( 'Due to a problem in GeoDirectory, there is no way to "Unselect" an image at this time, unless you access the database directly.', 'gd_business_hours' ),
				'type' => 'html_content',
			);

			$arr[] = array(
				'name' => __( '"Currently Open" image', 'gd_business_hours' ),
				'desc' => '',
				'id' => 'business_hours_currently_open_image',
				'type' => 'file',
			);

			$arr[] = array(
				'name' => __( '"Currently Closed" image', 'gd_business_hours' ),
				'desc' => '',
				'id' => 'business_hours_currently_closed_image',
				'type' => 'file',
			);

			$arr[] = array(
				'name' => __( '"Open All Day" text', 'gd_business_hours' ),
				'desc' => '',
				'id' => 'business_hours_open_24_hours_text',
				'css' => 'min-width:300px;',
				'std' => __( 'Open 24 Hours', 'gd_business_hours' ),
				'type' => 'text',
				'placeholder_text' => __( 'Open 24 Hours', 'gd_business_hours' ),
			);

			$arr[] = array(
				'name' => __( '"Closed Today" text', 'gd_business_hours' ),
				'desc' => '',
				'id' => 'business_hours_closed_today_text',
				'css' => 'min-width:300px;',
				'std' => __( 'Closed Today', 'gd_business_hours' ),
				'type' => 'text',
				'placeholder_text' => __( 'Closed Today', 'gd_business_hours' ),
			);


			$arr[] = array(
				'type' => 'sectionend',
				'id' => 'hours_widget_text'
			);

			$arr = apply_filters( 'geodir_business_hours_default_options', $arr );

			return $arr;
		}

		private static function geodir_business_hours_type_setting() {

			$post_type_arr = array();

			$post_types = geodir_get_posttypes( 'object' );

			foreach ( $post_types as $key => $post_types_obj ) {
				$post_type_arr[$key] = $post_types_obj->labels->singular_name;
			}

			return $post_type_arr;
		}

		public static function hours_manager_ajax() {
			if ( isset( $_REQUEST['subtab'] ) && $_REQUEST['subtab'] == 'bh_options' ) {

				geodir_update_options( self::gdbh_default_options() );

				$msg = __( 'Business hours options saved successfully', 'gd_business_hours' );

				$msg = urlencode( $msg );

				$location = admin_url() . "admin.php?page=geodirectory&tab=business_hours_fields&subtab=bh_options&hours_success=" . $msg;

				wp_redirect( $location );
				exit;

			}
		}

		public static function display_messages() {

			if ( isset( $_REQUEST['hours_success'] ) && $_REQUEST['hours_success'] != '' ) {
				echo '<div id="message" class="updated fade"><p><strong>' . $_REQUEST['hours_success'] . '</strong></p></div>';

			}

		}

		public static function hours_metabox_add() {
			global $post;
			$can_have_hours = self::can_have_hours();

			if ( $can_have_hours ) {
				add_meta_box( 'geodir_business_hours', __( 'Business Hours', 'gd_business_hours' ), array(
					__CLASS__,
					'hours_metabox_content'
				), $post->post_type, 'normal', 'high' );
			}

		}

		public static function add_listing_can_have_hours() {
			if ( self::can_have_hours() ) {
				add_action( 'geodir_after_main_form_fields', array(
					'GD_Business_hours',
					'hours_metabox_content'
				), 0 );
			}
		}

		public static function can_have_hours( $widget_post = '' ) {
			global $pagenow;
			$can_have_hours = false;

			if ( geodir_is_page( 'add-listing' ) ) {
				$can_have_hours = self::can_new_public_listing_have_hours();
			}

			if ( ( 'post.php' == $pagenow ) || ( 'post-new.php' == $pagenow ) ) {
				$can_have_hours = self::can_new_listing_have_hours( $widget_post );
			}

			if ( is_single() ) {
				$can_have_hours = self::can_listing_have_hours();
			}

			if ( geodir_is_geodir_page() && !geodir_is_page( 'add-listing' ) ) {
				$can_have_hours = self::can_listing_have_hours();
			}

			if ( !( empty( $widget_post ) ) ) {
				$can_have_hours = self::can_listing_have_hours( $widget_post );
			}

			return $can_have_hours;
		}

		private static function can_listing_have_hours() {
			// Should break this out a bit more, but for now this works.
			return self::can_new_listing_have_hours();
		}

		private static function can_new_public_listing_have_hours() {
			$can_have_hours = false;
			if ( function_exists( 'geodir_get_default_package' ) ) {
				if ( !( isset( $_GET['package_id'] ) ) ) {
					if ( isset( $_GET['pid'] ) ) {
						$package_id   = geodir_get_post_package_info( '', $_GET['pid'] );
						$package_info = geodir_get_package_info_by_id( $package_id['pid'] );
					} else {
						$package_info = geodir_get_default_package( $_GET['listing_type'] );
					}
				} else {
					$package_id = absint( $_GET['package_id'] );

					$package_info = geodir_get_package_info_by_id( $package_id );
				}
				$can_have_hours = $package_info->allow_business_hours;
			} else {
				$geodir_post_types = geodir_get_posttypes( 'array' );
				$geodir_posttypes  = array_keys( $geodir_post_types );
				if ( isset( $_GET['listing_type'] ) && in_array( $_GET['listing_type'], $geodir_posttypes ) ) {
					$valid_types = get_option( 'geodir_post_types_business_hours' );
					if ( current_user_can( "manage_options" ) || in_array( $_GET['listing_type'], $valid_types ) ) {
						$can_have_hours = true;
					}
				} else {
					if ( isset( $_GET['pid'] ) ) {
						$post_type   = get_post_type( absint( $_GET['pid'] ) );
						$valid_types = get_option( 'geodir_post_types_business_hours' );
						if ( current_user_can( "manage_options" ) || in_array( $post_type, $valid_types ) ) {
							$can_have_hours = true;
						}
					}
				}
			}

			return $can_have_hours;
		}

		private static function can_new_listing_have_hours( $widget_post = '' ) {
			// I think this code could be cleaner
			// @todo: clean this up.
			global $wpdb, $post;

			$post_types = geodir_get_posttypes( 'array' );

			if ( isset( $post->post_type ) && !( in_array( $post->post_type, array_keys( $post_types ) ) ) ) {
				return false;
			}

			if ( isset( $_GET['post_type'] ) && 'page' == $_GET['post_type'] ) {
				return;
			}

			if ( isset( $post->post_type ) && ( 'page' == $post->post_type ) ) {
				return false;
			}

			$can_have_hours = false;

			if ( empty( $widget_post ) ) {
				$our_post = $post;
			} else {
				$our_post = $widget_post;
			}

			/**
			 * If $our_post is empty, we're creating a new post in the backend.
			 * The empty $our_post->post_type is needed because when you change the package in the
			 * right column, the page posts & refreshes and the post_type is now set.
			 */
			if ( empty( $our_post ) || ( empty( $our_post->post_type ) ) ) {
				// Set the post_type to gd_place if it doesn't exist, probably not a great choice, but we know it exists.
				$post_type = isset( $_GET['post_type'] ) ? sanitize_text_field( $_GET['post_type'] ) : 'gd_place';

				// If we have BOTH on the $_GET, then we can just get the info
				if ( isset( $_GET['package_id'] ) && isset( $_GET['post_type'] ) ) {
					$package_id = absint( $_GET['package_id'] );
				} else {
					// If we're missing either package_id or post_type on the $_GET, grab the default package
					if ( function_exists( 'geodir_get_default_package' ) ) {
						$package_info = geodir_get_default_package( $post_type );
						$package_id   = absint( $package_info->pid );
					} else {
						$package_id = 0;
					}
				}
			} else {
				// Since we have a post, let's get the GD info for it.
				$geo_post = geodir_get_post_info( $our_post->ID );
				if ( !( empty( $geo_post ) ) ) {
					// First, in the best case our post tells us what we need.
					$package_id = $geo_post->package_id;
					$post_type  = $our_post->post_type;
				} else {
					// Ok, we don't have GD post info, which should mean this is the re-post from changing the package early on
					// So we try to get the default by the post_type which is set on the change.
					if ( !( isset( $_GET['package_id'] ) ) ) {
						if ( function_exists( 'geodir_get_default_package' ) ) {
							$package_info = geodir_get_default_package( $our_post->post_type );
							$package_id   = absint( $package_info->pid );
						} else {
							$package_id = 0;
						}
					} else {
						// If we get the package_id, use it.
						$package_id = absint( $_GET['package_id'] );
					}
					$post_type = $our_post->post_type;
				}
			}

			$geodir_post_types = geodir_get_posttypes( 'array' );
			$geodir_posttypes  = array_keys( $geodir_post_types );

			// If you're not admin, and the payment module is active, check the price table.
			if ( is_plugin_active( 'geodir_payment_manager/geodir_payment_manager.php' ) ) {
				$price_sql = "SELECT allow_business_hours FROM {$wpdb->prefix}geodir_price WHERE pid=%d AND post_type=%s";

				$can_have_hours = $wpdb->get_var( $wpdb->prepare( $price_sql, array( $package_id, $post_type ) ) );
			} else {
				//Otherwise, check the post types setting for GD Business Hours
				if ( isset( $post_type ) && in_array( $post_type, $geodir_posttypes ) ) {
					$valid_types = get_option( 'geodir_post_types_business_hours' );
					if ( current_user_can( "manage_options" ) || ( is_array( $valid_types ) && in_array( $post_type, $valid_types ) ) ) {
						$can_have_hours = true;
					}
				}
			}

			return $can_have_hours;
		}

		public static function hours_metabox_content() {
			global $post;
			if ( !( self::can_have_hours() ) ) {
				return;
			}
			wp_nonce_field( plugin_basename( __FILE__ ), 'business_hours_nonce_name' );

			$weekdays = self::getWeekdays();

			$current_hours = get_post_meta( $post->ID, 'business_hours', true );

			$precheck_no = get_option( 'geodir_business_hours_precheck_no_hours' );
			if ( isset( $current_hours['nohours'] ) ) {
				$precheck_no = '';
			} else {
				if ( 1 == $precheck_no ) {
					$precheck_no = 'nohours';
				}
			}


			$instructions = get_option( 'geodir_business_hours_instruction_text' );
			if ( !empty( $instructions ) ) {
				echo '<div style="font-weight: bold;margin-bottom:10px;margin-top:20px;" id="gdbh_instructions">';
				echo esc_textarea( $instructions );
				echo '</div>';
			}


			echo '<div style="font-weight: bold;margin-bottom:10px;margin-top:20px;">';
			echo '<span style="margin-right:20px;">';
			_e( 'Do not use business hours for this listing', 'gd_business_hours' );
			echo '</span>';
			echo '<input type="checkbox" value="nohours" id="nohours" name="nohours" ' . checked( 'nohours', isset( $current_hours["nohours"] ) ? $current_hours["nohours"] : $precheck_no, false ) . ' />';
			echo '</div>';
			?>
			<table cellpadding="0" cellspacing="0" id="hoursTable">
				<thead>
				<tr>
					<td style="width:140px;"><strong><?php _e( 'Weekday', 'gd_business_hours' ); ?></strong></td>
					<td><strong><?php _e( 'Open<br /> all day', 'gd_business_hours' ); ?></strong></td>
					<td><strong><?php _e( 'Open', 'gd_business_hours' ); ?></strong></td>
					<td><strong><?php _e( 'Close', 'gd_business_hours' ); ?></strong></td>
					<td><strong><?php _e( 'Closed<br /> all day', 'gd_business_hours' ); ?></strong></td>
				</tr>
				</thead>
				<?php
				foreach ( array_values( $weekdays ) as $day ) {
					if ( !( isset( $current_hours[strtolower( $day ) . "_closed"] ) ) ) {
						$current_hours[strtolower( $day ) . "_closed"] = 'false';
					}
					?>
					<tr id="<?php echo esc_attr( strtolower( $day ) ); ?>">
						<td class="day-label"><?php echo $day; ?></td>
						<td style="padding:17px;" class="open-all-day-cell"><input type="checkbox" class="is24hours"
						                                                           name="<?php echo strtolower( $day ) . "_24hours"; ?>" id="<?php echo strtolower( $day ) . "_24hours"; ?>"
						                                                           value="24hours" <?php checked( '24hours', isset( $current_hours[strtolower( $day ) . "_24hours"] ) ? $current_hours[strtolower( $day ) . "_24hours"] : '' ); ?> />
						</td>
						<td class="times-area-one">1) <input type="text" class="timepicker" name="<?php echo strtolower( $day ) . "_open"; ?>" id="<?php echo strtolower( $day ) . "_open"; ?>"
						                                     value="<?php echo issetor( $current_hours[strtolower( $day ) . "_open"], '' ); ?>"/></td>
						<td class="times-area-two"><input type="text" class="timepicker" name="<?php echo strtolower( $day ) . "_close"; ?>" id="<?php echo strtolower( $day ) . "_close"; ?>"
						                                  value="<?php echo issetor( $current_hours[strtolower( $day ) . "_close"], '' ); ?>"/></td>
						<td style="padding:17px;" class="closed-all-day-cell"><input type="checkbox" class="isClosed"
						                                                             name="<?php echo strtolower( $day ) . "_closed"; ?>" id="<?php echo strtolower( $day ) . "_closed"; ?>"
						                                                             value="closed" <?php checked( 'closed', $current_hours[strtolower( $day ) . "_closed"] ); ?> /></td>
					</tr>
					<?php // Add a second Open/Closed slot ?>
					<tr>
						<td style="border-bottom: 2px solid grey;padding-bottom:9px;"></td>
						<td style="border-bottom: 2px solid grey;padding-bottom:9px;"></td>
						<td style="border-bottom: 2px solid grey;padding-bottom:9px;">2) <input type="text" class="timepicker" name="<?php echo strtolower( $day ) . "_open_2"; ?>"
						                                                                        id="<?php echo strtolower( $day ) . "_open_2"; ?>"
						                                                                        value="<?php echo issetor( $current_hours[strtolower( $day ) . "_open_2"], '' ); ?>"/></td>
						<td style="border-bottom: 2px solid grey;padding-bottom:9px;"><input type="text" class="timepicker" name="<?php echo strtolower( $day ) . "_close_2"; ?>"
						                                                                     id="<?php echo strtolower( $day ) . "_close_2"; ?>"
						                                                                     value="<?php echo issetor( $current_hours[strtolower( $day ) . "_close_2"], '' ); ?>"/></td>
						<td style="border-bottom: 2px solid grey;padding-bottom:9px;"></td>
					</tr>
					<?php
				}
				?>
				<tbody>

				</tbody>
			</table>
			<?php
			if ( !( is_admin() ) ) {
				echo "<br />";
			}
		}

		public static function add_to_sidebar($areas) {
			$replace_timing = get_option( 'geodir_business_hours_replace_timing' );

			if ( self::can_have_hours() ) {
				if ( '1' == $replace_timing ) {
					render_our_sidebar();
					return;
				} else {
					$key = array_search( 'geodir_detail_page_more_info', $areas );
					unset( $areas[$key] );
					$areas[] = 'render_our_sidebar';
					$areas[] = 'geodir_detail_page_more_info';
				}
			}

			return $areas;
		}

		public static function add_timing_field( $return_arr ) {
			global $post;
			if( self::can_have_hours() ){
				$post->geodir_timing = ' ';
			}
			return $return_arr;
		}

		public static function gd_bh_listview( $template ) {

			if ( !( strpos( $template, 'widget' ) ) ) {
				return plugin_dir_path( __FILE__ ) . 'templates/listing-listview.php';
			}

			$show_status = get_option( 'geodir_business_hours_show_status_widget' );

			if ( $show_status ) {
				return plugin_dir_path( __FILE__ ) . 'templates/widget-listing-listview.php';
			} else {
				return $template;
			}
		}

		public static function gdbh_check_payment_manager() {
			if ( function_exists( 'is_plugin_active' ) && is_plugin_active( 'geodir_payment_manager/geodir_payment_manager.php' ) ) {
				if ( false == get_option( "gdbh_price_table_updated" ) ) {
					gdbh_add_hours_to_price_table();
				}
			}
		}

		public function hide_timing( $return_arr, $package_id, $post_type, $fields_location ) {
//			var_dump($return_arr);
			var_dump( $fields_location );
//			die();
			return;
		}

		public static function add_schema( $schema, $post ){
			if( !is_object( $post ) ){
				return $schema;
			}

			$metadata = get_post_meta( $post->ID, 'business_hours', true );

			if ( false == GD_Business_Hours::hasOpenHours( $metadata ) ) {
				return $schema;
			}

			if ( isset( $metadata['nohours'] ) && 'nohours' == $metadata['nohours'] ) {
				return $schema;
			}

			$timeformat = 'H:i';

			$weekdays = self::getWeekdays();

			foreach ( $weekdays as $key => $full_day ) {
				if ( !self::openToday( $metadata, $full_day ) ) {
					$hours_schema[] = array(
						'@type' => "OpeningHoursSpecification",
						'closes' => '00:00:00',
						'dayOfWeek' => 'http://purl.org/goodrelations/v1#' . $full_day,
						'opens' => '00:00:00'
					);
				}

				if ( isset( $metadata[strtolower( $full_day ). '_24hours'] ) && '24hours' == $metadata[strtolower( $full_day ) . '_24hours'] ) {
					$hours_schema[] = array(
						'@type' => "OpeningHoursSpecification",
						'closes' => '23:59:59',
						'dayOfWeek' => 'http://purl.org/goodrelations/v1#' . $full_day,
						'opens' => '00:00:00'
					);
				}

				if( self::openToday( $metadata, $full_day ) && isset( $metadata[strtolower( $full_day ) . '_open'] ) && ( $metadata[strtolower( $full_day ) . '_open'] !== '' ) ){
					$hours_schema[] = array(
						'@type' => "OpeningHoursSpecification",
						'closes' => date_i18n( $timeformat, strtotime( $metadata[strtolower( $full_day ) . '_close'] ) ),
						'dayOfWeek' => 'http://purl.org/goodrelations/v1#' . $full_day,
						'opens' => date_i18n( $timeformat, strtotime( $metadata[strtolower( $full_day ) . '_open'] ) ),
					);

					if( isset( $metadata[strtolower( $full_day ) . '_open_2'] ) && ( $metadata[strtolower( $full_day ) . '_open_2'] !== '' ) ){
						$hours_schema[] = array(
							'@type' => "OpeningHoursSpecification",
							'closes' => date_i18n( $timeformat, strtotime( $metadata[strtolower( $full_day ) . '_close_2'] ) ),
							'dayOfWeek' => 'http://purl.org/goodrelations/v1#' . $full_day,
							'opens' => date_i18n( $timeformat, strtotime( $metadata[strtolower( $full_day ) . '_open_2'] ) ),
						);
					}
				}
			}
			//Let's get down to business

			$schema['openingHoursSpecification'] = $hours_schema;
			return $schema;
		}
	}

	function render_our_sidebar() {
		global $post, $preview;

		$replace_timing = get_option( 'geodir_business_hours_replace_timing' );

		$atts = array();

		$show_closed_today = get_option( 'geodir_business_hours_show_closed_today' );
		if ( isset( $show_closed_today ) ) {
			$atts['show_closed_day'] = $show_closed_today;
		} else {
			$atts['show_closed_day'] = '0';
		}

		$show_header = get_option( 'geodir_business_hours_show_open_header' );
		if ( isset( $show_header ) ) {
			$atts['header'] = $show_header;
		} else {
			$atts['header'] = '0';
		}

		$show_footer = get_option( 'geodir_business_hours_show_open_footer' );
		if ( isset( $show_footer ) ) {
			$atts['footer'] = $show_footer;
		} else {
			$atts['footer'] = '0';
		}

		$show_status = get_option( 'geodir_business_hours_show_open_status' );
		if ( isset( $show_status ) ) {
			$atts['show_open_status'] = $show_status;
		} else {
			$atts['show_open_status'] = '0';
		}

		$show_highlight = get_option( 'geodir_business_hours_highlight_open_hours' );
		if ( isset( $show_highlight ) ) {
			$atts['highlight_open_period'] = $show_highlight;
		} else {
			$atts['highlight_open_period'] = '0';
		}

		$day_name         = get_option( 'geodir_business_hours_day_name' );
		$atts['day_name'] = in_array( $day_name, array(
			'full',
			'abbrev',
			'initial'
		) ) ? $day_name : 'abbrev';

		$metadata = get_post_meta( $post->ID, 'business_hours', true );

		if ( $preview ) {
			$metadata = gdbh_make_meta_data_from_object( $post );
		}

		if ( empty( $metadata ) ) {
			return;
		}

		if ( false == GD_Business_Hours::hasOpenHours( $metadata ) ) {
			return;
		}

		if ( isset( $metadata['nohours'] ) && 'nohours' == $metadata['nohours'] ) {
			return;
		}

		if ( '1' == $replace_timing ) {
			echo '<div style="clear:both;" class="geodir_more_info   geodir_contact">';
			echo '<span style="" class="geodir-i-time"><i class="fa fa-clock-o"></i>' . sanitize_text_field( get_option( 'geodir_business_hours_section_header' ) ) . ': </span>';
		} else {
			echo '<div class="geodir-company_info geodir-details-sidebar-listing-info">';
			echo get_option( 'geodir_business_hours_section_header' );
		}

		GD_Business_Hours::block( 'gdbh', $metadata, null, $atts );
		echo '</div>';
	}

	if ( !function_exists( 'ify_geodirectory_check' ) ) {
		function ify_geodirectory_check() {
			if ( function_exists( 'is_plugin_active' ) && is_plugin_active( 'geodirectory/geodirectory.php' ) ) {
				return true;
			} else {
				return false;
			}
		}
	}

	function gdbh_make_meta_data_from_object( $post ) {
		$hours = get_object_vars( $post );

		return $hours;
	}

	/**
	 * Start up the extension.
	 *
	 * @access public
	 * @since 1.0
	 *
	 * @return mixed (object)|(bool)
	 */
	function GD_Business_Hours() {

		if ( ify_geodirectory_check() ) {

			return new GD_Business_Hours();

		} else {

			add_action( 'admin_notices', create_function( '', 'echo \'<div id="message" class="error"><p><strong>ERROR:</strong> WPGeoDirectory plugin must be installed and active to GD Business Hours.</p></div>\';' ) );

			return false;
		}
	}
}

add_action( 'init', 'GD_Business_Hours', 1 );

$disable_widget = get_option( 'geodir_business_hours_disable_widget' );
$replace_timing = get_option( 'geodir_business_hours_replace_timing' );

if ( '1' != $disable_widget ) {
	require_once( plugin_dir_path( __FILE__ ) . 'includes/class.widgets.php' );
	add_action( 'widgets_init', array( "GD_Business_Hours", 'register_widgets' ) );
} else {
	if ( '1' == $replace_timing ) {
		add_filter( 'geodir_show_geodir_timing', array( 'GD_Business_Hours', 'add_to_sidebar' ) );
	} else {
		add_action( 'geodir_detail_page_sidebar_content', array(
			'GD_Business_Hours',
			'add_to_sidebar'
		), 5 );
	}
}
if ( !( function_exists( 'issetor' ) ) ) {
	function issetor( &$var, $default = false ) {
		return isset( $var ) ? $var : $default;
	}
}

function gdbh_add_hours_to_price_table() {
	if ( !( is_plugin_active( 'geodir_payment_manager/geodir_payment_manager.php' ) ) ) {
		add_filter( 'geodir_payment_package_table', 'gtv_add_hours_to_price_table' );
		update_option( "gdbh_price_table_updated", false );

		return;
	}

	$result = geodir_add_column_if_not_exist( GEODIR_PRICE_TABLE, 'allow_business_hours', "tinyint(2) NOT NULL DEFAULT '0'" );

	if ( 0 !== $result && false !== $result ) {
		update_option( "gdbh_price_table_updated", true );
	} else {
		error_log( strftime( "%b %d %Y %H:%M:%S", time() ) . ' = ' . $result );
	}

}

function gdbh_show_price_fields( $price_info ) {
	?>
	<tr valign="top">
		<th class="titledesc" scope="row"><?php _e( 'Allow business hours?', 'gd_business_hours' ); ?></th>
		<td class="forminp">
			<div class="gdbh-formfield">
				<select name="gdbh_allow_business_hours">
					<option value="0" <?php if ( !isset( $price_info->allow_business_hours ) || $price_info->allow_business_hours == '0' ) {
						echo 'selected="selected"';
					} ?> >
						<?php _e( "No", 'gd_business_hours' ); ?>
					</option>
					<option value="1" <?php if ( isset( $price_info->allow_business_hours ) && $price_info->allow_business_hours == '1' ) {
						echo 'selected="selected"';
					} ?> >
						<?php _e( "Yes", 'gd_business_hours' ); ?>
					</option>
				</select>
			</div>
		</td>
	</tr>
	<?php
}

function gdbh_save_hours_preference( $id ) {
	global $wpdb;
	if ( $_POST['gd_add_price'] == 'addprice' && wp_verify_nonce( $_REQUEST['package_add_update_nonce'], 'package_add_update' ) ) {
		if ( 0 < $id ) {
			$allow_business_hours = 0;
			if ( isset( $_POST['gdbh_allow_business_hours'] ) ) {
				$allow_business_hours = intval( $_POST['gdbh_allow_business_hours'] );
			}

			$sql = "UPDATE " . GEODIR_PRICE_TABLE . " SET allow_business_hours = %d WHERE pid = %d";

			$wpdb->query( $wpdb->prepare( $sql, array(
				$allow_business_hours,
				$id
			) ) );
		}
	}
}

function gdbh_output_open_on_listview( $post_id ) {
	global $post;

	if ( '' == $post_id ) {
		$post_id = $post->ID;
	}

	$show_status = get_option( 'geodir_business_hours_show_status_widget' );
	$open_status = '';
	$message     = '';
	$class       = '';

	if ( '1' == $show_status && GD_Business_Hours::can_have_hours( $post_id ) ) {
		$metadata = get_post_meta( $post_id, 'business_hours', true );

		// If the listing has no hours, just return.
		if ( 1 == count( $metadata ) ) {
			return;
		}

		if ( isset( $metadata['nohours'] ) && '' == $metadata['nohours'] ) {
			$open_status = GD_Business_Hours::openStatus( $metadata );
		}

		if ( true === $open_status ) {
			$class   = 'class="gdbh_open_icon"';
			$message = __( "Currently Open", 'gd_business_hours' );
		}

		if ( false === $open_status ) {
			$class   = 'class="gdbh_closed_icon"';
			$message = __( "Currently Closed", 'gd_business_hours' );
		}
		echo '<span>';
		echo '<i ' . $class . '></i>';
		echo '<a href="#">' . $message . '</a>';
		echo '</span>';
	}
}

add_action( 'geodir_after_favorite_html', 'gdbh_output_open_on_listview' );
