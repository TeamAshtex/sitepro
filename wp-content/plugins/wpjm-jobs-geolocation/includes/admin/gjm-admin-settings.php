<?php
// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

/**
 * GJM_Admin class
 */
class GJM_Admin_Settings {

	/**
	 * __construct function.
	 *
	 * @access public
	 * @return void
	 */
	public function __construct() {

		$this->settings = get_option( 'gjm_options' );	
		$this->prefix   = 'gjm';

		//default settings - load only on JObs settings page
		if ( !empty( $_GET['post_type'] ) && $_GET['post_type'] == 'job_listing' && !empty( $_GET['page' ] ) && $_GET['page'] == 'job-manager-settings' ) {
			add_action( 'admin_init', array( $this, 'default_options' ), 99 );
		}

		//hook custom function that creates the importer button
		add_action( 'wp_job_manager_admin_field_gjm_locations_importer', array( $this, 'locations_importer_form' ) );

		//hook custom function that shows instructions
		add_action( 'wp_job_manager_admin_field_gjm_user_incstructions', array( $this, 'shortcode_user_instructions' ) );
		
		//locations importer
		add_action( 'admin_init', 	 array( $this, 'locations_importer' ) );
		add_action( 'admin_notices', array( $this, 'admin_notices' 		) );

		//admin settings - append GJM settings to WPJM settings page
		add_filter( 'job_manager_settings', array( $this, 'admin_settings' ) );

		//make sure we updating WPJM settings page to also update our settings - run this only on UPDATE settings
		if ( !empty( $_POST['action'] ) && $_POST['action'] == 'update' && !empty( $_POST['option_page' ] ) && $_POST['option_page'] == 'job_manager' ) {
			add_action( 'admin_init', array( $this, 'options_validate' ), 10 );		
		}	
	}

	/**
	 * Set the default options of the plugin if not exist
	 */
	public function default_options() {

		//only if no options set
		if ( !empty( $this->settings ) ) {
			return;
		}

		$prefix = $this->prefix;

		$options = array(
			'general_settings' => array(
				$prefix.'_address_autocomplete_form_admin' 	  => 1,
				$prefix.'_address_autocomplete_form_frontend' => 1,
				$prefix.'_address_autocomplete_results_type'  => 'geocode',
				$prefix.'_address_autocomplete_country'	   	  => ''
			),
			'search_form'  		=> array(
				$prefix.'_auto_locator' 	=> 1,
				$prefix.'_autocomplete' 	=> 1,
				$prefix.'_locator_button' 	=> 1,
				$prefix.'_radius' 			=> '10,15,25,50,100',
				$prefix.'_units' 			=> 'both',
				$prefix.'_orderby' 			=> 'distance,title,featured,date',
				$prefix.'_map' 				=> 1,
				$prefix.'_map_width' 		=> '100%',
				$prefix.'_map_height' 		=> '250px',
				$prefix.'_map_type' 		=> 'ROADMAP',
				$prefix.'_group_markers' 	=> 'markers_clusterer',
				$prefix.'_user_marker' 		=> 'https://maps.google.com/mapfiles/ms/icons/blue-dot.png',
				$prefix.'_location_marker'	=> 'https://maps.google.com/mapfiles/ms/icons/red-dot.png',
				$prefix.'_clusters_path'	=> 'https://raw.githubusercontent.com/googlemaps/js-marker-clusterer/gh-pages/images/m',
				$prefix.'_scroll_wheel' 	=> 1,
				$prefix.'_distance' 		=> 1,

			),
			'single_page'  		=> array(
				$prefix.'_single_map_enabled' 			=> 'top',
				$prefix.'_single_map_width' 			=> '100%',
				$prefix.'_single_map_height' 			=> '200px',
				$prefix.'_single_map_type' 				=> 'ROADMAP',
				$prefix.'_single_map_location_marker'	=> 'https://maps.google.com/mapfiles/ms/icons/red-dot.png',
				$prefix.'_single_map_scroll_wheel' 		=> 1
			),
		);

		//save each field in its own database option.
		//we do this because thats how WP Job Manager saves its options and display them again on page load
		foreach ( $options as $groups ) {
			foreach ( $groups as $field => $value ) {
				update_option( $field, $value );
			}
		}
		
		//update default options into single database option
		update_option( $prefix.'_options', $options );
	}

	/**
	 * Locations importer button
	 * @param  $option 
	 * @return void
	 */
	function locations_importer_form( $option ) {
	?>		
		<input type="button" id="gjm-locations-import-toggle" class="button-secondary" value="<?php _e( "Import", "GJM" ); ?>" onclick="jQuery('#gjm_import_hidden').prop('disabled', false);jQuery(this).closest('form').submit();" />
		<input type="hidden" id="gjm_import_hidden" name="gjm_import_action" value="<?php echo $this->prefix; ?>" disabled="disabled" />
		<p class="description"><?php echo $option['desc']; ?></p>
		<?php wp_nonce_field( $this->prefix.'_locations_import_nonce', $this->prefix.'_locations_import_nonce' ); ?>
	<?php 				
	}
	
	/**
	 * Locations importer
	 * @return void
	 */
	function locations_importer() {
		
		if ( empty( $_POST['gjm_import_action'] ) ) {
			return;
		}
	
		$prefix = $_POST['gjm_import_action'];
		
		//get prefix
		if ( $prefix == 'gjm' ) {
			$post_type = 'job_listing';
			$page_name = 'job-manager-settings';
		} else {
			$post_type = 'resume';
			$page_name = 'resume-manager-settings';
		}

		//varify nonce
		if ( empty( $_POST[$prefix.'_locations_import_nonce'] ) || !wp_verify_nonce( $_POST[$prefix.'_locations_import_nonce'], $prefix.'_locations_import_nonce' ) ) {
			wp_safe_redirect( admin_url( 'edit.php?post_type='.$post_type.'&page='.$page_name.'&gjm_notice=locations_import_nonce_error&notice_status=error' ) );
		}
	
		global $wpdb;
		
		header( 'Content-Type: text/csv; charset=utf-8' );
		
		//get all posts with posts types jobs or resume
		$posts = $wpdb->get_results("
			SELECT `ID` as 'post_id', `post_title`, `post_type`, `post_status`
			FROM `{$wpdb->prefix}posts`
			WHERE `post_type` = '{$post_type}'
			", ARRAY_A );
		
		//abort if no posts found
		if ( empty( $posts ) ) {
			wp_safe_redirect( admin_url( 'edit.php?post_type='.$post_type.'&page='.$page_name.'&gjm_notice=no_locations_to_import&notice_status=error' ) );
			exit;
		}
		
		//number of locations imported
		$imported = 0;
		
		//loop found posts
		foreach ( $posts as $post_key => $post_info ) {
			
			//check that location does not already exist in gmw table database
			$check_location = $wpdb->get_col( "SELECT `post_id` FROM `{$wpdb->prefix}places_locator` WHERE `post_id` = {$post_info['post_id']}", 0 );
				
			//skip location if exist in gmw table
			if ( !empty( $check_location ) )
				continue;
							
			//get all jobs or resume custom fields with location from database
			$post_location = $wpdb->get_results("
				SELECT `meta_key`, `meta_value`
				FROM `{$wpdb->prefix}postmeta`
				WHERE `post_id` = {$post_info['post_id']}
				AND `meta_key` IN ( 'geolocation_street', 'geolocation_city', 'geolocation_state_short', 'geolocation_state_long', 'geolocation_postcode',
				'geolocation_country_short', 'geolocation_country_long', 'geolocation_lat', 'geolocation_long', 'geolocation_formatted_address')
			", ARRAY_A );

			//loop custom fields and add them to array of posts
			foreach ( $post_location as $location_key => $location ) {			
				$posts[$post_key][$location['meta_key']] =  $location['meta_value'];
			}

			//verify coordinates
			if ( empty( $posts[$post_key]['geolocation_lat'] ) || empty( $posts[$post_key]['geolocation_long'] ) )
				continue;
			
			//create entry in gmw table in database
			$created = $wpdb->query( $wpdb->prepare(
				"INSERT INTO `{$wpdb->prefix}places_locator`
				( `post_id`, `feature`, `post_status`, `post_type`, `post_title`, `lat`,
				`long`, `street`, `apt`, `city`, `state`, `state_long`, `zipcode`, `country`,
				`country_long`, `address`, `formatted_address`, `phone`, `fax`, `email`, `website`, `map_icon` )
				VALUES ( %d, %d, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s )",
				array(
					$posts[$post_key]['post_id'],
					0,
					$posts[$post_key]['post_status'],
					$post_type,
					$posts[$post_key]['post_title'],
					$posts[$post_key]['geolocation_lat'],
					$posts[$post_key]['geolocation_long'],
					$posts[$post_key]['geolocation_street'],
					'',
					$posts[$post_key]['geolocation_city'],
					$posts[$post_key]['geolocation_state_short'],
					$posts[$post_key]['geolocation_state_long'],
					$posts[$post_key]['geolocation_postcode'],
					$posts[$post_key]['geolocation_country_short'],
					$posts[$post_key]['geolocation_country_long'],
					$posts[$post_key]['geolocation_formatted_address'],
					$posts[$post_key]['geolocation_formatted_address'],
					'',
					'',
					'',
					'',
					'default.png'
				) ) );
	
			if ( $created )
				$imported++;		
		}
					
		if ( $imported == 0 ) {
			wp_safe_redirect( admin_url( 'edit.php?post_type='.$post_type.'&page='.$page_name.'&gjm_notice=no_locations_imported&notice_status=error' ) );
		} else {
			wp_safe_redirect( admin_url( 'edit.php?post_type='.$post_type.'&page='.$page_name.'&gjm_notice=locations_imported&number_of_locations='.$imported.'&notice_status=updated' ) );
		}
		exit;
	}
	
	/**
	 * Importer admin notice
	 * @return void
	 */
	function admin_notices() {
		
		//check if notice exist
		if ( empty( $_GET['gjm_notice'] ) )
			return;
	
		$notice 	= $_GET['gjm_notice'];
		$status 	= $_GET['notice_status'];
		$locations  = ( !empty( $_GET['number_of_locations'] ) ) ? $_GET['number_of_locations'] : 0;
		
		$messages 	= array();
		$messages['locations_import_nonce_error'] = __( 'There was an error while trying to import locations.', 'GJM' );
		$messages['no_locations_to_import'] 	  = __( 'No locations to import were found.', 'GJM' );
		$messages['no_locations_imported'] 		  = __( 'No locations were imported.', 'GJM' );
		$messages['locations_imported'] 		  = sprintf( __( '%d Locations successfully imported.', 'GJM' ), $locations );
							
		?>
	    <div class="<?php echo $status;?>" style="display:block;margin-top:10px;">
	    	<p><?php echo $messages[$notice]; ?></p>
	    </div>
		<?php
	}

	/**
	 * Shortcodes usage
	 * @return void
	 */
	public function shortcode_user_instructions() {
		
		$output  = '<p>'. __( 'There are two ways you can apply the Geolocaiton features to a resume form:</p>', 'GJM' );
		$output .= 	  '<ol>';
		$output .= 	    '<li>' . sprintf( __( 'Add the attribute %s with the value 2 to the %s shortcode ( ex. %s ) if you\'d like to apply the geolocation features based on the settings on this page.', 'GJM'), '<code>gjm_use</code>' , '<code>[jobs]</code>', '<code>[jobs gjm_use="2"]</code>' ).'</li>';
		$output .= 		'<li>' . sprintf( __( 'Add the attribute %s with the value 1 to the %s shortcode ( ex. %s ) when you use multiple %s shortcodes and you\'d like to defined each of the shortcodes with different geolocation features. You can do so by using the other shotcode attributes that %s provides. The list of shortcode attributes can be found <a %s>here</a>.', 'GJM'), '<code>gjm_use</code>', '<code>[jobs]</code>', '<code>[jobs gjm_use="1"]</code>.', '<code>[jobs]</code>', 'Wp Job Manager Geolocation plugin', 'href="http://docs.geomywp.com/wp-job-manager-geolocation-shortcodes/" target="_blank"' ).'</li>';
		$output .= 	  '</ol>'; 
		$output .= '<hr />';

		echo $output;
	}

	/**
	 * Admin settings page
	 * @param  array $args 
	 * @return void     
	 */
	public function admin_settings( $args ) {

		$prefix = $this->prefix;

		if ( $this->prefix == 'gjm' ) {
			$scs_px   =  __( 'job', 'GJM' );
			$sc_px    =  __( 'jobs', 'GJM' );
			$sc_px_s  = 'WP Job Manager Geolocation';
		} else {
			$scs_px   =  __( 'resume', 'GJM' );
			$sc_px    =  __( 'job', 'GJM' );
			$sc_px_s  = 'Resume Manager Geolocation';
		}
		
		//general settings
		$args['gjm_general_settings'] = array(

			__( 'GEO General Settings', 'GJM' ),
			array(
				$prefix.'_import_locations' => array(
					'name'        => $prefix.'_import_locations',
					'type'        => $prefix.'_locations_importer',
					'std'         => '',
					'label'       => __( 'Import Locations', 'GJM' ),
					'desc'        => sprintf( __( "Import %s location to database. You should do if you have created %s previously the installation of %s plugin.", 'GJM' ), $sc_px, $sc_px, $sc_px_s ),    
					'attributes'  => array(),
				),
				array(
					'name'        => $prefix.'_address_autocomplete_form_admin',
					'type'        => 'checkbox',
					'std'         => '1',
					'label'       => __( 'Google address autocomplete (admin)', 'GJM' ),
					'cb_label'    => __( 'Yes', 'GJM' ),
					'desc'        => sprintf( __( 'Disply suggested results by Google Places when typing an address in the location field of the new/edit %s screen.', 'GJM' ), $scs_px ),
					'attributes'  => array()
				),
				array(
					'name'        => $prefix.'_address_autocomplete_form_frontend',
					'type'        => 'checkbox',
					'std'         => '1',
					'label'       => __( 'Google address autocomplete (front-end)', 'GJM' ),
					'cb_label'    => __( 'Yes', 'GJM' ),
					'desc'        => sprintf( __( 'Disply suggested results by Google Places when typing an address in the location field of the new/edit %s form in the front-end.', 'GJM' ), $scs_px ),						
					'attributes'  => array()
				),
				array(
					'name'        => $prefix.'_address_autocomplete_results_type',
					'type'		  => 'select',
					'std'         => 'geocode',
					'label'       => __( 'Autocomplete results type', 'GJM' ),
					'desc'        => sprintf( __( 'Choose the type of the suggested results. See <a href="%s" target="_blank">this page</a> for more information about the results type.', 'GJM' ), 'https://developers.google.com/maps/documentation/javascript/places-autocomplete#add_autocomplete' ),
					'options'	  => array(
						'geocode'		=> 'geocode',
						'establishment' => 'establishment',
						'address'		=> 'address',
						'(regions)' 	=> '(regions)',
						'(cities)'		=> '(cities)'
					),
					'attributes'  => array()
				),
				array(
					'name'        => $prefix.'_address_autocomplete_country',
					'type'		  => 'text',
					'std'         => '',
					'placeholder' => '',
					'label'       => __( 'Autocomplete country restriction', 'GJM' ),
					'desc'        => __( "Enter the country code of the country which you would like to restrict the autocomplete results to. Leave it empty to show results from all countries.", 'GJM' ),
					'attributes'  => array( 'size' => 3 )
				),
			),
		);
		
		//apply only for WP Job Manager settings page
		if ( 
			isset( $_GET['page'] ) && $_GET['page'] == 'job-manager-settings' ||
			isset( $_POST['option_page']) && $_POST['action'] == 'update' && $_POST['option_page'] == 'job_manager' ) {
			
			array_unshift( $args['gjm_general_settings'][1], 
				array(
					'name'        => $prefix.'_google_api_key',
					'type'		  => 'text',
					'std'         => '',
					'placeholder' => '',
					'label'       => __( 'Google API Key', 'GJM' ),
					'desc'        => sprintf( __( 'Enter your Google API key. Click <a href="%s" target="_blank">here</a> to create a new API key.', 'GJM' ), 'https://developers.google.com/maps/documentation/javascript/get-api-key#get-an-api-key' ),
					'attributes'  => array( 'size' => 40 )
				),	
				array(
					'name'        => 'gjm_region',
					'type'		  => 'text',
					'std'         => '',
					'placeholder' => '',
					'label'       => __( 'Google API default region', 'GJM' ),
					'desc'        => sprintf( __( "Set the region of Google Maps features ( address autocomplete and maps ). Enter a country code of the country that you would like to use. For example for United States enter US. you can find your country code <a %s>here</a>.", 'GJM' ), 'href="http://geomywp.com/country-code/" target="blank"' ),
					'attributes'  => array( 'size' => 5 )
				),
				array(
					'name'        => 'gjm_language',
					'type'		  => 'text',
					'std'         => '',
					'placeholder' => '',
					'label'       => __( 'Google API language', 'GJM' ),
					'desc'        => sprintf( __( "Set the language of Google Maps features ( address autocomplete and maps ). Enter the language code of the langauge that you would like to use. List of avaliable langauges can be found <a %s>here</a>.", 'GJM' ), 'href="https://spreadsheets.google.com/spreadsheet/pub?key=0Ah0xU81penP1cDlwZHdzYWkyaERNc0xrWHNvTTA1S1E&gid=1" target="_blank"' ),
					'attributes'  => array( 'size' => 5 )
				)
			);	
			
			$temp_item = $args['gjm_general_settings'][1][$prefix.'_import_locations'];
			unset( $args['gjm_general_settings'][1][$prefix.'_import_locations'] );
			array_unshift( $args['gjm_general_settings'][1], $temp_item);
		}
		
		//search form options
		$args['gjm_search_form'] = array(
			__( 'Geo Search Form Settings', 'GJM' ),
			array(
				array(
					'name'        => 'top_message',
					'std'         => '1',
					'label'       => __( 'Shortcode usage', 'GJM' ),
					'desc'        => '',
					'type'        => $prefix.'_user_incstructions',
					'attributes'  => array( 'style' => 'display:none')
				),
				array(
					'name'        => $prefix.'_auto_locator',
					'std'         => '1',
					'label'       => __( 'Page load auto-locator', 'GJM' ),
					'cb_label'    => __( 'Enable', 'GJM' ),
					'desc'        => sprintf( __( 'When enabled the browser will try to detect the user\'s current position on page load and If found the form will dynamically submitted based on that location.' , 'GJM' ), $sc_px ),
					'type'        => 'checkbox',
					'attributes'  => array()
				),
				array(
					'name'        => $prefix.'_autocomplete',
					'std'         => '1',
					'label'       => __( 'Google address autocomplete', 'GJM' ),
					'cb_label'    => __( 'Enable', 'GJM' ),
					'desc'        => sprintf( __( 'Disply suggested results by Google Places when typing an address in the location field of the %s search form', 'GJM' ), $sc_px ),
					'type'        => 'checkbox',
					'attributes'  => array()
				),
				array(
					'name'        => $prefix.'_locator_button',
					'std'         => '0',
					'label'       => __( 'Locator button', 'GJM' ),
					'cb_label'    => __( 'Enable', 'GJM' ),
					'desc'        => __( 'Disply locator button in the location input box that will allow the user to dynamically get his current position.', 'GJM' ),
					'type'        => 'checkbox',
					'attributes'  => array()
				),
				array(
					'name'        => $prefix.'_radius',
					'std'         => '5,10,15,25,50,100',
					'placeholder' => '',
					'label'       => __( 'Radius', 'GJM' ),
					'desc'        => __( 'Enter single value to be the default value or multiple values comma separated to displaed as a dropdown', 'GJM' ),
					'type'		  => 'text',
					'attributes'  => array()
				),
				array(
					'name'        => $prefix.'_units',
					'std'         => 'both',
					'placeholder' => '',
					'label'       => __( 'Units', 'GJM' ),
					'desc'        => __( 'Choose single unit to be used as default or choose "Both" to display both units in a dropdown select menu.', 'GJM' ),
					'type'        => 'select',
					'options'	  => array( 
						'both' 		=> 'Both', 
						'imperial' 	=> 'Miles', 
						'metric' 	=> 'Kilometers' ),
					'attributes'  => array()
				),
				array(
					'name'        => $prefix.'_orderby',
					'type'		  => 'text',
					'std'         => 'distance,title,featured,date',
					'placeholder' => '',
					'label'       => __( 'Order-by', 'GJM' ),
					'desc'        => __( 'Enter the values that you\'d like to display in the "Sort by" dropdown select box. Enter the values comma separated in the order that you\'d like them to appear. The values available are: distance, title, featured, date, modified, ID, parent, rand, name.', 'GJM' ),
					'attributes'  => array( 'size' => '50' )
				),
				array(
					'name'        => $prefix.'_map',
					'std'         => '1',
					'label'       => __( 'Display map', 'GJM' ),
					'cb_label'    => __( 'Enable', 'GJM' ),
					'desc'        => __( 'Disply map showing the '.$sc_px.' location above the list of results.', 'GJM' ),
					'type'        => 'checkbox',
					'attributes'  => array()
				),
				array(
					'name'        => $prefix.'_map_height',
					'type'		  => 'text',
					'std'         => '250px',
					'placeholder' => '',
					'label'       => __( 'Map height', 'GJM' ),
					'desc'        => __( 'Map height in pixels or percentage (ex. 100% or 250px).', 'GJM' ),
					'attributes'  => array( 'size' => '10' )
				),
				array(
					'name'        => $prefix.'_map_width',
					'type'		  => 'text',
					'std'         => '100%',
					'placeholder' => '',
					'label'       => __( 'Map Width', 'GJM' ),
					'desc'        => __( 'Map width in pixels or percentage (ex. 100% or 250px).', 'GJM' ),
					'attributes'  => array( 'size' => '10' )
				),
				array(
					'name'        => $prefix.'_map_type',
					'std'         => 'ROADMAP',
					'label'       => __( 'Map Type', 'GJM' ),
					'desc'        => __( 'Choose the map type.', 'GJM' ),
					'type'		  => 'select',
					'options'	  => array(
							'ROADMAP' 	=> __( 'ROADMAP' , 'GJM' ),
							'SATELLITE' => __( 'SATELLITE' , 'GJM' ),
							'HYBRID'    => __( 'HYBRID' , 'GJM' ),
							'TERRAIN'   => __( 'TERRAIN' , 'GJM' )
					),
				),
				array(
					'name'        => $prefix.'_group_markers',
					'std'         => 'markers_clusterer',
					'label'       => __( "Map markers grouping", 'GJM' ),
					'cb_label'    => __( 'Enable', 'GJM' ),
					'desc'        => __( "How would you like to group nearby map markers.", 'GJM' ),
					'type'        => 'select',
					'options'	  => array(
							'normal' 			=> __( 'No grouping' , 'GJM' ),
							'markers_clusterer' => __( 'Markers Clusters' , 'GJM' ),
							'markers_spiderfier'=> __( 'Markers Spiderfier' , 'GJM' ),
					),
				),
				array(
					'name'        => $prefix.'_user_marker',
					'std'         => 'https://maps.google.com/mapfiles/ms/icons/blue-dot.png',
					'label'       => __( "User marker", 'GJM' ),
					'desc'        => __( 'Url to the marker represents the user\'s location on the map.', 'GJM' ),
					'type'        => 'text',
					'attributes'  => array( 'style' => 'width:650px' ) 
				),
				array(
					'name'        => $prefix.'_location_marker',
					'std'         => 'https://maps.google.com/mapfiles/ms/icons/red-dot.png',
					'label'       => __( "Location marker", 'GJM' ),
					'desc'        => __( "Url to the marker represents locations on the map.", 'GJM' ),
					'type'        => 'text',
					'attributes'  => array( 'style' => 'width:650px' ) 
				),
				array(
					'name'        => $prefix.'_clusters_path',
					'std'         => 'https://raw.githubusercontent.com/googlemaps/js-marker-clusterer/gh-pages/images/m',
					'label'       => __( "Clusters path", 'GJM' ),
					'desc'        => __( "The path to the folder that contains the markers clusters images. The images must renamed m1.png, m2.png, m3.png, m4.png and m5.png respective to the cluster size.", 'GJM' ),
					'type'        => 'text',
					'attributes'  => array( 'style' => 'width:650px' ) 
				),
				array(
					'name'        => $prefix.'_scroll_wheel',
					'std'         => '1',
					'label'       => __( 'Scroll-wheel map control', 'GJM' ),
					'cb_label'    => __( 'Enable', 'GJM' ),
					'desc'        => __( "Enable map zoom in/out using the mouse wheel.", 'GJM' ),
					'type'        => 'checkbox',
					'attributes'  => array()
				),
				array(
					'name'        => $prefix.'_distance',
					'std'         => '1',
					'label'       => __( "Display distance", 'GJM' ),
					'cb_label'    => __( 'Enable', 'GJM' ),
					'desc'        => sprintf( __( 'Display the distance to each %s in the list of results.', 'GJM' ), $scs_px ),
					'type'        => 'checkbox',
					'attributes'  => array()
				),
						
			),
		);
	
		//single page options
		$args['gjm_single_page'] = array(
			__( 'Geo Single Page Settings', 'GJM' ),
			array(
				array(
					'name'        => $prefix.'_single_map_enabled',
					'std'         => 'top',
					'label'       => __( 'Display map', 'GJM' ),
					'desc'        => sprintf( __( 'Where on the page would you like to display the single %s map.', 'GJM' ), $scs_px, $scs_px ),
					'type'        => 'select',
					'attributes'  => array(),
					'options'	  => array( 
						'disabled' => __( 'Disabled', 'GMW' ),
						'top'	   => __( 'Top', 'GMW' ),
						'bottom'   => __( 'Bottom', 'GMW' )
					)
				),
				array(
					'name'        => $prefix.'_single_map_width',
					'type'		  => 'text',
					'std'         => '100%',
					'placeholder' => '',
					'label'       => __( 'Map Width', 'GJM' ),
					'desc'        => __( 'Map width in pixels or percentage (ex. 100% or 250px).', 'GJM' ),
					'attributes'  => array()
				),
				array(
					'name'        => $prefix.'_single_map_height',
					'type'		  => 'text',
					'std'         => '250px',
					'placeholder' => '',
					'label'       => __( 'Map height', 'GJM' ),
					'desc'        => __( 'Map height in pixels or percentage (ex. 100% or 250px).', 'GJM' ),
					'attributes'  => array()
				),
				array(
					'name'        => $prefix.'_single_map_type',
					'std'         => 'ROADMAP',
					'label'       => __( 'Map Type', 'GJM' ),
					'desc'        => __( 'Choose the map type.', 'GJM' ),
					'type'		  => 'select',
					'options'	  => array(
							'ROADMAP' 	=> __( 'ROADMAP' , 'GJM' ),
							'SATELLITE' => __( 'SATELLITE' , 'GJM' ),
							'HYBRID'    => __( 'HYBRID' , 'GJM' ),
							'TERRAIN'   => __( 'TERRAIN' , 'GJM' )
					),
				),
				array(
					'name'        => $prefix.'_single_map_location_marker',
					'type'		  => 'text',
					'std'         => 'https://maps.google.com/mapfiles/ms/icons/red-dot.png',
					'label'       => __( "Location marker", 'GJM' ),
					'desc'        => __( "Url to the marker represents locations on the map.", 'GJM' ),
					'attributes'  => array( 'size' => '60') 
				),
				array(
					'name'        => $prefix.'_single_map_scroll_wheel',
					'type'        => 'checkbox',
					'std'         => '1',
					'label'       => __( 'Scroll-wheel map control', 'GJM' ),
					'cb_label'    => __( 'Enable', 'GJM' ),
					'desc'        => __( "Enable map zoom in/out using the mouse wheel.", 'GJM' ),
					'attributes'  => array()
				),
			),
		);

		return $args;
	}

	/**
	 * Validate the updated values and return them to be saved in gjm_options single option.
	 * i like keeping all the options saved in a single array which can later be pulled with
	 * a single call to database instead of multiple calles per option
	 */
	function options_validate() {

		$prefix = $this->prefix;

		$valid_input = array();

		foreach ( $this->admin_settings( array() ) as $section_name => $section ) {
			
			$sn = str_replace( 'gjm_','', $section_name );

			foreach ( $section[1] as $option ) {

				if ( empty( $option['type'] ) )
					continue;				
				
				switch ( $option['type'] ) {

					case "checkbox" :

						$valid_input[$sn][$option['name']] = $_POST[$option['name']] = ( !empty( $_POST[$option['name']] ) ) ? 1 : 0; 

					break;

					case "select" :

						if ( !empty( $_POST[$option['name']] ) && in_array( $_POST[$option['name']], array_keys( $option['options'] ) ) ) {
							$valid_input[$sn][$option['name']] = $_POST[$option['name']];
						} else {
							$valid_input[$sn][$option['name']] = $_POST[$option['name']] = ( !empty( $option['std'] ) ) ? $option['std'] : '';
						}

					break;
					case "text" :
						if ( !empty( $_POST[$option['name']] ) ) {
							$valid_input[$sn][$option['name']] = $_POST[$option['name']] = esc_attr( sanitize_text_field( $_POST[$option['name']] ) );
						} else {
							$valid_input[$sn][$option['name']] = $_POST[$option['name']] = ( !empty( $option['std'] ) ) ? $option['std'] : '';
						}
					break;
				}
			}
		}

		update_option( $prefix.'_options', $valid_input );
	}
}
new GJM_Admin_Settings;