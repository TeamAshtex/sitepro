<?php
// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * 
 */
class GJM_Form_Query_Class {

	function __construct() {

		$this->settings = get_option( 'gjm_options' );
		$this->prefix 	= 'gjm';
		$this->labels 	= gjm_labels();

		add_filter( 'job_manager_output_jobs_defaults', 		array( $this, 'default_atts' 		   ) );
		add_action( 'job_manager_job_filters_search_jobs_end',  array( $this, 'modify_search_form' ), 8 );
		add_filter( 'get_job_listings_query_args', 				array( $this, 'search_query' 	   ), 99 );
		add_filter( 'the_job_location', 						array( $this, 'job_distance'    ), 99, 2 );
		add_action( 'listable_job_listing_card_image_top',   	array( $this, 'listable_job_distance'  ), 99 );
		add_action( 'job_manager_job_filters_after', 			array( $this, 'results_map' 		   ) );
		add_shortcode( 'gjm_results_map', 						array( $this, 'results_map_shortcode'  ) );
	}
	
	/**
	 * GJM Shortcode attributes
	 * @return void
	 */
	public function shortcode_atts() {

		return array(
			$this->prefix.'_element_id' 	=> rand( 10,1000),
			$this->prefix.'_use'         	=> 0,
			$this->prefix.'_map'           	=> 0,
			$this->prefix.'_map_width'     	=> '100%',
			$this->prefix.'_map_height'    	=> '250px',
			$this->prefix.'_orderby'       	=> 'distance,featured,title,date',
			$this->prefix.'_autocomplete'  	=> 1,
			$this->prefix.'_radius'        	=> '5,10,15,25,50,100',
			$this->prefix.'_units'         	=> 'both',
			$this->prefix.'_distance'      	=> 1,
			$this->prefix.'_auto_locator' 	=> 1,
			$this->prefix.'_locator_button' => 1,
			$this->prefix.'_map_type'      	=> 'ROADMAP',
			$this->prefix.'_scroll_wheel'  	=> 1,
			$this->prefix.'_group_markers' 	=> 'markers_clusterer',
			$this->prefix.'_user_marker' 	=> 'https://maps.google.com/mapfiles/ms/icons/blue-dot.png',
			$this->prefix.'_location_marker'=> 'https://maps.google.com/mapfiles/ms/icons/red-dot.png',
			$this->prefix.'_clusters_path'  => 'https://raw.githubusercontent.com/googlemaps/js-marker-clusterer/gh-pages/images/m'
		);
	}
	/**
	 * GJM Function - merge GJM and WP JOb Manager shortcode attributes
	 * @since 1.0
	 * @author Eyal Fitoussi
	 */
	function default_atts( $args ) {
		return array_merge( $args, $this->shortcode_atts() );
	}

	/**
	 * Locator button
	 * @since 1.2
	 * @author Eyal Fitoussi
	 */
	protected function locator_button() {

		if ( empty( $this->filters[$this->prefix.'_locator_button'] ) )
			return;

		//Locator btn 
		$locator  = '<div class="'.$this->prefix.'-locator-btn-holder" style="display:none;">';
		$locator .=  '<i class="'.$this->prefix.'-filter '.$this->prefix.'-locator-btn  gjm-icon-target" title="Get your current position"></i>';
	    $locator .=  '<i class="'.$this->prefix.'-filter '.$this->prefix.'-locator-loader gjm-icon-spin-thin animate-spin"></i>';
		$locator .= '</div>';
			
		return apply_filters( $this->prefix.'_locator_button', $locator, $this->filters );
	}

	/**
	 * radius filter to display in search form
	 * If multiple values we will display dropdown select box otherwise if single value it will be default and hidden
	 * @since 1.0
	 * @author Eyal Fitoussi
	 */
	protected function filters_radius() {
				
		$radius = explode( ",", $this->filters[$this->prefix.'_radius'] );
		$output = '';
		
		//display dropdown
		if ( count( $radius ) > 1 ) {
			
			//Displace dropdown
			$output .= '<div class="'.$this->prefix.'-filter-wrapper '.$this->prefix.'-radius-wrapper radius dropdown '.$this->prefix.'-filters-count-'.esc_attr( $this->filters['filters_count'] ).'">';
			$output .= '<select name="'.$this->prefix.'_radius" class="'.$this->prefix.'-filter" id="'.$this->prefix.'-radius">';
			$output .= '<option value="' . esc_attr( end( $radius ) ) . '">';
						
			//set the first option of the dropdown
			if ( $this->filters[$this->prefix.'_units'] == 'imperial' ) {

				$output .= $this->labels['miles'];

			} elseif ( $this->filters[$this->prefix.'_units'] == 'metric' ) { 

				$output .= $this->labels['kilometers'];
			} else {

				$output .= $this->labels['within'];
			}

			$output .= '</option>';

			foreach ( $radius as $value ) {
				$value = esc_attr( $value );
				$output .=  '<option value="' . $value . '">' . $value . '</option>';
			}
			
			$output .= '</select>';
			$output .= '</div>';
		
		} else {
			//display hidden default value
			$output .=  '<input type="hidden" id="'.$this->prefix.'-radius" name="'. $this->prefix .'_radius" value="' . esc_attr( end( $radius ) ) . '" />';
		}
		
		return apply_filters( $this->prefix.'_form_radius_filter', $output, $radius, $this );
	}

	/**
	 * Units filter for search form
	 * Will display dropdown when choosing to display both units otherwise the deault will be hidden
	 * @since  1.0
	 * @author Eyal Fitoussi
	 */
	protected function filters_units() {

		$output = '';

		//display dropdown
		if ( $this->filters[$this->prefix.'_units'] == 'imperial' || $this->filters[$this->prefix.'_units'] == 'metric' ) {

			//display hidden field
			$output .= '<input type="hidden" id="'.$this->prefix.'-units" name="'.$this->prefix.'_units" value="' . esc_attr( $this->filters[$this->prefix.'_units'] ) . '" />';

		} else {
			
			//display dropdown
			$output .= '<div class="'.$this->prefix.'-filter-wrapper '.$this->prefix.'-units-wrapper units dropdown '.$this->prefix.'-filters-count-'.esc_attr( $this->filters['filters_count'] ).'">';			
			$output .= '<select name="'.$this->prefix.'_units" class="'.$this->prefix.'-filter" id="'.$this->prefix.'-units">';
			$output .= 	'<option value="imperial">' . esc_attr( $this->labels['miles'] ) . '</option>';
			$output .= 	'<option value="metric">' . esc_attr( $this->labels['kilometers'] ) . '</option>';
			$output .= 	'</select>';
			$output .= '</div>';
		}

		return apply_filters( $this->prefix.'_form_units_filter', $output, $this );
	}

	/**
	 * radius filter to display in search form
	 * If multiple values we will display dropdown select box otherwise if single value it will be default and hidden
	 * @since  1.0
	 * @author Eyal Fitoussi
	 */
	protected function filters_sort() {

		$orderby = explode( ",", str_replace(' ', '', $this->filters[$this->prefix.'_orderby'] ) );
		$output  = '';

		//display dropdown
		if ( count( $orderby ) > 1 ) {
						
			//display dropdown
			$output .= '<div class="'.$this->prefix.'-filter-wrapper '.$this->prefix.'-orderby-wrapper orderby dropdown '.$this->prefix.'-filters-count-'.esc_attr( $this->filters['filters_count'] ) .'">';			
			$output .= '<select name="'.$this->prefix.'_orderby" class=" job-manager-filter '.$this->prefix.'-filter" id="'.$this->prefix.'-orderby">';
			$output .= 	'<option class="orderby-first-option" value="">' . esc_attr( $this->labels['orderby_filters']['label'] ) . '</option>';
		
			$valCount = 1;

			foreach ( $orderby as $value ) {
				if ( in_array( $value, array( 'distance','title','featured' ,'date' ,'modified','ID','parent','rand','name' ) ) ) {				
					$output .= '<option value="' . esc_attr( $value ) . '" class="'.$this->prefix.'-orderby-value-' . $valCount . '">'.esc_attr( $this->labels['orderby_filters'][$value] ).'</option>';
				}
				$valCount++;
			}
			
			$output .= 	'</select>';
			$output .= '</div>';
		
		} else {
		
			//display hidden default value
			$output .= '<input type="hidden" id="'.$this->prefix.'-orderby" name="'.$this->prefix.'_orderby" value="' . esc_attr( reset( $orderby ) ) . '" />';
		}

		return apply_filters( $this->prefix.'_form_orderby_filter', $output, $orderby, $this );
	}
	
	public function results_map( $atts ) {

		if ( empty( $this->filters[$this->prefix.'_map'] ) || $this->filters[$this->prefix.'_map'] != 1 ) {
			return;
		}

    	//map arguments
    	$args = array(
    		'mapId'		 	=> $this->filters[$this->prefix.'_element_id'],
			'prefix'	 	=> $this->prefix,
			'addon'		 	=> $this->prefix,
			'init_map_show'	=> false,
			'map_width'  	=> $this->filters[$this->prefix.'_map_width'],
			'map_height' 	=> $this->filters[$this->prefix.'_map_height'],
    	);
 
    	//get the map element
    	echo gjm_get_results_map( $args );
	}

	/**
	 * add some elements to search form
	 * @since  1.0
	 * @author Eyal Fitoussi
	 */
	public function modify_search_form( $atts ) {

		$this->prefix = esc_attr( $this->prefix );

		if ( empty( $atts[$this->prefix.'_use'] ) ) {
			$atts[$this->prefix.'_use'] = 0;
		}

		$hiddenFields = '';
		
		//When using shotcode attributes
		if ( $atts[$this->prefix.'_use'] == 1 ) {

			$this->filters = $atts;

			//add gjm hidden values to the form to be able to pass it into the query and otehr functions
			$hiddenFields .= '<input type="hidden" name="'.$this->prefix.'_use" value="1" />';
			$hiddenFields .= '<input type="hidden" name="'.$this->prefix.'_map" value="' . esc_attr( $this->filters[$this->prefix.'_map'] ) . '" />';
			$hiddenFields .= '<input type="hidden" name="'.$this->prefix.'_user_marker" value="' . esc_attr( $this->filters[$this->prefix.'_user_marker'] ) . '" />';
			$hiddenFields .= '<input type="hidden" name="'.$this->prefix.'_location_marker" value="' . esc_attr( $this->filters[$this->prefix.'_location_marker'] ) . '" />';
			$hiddenFields .= '<input type="hidden" name="'.$this->prefix.'_clusters_path" value="' . esc_attr( $this->filters[$this->prefix.'_clusters_path'] ) . '" />';
			$hiddenFields .= '<input type="hidden" name="'.$this->prefix.'_group_markers" value="' . esc_attr( $this->filters[$this->prefix.'_group_markers'] ) . '" />';			

		//when using the options set in the admin
		} elseif ( $atts[$this->prefix.'_use'] == 2 ) {

			$this->filters = wp_parse_args( $this->settings['search_form'], $atts );

			$hiddenFields .= '<input type="hidden" name="'.$this->prefix.'_use" value="2" />';

		} else {
			return;
		}

		if ( empty( $this->filters['gjm_clusters_path'] ) || $this->filters['gjm_clusters_path'] == 'https://google-maps-utility-library-v3.googlecode.com/svn/trunk/markerclustererplus/images/m' ) {
			$this->filters['gjm_clusters_path'] = 'https://raw.githubusercontent.com/googlemaps/js-marker-clusterer/gh-pages/images/m';
		}

		$this->filters['gjm_clusters_path'] = apply_filters( $this->prefix.'_clusters_path', $this->filters['gjm_clusters_path'], $this );
		wp_localize_script( 'gmw-marker-clusterer', 'clusterImage', $this->filters['gjm_clusters_path'] );

		//hidden fields to hold values
		$hiddenFields .= '<input type="hidden" id="gjm_lat" name="gjm_lat" value="" />';
		$hiddenFields .= '<input type="hidden" id="gjm_lng" name="gjm_lng" value="" />';
		$hiddenFields .= '<input type="hidden" id="gjm_prev_address" name="gjm_prev_address" value="" />';
		$hiddenFields .= '<input type="hidden" id="'.$this->prefix.'_element_id" name="'.$this->prefix.'_element_id" value="'.$atts[$this->prefix.'_element_id'].'" />';
		$hiddenFields .= '<input type="hidden" id="gjm_prefix" name="gjm_prefix" value="'.$this->prefix.'" />';
		$hiddenFields .= '<input type="hidden" id="'.$this->prefix.'_scroll_wheel" name="'.$this->prefix.'_scroll_wheel" value="'.$atts[$this->prefix.'_scroll_wheel'].'" />';

		$hiddenFields .= '<input type="hidden" class="gjm-country-name" name="gjm_county_name" value="" />';
		$hiddenFields .= '<input type="hidden" class="gjm-country-code" name="gjm_country_code" value="" />';

		$mapIcon 	   = ! empty( $this->filters[$this->prefix.'_location_marker'] ) ? $this->filters[$this->prefix.'_location_marker'] : 'https://maps.google.com/mapfiles/ms/icons/red-dot.png';

		$hiddenFields .= '<input type="hidden" name="'.$this->prefix.'_location_marker" value="' . esc_attr( $mapIcon ) . '" />';
		$hiddenFields .= '<input type="hidden" class="gjm-state-name" name="gjm_state_name" value="" />';
		$hiddenFields .= '<input type="hidden" class="gjm-state-code" name="gjm_state_code" value="" />';

		//add hidden locator button that will dynamically append into the address field
		$hiddenFields .= self::locator_button();

		// user position
		$this->filters['user_location']['lat'] = false;
		$this->filters['user_location']['lng'] = false;
		
		// Field counter
		$this->filters['filters_count'] = 0;

		// hidden field to trigger autocomplete
		if ( !empty( $this->filters[$this->prefix.'_autocomplete'] ) ) {
			$results_type  = ! empty( $this->settings['general_settings'][$this->prefix."_address_autocomplete_results_type"] ) ? $this->settings['general_settings'][$this->prefix."_address_autocomplete_results_type"] : 'geocode'; 
			$country 	   = ! empty( $this->settings['general_settings'][$this->prefix."_address_autocomplete_country"] ) ? $this->settings['general_settings'][$this->prefix."_address_autocomplete_country"] : ''; 
			$hiddenFields .= '<div class="'.$this->prefix.'-autocomplete-trigger search-form" data-country="'.$country.'" data-results-type="'.$results_type .'" style="display:none;"></div>';
		}

		// hidden field to trigger auto-locator
		if ( !empty( $this->filters[$this->prefix.'_auto_locator'] ) ) {
			$hiddenFields .= '<div class="'.$this->prefix.'-autolocator-trigger" style="display:none;"></div>';
		}

		echo $hiddenFields;

		//look for radius dropdown
		if ( count( explode( ",", $this->filters[$this->prefix.'_radius'] ) ) > 1 ) {
			$this->filters['filters_count']++;
		}

		//look for orderby dropdown
		if ( isset( $this->filters[$this->prefix.'_orderby'] ) && count( explode( ",", $this->filters[$this->prefix.'_orderby'] ) ) > 1 ) {
			$this->filters['filters_count']++;
		}

		//look for units dropdown
		if ( !in_array( $this->filters[$this->prefix.'_units'], array( 'imperial', 'metric' ) ) ) {
			$this->filters['filters_count']++;
		}

		//load stylsheet
		wp_enqueue_style( 'gjm-frontend-style' );
		
		$output_fields = array();

		$output_fields['start'] = '<div class="'.$this->prefix.'-filters-wrapper">';
		
		//add radius filter
		$output_fields['radius'] = self::filters_radius();
		
		//add units filter
		$output_fields['units'] = self::filters_units();
		
		//add sort by filter
		$output_fields['orderby'] = self::filters_sort();

		$output_fields['end'] = '</div>';

		// display fields
		echo implode( ' ', $output_fields );

		// display map
		if ( $this->filters[$this->prefix.'_map'] == 1 ) {

			//enqueue the map script
			wp_enqueue_script( 'gjm-map' );
			wp_localize_script( 'gjm-map', 'formFilters', $this->filters );

			//Load marker clusterer library - to be used with premium features
			if ( $this->filters[$this->prefix.'_group_markers'] == 'markers_clusterer' && ! wp_script_is( 'gmw-marker-clusterer', 'enqueued' )  ) {	
				wp_enqueue_script( 'gmw-marker-clusterer' );
			}

			//load marker spiderfier library - to be used with premium features
			if ( $this->filters[$this->prefix.'_group_markers'] == 'markers_spiderfier' && ! wp_script_is( 'gmw-marker-spiderfier', 'enqueued' )  ) {	
				wp_enqueue_script( 'gmw-marker-spiderfier' );
			}
		}

	}
	
	/**
	 * GJM Function - do some distance quering
	 * @since  1.0
	 * @author Eyal Fitoussi
	 */
	function search_query( $query_args ) {
				
		if ( empty( $_REQUEST['form_data'] ) )
			return $query_args;
		
		// get form values
		$atts = wp_parse_args( $_REQUEST['form_data'], array_merge( $this->shortcode_atts(), array(
			'search_location'  			  => '',
			'gjm_lat'					  => '',
			'gjm_lng'					  => '',
			'gjm_country_name'			  => '',
			'gjm_country_code'			  => '',
			'gjm_state_name'			  => '',
			'gjm_state_code'			  => '',
		) ) );
		
		//check if we are using the shortcode attributes settings
		if ( $atts[$this->prefix.'_use'] == 1 ) {

			//set attributes to global, we will pass it to otehr functions
			$this->filters = $atts;

		//otherwise, are we using the admin settings
		} elseif ( $atts[$this->prefix.'_use'] == 2 ) {

			$this->filters                    		 	= $this->settings['search_form'];
			$this->filters[$this->prefix.'_element_id'] = $atts[$this->prefix.'_element_id'];
			$this->filters['search_location'] 		 	= $atts['search_location'];
			$this->filters['gjm_lat'] 		 		 	= $atts['gjm_lat'];
			$this->filters['gjm_lng'] 		 		 	= $atts['gjm_lng'];
			$this->filters['gjm_country_name'] 		 	= $atts['gjm_country_name'];
			$this->filters['gjm_country_code'] 		 	= $atts['gjm_country_code'];
			$this->filters['gjm_state_name'] 		 	= $atts['gjm_state_name'];
			$this->filters['gjm_state_code'] 		 	= $atts['gjm_state_code'];
			$this->filters[$this->prefix.'_use']     	= $atts[$this->prefix.'_use'];
			$this->filters[$this->prefix.'_units']   	= $atts[$this->prefix.'_units'];
			$this->filters[$this->prefix.'_radius']  	= $atts[$this->prefix.'_radius'];
			$this->filters[$this->prefix.'_orderby']    = $atts[$this->prefix.'_orderby'];

		//abort if we do not use gjm in this shortcode!
		} else {
			return $query_args;
		}

		/*
		 * if we are using gjm orderby we will need to override the original setting created by Wp Jobs Manager plugin
		 * Unless when using orderby "featured" we will leave it as is
		 */
		if ( ! empty( $this->filters[$this->prefix.'_orderby'] ) && $this->filters[$this->prefix.'_orderby'] != 'featured' ) {

			//force gjm orderby value from dropdown or default value
			$query_args['orderby'] = $this->filters[$this->prefix.'_orderby'];

			//remove orderby "featured" filter in case it was set via WP Job Manager
			remove_filter( 'posts_clauses', 'order_featured_job_listing' );
			remove_filter( 'posts_clauses', 'order_featured_resume' );
			
			//remove the meta key of orderby featured
			unset( $query_args['meta_key'] );

			//adjust the order of posts when choosing to order by title
			if ( $this->filters[$this->prefix.'_orderby'] == 'title' ) {
				$query_args['order'] = 'ASC';
			}
						
		// set the original orderby by Wp JOb Manager plugin
		} elseif ( empty( $this->filters[$this->prefix.'_orderby'] ) && !empty( $this->filters['search_location'] ) ) {

			//$query_args['orderby'] = 'meta_key';
			$this->filters[$this->prefix.'_orderby'] = apply_filters( 'gjm_default_orderby', $query_args['orderby'], $this );
		}
 
		// add geo values to query args so it can be saved in Wp Job Manager cache
		$query_args[$this->prefix]['location'] = $atts['search_location'];
		$query_args[$this->prefix]['units']    = $atts[$this->prefix.'_units'];
		$query_args[$this->prefix]['radius']   = $atts[$this->prefix.'_radius'];
		
		// disable the location query made by WP Job Manager
		if ( !empty( $query_args[$this->prefix]['location'] ) ) {
			unset($query_args['meta_query'][0]);
		}

		$this->filters['user_location']['lat'] = false;
		$this->filters['user_location']['lng'] = false;

		// when searhing by address
		if ( ! empty( $this->filters['search_location'] ) && $this->filters['search_location'] != 'Any Location' && $this->filters['search_location'] != 'Location' ) {

			if ( ! empty( $this->filters['gjm_lat'] ) && ! empty( $this->filters['gjm_lng'] ) ) {
	
				$this->filters['user_location']['lat'] = $this->filters['gjm_lat'];
				$this->filters['user_location']['lng'] = $this->filters['gjm_lng'];
			
			//in case that an address was entered and was not geocoded via client site
			//try again via serverside.
			} elseif ( class_exists( 'WP_Job_Manager_Geocode' ) ) {
				
				$this->geocoded = WP_Job_Manager_Geocode::get_location_data( $this->filters['search_location'] );
					
				if ( ! is_wp_error( $this->geocoded ) && ! empty( $this->geocoded['lat'] ) && ! empty( $this->geocoded['long'] ) ) {
						
					$this->filters['user_location']['lat'] = $this->geocoded['lat'];
					$this->filters['user_location']['lng'] = $this->geocoded['long'];
					?>
					<script>
						jQuery('#gjm_lat').val('<?php echo $this->geocoded['lat']; ?>');
						jQuery('#gjm_lng').val('<?php echo $this->geocoded['long']; ?>');
					</script>
					<?php 
				}
			} 
		}

		add_filter( 'posts_clauses', array( $this, 'query_clauses' ), 99 );
		
		/*
		 * add filter that will pass the results of this query to javascipt
		 * In order to display the results on the map
		 */
		if ( $this->filters[$this->prefix.'_map'] == 1 ) {
			add_action( 'the_post', array( $this, 'the_post' 	) );
			add_action( 'loop_end', array( $this, 'map_element' ) );
		}
		
		return $query_args;
	}

	/**
	 * Query clauses
	 * @param  object $clauses
	 * @return object new clauses
	 */
	public function query_clauses( $clauses ) {

		global $wpdb;

		// when searhing by location
		if ( ! empty( $this->filters['search_location'] ) && $this->filters['search_location'] != 'Any Location' && $this->filters['search_location'] != 'Location' ) {
			
			// if there are no coords show no results
			if ( empty( $this->filters['user_location']['lat'] ) || empty( $this->filters['user_location']['lng'] ) ) {
				
				$this->geocode_message = ( ! empty( $this->geocoded['error'] ) ) ? $this->geocoded['error'] : '';
					
				//abort the query
				$clauses['where'] .= " AND 1 = 2";
	
				return $clauses;							
			}

			// set earth radius
			$earthRadius = ( $this->filters[$this->prefix.'_units'] == 'imperial' ) ? 3959 : 6371;

			// search within boundaries
			$boundaries_search = apply_filters( 'gjm_search_within_boundaries', false );

			// join GJM locations table into the job query
			$clauses['join']   .= " INNER JOIN " . $wpdb->prefix . "places_locator gmwlocations ON $wpdb->posts.ID = gmwlocations.post_id ";
			$clauses['fields'] .= ", gmwlocations.formatted_address, gmwlocations.address, gmwlocations.lat, gmwlocations.long ";
			
			// make sure coordinates valid
			$clauses['where'] .= " AND ( gmwlocations.lat != 0.000000 && gmwlocations.long != 0.000000 ) ";

        	if ( $boundaries_search && ( ! empty( $this->filters['gjm_state_name'] ) || ! empty( $this->filters['gjm_state_code'] ) ) ) {

        		$clauses['where'] .= $wpdb->prepare( " AND ( gmwlocations.state = %s OR gmwlocations.state_long = %s )", $this->filters['gjm_state_code'], $this->filters['gjm_state_name'] );
        	
        	// filter country
        	} elseif ( $boundaries_search && ( ! empty( $this->filters['gjm_country_name'] ) || ! empty( $this->filters['gjm_country_code'] ) ) ) {
        		
        		$clauses['where'] .= $wpdb->prepare( " AND ( gmwlocations.country = %s OR gmwlocations.country_long = %s )", $this->filters['gjm_country_code'], $this->filters['gjm_country_name'] );
        	
			} else {

				$clauses['fields'] .= $wpdb->prepare( ", ROUND( %d * acos( cos( radians( %s ) ) * cos( radians( gmwlocations.lat ) ) * cos( radians( gmwlocations.long ) - radians( %s ) ) + sin( radians( %s ) ) * sin( radians( gmwlocations.lat) ) ),1 ) AS distance", 
					array( $earthRadius, $this->filters['user_location']['lat'], $this->filters['user_location']['lng'], $this->filters['user_location']['lat'] ) );
				
				// make sure we pass numeric or decimal as radius
	            if ( is_numeric( $this->filters[$this->prefix.'_radius'] ) || is_float( $this->filters[$this->prefix.'_radius'] ) )  {
	        	   $clauses['having'] = $wpdb->prepare( "HAVING distance <= %s OR distance IS NULL", $this->filters[$this->prefix.'_radius'] );
	        	}
			}

			// order by distance when needed
			if ( $this->filters[$this->prefix.'_orderby'] == 'distance' ) {
				
				$clauses['orderby'] = 'distance';
				
			} elseif ( $this->filters[$this->prefix.'_orderby'] == 'featured' ) {

				$clauses['orderby'] = "$wpdb->posts.menu_order ASC, distance ASC";
			} 

		//when no address entereed we will display all job
		} else {

			// left join the location table into the query to show both located and unlocated jobs/resumes
			$clauses['join']   .= " LEFT JOIN " . $wpdb->prefix . "places_locator gmwlocations ON $wpdb->posts.ID = gmwlocations.post_id ";
			$clauses['fields'] .= ", gmwlocations.lat, gmwlocations.long, gmwlocations.formatted_address, gmwlocations.address ";
		}
		
		$clauses = apply_filters( $this->prefix.'_query_clauses', $clauses, $this );

        if ( ! empty( $clauses['having'] ) ) {

            if ( empty( $clauses['groupby'] ) ) {
                $clauses['groupby'] = $wpdb->prefix.'posts.ID';
            }
            $clauses['groupby'] .= ' '.$clauses['having'];
            unset( $clauses['having'] );
        } 

		return $clauses;
	}
	
	/**
	 * Pass locations and other values to javasctip to display on the map
	 * @since  1.0
	 * @author Eyal Fitoussi
	 */
	public function map_element( $query ) {
		
		global $gjmMapElements;

		//map options - pass to via global map object to JavaScript which will display the map
		$mapArgs = array(
			'mapId'				=> $this->filters[$this->prefix.'_element_id'],
			'mapType'			=> $this->prefix,
			'prefix'			=> $this->prefix,
			'form' 		 		=> '',
			'locations'			=> $query->posts,
			'infoWindowType'	=> 'normal',
			'zoomLevel'			=> 'auto',
			'mapOptions'		=> array(
				'mapTypeId'		=> $this->filters[$this->prefix.'_map_type'],
				'scrollwheel'	=> $this->filters[$this->prefix.'_scroll_wheel']
			),
			'userPosition'		=> array(
				'lat'		=> $this->filters['user_location']['lat'],
				'lng'		=> $this->filters['user_location']['lng'],
				'address' 	=> $this->filters['search_location'],
				'mapIcon'	=> $this->filters[$this->prefix.'_user_marker'],
				'iwContent' => 'You are here',
				'iwOpen'	=> false
			),
			'markersDisplay' => ( !empty( $this->filters[$this->prefix.'_group_markers'] ) ) ? $this->filters[$this->prefix.'_group_markers'] : 'markers_clusterer',
		);

		gjm_new_map_element( $mapArgs );

		echo '
		<script> 
        	//pass some values to javascript     	
        	jQuery(window).ready(function($) {
        		//pass the map object to JS
                gjmMapsObjectInit( '.json_encode( $gjmMapElements ).' );
        	});
       	</script>';
       	
		//remove actions
		remove_action( 'the_post', array( $this, 'the_post'    ) );
		remove_action( 'loop_end', array( $this, 'map_element' ) );
	}

	/**
	 * The post
	 * modify each post in the loop with information that will be added
	 * to map markers
	 */
	function  the_post() {
		global $post;

		//append info window content to post object
		$post->info_window_content = $this->info_window_content( $post );

		//append map iconto post object
		$mapIcon 	   = ! empty( $this->filters[$this->prefix.'_location_marker'] ) ? $this->filters[$this->prefix.'_location_marker'] : 'https://maps.google.com/mapfiles/ms/icons/red-dot.png';
		$post->mapIcon = apply_filters( $this->prefix.'_map_icon', $mapIcon, $post, $this->filters );
	}

	/**
	 * info window function callback
	 * @param  object $post
	 * @return [type]       [description]
	 */
	public function info_window_content( $post ) {
		return gjm_info_window_content( $post );
	}
	
	/**
	 * GJM Function - add distance value to results
	 * @since  1.0
	 * @author Eyal Fitoussi
	 */
	public function job_distance( $location, $post ) {

		if ( ! isset( $this->filters ) ) {
			return $location;
		}

		if ( $this->filters[$this->prefix.'_use'] == 0 || $this->filters[$this->prefix.'_distance'] != 1 || empty( $this->filters['search_location'] ) ) {
			return $location;
		}

		if ( empty( $post->distance ) ) {
			return $location;
		}
		
		$units = ( $this->filters[$this->prefix.'_units'] == 'imperial' ) ? $this->labels['search_result']['mi'] : $this->labels['search_result']['km'];

		return apply_filters( $this->prefix.'_add_distance_to_results', $location . '<span class="'.$this->prefix.'-distance-wrapper">' . esc_attr( $post->distance ) . ' ' . esc_attr(  $units ) . '</span>', $location, $post, $this->filters );
	}

	public function listable_job_distance( $post ) {

		if ( ! isset( $this->filters ) ) {
			return;
		}

		if ( $this->filters[$this->prefix.'_use'] == 0 || $this->filters[$this->prefix.'_distance'] != 1 || empty( $this->filters['search_location'] ) ) {
			return;
		}

		if ( empty( $post->distance ) ) {
			return;
		}
		
		$units = ( $this->filters[$this->prefix.'_units'] == 'imperial' ) ? $this->labels['search_result']['mi'] : $this->labels['search_result']['km'];

		echo apply_filters( $this->prefix.'_add_distance_to_results', '<span class="'.$this->prefix.'-distance-wrapper">' . esc_attr( $post->distance ) . ' ' . esc_attr(  $units ) . '</span>', $post, $this->filters );
	}

	/**
	 * Map holder for map when using map results shortcode
	 * @param  array $atts element ID
	 * @return void
	 */
	public function results_map_shortcode( $atts ) {

		$element_id = ( !empty( $atts['element_id'] ) ) ? '-'.$atts['element_id'].'"' : '';

		return '<div id="'.$this->prefix.'-results-map-holder'.$element_id.'" class="results-map-holder '.$this->prefix.'"></div>';
	}
}
new GJM_Form_Query_Class();