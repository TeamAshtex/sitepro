jQuery(document).ready(function($) {

	//do it for each autocomplete field in the form
	jQuery( '#'+AutoCompOptions.input_field ).each(function() {

		inputField = document.getElementById( AutoCompOptions.input_field );

		var acOptions = {
            types : [AutoCompOptions.results_type]
        };

        if ( AutoCompOptions.country != '' ) {
            acOptions.componentRestrictions = { country: AutoCompOptions.country }
        } 

        var autocomplete = new google.maps.places.Autocomplete( inputField, acOptions );
        
        google.maps.event.addListener(autocomplete, 'place_changed', function(e) {

    		var place = autocomplete.getPlace();

			if (!place.geometry)
				return;

    	});
    });	      
});