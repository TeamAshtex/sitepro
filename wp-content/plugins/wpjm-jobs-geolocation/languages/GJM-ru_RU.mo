��    8      �  O   �      �  #   �     �                <     E     J     a     r     ~     �     �     �     �     �     �     �       #   '     K     R     Y  	   j  
   t       1   �  =   �     �  	     
          C      "   d     �     �     �     �  
   �  	   �  Q   �     '  �   /  x   �     B	     H	     N	     U	  4   Y	  K   �	  N   �	  4   )
  e   ^
     �
     �
     �
  ,  �
  <   �  2   9     l  .   x     �     �  6   �  )   �     )     I     ^     k  '   �  .   �  '   �  6        :  0   N  X        �     �     �          '     <  R   W  }   �     (     :     R     j  �   s  9     ,   M      z  
   �     �  (   �     �  y   �     e  �   x  �   M     �               )  k   .  �   �  �   /  \   �  k        �     �     �            	                 .             
   4             )                  5          0      7   /            8   1   %   6      3   #   ,               +                               (       '           2   !                 &               "      $         *       -               %d Locations successfully imported. Activate License Key Address: Autocomplete country restriction Company: Date Deactivate License Key Display distance Display map Distance Error Featured GEO General Settings Geo Search Form Settings Geo Single Page Settings Google API default region Google API language Google address autocomplete Google address autocomplete (admin) HYBRID Import Import Locations Job Type: Kilometers License Key: License is activated. Thank you for your support! Lost or forgot your license key? <a %s >Retrieve it here.</a> Map Type Map Width Map height Miles No license key entered. Please enter the license key and try again. No locations to import were found. No locations were imported. Posted %s ago ROADMAP Radius Resize map SATELLITE Something is wrong with the key you entered. Please verify the key and try again. TERRAIN There is a new version of %1$s available. <a target="_blank" class="thickbox" href="%2$s">View version %3$s details</a> or <a href="%4$s">update now</a>. There is a new version of %1$s available. <a target="_blank" class="thickbox" href="%2$s">View version %3$s details</a>. Title Units Within Yes You do not have permission to install plugin updates Your license for %s plugin could not be activated. See error message below! Your license for %s plugin successfully activated. Thank you for your support! Your license for %s plugin successfully deactivated. Your license has expired. Please renew your license in order to keep getting its updates and support. jobs km mi Project-Id-Version: GEO Job Manager
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2016-11-06 19:20-1000
PO-Revision-Date: 2016-11-06 19:20-1000
Last-Translator: Eyal Fitoussi <fitoussi_eyal@hotmail.com>
Language-Team: 
Language: ru
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
X-Poedit-SourceCharset: UTF-8
X-Loco-Source-Locale: ru_RU
X-Generator: Poedit 1.8.11
X-Poedit-Basepath: ..
X-Poedit-KeywordsList: __;_e;_n:1,2;_x:1,2c;_ex:1,2c;_nx:4c,1,2;esc_attr__;esc_attr_e;esc_attr_x:1,2c;esc_html__;esc_html_e;esc_html_x:1,2c;_n_noop:1,2;_nx_noop:3c,1,2;__ngettext_noop:1,2
X-Loco-Parser: loco_parse_po
X-Loco-Target-Locale: en_GB
X-Poedit-SearchPath-0: .
 %d локации успешно импортированы. Активировать Ключ Лицензии Адрес: Ограничить страну поиска Организация: Дата Деактивировать Ключ Лицензии Отображать расстояния Отобразить карту Расстояние Ошибка Популярность Основные настройки GEO GEO настройки формы поиска GEO настройки страницы Регион по-умолчанию для Goolge API Язык Google API Автозаполнение адреса Google Автозаполнение адреса Google (под администратором) ГИБРИД Импорт Импорт локаций Тип вакансии: Километров Ключ Лицензии: Лицензия активирована. Спасибо за поддержку! Потеряли или забыли ваш лицензионный ключ? <a %s >Получить его здесь.</a> Тип карты Ширина карты высота карты Миль Лицензионный ключ не введен. Пожалуйста, введите лицензионный ключ и попробуйте снова. Локаций для импорта не найдено. Нет локаций для импорта. Размещено %s назад КАРТА Радиус Изменить размер карты СПУТНИК Введен неверный ключ. Проверьте правильность и повторите попытку. МЕСТНОСТЬ Существуетновая версия %1$s доступны . <a target="_blank" class="thickbox" href="%2$s"> Посмотреть версию%3$s детали </a> или <a href="%4$s">Сейчас</a>. Существуетновая версия %1$s доступны . <a target="_blank" class="thickbox" href="%2$s"> Посмотреть версию%3$s детали </a>. Названиe Значения Внутри Да Вы не имеете разрешения для установки плагинов обновления Ваша лицензия на плагин %s не может быть активирован . См ниже сообщение об ошибке! Ваша лицензия на плагин %s успешно активирована . Спасибо за вашу поддержку! Ваша лицензия на плагин %s успешно деактивирована . License has expired . Please renew your license in order to continue to receive their upgrades and support. работы км миль 