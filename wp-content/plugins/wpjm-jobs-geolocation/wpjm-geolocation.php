<?php
/*
 Plugin Name: WP Job Manager add-on  - Jobs Geolocation
 Plugin URI: http://www.geomywp.com/add-ons/geo-job-manager
 Description: Enhance WP Job Manager plugin with geolocation features
 Version: 1.8.1.1
 Author: Eyal Fitoussi
 Author URI: http://www.geomywp.com
 Requires at least: 4.0
 Tested up to: 4.3.1
 Text Domain: GJM
 Domain Path: /languages/
 License URI: http://www.gnu.org/licenses/gpl-3.0.html
*/

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * GEO_Job_Manager class.
 */
class GJM_Init {

	/**
	 * Constructor
	 */
	public function __construct() {

		// declare constants
		define( 'GJM_ITEM_NAME', 'Jobs Geolocation' );
		define( 'GJM_TITLE', __( 'Jobs Geolocation', 'GJM' ) );
		define( 'GJM_LICENSE_NAME', 'geo_job_manager' );
		define( 'GJM_ITEM_ID', 5417 );
		define( 'GJM_VERSION', '1.8.1.1' );
		define( 'GJM_DB_VERSION', '1.2' );
		define( 'GJM_FILE', __FILE__ );
		define(	'GJM_URL', untrailingslashit( plugins_url( basename( plugin_dir_path( __FILE__ ) ), basename( __FILE__ ) ) ) );
		define( 'GJM_PATH', untrailingslashit( plugin_dir_path( __FILE__ ) ) );
		
		if ( ! defined( 'GMW_REMOTE_SITE_URL' ) ) {
			define( 'GMW_REMOTE_SITE_URL', 'https://geomywp.com' );
		}

		// load text domain
		load_plugin_textdomain( 'GJM', FALSE, dirname( plugin_basename( __FILE__) ).'/languages/' );

        //make sure GEO my WP is activated and compare version, otherwise abort.
        if ( ! class_exists( 'WP_Job_Manager') || version_compare( JOB_MANAGER_VERSION, '1.23.1', '<' ) ) {
            add_action( 'admin_notices', array( $this, 'admin_notice' ) );      
            return;
        }

        //init add-on	
        add_action( 'plugins_loaded', 		 array( $this, 'plugins_loaded'   ) );
		add_action( 'wp_enqueue_scripts', 	 array( $this, 'register_scripts' ) );
		add_action( 'admin_enqueue_scripts', array( $this, 'register_scripts' ) );
			
		//admin files
		if ( is_admin() ) {
			include( 'includes/admin/gjm-admin.php' );
		}      

        //include frontend files
        if ( ! is_admin() || defined( 'DOING_AJAX' ) ) {
			include( 'includes/gjm-functions.php' );
			include( 'includes/gjm-jobs-query-class.php' );
			include( 'includes/gjm-global-map-class.php' );
			include( 'includes/gjm-shortcodes.php' );
		}
	}
	
	/**
	 * Plugins loaded hooks
	 * @return [type] [description]
	 */
	public function plugins_loaded() {
        
        if ( is_admin() ) {
        	// plugin updater
			$this->updater();
		}
	}

	/**
	 * Admin notice if WP JOb Manager doesnt match
	 * @return void
	 */
	public function admin_notice() {
		?>
		<div class="error">
			<p>Wp Job Manager Geolocation <?php printf( __( "requires <a %s>WP Job Manager</a> plugin version 1.23.1 or higher.", "GJM" ), 'href=\"http://wordpress.org/plugins/wp-job-manager/\" target=\"_blank\"' ); ?></p>
		</div>
		<?php       
	}

	/**
	 * Include addon function.
	 *
	 * @since 1.1
	 * @access public
	 * @return $addons
	 */
	public function addon_init( $addons ) {
		
		$addons[GJM_LICENSE_NAME] = array(
			'name' 	  		=> GJM_LICENSE_NAME,
			'item'	  		=> GJM_ITEM_NAME,
			'item_id'		=> GJM_ITEM_ID,
			'title'   		=> GJM_TITLE,
			'version' 		=> GJM_VERSION,
			'file' 	  		=> GJM_FILE,
			'basename'  	=> plugin_basename( GJM_FILE ),
			'author'  		=> 'Eyal Fitoussi',
			'desc'    		=> __( 'Enhance WP Job Manager plugin with geolocation features.', 'GJM' ),
			'image'  		=> false,
			'require' 		=> array(
				array( 'title' => 'WP Job Manager plugin', 'basename' => 'wp-job-manager/wp-job-manager.php', 'link' => 'http://wordpress.org/plugins/wp-job-manager/' )
			),
			'license' 		=> true,
			'auto_trigger' 	=> true,
            'min_version'  	=> false,
            'stand_alone'  	=> true,
            'core'         	=> false,
            'gmw_version'  	=> '2.5'					
		);

		return $addons;
	}

	/**
	 * Plugin updater and license key input field
	 * @return [type] [description]
	 */
	public function updater() {

		//if GEO my WP install let it do the udpating
		if ( class_exists( 'GEO_my_WP') && version_compare( GMW_VERSION, '2.6', '>' ) ) {

			// pass addon args to updater
			add_filter( 'gmw_admin_addons_page', array( $this, 'addon_init' ) );
			return;
		}

		//include updater files
		if ( ! class_exists( 'GMW_License' ) ) {
			include( 'updater/geo-my-wp-license-handler.php' );
		}

		//Check for plugin updates
        if ( class_exists( 'GMW_License' ) ) {
            new GMW_License( GJM_FILE, GJM_ITEM_NAME, GJM_LICENSE_NAME, GJM_VERSION, 'Eyal Fitoussi', GMW_REMOTE_SITE_URL, GJM_ITEM_ID  );
        }
    }
	
	/**
	 * register scripts function.
	 *
	 * @access public
	 * @return void
	 */
	public function register_scripts() {
		
		$settings = get_option('gjm_options');
		$language = ! empty( $settings['general_settings']['gjm_language'] ) ? $settings['general_settings']['gjm_language'] : 'en';
		$region  = ! empty( $settings['general_settings']['gjm_region'] ) ? $settings['general_settings']['gjm_region'] : 'us';
		$api_key = ! empty( $settings['general_settings']['gjm_google_api_key'] ) ? $settings['general_settings']['gjm_google_api_key'] : '';

		if ( ! class_exists( 'GEO_my_WP' ) ) {
			
			//register google maps api
            if ( ! wp_script_is( 'google-maps', 'registered' ) ) {
                
                $protocol  = is_ssl() ? 'https' : 'http';

                //Build Google API url. elements can be modified via filters
                $google_url = apply_filters( 'gjm_google_maps_api_url', array( 
                    'protocol'  => is_ssl() ? 'https' : 'http',
                    'url_base'  => '://maps.googleapis.com/maps/api/js?',
                    'url_data'  => http_build_query( apply_filters( 'gjm_google_maps_api_args', array(
	                    	'key'		=> $api_key,
	                       	'libraries' => 'places',
	                        'region'    => $region,
	                        'language'  => $language,
                    ) ), '', '&amp;'),
                ) );
         
                wp_register_script( 'google-maps', implode( '', $google_url ) , array( 'jquery' ), GJM_VERSION, true );      
	    	}
	    	
			if ( ! wp_script_is( 'gmw-marker-clusterer', 'registered' ) ) {
				wp_register_script( 'gmw-marker-clusterer', GJM_URL . '/assets/js/marker-clusterer.min.js', array( 'jquery' ), GJM_VERSION, true );
			}

			if ( !wp_script_is( 'gmw-marker-spiderfier', 'registered' ) ) {
				wp_register_script( 'gmw-marker-spiderfier', GJM_URL . '/assets/js/marker-spiderfier.min.js', array( 'jquery' ), GJM_VERSION, true );
			}
		}
		
		// check if WorkScout theme activeted. This is a work around 
		// to fix a conflict with WorkScout theme
		if ( function_exists( 'workscout_setup' ) ) {

			$reg_script = 'workscout-wp-job-manager-ajax-filters';
			$reg_styles = array( 'workscout-base','workscout-responsive','workscout-font-awesome' );
		
		} elseif ( function_exists( 'listable_setup' ) ) {

			$reg_script = 'listable-scripts';
			$reg_styles = array();

		} else {
		
			$reg_script = 'wp-job-manager-ajax-filters';
			$reg_styles = array();
		}

		if ( is_admin() ) {

			//register autocomplete file	
			wp_register_script( 'gjm-autocomplete', GJM_URL .'/assets/js/gjm.autocomplete.min.js', array( 'jquery' ), GJM_VERSION, true );

		//frontend only
		} else {
			
			//register autocomplete file	
			wp_register_script( 'gjm-autocomplete', GJM_URL .'/assets/js/gjm.autocomplete.min.js', array( 'jquery', $reg_script ), GJM_VERSION, true );
				
			//main js file
			wp_register_script( 'gjm-js', GJM_URL .'/assets/js/gjm.min.js',	array( 'jquery', $reg_script ), GJM_VERSION, true );
			wp_enqueue_script( 'gjm-js' );

			//map style
			wp_register_script( 'gjm-map', GJM_URL .'/assets/js/gjm.map.min.js', array( 'jquery',
					$reg_script ), GJM_VERSION, true );

			//frontendstyles
			if ( class_exists( 'Jobify' ) ) {

				// temporary fix to conflict with Application rating Icons and jobify theme
				// preventing css from loading if in applications page
				if ( empty( $_GET['action'] ) || $_GET['action'] != 'show_applications' ) {
					wp_enqueue_style( 'gjm-frontend', GJM_URL . '/assets/css/gjm.frontend.min.css', array( 'jobify-parent' ), GJM_VERSION );
				}

			} else {
				wp_enqueue_style( 'gjm-frontend', GJM_URL . '/assets/css/gjm.frontend.min.css', $reg_styles , GJM_VERSION );
			}
		}

		// register google maps api
        if ( ! wp_script_is( 'google-maps', 'enqueued' ) ) {
        	wp_enqueue_script( 'google-maps' );
    	}	
	}
}

// abort if GEO Job Manager still activated
function gjm_old_verion_admin_notice() {
	?>
	<div class="error">
		<p><?php _e( "An older version of WP Job Manager Geolocation ( GEO Job Manager ) is activated. Please deactivate GEO Job Manager plugin in order to use WP Job Manager Geolocation.", "GJM" ); ?></p>
	</div>
	<?php       
}

if ( class_exists( 'GEO_Job_Manager' ) ) {
	add_action( 'admin_notices', 'gjm_old_verion_admin_notice' );
	return;
}

// Get GJM Running
new GJM_Init();