<?php
/**
 * Addon: Coupons
 * Addon URI: http://mycred.me/add-ons/coupons/
 * Version: 1.3
 */
if ( ! defined( 'myCRED_VERSION' ) ) exit;

define( 'myCRED_COUPONS',         __FILE__ );
define( 'myCRED_COUPONS_DIR',     myCRED_ADDONS_DIR . 'coupons/' );
define( 'myCRED_COUPONS_VERSION', '1.3' );

require_once myCRED_COUPONS_DIR . 'includes/mycred-coupon-functions.php';
require_once myCRED_COUPONS_DIR . 'includes/mycred-coupon-shortcodes.php';

/**
 * myCRED_Coupons_Module class
 * @since 1.4
 * @version 1.0
 */
if ( ! class_exists( 'myCRED_Coupons_Module' ) ) :
	class myCRED_Coupons_Module extends myCRED_Module {

		public $instances = array();

		/**
		 * Construct
		 */
		function __construct() {

			parent::__construct( 'myCRED_Coupons_Module', array(
				'module_name' => 'coupons',
				'defaults'    => array(
					'log'         => 'Coupon redemption',
					'invalid'     => 'This is not a valid coupon',
					'expired'     => 'This coupon has expired',
					'user_limit'  => 'You have already used this coupon',
					'min'         => 'A minimum of %min% is required to use this coupon',
					'max'         => 'A maximum of %max% is required to use this coupon',
					'success'     => 'Coupon successfully deposited into your account'
				),
				'register'    => false,
				'add_to_core' => true,
				'menu_pos'    => 80
			) );

			add_filter( 'mycred_parse_log_entry_coupon', array( $this, 'parse_log_entry' ), 10, 2 );

		}

		/**
		 * Hook into Init
		 * @since 1.4
		 * @version 1.0
		 */
		public function module_init() {

			$this->register_coupons();

			add_shortcode( 'mycred_load_coupon', 'mycred_render_shortcode_load_coupon' );
			add_shortcode( 'coupon_form', array( $this, 'mycred_coupon_form' ) );

			add_action( 'mycred_add_menu',   array( $this, 'add_to_menu' ), $this->menu_pos );

		}

		/**
		 * Hook into Admin Init
		 * @since 1.4
		 * @version 1.1
		 */
		public function module_admin_init() {

			add_filter( 'post_updated_messages',                    array( $this, 'post_updated_messages' ) );

			add_filter( 'manage_mycred_coupon_posts_columns',       array( $this, 'adjust_column_headers' ) );
			add_action( 'manage_mycred_coupon_posts_custom_column', array( $this, 'adjust_column_content' ), 10, 2 );

			add_filter( 'parent_file',                             array( $this, 'parent_file' ) );
			add_filter( 'submenu_file',                            array( $this, 'subparent_file' ), 10, 2 );

			add_filter( 'enter_title_here',                         array( $this, 'enter_title_here' ) );
			add_filter( 'post_row_actions',                         array( $this, 'adjust_row_actions' ), 10, 2 );
			add_filter( 'bulk_actions-edit-mycred_coupon',          array( $this, 'bulk_actions' ) );
			add_action( 'save_post_mycred_coupon',                  array( $this, 'save_coupon' ), 10, 2 );

			add_action( 'admin_head-post.php',                      array( $this, 'edit_coupons_style' ) );
			add_action( 'admin_head-post-new.php',                  array( $this, 'edit_coupons_style' ) );
			add_action( 'admin_head-edit.php',                      array( $this, 'coupon_style' ) );

		}

		/**
		 * Register Coupons Post Type
		 * @since 1.4
		 * @version 1.0.1
		 */
		protected function register_coupons() {

			$labels = array(
				'name'               => __( 'Coupons', 'mycred' ),
				'singular_name'      => __( 'Coupon', 'mycred' ),
				'add_new'            => __( 'Create New', 'mycred' ),
				'add_new_item'       => __( 'Create New', 'mycred' ),
				'edit_item'          => __( 'Edit Coupon', 'mycred' ),
				'new_item'           => __( 'New Coupon', 'mycred' ),
				'all_items'          => __( 'Coupons', 'mycred' ),
				'view_item'          => '',
				'search_items'       => __( 'Search coupons', 'mycred' ),
				'not_found'          => __( 'No coupons found', 'mycred' ),
				'not_found_in_trash' => __( 'No coupons found in Trash', 'mycred' ), 
				'parent_item_colon'  => '',
				'menu_name'          => __( 'Email Notices', 'mycred' )
			);
			$args = array(
				'labels'               => $labels,
				'supports'             => array( 'title' ),
				'hierarchical'         => false,
				'public'               => false,
				'show_ui'              => true,
				'show_in_menu'         => false,
				'show_in_nav_menus'    => false,
				'show_in_admin_bar'    => false,
				'can_export'           => true,
				'has_archive'          => false,
				'exclude_from_search'  => true,
				'publicly_queryable'   => false,
				'register_meta_box_cb' => array( $this, 'add_metaboxes' )
			);

			register_post_type( 'mycred_coupon', apply_filters( 'mycred_register_coupons', $args ) );

		}

		/**
		 * Adjust Update Messages
		 * @since 1.4
		 * @version 1.0.1
		 */
		public function post_updated_messages( $messages ) {

			$messages['mycred_coupon'] = array(
				0  => '',
				1  => __( 'Coupon updated.', 'mycred' ),
				2  => __( 'Coupon updated.', 'mycred' ),
				3  => __( 'Coupon updated.', 'mycred' ),
				4  => __( 'Coupon updated.', 'mycred' ),
				5  => false,
				6  => __( 'Coupon published.', 'mycred' ),
				7  => __( 'Coupon updated.', 'mycred' ),
				8  => __( 'Coupon updated.', 'mycred' ),
				9  => __( 'Coupon updated.', 'mycred' ),
				10 => __( 'Coupon updated.', 'mycred' ),
			);

			return $messages;

		}

		/**
		 * Add Admin Menu Item
		 * @since 1.7
		 * @version 1.0
		 */
		public function add_to_menu() {

			add_submenu_page(
				'mycred',
				__( 'Coupons', 'mycred' ),
				__( 'Coupons', 'mycred' ),
				$this->core->edit_creds_cap(),
				'edit.php?post_type=mycred_coupon'
			);

		}

		/**
		 * Parent File
		 * @since 1.7
		 * @version 1.0
		 */
		public function parent_file( $parent = '' ) {

			global $pagenow;

			if ( isset( $_GET['post'] ) && get_post_type( $_GET['post'] ) == 'mycred_coupon' && isset( $_GET['action'] ) && $_GET['action'] == 'edit' )
				return 'mycred';

			if ( $pagenow == 'post-new.php' && isset( $_GET['post_type'] ) && $_GET['post_type'] == 'mycred_coupon' )
				return 'mycred';

			return $parent;

		}

		/**
		 * Sub Parent File
		 * @since 1.7
		 * @version 1.0
		 */
		public function subparent_file( $subparent = '', $parent = '' ) {

			global $pagenow;

			if ( ( $pagenow == 'edit.php' || $pagenow == 'post-new.php' ) && isset( $_GET['post_type'] ) && $_GET['post_type'] == 'mycred_coupon' ) {

				return 'edit.php?post_type=mycred_coupon';
			
			}

			elseif ( $pagenow == 'post.php' && isset( $_GET['post'] ) && get_post_type( $_GET['post'] ) == 'mycred_coupon' ) {

				return 'edit.php?post_type=mycred_coupon';

			}

			return $subparent;

		}

		/////////////////////////////////////////////////////////////////////////////////////////
		public function mycred_coupon_form($post)
		{

			//echo "<form action='' method='post' id=>";
			//echo "<input type='hidden' name='action' value='submit-form' />";
    		//echo "<input type='hidden' name='hide' value='$ques' />";
    		global $wpdb;
		    global $current_user;
		    $current_user = get_current_user_id();
		    if($current_user!=0)
		    {
		    $table_usermeta = $wpdb->prefix . 'usermeta';
		    $business_check_query = "SELECT meta_value FROM ".$table_usermeta.
		    " WHERE meta_key='user_category' && user_id='$current_user'";
		    $business_check = $wpdb->get_row($business_check_query,ARRAY_N);

		    if($business_check[0] == 'bus')
		    {
		    	//echo $current_user;

			?>
			
			<script>
			var $=jQuery;
			/////////////////////////////////////
			
            ///////////////////////////////////////
			$(document).ready(function() {
				///////////////////////////

				/*$("#search_affiliate").keyup(function(e){
        e.preventDefault();
        var search_val=$("#search_affiliate").val();
        //if(search_val!='')
        //{
        //alert(search_val); 
        /*$.ajax({
            type:"POST",
            url: "/wp-admin/admin-ajax.php",
            data: {
                action:'auto_search', 
                'affiliate':search_val
            },
            success:function(data){

                //$('#search_affiliate').html(data);
                alert(data);
                $( "#search_affiliate" ).autocomplete({
                source: data
                });

            }
        }); ///
				jQuery.ajax ( 
    			data = {
                action: 'auto_search',
                url: '/wp-admin/admin-ajax.php',
                type: 'POST',
                dataType: 'json',
                'affiliate' : search_val
                

            });
				//alert("test");
            jQuery.post(ajaxurl, data, function(response) {

                //var dataz = jQuery.parseJSON(response);
                //console.log(JSON.parse(response));
                //return;
                var dataz = JSON.parse(response);
                //document.getElementById('friends_list').innerHTML += dataz;
                //alert( dataz );
                //var html='';
                //for(var i = 0; i < dataz.length; i++){
                //	html += "<a class='name_li' href='javascript:void(0);'><li>"+dataz[i]+"</li></a>";
                	//$("#ul").append(html);
 					

                //}
				//$("#ul").html(html);               
                //$("#ul").show();
                //$("#search_affiliate").html(dataz);
                $( "#search_affiliate" ).autocomplete({
                source: dataz,
             	minLength:1
                });
                console.log (dataz); //show json in console


            });
            //alert("test");
        //}
        //else
        //{
        //	$("#ul").html('');
        //}



});
$("body").delegate(".name_li","click",function(){
	//alert("a ja");
	$( "#search_affiliate" ).val($(this).children().html());
	$("#ul").hide();
});*/
				////////////////////////////////////
			
  			$('#form').on('submit', function(e) {
  			e.preventDefault();
  			$('input[type="submit"]').prop('disabled', true);
  			//$('input[type="submit"]').unbind('mouseenter mouseleave');
  			var code = $('#form').find('input[id=title]').val();
  			var value = $('#form').find('input[id=mycred-coupon-value]').val();
  			var type = $('#form').find('input[id=mycred-coupon-type]').val();
  			var expire = $('#form').find('input[id=mycred-coupon-expire]').val();
  			var global = $('#form').find('input[id=mycred-coupon-global]').val();
                        var discount = $('#form').find('input[id=mycred-coupon-discount]').val();
                        var detail = $('#form').find('input[id=mycred-coupon-detail]').val();
  			var user = $('#form').find('input[id=mycred-coupon-user]').val();
  			//var aff = $('#form').find('input[id=search_affiliate]').val();
  			var aff = $('#all_ids').find(":selected").val();
  			if(code=='')
  			{
        		document.getElementById('title').style.borderColor = "red";
        		document.getElementById('error_msg').style.display = "";
        		document.getElementById('error_msg').innerHTML = "The coupon code field is mandatory";
        		$('input[type="submit"]').prop('disabled', false);
        	
        		
    		}
    		else
    		{

    			//alert("check");
    			jQuery.ajax ( 
    			data = {
                action: 'check_duplicate_code',
                url: '/wp-admin/admin-ajax.php',
                type: 'POST',
                dataType: 'text',
                'value' : value,
                'expire' : expire,
                'type' : type,
                'global' : global,
                'user' : user,
                'code' : code,
                'discount' : discount,
                'detail' : detail,
                'affiliate' : aff
                

            });
    		//alert("test");
            jQuery.post(ajaxurl, data, function(response) {

                var dataz = response;
                //document.getElementById('friends_list').innerHTML += dataz;
                //alert( dataz );
                console.log (dataz);

                 //show json in console
                 
                if(dataz=='0')
                {
                	document.getElementById('title').style.borderColor = "red";
                	document.getElementById('error_msg').style.display = "";
        			document.getElementById('error_msg').innerHTML = "This coupon code already exists";
        			$('input[type="submit"]').prop('disabled', false);

        			
        			//alert("lala");
                	
                }
                else
                {
            		alert('Coupon has been successfully submitted.');
            		window.top.location.href = '<?php echo site_url(); ?>'+'/view-all-coupons/';
                	//form.submit();
                }
                
            });
            

            //alert($counter);
            
    		}
    		//console.log(MY.counter);
    		//sreturn false;
  			
  			//alert(aff);
    		///////////////////////////////////////////////
    		//////////////////////////////////////////////
            //alert("test");

  });
});
			</script>
			<form name="form" method="post" id="form" autocomplete="off">
			<!--<input type='hidden' name='action' value='submit-form' />-->
			<h2>Create New Coupon</h2>
			<p id="note">(NOTE : You can modify the below given coupon code)</p>
			<p id="error_msg" style="display:none;"></p>
			<div id="titlewrap">
				<?php
				//$code = uniqid('PC');
				//$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
				$characters = '0123456789';
			    $charactersLength = strlen($characters);
			    $code = 'PC';
			    for ($i = 0; $i < 6; $i++) 
			    {
			        $code .= $characters[rand(0, $charactersLength - 1)];
			    }
				?>
				<!--<label class="" id="title-prompt-text" for="title">Unique Coupon Code</label>-->
					<input type="text" name="post_title" size="30" value="<?php echo htmlspecialchars($code); ?>" id="title" spellcheck="true" autocomplete="off" placeholder="Unique Coupon Code" >
			</div>
			<br>

			<?php
			?>
			<br>
			<h3>Coupon Setup</h3>
			<?php
			global $mycred;

			$value    = get_post_meta( $post->ID, 'value', true );
			if ( empty( $value ) )
				$value = 1;

			$expires  = get_post_meta( $post->ID, 'expires', true );
			$set_type = get_post_meta( $post->ID, 'type', true );

?>
<style type="text/css">
table { width: 100%; }
table th { width: 20%; text-align: right; }
table th label { padding-right: 12px; }
table td { width: 80%; padding-bottom: 6px; }
table td textarea { width: 95%; }
#submitdiv .misc-pub-curtime, #submitdiv #visibility, #submitdiv #misc-publishing-actions { display: none; }
#submitdiv #minor-publishing-actions { padding-bottom: 10px; }
<?php if ( $post->post_status == 'publish' ) : ?>
#submitdiv #minor-publishing-actions { padding: 0 0 0 0; }
<?php endif; ?>
</style>
<input type="hidden" name="mycred-coupon-nonce" value="<?php echo wp_create_nonce( 'update-mycred-coupon' ); ?>" />
<table class="table wide-fat">
	<tbody>
		<tr valign="top">
			<th scope="row"><label for="mycred-coupon-value"><?php _e( 'Value', 'mycred' ); ?></label></th>
			<td>
				<input type="text" name="mycred_coupon[value]" id="mycred-coupon-value" value="<?php echo $mycred->number( $value ); ?>" /><br />
				<span class="description"><?php echo $mycred->template_tags_general( __( 'The amount of %plural% this coupon is worth.', 'mycred' ) ); ?></span>
			</td>
		</tr>
		<tr valign="top">
			<th scope="row"><label for="mycred-coupon-value"><?php _e( 'Point Type', 'mycred' ); ?></label></th>
			<td>
				<?php if ( count( $this->point_types ) > 1 ) : ?>

					<?php mycred_types_select_from_dropdown( 'mycred_coupon[type]', 'mycred-coupon-type', $set_type ); ?><br />
					<span class="description"><?php _e( 'Select the point type that this coupon is applied.', 'mycred' ); ?></span>

				<?php else : ?>

					<?php echo $this->core->plural(); ?>

				<?php endif; ?>
			</td>
		</tr>
		<tr valign="top">
			<th scope="row"><label for="mycred-coupon-value"><?php _e( 'Expire', 'mycred' ); ?></label></th>
			<td>
				<input type="text" name="mycred_coupon[expires]" id="mycred-coupon-expire" value="<?php echo $expires; ?>" placeholder="YYYY-MM-DD" /><br />
				<span class="description"><?php _e( 'Optional date when this coupon expires. Expired coupons will be trashed.', 'mycred' ); ?></span>
                    
			</td>


		</tr>

<tr valign="top">
	<th scope="row">
            <label for="mycred-coupon-discount"><?php _e( 'Offer', 'mycred' ); ?>
            </label>
        </th>
		<td>
                    <input type="text" name="mycred_coupon[discount]" id="mycred-coupon-discount" value="1" required /><br />
                    <span class="description"><?php echo $mycred->template_tags_general( __( 'How much discount this coupon will worth e.g 5%, 10%', 'mycred' ) ); ?></span>
		</td>
</tr>

<tr valign="top">
	<th scope="row">
            <label for="mycred-coupon-detail"><?php _e( 'Offer Detail', 'mycred' ); ?>
            </label>
        </th>
		<td>
                    <input type="text" name="mycred_coupon[detail]" id="mycred-coupon-detail" value="" required /><br />
                    <span class="description"><?php echo $mycred->template_tags_general( __( 'Enter Offer Detail e.g 5% discount valid until Dec 31, 2016', 'mycred' ) ); ?></span>
		</td>
</tr>


	</tbody>
</table>

<?php do_action( 'mycred_coupon_after_setup', $post ); ?>

<?php
?>
			<br>
			<h3>Coupon Limits</h3>
			<?php
			global $mycred;

			$global_max = get_post_meta( $post->ID, 'global', true );
			if ( empty( $global_max ) )
				$global_max = 1;

			$user_max = get_post_meta( $post->ID, 'user', true );
			if ( empty( $user_max ) )
				$user_max = 1;

?>
<table class="table wide-fat">
	<tbody>
		<tr valign="top">
			<th scope="row"><label for="mycred-coupon-global"><?php _e( 'Global Maximum', 'mycred' ); ?></label></th>
			<td>
				<input type="text" name="mycred_coupon[global]" id="mycred-coupon-global" value="<?php echo abs( $global_max ); ?>" /><br />
				<span class="description"><?php _e( 'The maximum number of times this coupon can be used. Note that the coupon will be automatically trashed once this maximum is reached!', 'mycred' ); ?></span>
			</td>
		</tr>
		<tr valign="top">
			<th scope="row"><label for="mycred-coupon-user"><?php _e( 'User Maximum', 'mycred' ); ?></label></th>
			<td>
				<input type="text" name="mycred_coupon[user]" id="mycred-coupon-user" value="<?php echo abs( $user_max ); ?>" /><br />
				<span class="description"><?php _e( 'The maximum number of times this coupon can be used by a user.', 'mycred' ); ?></span>
			</td>
		</tr>
	</tbody>
</table>

<?php do_action( 'mycred_coupon_after_limits', $post ); ?>

<?php
?>
			<br>
			<h2>Affiliate:</h2>
					<div id="titlewrap">
				<!--<label class="" id="title-prompt-text" for="title">Unique Coupon Code</label>
					<input type="text" name="affiliate" size="30" value="" id="search_affiliate" placeholder="Affiliate" spellcheck="true" >-->
					
					<select id='all_ids' name='all_ids' onchange='load_bookmarks(this)'>
		            <option value='default' selected='selected'>Select an affiliate</option>
		            <?php
		            //echo $current_user;
		            /*$table_users = $wpdb->prefix . 'users';
		            $query="SELECT user_login FROM ".$table_users;
		            $result = $wpdb->get_results($query,ARRAY_N);
				    foreach ($result as $row)
				    {
				        if($row[0] == $_POST['all_ids']){
				          $isSelected = ' selected="selected"'; // if the option submited in form is as same as this row we add the selected tag
				     } else {
				          $isSelected = ''; // else we remove any tag
				     }
				        echo "<option value='".$row[0]."'".$isSelected.">".$row[0]."</option>";
				    //    echo "<p>".$row[0]."</p>";
				    }*/
				    global $wpdb;
		    		global $current_user;
		    		//$current_user = get_current_user_id();
		    		$current_user = apply_filters( 'determine_current_user', false );
					//wp_set_current_user( $current_user );
				    $table_users = $wpdb->prefix . 'users';
				    $table_mlm = $wpdb->prefix . 'uap_mlm_relations';
				    $table_affiliates = $wpdb->prefix . 'uap_affiliates';
				    //global $current_user;
				    //$current_user = get_current_user_id();

				    //echo $current_user;
				    $p_aff_id=$wpdb->get_row("SELECT id FROM " .$table_affiliates.
				     " WHERE uid = '$current_user' ",ARRAY_N);
				    $c_aff_id=$wpdb->get_results("SELECT affiliate_id FROM " .$table_mlm.
				     " WHERE parent_affiliate_id = '$p_aff_id[0]' ",ARRAY_N);
				    //$names=array();
				    foreach($c_aff_id as $child)
				    {
				        $c_uid=$wpdb->get_row("SELECT uid FROM " .$table_affiliates.
				        " WHERE id = '$child[0]' ",ARRAY_N);
				        
				        //echo $name;
				        $affiliate=$wpdb->get_row("SELECT display_name, user_nicename FROM " .$table_users. " WHERE ID = '$c_uid[0]'",ARRAY_N);
				        //$affiliate=$wpdb->get_results("SELECT display_name FROM " .$table_users. " WHERE display_name //LIKE '$name%'");
				        //array_push($names,$affiliate[0]);
				        if($affiliate[0] == $_POST['all_ids']){
				          $isSelected = ' selected="selected"'; // if the option submited in form is as same as this row we add the selected tag
				     } else {
				          $isSelected = ''; // else we remove any tag
				     }
				        echo "<option value='".$affiliate[1]."'".$isSelected.">".$affiliate[0]."</option>";    
				    }
				    
					?>
					</select>
				    <br>		
			</div>
				<!--<label class="" id="title-prompt-text" for="title">Unique Coupon Code</label>-->
					
					<ul id="ul" style="display:none; list-style:none;">
					</ul>
			<br>
			<div id="publishing-action">
				<span class="spinner"></span>
				<input name="original_publish" type="hidden" id="original_publish" value="Publish">
				<input type="submit" name="publish" id="publish" class="button button-primary button-large" value="Publish"></div>
			</form>
			
				
				<?php do_action( 'mycred_after_publish', $post ); ?>
			<?php
			}
			else
			{
				?>
				<h2>You are not authorized to create a coupon!</h2>
				<?php
			}
		}
		else
		{
			?>
			<h3>Please Log in!</h3>
			<?php
		}
			



		}
		////////////////////////////////////////////////////////////////////////////////////////
		

		/**
		 * Adjust Enter Title Here
		 * @since 1.4
		 * @version 1.0
		 */
		public function enter_title_here( $title ) {

			global $post_type;

			if ( $post_type == 'mycred_coupon' )
				return __( 'Coupon Code', 'mycred' );

			return $title;

		}

		/**
		 * Adjust Column Header
		 * @since 1.4
		 * @version 1.1
		 */
		public function adjust_column_headers( $defaults ) {

			$columns            = array();
			$columns['cb']      = $defaults['cb'];

			// Add / Adjust
			$columns['title']   = __( 'Coupon Code', 'mycred' );
			$columns['value']   = __( 'Value', 'mycred' );
			$columns['usage']   = __( 'Used', 'mycred' );
			$columns['limits']  = __( 'Limits', 'mycred' );
			$columns['expires'] = __( 'Expires', 'mycred' );

			if ( count( $this->point_types ) > 1 )
				$columns['ctype'] = __( 'Point Type', 'mycred' );

			return $columns;

		}

		/**
		 * Adjust Column Body
		 * @since 1.4
		 * @version 1.1.2
		 */
		public function adjust_column_content( $column_name, $post_id ) {

			global $mycred;

			switch ( $column_name ) {

				case 'value' :

					$value = mycred_get_coupon_value( $post_id );
					if ( empty( $value ) ) $value = 0;
					
					echo $mycred->format_creds( $value );

				break;

				case 'usage' :

					$count = mycred_get_global_coupon_count( $post_id );

					if ( $count == 0 )
						echo '-';

					else {

						$set_type = get_post_meta( $post_id, 'type', true );
						$page     = MYCRED_SLUG;

						if ( $set_type != MYCRED_DEFAULT_TYPE_KEY && array_key_exists( $set_type, $this->point_types ) )
							$page .= '_' . $set_type;

						$url = add_query_arg( array( 'page' => $page, 'ref' => 'coupon', 'data' => get_the_title( $post_id ) ), admin_url( 'admin.php' ) );
						echo '<a href="' . esc_url( $url ) . '">' . sprintf( _n( '1 time', '%d times', $count, 'mycred' ), $count ) . '</a>';

					}

				break;

				case 'limits' :

					$total = mycred_get_coupon_global_max( $post_id );
					$user  = mycred_get_coupon_user_max( $post_id );

					printf( '%1$s: %2$d<br />%3$s: %4$d', __( 'Total', 'mycred' ), $total, __( 'Per User', 'mycred' ), $user );

				break;

				case 'expires' :

					$expires = mycred_get_coupon_expire_date( $post_id, true );

					if ( empty( $expires ) || $expires === 0 )
						_e( 'Never', 'mycred' );

					else {

						if ( $expires < current_time( 'timestamp' ) ) {
							wp_trash_post( $post_id );
							echo '<span style="color:red;">' . __( 'Expired', 'mycred' ) . '</span>';
						}

						else {
							echo sprintf( __( 'In %s time', 'mycred' ), human_time_diff( $expires ) ) . '<br /><small class="description">' . date( get_option( 'date_format' ), $expires ) . '</small>';
						}

					}

				break;

				case 'ctype' :

					$type = get_post_meta( $post_id, 'type', true );

					if ( isset( $this->point_types[ $type ] ) )
						echo $this->point_types[ $type ];

					else
						echo '-';

				break;

			}
		}

		/**
		 * Adjust Bulk Actions
		 * @since 1.7
		 * @version 1.0
		 */
		public function bulk_actions( $actions ) {

			unset( $actions['edit'] );
			return $actions;

		}

		/**
		 * Adjust Row Actions
		 * @since 1.4
		 * @version 1.0
		 */
		public function adjust_row_actions( $actions, $post ) {

			if ( $post->post_type == 'mycred_coupon' ) {
				unset( $actions['inline hide-if-no-js'] );
				unset( $actions['view'] );
			}

			return $actions;

		}

		/**
		 * Edit Coupon Style
		 * @since 1.7
		 * @version 1.0
		 */
		public function edit_coupons_style() {

			global $post_type;

			if ( $post_type !== 'mycred_coupon' ) return;

			wp_enqueue_style( 'mycred-bootstrap-grid' );
			wp_enqueue_style( 'mycred-forms' );

			add_filter( 'postbox_classes_mycred_coupon_mycred-coupon-setup',        array( $this, 'metabox_classes' ) );
			add_filter( 'postbox_classes_mycred_coupon_mycred-coupon-limits',       array( $this, 'metabox_classes' ) );
			add_filter( 'postbox_classes_mycred_coupon_mycred-coupon-requirements', array( $this, 'metabox_classes' ) );
			add_filter( 'postbox_classes_mycred_coupon_mycred-coupon-usage',        array( $this, 'metabox_classes' ) );

?>
<style type="text/css">
#misc-publishing-actions #visibility { display: none; }
</style>
<?php

		}

		/**
		 * Coupon Style
		 * @since 1.7
		 * @version 1.0
		 */
		public function coupon_style() {

			global $post_type;

			if ( $post_type !== 'mycred_coupon' ) return;

		}

		/**
		 * Add Meta Boxes
		 * @since 1.4
		 * @version 1.1
		 */
		public function add_metaboxes( $post ) {

			add_meta_box(
				'mycred-coupon-setup',
				__( 'Coupon Setup', 'mycred' ),
				array( $this, 'metabox_coupon_setup' ),
				'mycred_coupon',
				'normal',
				'core'
			);

			add_meta_box(
				'mycred-coupon-limits',
				__( 'Coupon Limits', 'mycred' ),
				array( $this, 'metabox_coupon_limits' ),
				'mycred_coupon',
				'normal',
				'core'
			);

			add_meta_box(
				'mycred-coupon-requirements',
				__( 'Coupon Requirements', 'mycred' ),
				array( $this, 'mycred_coupon_requirements' ),
				'mycred_coupon',
				'side',
				'core'
			);

			if ( $post->post_status == 'publish' )
				add_meta_box(
					'mycred-coupon-usage',
					__( 'Coupon Usage', 'mycred' ),
					array( $this, 'mycred_coupon_usage' ),
					'mycred_coupon',
					'side',
					'core'
				);

		}

		/**
		 * Metabox: Coupon Setup
		 * @since 1.4
		 * @version 1.2
		 */
		public function metabox_coupon_setup( $post ) {

			$coupon = mycred_get_coupon( $post->ID );

			if ( $coupon->point_type != $this->core->cred_id )
				$mycred = mycred( $coupon->point_type );
			else
				$mycred = $this->core;

?>
<div class="form">
	<div class="row">
		<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
			<div class="form-group">
				<label for=""><?php _e( 'Value', 'mycred' ); ?></label>
				<input type="text" name="mycred_coupon[value]" class="form-control" id="mycred-coupon-value" value="<?php echo $mycred->number( $coupon->value ); ?>" />
				<span class="description"><?php echo $mycred->template_tags_general( __( 'The amount of %plural% this coupon is worth.', 'mycred' ) ); ?></span>
			</div>
		</div>
		<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
			<div class="form-group">
				<label for=""><?php _e( 'Point Type', 'mycred' ); ?></label>
				<?php if ( count( $this->point_types ) > 1 ) : ?>

					<?php mycred_types_select_from_dropdown( 'mycred_coupon[type]', 'mycred-coupon-type', $coupon->point_type, false, ' class="form-control"' ); ?><br />
					<span class="description"><?php _e( 'Select the point type that this coupon is applied.', 'mycred' ); ?></span>

				<?php else : ?>

					<p class="form-control-static"><?php echo $mycred->plural(); ?></p>
					<input type="hidden" name="mycred_coupon[type]" value="mycred_default" />

				<?php endif; ?>
			</div>
		</div>
		<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
			<div class="form-group">
				<label for=""><?php _e( 'Expire', 'mycred' ); ?></label>
				<input type="text" name="mycred_coupon[expires]" class="form-control" id="mycred-coupon-expire" value="<?php echo $coupon->expires; ?>" placeholder="YYYY-MM-DD" />
				<span class="description"><?php _e( 'Optional date when this coupon expires. Expired coupons will be trashed.', 'mycred' ); ?></span>
			</div>
		</div>
	</div>
</div>
<?php

		}

		/**
		 * Metabox: Coupon Limits
		 * @since 1.4
		 * @version 1.1
		 */
		public function metabox_coupon_limits( $post ) {

			$coupon = mycred_get_coupon( $post->ID );

			if ( $coupon->point_type != $this->core->cred_id )
				$mycred = mycred( $coupon->point_type );
			else
				$mycred = $this->core;

?>
<div class="form">
	<div class="row">
		<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
			<div class="form-group">
				<label for="mycred-coupon-global"><?php _e( 'Global Maximum', 'mycred' ); ?></label>
				<input type="text" name="mycred_coupon[global]" class="form-control" id="mycred-coupon-global" value="<?php echo absint( $coupon->max_global ); ?>" />
				<span class="description"><?php _e( 'The maximum number of times this coupon can be used.', 'mycred' ); ?></span>
			</div>
		</div>
		<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
			<div class="form-group">
				<label for="mycred-coupon-user"><?php _e( 'User Maximum', 'mycred' ); ?></label>
				<input type="text" name="mycred_coupon[user]" class="form-control" id="mycred-coupon-user" value="<?php echo absint( $coupon->max_user ); ?>" />
				<span class="description"><?php _e( 'The maximum number of times this coupon can be used by a user.', 'mycred' ); ?></span>
			</div>
		</div>
	</div>
</div>
<?php

		}

		/**
		 * Metabox: Coupon Requirements
		 * @since 1.4
		 * @version 1.1
		 */
		public function mycred_coupon_requirements( $post ) {

			$coupon = mycred_get_coupon( $post->ID );

			if ( $coupon->point_type != $this->core->cred_id )
				$mycred = mycred( $coupon->point_type );
			else
				$mycred = $this->core;

			$min_balance_type = mycred_types_select_from_dropdown( 'mycred_coupon[min_balance_type]', 'mycred-coupon-min_balance_type', $coupon->requires_min_type, true, ' style="vertical-align: top;"' );
			$max_balance_type = mycred_types_select_from_dropdown( 'mycred_coupon[max_balance_type]', 'mycred-coupon-max_balance_type', $coupon->requires_max_type, true, ' style="vertical-align: top;"' );

?>
<div class="form">
	<div class="row">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<div class="form-group">
				<label for="mycred-coupon-min_balance"><?php _e( 'Minimum Balance', 'mycred' ); ?></label>
				<div>
					<input type="text" name="mycred_coupon[min_balance]" <?php if ( count( $this->point_types ) > 1 ) echo 'size="8"'; else echo ' style="width: 99%;"'; ?> id="mycred-coupon-min_balance" value="<?php echo $mycred->number( $coupon->requires_min ); ?>" />
					<?php echo $min_balance_type; ?>
				</div>
				<span class="description"><?php _e( 'Optional minimum balance a user must have in order to use this coupon. Use zero to disable.', 'mycred' ); ?></span>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<div class="form-group">
				<label for="mycred-coupon-max_balance"><?php _e( 'Maximum Balance', 'mycred' ); ?></label>
				<div>
					<input type="text" name="mycred_coupon[max_balance]" <?php if ( count( $this->point_types ) > 1 ) echo 'size="8"'; else echo ' style="width: 99%;"'; ?> id="mycred-coupon-max_balance" value="<?php echo $mycred->number( $coupon->requires_max ); ?>" />
					<?php echo $max_balance_type; ?>
				</div>
				<span class="description"><?php _e( 'Optional maximum balance a user can have in order to use this coupon. Use zero to disable.', 'mycred' ); ?></span>
			</div>
		</div>
	</div>
</div>
<?php

		}

		/**
		 * Metabox: Coupon Usage
		 * @since 1.6
		 * @version 1.0
		 */
		public function mycred_coupon_usage( $post ) {

			$count = mycred_get_global_coupon_count( $post->ID );
			if ( empty( $count ) )
				echo '-';

			else {

				$set_type = get_post_meta( $post->ID, 'type', true );
				$page     = MYCRED_SLUG;

				if ( $set_type != MYCRED_DEFAULT_TYPE_KEY && array_key_exists( $set_type, $this->point_types ) )
					$page .= '_' . $set_type;

				$url = add_query_arg( array( 'page' => $page, 'ref' => 'coupon', 'data' => $post->post_title ), admin_url( 'admin.php' ) );
				echo '<a href="' . esc_url( $url ) . '">' . sprintf( __( '1 time', '%d times', $count, 'mycred' ), $count ) . '</a>';

			}

		}

		/**
		 * Save Coupon
		 * @since 1.4
		 * @version 1.0
		 */
		public function save_coupon( $post_id, $post = NULL ) {

			if ( $post === NULL || ! current_user_can( $this->core->edit_creds_cap() ) || ! isset( $_POST['mycred_coupon'] ) ) return $post_id;

			foreach ( $_POST['mycred_coupon'] as $meta_key => $meta_value ) {

				$new_value = sanitize_text_field( $meta_value );
				$old_value = get_post_meta( $post_id, $meta_key, true );
				if ( $new_value != $old_value )
					update_post_meta( $post_id, $meta_key, $new_value );

			}

		}

		/**
		 * Add to General Settings
		 * @since 1.4
		 * @version 1.0
		 */
		public function after_general_settings( $mycred = NULL ) {

			if ( ! isset( $this->coupons ) )
				$prefs = $this->default_prefs;
			else
				$prefs = mycred_apply_defaults( $this->default_prefs, $this->coupons );

?>
<h4><span class="dashicons dashicons-admin-plugins static"></span><?php _e( 'Coupons', 'mycred' ); ?></h4>
<div class="body" style="display:none;">
	<label class="subheader" for="<?php echo $this->field_id( 'log' ); ?>"><?php _e( 'Log Template', 'mycred' ); ?></label>
	<ol id="myCRED-coupon-log">
		<li>
			<div class="h2"><input type="text" name="<?php echo $this->field_name( 'log' ); ?>" id="<?php echo $this->field_id( 'log' ); ?>" value="<?php echo $prefs['log']; ?>" class="long" /></div>
			<span class="description"><?php _e( 'Log entry for successful coupon redemption. Use %coupon% to show the coupon code.', 'mycred' ); ?></span>
		</li>
	</ol>
	<label class="subheader" for="<?php echo $this->field_id( 'invalid' ); ?>"><?php _e( 'Invalid Coupon Message', 'mycred' ); ?></label>
	<ol id="myCRED-coupon-log">
		<li>
			<div class="h2"><input type="text" name="<?php echo $this->field_name( 'invalid' ); ?>" id="<?php echo $this->field_id( 'invalid' ); ?>" value="<?php echo $prefs['invalid']; ?>" class="long" /></div>
			<span class="description"><?php _e( 'Message to show when users try to use a coupon that does not exists.', 'mycred' ); ?></span>
		</li>
	</ol>
	<label class="subheader" for="<?php echo $this->field_id( 'expired' ); ?>"><?php _e( 'Expired Coupon Message', 'mycred' ); ?></label>
	<ol id="myCRED-coupon-log">
		<li>
			<div class="h2"><input type="text" name="<?php echo $this->field_name( 'expired' ); ?>" id="<?php echo $this->field_id( 'expired' ); ?>" value="<?php echo $prefs['expired']; ?>" class="long" /></div>
			<span class="description"><?php _e( 'Message to show when users try to use that has expired.', 'mycred' ); ?></span>
		</li>
	</ol>
	<label class="subheader" for="<?php echo $this->field_id( 'user_limit' ); ?>"><?php _e( 'User Limit Message', 'mycred' ); ?></label>
	<ol id="myCRED-coupon-log">
		<li>
			<div class="h2"><input type="text" name="<?php echo $this->field_name( 'user_limit' ); ?>" id="<?php echo $this->field_id( 'user_limit' ); ?>" value="<?php echo $prefs['user_limit']; ?>" class="long" /></div>
			<span class="description"><?php _e( 'Message to show when the user limit has been reached for the coupon.', 'mycred' ); ?></span>
		</li>
	</ol>
	<label class="subheader" for="<?php echo $this->field_id( 'min' ); ?>"><?php _e( 'Minimum Balance Message', 'mycred' ); ?></label>
	<ol id="myCRED-coupon-log">
		<li>
			<div class="h2"><input type="text" name="<?php echo $this->field_name( 'min' ); ?>" id="<?php echo $this->field_id( 'min' ); ?>" value="<?php echo $prefs['min']; ?>" class="long" /></div>
			<span class="description"><?php _e( 'Message to show when a user does not meet the minimum balance requirement. (if used)', 'mycred' ); ?></span>
		</li>
	</ol>
	<label class="subheader" for="<?php echo $this->field_id( 'max' ); ?>"><?php _e( 'Maximum Balance Message', 'mycred' ); ?></label>
	<ol id="myCRED-coupon-log">
		<li>
			<div class="h2"><input type="text" name="<?php echo $this->field_name( 'max' ); ?>" id="<?php echo $this->field_id( 'max' ); ?>" value="<?php echo $prefs['max']; ?>" class="long" /></div>
			<span class="description"><?php _e( 'Message to show when a user does not meet the maximum balance requirement. (if used)', 'mycred' ); ?></span>
		</li>
	</ol>
	<label class="subheader" for="<?php echo $this->field_id( 'success' ); ?>"><?php _e( 'Success Message', 'mycred' ); ?></label>
	<ol id="myCRED-coupon-log">
		<li>
			<div class="h2"><input type="text" name="<?php echo $this->field_name( 'success' ); ?>" id="<?php echo $this->field_id( 'success' ); ?>" value="<?php echo $prefs['success']; ?>" class="long" /></div>
			<span class="description"><?php _e( 'Message to show when a coupon was successfully deposited to a users account.', 'mycred' ); ?></span>
		</li>
	</ol>
</div>
<?php

		}

		/**
		 * Save Settings
		 * @since 1.4
		 * @version 1.0
		 */
		public function sanitize_extra_settings( $new_data, $data, $core ) {

			$new_data['coupons']['log']        = sanitize_text_field( $data['coupons']['log'] );
			
			$new_data['coupons']['invalid']    = sanitize_text_field( $data['coupons']['invalid'] );
			$new_data['coupons']['expired']    = sanitize_text_field( $data['coupons']['expired'] );
			$new_data['coupons']['user_limit'] = sanitize_text_field( $data['coupons']['user_limit'] );
			$new_data['coupons']['min']        = sanitize_text_field( $data['coupons']['min'] );
			$new_data['coupons']['max']        = sanitize_text_field( $data['coupons']['max'] );
			$new_data['coupons']['success']    = sanitize_text_field( $data['coupons']['success'] );

			return $new_data;

		}

		/**
		 * Parse Log Entries
		 * @since 1.4
		 * @version 1.0
		 */
		public function parse_log_entry( $content, $log_entry ) {

			return str_replace( '%coupon%', $log_entry->data, $content );

		}

	}
endif;

/**
 * Load Coupons Module
 * @since 1.7
 * @version 1.0
 */
if ( ! function_exists( 'mycred_load_coupons_addon' ) ) :
	function mycred_load_coupons_addon( $modules, $point_types ) {

		$modules['solo']['coupons'] = new myCRED_Coupons_Module();
		$modules['solo']['coupons']->load();

		return $modules;

	}
endif;
add_filter( 'mycred_load_modules', 'mycred_load_coupons_addon', 50, 2 );

?>