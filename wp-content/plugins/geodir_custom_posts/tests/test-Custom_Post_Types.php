<?php
class Custom_Post_Type_Tests extends WP_UnitTestCase
{
    public function setUp()
    {
        parent::setUp();
        wp_set_current_user(1);
    }

    public function testAdminOptionForms() {

//        ob_start();
//        geodir_get_admin_cp_form('geodir_manage_custom_posts');
//        $output = ob_get_contents();
//        ob_end_clean();
//        $this->assertContains('Manage Custom Post Types', $output);


//        $_REQUEST['action'] = 'cp_addedit';
//        $_REQUEST['posttype'] = 'gd_job';
//        ob_start();
//        geodir_get_admin_cp_form('geodir_manage_custom_posts');
//        $output = ob_get_contents();
//        ob_end_clean();
//        $this->assertContains('Order in post type list', $output);

    }

    public function testCptShortcodes() {

    }

    public function testCptWidget() {

        register_geodir_cpt_widgets();

        ob_start();
        the_widget( 'geodir_cpt_listings' );
        $output = ob_get_contents();
        ob_end_clean();
        $this->assertContains('geodir_cpt_listings', $output);

        ob_start();
        $this->the_widget_form( 'geodir_cpt_listings' );
        $output = ob_get_contents();
        ob_end_clean();
        $this->assertContains('Hide CPT name', $output);

        $new_instance = array(
            'title' => 'Hello',
            'cpt_exclude' => array(),
            'cpt_img_width' => 90,
            'cpt_img_height' => 90,
            'cpt_hide_name' => false
        );
        $output = $this->the_widget_form_update( 'geodir_cpt_listings', $new_instance );
        $this->assertContains('Hello', $output['title']);

    }

    public function testCptForms() {

        ob_start();
        $_REQUEST['action'] = 'cp_addedit';
        geodir_get_admin_cp_form('geodir_manage_custom_posts');
        $output = ob_get_contents();
        ob_end_clean();
        $this->assertContains('<h3>Post Type</h3>', $output);

    }

    public function testArchiveLink() {
        $link = get_post_type_archive_link('gd_resume');
        //var_dump($link);
    }

    public function testSettingsArray() {
        $general_options = array(

            array('name' => __('General', 'geodirectory'), 'type' => 'title', 'desc' => '', 'id' => 'general_options'),

            array('name' => __('General Options', 'geodirectory'), 'type' => 'sectionstart', 'id' => 'general_options'),

            array(
                'name' => __('Sender name', 'geodirectory'),
                'desc' => __('(Name that will be shown as email sender when users receive emails from this site)', 'geodirectory'),
                'id' => 'site_email_name',
                'type' => 'text',
                'css' => 'min-width:300px;',
                'std' => get_bloginfo('name') // Default value for the page title - changed in settings
            ),
            array('type' => 'sectionend', 'id' => 'general_options'),
        );
        $array = geodir_cpt_tab_general_settings($general_options);

        ob_start();
        var_dump($array);
        $output = ob_get_contents();
        ob_end_clean();
        $this->assertContains('Select CPT to disable physical location', $output);

    }

    public function texstJobTemplates() {

        global $current_user;

        $user_id = $current_user->ID;

        $this->setPermalinkStructure();

        $args = array(
            'listing_type' => 'gd_job',
            'post_title' => 'Test Listing Title',
            'post_desc' => 'Test Desc',
            'post_tags' => 'test1,test2',
            'post_address' => 'New York City Hall',
            'post_zip' => '10007',
            'post_mapview' => 'ROADMAP',
            'post_mapzoom' => '10',
            'geodir_timing' => '10.00 am to 6 pm every day',
            'geodir_contact' => '1234567890',
            'geodir_email' => 'test@test.com',
            'geodir_website' => 'http://test.com',
            'geodir_twitter' => 'http://twitter.com/test',
            'geodir_facebook' => 'http://facebook.com/test',
            'geodir_special_offers' => 'Test offer'
        );
        $post_id = geodir_save_listing($args, true);

        $this->assertTrue(is_int($post_id));


        ob_start();
        $this->go_to( home_url('/?post_type=gd_job') );
        $this->load_template();
        $output = ob_get_contents();
        ob_end_clean();
        $this->assertContains('All Jobs', $output);

        $query_args = array(
            'post_status' => 'publish',
            'post_type' => 'gd_job',
            'posts_per_page' => 1,
        );

        $all_posts = new WP_Query( $query_args );
        $post_id = null;
        while ( $all_posts->have_posts() ) : $all_posts->the_post();
            $post_id = get_the_ID();
        endwhile;

        $this->assertTrue(is_int($post_id));

        global $preview;
        $preview = false;
        ob_start();
        $this->go_to( get_permalink($post_id) );
        $this->load_template();
        $output = ob_get_contents();
        ob_end_clean();
        $this->assertContains('post_profileTab', $output);

        ob_start();
        $_REQUEST['geodir_search'] = "1";
        $_REQUEST['stype'] = 'gd_job';
        $this->go_to( home_url('/?geodir_search=1&stype=gd_job&s=test&snear=&sgeo_lat=&sgeo_lon=') );
        $this->load_template();
        $output = ob_get_contents();
        ob_end_clean();
        $this->assertContains('Search Results for', $output);

    }

    public function texstResumeTemplates() {

        global $current_user;

        $user_id = $current_user->ID;

        $this->setPermalinkStructure();


        ob_start();
        $this->go_to( home_url('/?post_type=gd_resume') );
        $this->load_template();
        $output = ob_get_contents();
        ob_end_clean();
        $this->assertContains('All Resumes', $output);

        $query_args = array(
            'post_status' => 'publish',
            'post_type' => 'gd_resume',
            'posts_per_page' => 1,
        );

        $all_posts = new WP_Query( $query_args );
        $post_id = null;
        while ( $all_posts->have_posts() ) : $all_posts->the_post();
            $post_id = get_the_ID();
        endwhile;

        $this->assertTrue(is_int($post_id));

        global $preview;
        $preview = false;
        ob_start();
        $this->go_to( get_permalink($post_id) );
        $this->load_template();
        $output = ob_get_contents();
        ob_end_clean();
        $this->assertContains('post_profileTab', $output);

        ob_start();
        $_REQUEST['geodir_search'] = "1";
        $_REQUEST['stype'] = 'gd_resume';
        $this->go_to( home_url('/?geodir_search=1&stype=gd_resume&s=test&snear=&sgeo_lat=&sgeo_lon=') );
        $this->load_template();
        $output = ob_get_contents();
        ob_end_clean();
        $this->assertContains('Search Results for', $output);

    }

    private function load_template() {
        do_action( 'template_redirect' );
        $template = false;
        if	 ( is_404()			&& $template = get_404_template()			) :
        elseif ( is_search()		 && $template = get_search_template()		 ) :
        elseif ( is_front_page()	 && $template = get_front_page_template()	 ) :
        elseif ( is_home()		   && $template = get_home_template()		   ) :
        elseif ( is_post_type_archive() && $template = get_post_type_archive_template() ) :
        elseif ( is_tax()			&& $template = get_taxonomy_template()	   ) :
        elseif ( is_attachment()	 && $template = get_attachment_template()	 ) :
            remove_filter('the_content', 'prepend_attachment');
        elseif ( is_single()		 && $template = get_single_template()		 ) :
        elseif ( is_page()		   && $template = get_page_template()		   ) :
        elseif ( is_category()	   && $template = get_category_template()	   ) :
        elseif ( is_tag()			&& $template = get_tag_template()			) :
        elseif ( is_author()		 && $template = get_author_template()		 ) :
        elseif ( is_date()		   && $template = get_date_template()		   ) :
        elseif ( is_archive()		&& $template = get_archive_template()		) :
        elseif ( is_paged()		  && $template = get_paged_template()		  ) :
        else :
            $template = get_index_template();
        endif;
        /**
         * Filter the path of the current template before including it.
         *
         * @since 3.0.0
         *
         * @param string $template The path of the template to include.
         */
        if ( $template = apply_filters( 'template_include', $template ) ) {
            $template_contents = file_get_contents( $template );
            $included_header = $included_footer = false;
            if ( false !== stripos( $template_contents, 'get_header();' ) ) {
                do_action( 'get_header', null );
                locate_template( 'header.php', true, false );
                $included_header = true;
            }
            include( $template );
            if ( false !== stripos( $template_contents, 'get_footer();' ) ) {
                do_action( 'get_footer', null );
                locate_template( 'footer.php', true, false );
                $included_footer = true;
            }
            if ( $included_header && $included_footer ) {
                global $wp_scripts;
                $wp_scripts->done = array();
            }
        }
        return;
    }

    public static function setPermalinkStructure( $struc = '/%postname%/' ) {
        global $wp_rewrite;
        $wp_rewrite->set_permalink_structure( $struc );
        $wp_rewrite->flush_rules();
        update_option( 'permalink_structure', $struc );
        flush_rewrite_rules( true );
    }


    function the_widget_form( $widget, $instance = array() ) {
        global $wp_widget_factory;

        $widget_obj = $wp_widget_factory->widgets[$widget];
        if ( ! ( $widget_obj instanceof WP_Widget ) ) {
            return;
        }

        $widget_obj->_set(-1);
        $widget_obj->form($instance);
    }

    function the_widget_form_update( $widget, $new_instance = array(), $old_instance = array() ) {
        global $wp_widget_factory;

        $widget_obj = $wp_widget_factory->widgets[$widget];
        if ( ! ( $widget_obj instanceof WP_Widget ) ) {
            return;
        }

        $widget_obj->_set(-1);
        return $widget_obj->update($new_instance, $old_instance);
    }

    public function tearDown()
    {
        parent::tearDown();
    }
}
?>