<?php
class LocationlessCPT extends WP_UnitTestCase
{
    public function setUp()
    {
        parent::setUp();
    }

    public function testLocationlessCPT()
    {
        $this->assertFalse(post_type_exists( 'gd_resume' ));

        $_REQUEST = array(
            'page' => 'geodirectory' ,
            'tab' => 'geodir_manage_custom_posts',
            'action' => 'cp_addedit',
            'posttype' => '',
            'active_tab' => '',
            'geodir_custom_post_type' => 'resume',
            'geodir_listing_slug' => 'resumes',
            'geodir_listing_order' => '12',
            'geodir_cpt_img_remove' => '0',
            'geodir_categories' => 'geodir_categories',
            'geodir_tags' => 'geodir_tags',
            'geodir_name' => 'Resumes',
            'geodir_singular_name' => 'Resume',
            'geodir_add_new' => 'Add New',
            'geodir_add_new_item' => 'Add New Resume',
            'geodir_edit_item' => 'Edit Resume',
            'geodir_new_item' => 'New Resume',
            'geodir_view_item' => 'View Resume',
            'geodir_search_item' => 'Search Resumes',
            'geodir_not_found' => 'No Resume Found',
            'geodir_not_found_trash' => 'No Resume Found In Trash',
            'geodir_label_post_profile' => 'Details',
            'geodir_label_post_info' => '',
            'geodir_label_post_images' => '',
            'geodir_label_post_map' => '',
            'geodir_label_reviews' => '',
            'geodir_label_related_listing' => '',
            'geodir_support' => array('title', 'editor', 'author', 'thumbnail', 'excerpt', 'custom-fields', 'comments'),
            'geodir_description' => '',
            'geodir_menu_icon' => '',
            'geodir_can_export' => 'true',
            'geodir_cp_meta_keyword' => '',
            'geodir_cp_meta_description' => '',
            'geodir_save_post_type' => 'Save changes',
            'linkable_to' => 'gd_place',
            'linkable_from' => '',
            'subtab' => '',
        );

        add_filter('wp_redirect', '__return_false');
        geodir_cp_from_submit_handler();
        remove_filter('wp_redirect', '__return_false');

        $option = get_option('geodir_cpt_disable_location');

        if(is_array($option))
            $option[] = 'gd_resume';
        else
            $option = array('gd_resume');

        update_option('geodir_cpt_disable_location', $option);
        $_REQUEST['geodir_cpt_disable_location'] = array('gd_resume');
        geodir_cpt_submit_general_settings();

        $result = geodir_cpt_no_location('gd_resume');
        $this->assertTrue($result);

        $this->assertTrue(post_type_exists( 'gd_resume' ));
    }

    public function tearDown()
    {
        parent::tearDown();
    }
}
?>