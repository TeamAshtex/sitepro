<?php
class CptAjaxTests extends WP_Ajax_UnitTestCase
{
    public function setUp()
    {
        parent::setUp();
        wp_set_current_user(1);
    }

    public function testCptAjax() {

        $_REQUEST = array(
            'page' => 'geodirectory' ,
            'tab' => 'geodir_manage_custom_posts',
            'action' => 'cp_addedit',
            'posttype' => '',
            'active_tab' => '',
            'geodir_custom_post_type' => 'job',
            'geodir_listing_slug' => 'jobs',
            'geodir_listing_order' => '12',
            'geodir_cpt_img_remove' => '0',
            'geodir_categories' => 'geodir_categories',
            'geodir_tags' => 'geodir_tags',
            'geodir_name' => 'Jobs',
            'geodir_singular_name' => 'Job',
            'geodir_add_new' => 'Add New',
            'geodir_add_new_item' => 'Add New Job',
            'geodir_edit_item' => 'Edit Job',
            'geodir_new_item' => 'New Job',
            'geodir_view_item' => 'View Job',
            'geodir_search_item' => 'Search Jobs',
            'geodir_not_found' => 'No Job Found',
            'geodir_not_found_trash' => 'No Job Found In Trash',
            'geodir_label_post_profile' => 'Details',
            'geodir_label_post_info' => '',
            'geodir_label_post_images' => '',
            'geodir_label_post_map' => '',
            'geodir_label_reviews' => '',
            'geodir_label_related_listing' => '',
            'geodir_support' => array('title', 'editor', 'author', 'thumbnail', 'excerpt', 'custom-fields', 'comments'),
            'geodir_description' => '',
            'geodir_menu_icon' => '',
            'geodir_can_export' => 'true',
            'geodir_cp_meta_keyword' => '',
            'geodir_cp_meta_description' => '',
            'geodir_save_post_type' => 'Save changes',
            'linkable_to' => 'gd_place',
            'linkable_from' => '',
            'subtab' => '',
        );

        add_filter('wp_redirect', '__return_false');
        geodir_cp_from_submit_handler();
        remove_filter('wp_redirect', '__return_false');

        $this->assertTrue(post_type_exists( 'gd_job' ));

//        $_POST['geodir_deleteposttype'] = 'gd_job';
//        add_filter('wp_redirect', '__return_false');
//        $this->_handleAjax('geodir_cp_ajax_action');
//        remove_filter('wp_redirect', '__return_false');
//        $this->assertContains("bestof-cat-title", $this->_last_response);


    }


    public function tearDown()
    {
        parent::tearDown();
    }
}
?>