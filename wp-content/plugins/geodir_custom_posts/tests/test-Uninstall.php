<?php
class UninstallCPT extends WP_UnitTestCase
{
    public function setUp()
    {
        parent::setUp();
    }

    public function testUninstall()
    {
        ob_start();
        geodir_custom_post_type_uninstall();
        $output = ob_get_contents();
        ob_end_clean();
        $this->assertContains('You are about to delete a Geodirectory Add-on', $output);

        $geodir_custom_post_types = get_option('geodir_custom_post_types');
        $this->assertContains('gd_job', $geodir_custom_post_types);
        $_REQUEST['verify-delete-adon'] = true;
        $_REQUEST['verify-delete-adon-data'] = true;
        geodir_custom_post_type_uninstall();
        $geodir_custom_post_types = get_option('geodir_custom_post_types');
        $this->assertNotContains('gd_job', $geodir_custom_post_types);
    }

    public function tearDown()
    {
        parent::tearDown();
    }
}
?>