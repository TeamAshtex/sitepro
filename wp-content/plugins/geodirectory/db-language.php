<?php
/**
 * Translate language string stored in database. Ex: Custom Fields
 *
 * @package GeoDirectory
 * @since 1.4.2
 */

// Language keys
__('Free', 'geodirectory');
__('Free: number of publish days are unlimited (0.00 USD)', 'geodirectory');
__('Terrible', 'geodirectory');
__('Poor', 'geodirectory');
__('Average', 'geodirectory');
__('Very Good', 'geodirectory');
__('Excellent', 'geodirectory');
__('Category', 'geodirectory');
__('SELECT listing category FROM here. SELECT at least one CATEGORY', 'geodirectory');
__('Address', 'geodirectory');
__('Please enter listing address. eg. : 230 Vine Street', 'geodirectory');
__('Address fields are required', 'geodirectory');
__('Time', 'geodirectory');
__('Enter Business or Listing Timing Information.<br/>eg. : 10.00 am to 6 pm every day', 'geodirectory');
__('Phone', 'geodirectory');
__('You can enter phone number,cell phone number etc.', 'geodirectory');
__('Email', 'geodirectory');
__('You can enter your business or listing email.', 'geodirectory');
__('Website', 'geodirectory');
__('You can enter your business or listing website.', 'geodirectory');
__('Twitter', 'geodirectory');
__('You can enter your business or listing twitter url.', 'geodirectory');
__('Facebook', 'geodirectory');
__('You can enter your business or listing facebook url.', 'geodirectory');
__('Video', 'geodirectory');
__('Add video code here, YouTube etc.', 'geodirectory');
__('Special Offers', 'geodirectory');
__('Note: List out any special offers (optional)', 'geodirectory');
__('Select listing category from here. Select at least one category', 'geodirectory');
__('Places', 'geodirectory');
__('Place', 'geodirectory');
__('Add New', 'geodirectory');
__('Add New Place', 'geodirectory');
__('Edit Place', 'geodirectory');
__('New Place', 'geodirectory');
__('View Place', 'geodirectory');
__('Search Places', 'geodirectory');
__('No Place Found', 'geodirectory');
__('No Place Found In Trash', 'geodirectory');
__('Place post type.', 'geodirectory');
__('Events', 'geodirectory');
__('Event', 'geodirectory');
__('Add New Event', 'geodirectory');
__('Edit Event', 'geodirectory');
__('New Event', 'geodirectory');
__('View Event', 'geodirectory');
__('Search Events', 'geodirectory');
__('No Event Found', 'geodirectory');
__('No Event Found In Trash', 'geodirectory');
__('Event post type.', 'geodirectory');
__('Post Submitted Successfully', 'geodirectory');
__('<p>Dear Admin,</p><p>A new  listing has been published [#listing_link#]. This email is just for your information.</p><br><p>[#site_name#]</p>', 'geodirectory');
__('<p>Dear [#client_name#],</p><p>You submitted the below listing information. This email is just for your information.</p><p>[#listing_link#]</p><br><p>Thank you for your contribution.</p><p>[#site_name#]</p>', 'geodirectory');
__('[#site_name#] - Your new password', 'geodirectory');
__('<p>Dear [#client_name#],<p><p>You requested a new password for [#site_name_url#]</p><p>[#login_details#]</p><p>You can login here: [#login_url#]</p><p>Thank you,<br /><br />[#site_name_url#].</p>', 'geodirectory');
__('Your Log In Details', 'geodirectory');
__('<p>Dear [#client_name#],</p><p>You can log in  with the following information:</p><p>[#login_details#]</p><p>You can login here: [#login_url#]</p><p>Thank you,<br /><br />[#site_name_url#].</p>', 'geodirectory');
__('Listing Published Successfully', 'geodirectory');
__('<p>Dear [#client_name#],</p><p>Your listing [#listing_link#] has been published. This email is just for your information.</p><p>[#listing_link#]</p><br><p>Thank you for your contribution.</p><p>[#site_name#]</p>', 'geodirectory');
__('[#from_name#] thought you might be interested in..', 'geodirectory');
__('<p>Dear [#to_name#],<p><p>Your friend has sent you a message from <b>[#site_name#]</b> </p><p>===============================</p><p><b>Subject : [#subject#]</b></p><p>[#comments#] [#listing_link#]</p><p>===============================</p><p>Thank you,<br /><br />[#site_name#].</p>', 'geodirectory');
__('Website Enquiry', 'geodirectory');
__('<p>Dear [#to_name#],<p><p>An enquiry has been sent from <b>[#listing_link#]</b></p><p>===============================</p><p>[#comments#]</p><p>===============================</p><p>Thank you,<br /><br />[#site_name_url#].</p>', 'geodirectory');
__('<p>Thank you, your information has been successfully received.</p><p><a href="[#submited_information_link#]" >View your submitted information &raquo;</a></p><p>Thank you for visiting us at [#site_name#].</p>', 'geodirectory');
__('[[#site_name#]] Listing edited by Author', 'geodirectory');
__('<p>Dear Admin,</p><p>A listing [#listing_link#] has been edited by it\'s author [#post_author_name#].</p><br><p><b>Listing Details:</b></p><p>Listing ID: [#post_id#]</p><p>Listing URL: [#listing_link#]</p><p>Date: [#current_date#]</p><br><p>This email is just for your information.</p><p>[#site_name#]</p>', 'geodirectory');
__('Acknowledgment for your Payment', 'geodirectory');
__('<p>Dear [#client_name#],</p><p>Payment has been successfully received. Your details are below</p><p>[#transaction_details#]</p><br><p>We hope you enjoy. Thanks!</p><p>[#site_name#]</p>', 'geodirectory');
__('Renewal of listing ID:#[#post_id#]', 'geodirectory');
__('<p>Dear [#client_name#],</p><p>Your listing [#listing_link#] has been renewed.</p><p>NOTE: If your listing is not active yet your payment may be being checked by an admin and it will be activated shortly.</p><br><p>[#site_name#]</p>', 'geodirectory');
__('Place listing expiration Notification', 'geodirectory');
__('<p>Dear [#client_name#],<p><p>Your listing - [#listing_link#] posted on  <u>[#posted_date#]</u> for [#number_of_days#] days.</p><p>It\'s going to expiry after [#number_of_grace_days#] day(s). If the listing expire, it will no longer appear on the site.</p><p> If you want to renew, Please login to your member area of our site and renew it as soon as it expire.</p><p>You may like to login the site from [#login_url#].</p><p>Your login ID is <b>[#username#]</b> and Email ID is <b>[#user_email#]</b>.</p><p>Thank you,<br />[#site_name_url#].</p>', 'geodirectory');
__('Upgrade of listing ID:#[#post_id#]', 'geodirectory');
__('<p>Dear [#client_name#],</p><p>Your listing [#listing_link#] has been upgraded.</p><p>NOTE: If your listing is not active yet your payment may be being checked by an admin and it will be activated shortly.</p><br><p>[#site_name#]</p>', 'geodirectory');
__('[#site_name#] - Invoice Details #[#invoice_id#]', 'geodirectory');
__('<p>Dear [#client_name#],</p><p>Here is details for your invoice <a href="[#invoice_link#]">#[#invoice_id#] - [#invoice_title#]</a> at <a href="[#site_name_url#]">[#site_name#]</a>.</p><p><b>Invoice Details:</b></p><p>Type: [#invoice_type#]</p><p>Date: [#invoice_date#]</p><p>Status: [#invoice_status#]</p><p>Payment Method: [#payment_method#]</p><p>Payable Amount: [#invoice_amount#]</p>[#invoice_discount_details#][#invoice_listing_details#][#invoice_package_details#][#invoice_transaction_details#]<p>---</p><p>Thank you for your contribution.</p><p><a href="[#site_name_url#]">[#site_name#]</a></p>', 'geodirectory');
__('Payment received successfully', 'geodirectory');
__('<p>Dear Admin,</p><p>Payment has been received. Below are the transaction details.</p><p>[#transaction_details#]</p><br><p>[#site_name#]</p>', 'geodirectory');
__('<p>Dear Admin,</p><p>Listing [#listing_link#] has been renewed. Please confirm payment and then update the listings published date to todays date. </p><p>NOTE: If payment was made by paypal the "published date" should be updated automatically. </p><br><p>[#site_name#]</p>', 'geodirectory');
__('<p>Dear Admin,</p><p>Listing [#listing_link#] has been upgraded. Please confirm payment and then update the listings published date to todays date. </p><p>NOTE: If payment was made by paypal the "published date" should be updated automatically. </p><br><p>[#site_name#]</p>', 'geodirectory');
__('IPN INVALID - Place Listing Submitted', 'geodirectory');
__('<p>Dear Admin,</p><p>Paypal IPN Invalid for listing ID: #[#post_id#]</p><p>Please manually check your paypal logs, and if payment was received manually publish the listing.</p><p>[#listing_link#]</p><br><p>[#site_name#]</p>', 'geodirectory');
