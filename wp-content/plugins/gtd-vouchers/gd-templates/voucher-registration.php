<?php
/**
 * Template for the details (post) page
 *
 * You can make most changes via hooks or see the link below for info on how to replace the template in your theme.
 *
 * @link http://docs.wpgeodirectory.com/customizing-geodirectory-templates/
 * @since 1.0.0
 * @package GeoDirectory
 *

$plain = false;

$voucher_guid  = $_GET['voucher'];
$download_guid = '';
if ( isset( $_GET['guid'] ) ) {
	$download_guid = $_GET['guid'];
}

global $wpdb;
$options = get_option( 'gtvouchers_options' );

$out           = '';
$showform      = true;
$download_guid = '';
$sql     = $wpdb->prepare( "select * from {$wpdb->prefix}gtvouchers_vouchers where guid = %s and deleted = 0;", $voucher_guid );
$voucher = $wpdb->get_row( $sql );

if ( !$plain ) {
header("X-XSS-Protection: 0"); // IE requirement
// call header
get_header();

###### WRAPPER OPEN ######
/** This action is documented in geodirectory-templates/add-listing.php */
do_action('geodir_wrapper_open', 'details-page', 'geodir-wrapper', '');

###### TOP CONTENT ######
/** This action is documented in geodirectory-templates/add-listing.php */
do_action('geodir_top_content', 'details-page');

/**
 * Calls the top section widget area and the breadcrumbs on the details page.
 *
 * @since 1.1.0
 */
do_action('geodir_detail_before_main_content');

/** This action is documented in geodirectory-templates/add-listing.php */
do_action('geodir_before_main_content', 'details-page');

###### SIDEBAR ON LEFT ######
if (get_option('geodir_detail_sidebar_left_section')) {
	/**
	 * Adds the details page sidebar to the details template page.
	 *
	 * @since 1.1.0
	 */
	do_action('geodir_detail_sidebar');
}

###### MAIN CONTENT WRAPPERS OPEN ######
/** This action is documented in geodirectory-templates/add-listing.php */
do_action('geodir_wrapper_content_open', 'details-page', 'geodir-wrapper-content', '');

/**
 * Adds the opening HTML wrapper for the article on the details page.
 *
 * @since 1.1.0
 * @since 1.5.4 Removed http://schema.org/LocalBusiness parameter as its now added via JSON-LD
 * @global object $post The current post object.
 * @global object $post_images Image objects of current post if available.
 * @param string $type Page type.
 * @param string $id The id of the HTML element.
 * @param string $class The class of the HTML element.
 * @param string $itemtype The itemtype value of the HTML element.
 * @see 'geodir_article_close'
 */
do_action('geodir_article_open', 'details-page', 'post-' . get_the_ID(), get_post_class(), '');

if ( isset( $_POST['voucher_email'] ) && ( is_email( trim( $_POST['voucher_email'] ) ) ) ) {
	if ( ( '1' != $voucher->require_phone ) || ( ( '1' == $voucher->require_phone ) && ( !empty( $_POST['voucher_phone'] ) ) ) ) {
		// register the email address
		$phone         = isset( $_POST['voucher_phone'] ) ? sanitize_text_field( $_POST['voucher_phone'] ) : '';
		$download_guid = gtvouchers_register_person( $voucher_guid, trim( $_POST['voucher_email'] ), trim( $_POST['voucher_name'] ), trim( $phone ) );
		if ( 'false' != $download_guid ) {
			echo '<h1>' . __( 'Registration complete', 'gtvouchers-d' ) . '</h1>';
		} else {
			echo '<h1>' . __( 'There was a problem', 'gtvouchers-d' ) . '</h1>';
		}
	}
} else {
	echo '<h1>' . __( 'Please provide some details', 'gtvouchers-d' ) . '</h1>';
}

}

// if registering
if ( ( isset( $_POST['voucher_email'] ) && '' != $_POST['voucher_email'] ) && ( isset( $_POST['voucher_name'] ) && '' != $_POST['voucher_name'] ) ) {

	// if the email address is valid
	if ( is_email( trim( $_POST['voucher_email'] ) ) ) {
		// if the guid has been generated
		if ( false != $download_guid ) {
			$voucher = gtvouchers_get_voucher( $voucher_guid );
			$details = wp_get_single_post( $voucher->place_id );

			$tokens       = array(
				'{email}',
				'{name}',
				'{voucher}',
				'{listing}',
				'{sitename}'
			);
			$replacements = array(
				trim( esc_attr( $_POST['voucher_email'] ) ),
				trim( esc_attr( $_POST['voucher_name'] ) ),
				trim( esc_attr( $voucher->name ) ),
				trim( esc_attr( $details->post_title ) ),
				trim( get_option( 'blogname' ) ),
				// trim( )
			);

			$message = '';
			if ( '' != $voucher->description ) {
				$message .= htmlspecialchars( stripslashes( $voucher->description ) ) . "\n\n";
			}

			if ( !empty( $options["email_body"] ) ) {
				$message .= gtvouchers_customize_string( $options["email_body"], $tokens, $replacements ) . "\n\n" . gtvouchers_link( $voucher_guid, $download_guid, false );
			} else {
				$message .= sprintf( __( 'You have successfully registered to download this %s, please download the %s from here:', 'gtvouchers-d' ), strtolower( $options['replace_voucher_singular_word'] ), strtolower( $options['replace_voucher_singular_word'] ) ) . "\n\n" . gtvouchers_link( $voucher_guid, $download_guid, false );
			}

			if ( !isset( $options['email_from_name'] ) ) {
				$name = get_option( 'blogname' );
			} else {
				$name = $options['email_from_name'];
			}

			if ( !isset( $options['email_from_email'] ) ) {
				$sender = get_option( 'admin_email' );
			} else {
				$sender = $options['email_from_email'];
			}

			$headers = "From: $name <$sender>" . "\r\n";

			// send the email
			wp_mail( trim( $_POST['voucher_email'] ), $voucher->name . __( ' for ', 'gtvouchers-d' ) . trim( $_POST['voucher_name'] ), $message, $headers );

			do_action( 'gtvouchers_register', $voucher->id, $voucher->name, $_POST['voucher_email'], $_POST['voucher_name'] );

			if ( !empty( $options['thank_you_page'] ) ) {
				$page = get_page_link( intval( $options['thank_you_page'] ) );
				wp_redirect( $page );
				die();
			}

			if ( !empty( $options['thank_you_url'] ) ) {
				$page = $options['thank_you_url'];
				wp_redirect( $page );
				die();
			}

			if ( !empty( $options["thank_you_text"] ) ) {
				$out .= '<p>' . gtvouchers_customize_string( $options["thank_you_text"], $tokens, $replacements ) . '</p>';
			} else {
				$out .= '
                    <p>' . sprintf( __( "Thank you for registering. You will shortly receive an email sent to '%s' with a link to your personalised %s.", 'gtvouchers-d' ), trim( $_POST["voucher_email"] ), strtolower( $options['replace_voucher_singular_word'] ) ) . '</p>
                    ';
			}
			if ( !$plain ) {
				echo $out;
				$out = '';
			}
			$showform = false;

		} else {

			if ( !empty( $options["cannot_reg_text"] ) ) {
				$out .= '<p>' . gtvouchers_customize_string( $options["cannot_reg_text"], $tokens, $replacements ) . '</p>';
			} else {
				$message = '
                    <p>' . sprintf( __( 'Sorry, your email address and name could not be registered. Have you already registered for this %s? Please try again.', 'gtvouchers-d' ), strtolower( $options['replace_voucher_singular_word'] ) ) . '</p>
                    ';
				if ( empty( $_POST['voucher_email'] ) ) {
					$message .= __( 'Your email address is required.', 'gtvouchers-d' );
				}

				if ( "1" == $voucher->require_phone && empty( $_POST['voucher_phone'] ) ) {
					$message .= ' ';
					$message .= __( 'Your phone number is required.', 'gtvouchers-d' );
				}

				$out .= '<p>' . $message . '</p>';
			}
			if ( !$plain ) {
				echo $out;
				$out = '';
			}

		}

	} else {

		$out .= '
            <p>' . __( 'Sorry, your email address was not valid. Please try again.', 'gtvouchers-d' ) . '</p>
            ';
		if ( !$plain ) {
			echo $out;
			$out = '';
		}

	}
}

if ( $showform ) {
	$voucher = gtvouchers_get_voucher( $voucher_guid );
	$details = get_post( $voucher->place_id );
	if ( !$plain ) {
		if ( !empty( $options["pre_download_text"] ) ) {
			$tokens       = array(
				'{voucher}',
				'{listing}'
			);
			$replacements = array(
				trim( esc_attr( $voucher->name ) ),
				trim( esc_attr( $details->post_title ) ),
			);

			$out .= '<p>' . gtvouchers_customize_string( $options["pre_download_text"], $tokens, $replacements ) . '</p>';
			$out .= '<form action="' . gtvouchers_link( $voucher_guid ) . '" method="post" class="gtvouchers_form" id="gtvouchers_form">';
		} else {
			$out .= '<p>' . sprintf( __( 'To download this %s you must provide your name and email address.', 'gtvouchers-d' ), strtolower( $options['replace_voucher_singular_word'] ) ) . '</p>';
			$out .= '<p>' . sprintf( __( 'You will then receive a link by email to download your personalised %s.', 'gtvouchers-d' ), strtolower( $options['replace_voucher_singular_word'] ) ) . '</p>
                <form action="' . gtvouchers_link( $voucher_guid ) . '" method="post" class="gtvouchers_form" id="gtvouchers_form">
                ';
		}
	} else {
		$out .= '
            <form action="' . gtvouchers_page_url() . '" method="post" class="gtvouchers_form" id="gtvouchers_form">
            ';
	}

	if ( is_user_logged_in() ) {
		$current_user = wp_get_current_user();
	}

	if ( isset( $_POST['voucher_name'] ) && !( empty( $_POST['voucher_name'] ) ) ) {
		$entered_name = sanitize_text_field( $_POST['voucher_name'] );
	} else {
		if ( is_user_logged_in() && ( $current_user instanceof WP_User ) ) {
			$entered_name = sanitize_text_field( $current_user->user_firstname );
			if ( !( empty( $current_user->user_lastname ) ) ) {
				$entered_name .= ' ' . sanitize_text_field( $current_user->user_lastname );
			}
		}
	}

	if ( isset( $_POST['voucher_email'] ) && !( empty( $_POST['voucher_email'] ) ) ) {
		$entered_email = sanitize_text_field( $_POST['voucher_email'] );
	} else {
		if ( is_user_logged_in() && ( $current_user instanceof WP_User ) ) {
			$entered_email = sanitize_text_field( $current_user->user_email );
		}
	}

	$out .= '
        <p><label for="voucher_email">' . __( 'Your email address', 'gtvouchers-d' ) . '</label>
        <input type="text" name="voucher_email" id="voucher_email" value="' . $entered_email . '" /></p>
        <p><label for="voucher_name">' . __( 'Your name', 'gtvouchers-d' ) . '</label>
        <input type="text" name="voucher_name" id="voucher_name" value="' . $entered_name . '" /></p>';

	if ( '1' == $voucher->require_phone ) {
		$entered_phone = '';
		if ( isset( $_POST['voucher_phone'] ) ) {
			$entered_phone = sanitize_text_field( $_POST['voucher_phone'] );
		}
		$out .= '
            <p><label for="voucher_phone">' . __( 'Your phone number', 'gtvouchers-d' ) . '</label>
            <input type="text" name="voucher_phone" id="voucher_phone" value="' . $entered_phone . '" /></p>';
	}

	$out .= '<p><input type="submit" name="voucher_submit" id="voucher_submit" value="' . sprintf( __( 'Register for this %s', 'gtvouchers-d' ), strtolower( $options['replace_voucher_singular_word'] ) ) . '" /></p>
            </form>
    ';

	if ( !$plain ) {
		echo $out;
		$out = '';
	}

}

if ( !$plain ) {


###### MAIN CONTENT WRAPPERS CLOSE ######
	/**
	 * Adds the closing HTML wrapper for the article on the details page.
	 *
	 * @since 1.1.0
	 * @param string $type Page type.
	 * @see 'geodir_article_open'
	 */
	do_action( 'geodir_article_close', 'details-page' );

	/** This action is documented in geodirectory-templates/add-listing.php */
	do_action( 'geodir_after_main_content' );

	/** This action is documented in geodirectory-templates/add-listing.php */
	do_action( 'geodir_wrapper_content_close', 'details-page' );

###### SIDEBAR ON RIGHT ######
	if ( !get_option( 'geodir_detail_sidebar_left_section' ) ) {
		/** This action is documented in geodirectory-templates/listing-detail.php */
		do_action( 'geodir_detail_sidebar' );
	}


###### WRAPPER CLOSE ######
	/** This action is documented in geodirectory-templates/add-listing.php */
	do_action( 'geodir_wrapper_close', 'details-page' );

###### BOTTOM SECTION WIDGET AREA ######
	/**
	 * Adds the details page bottom section widget area to the details template page.
	 *
	 * @since 1.1.0
	 */
	do_action( 'geodir_sidebar_detail_bottom_section', '' );


	get_footer();
}
