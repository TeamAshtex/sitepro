=== GT-Vouchers ===
Requires at least: 3.9
Tested up to: 4.3
Stable tag: 1.5.6.8

GT-Vouchers for GeoDirectory is a WordPress plugin that allows you to give downloadable, printable vouchers/tickets/coupons/tokens in PDF format away on your site. It has been highly customized to work with the GeoDirectory plugin allowing business/listing owners to attach special vouchers to their listing and edit them from the front end.

== Description ==

Have you ever wanted to give away vouchers, tickets, coupons or tokens on your website? If so this plugin is for you. You can create a voucher with whatever text you want, choosing the layout and font from a range of templates (you can also add your own templates). Vouchers can then be viewed, downloaded and printed from a specified URL.

There are shortcodes to add a link to a particular voucher, to show an unordered list of all your vouchers, or to show the registration for to request a restricted voucher.

You can require visitors to provide their name and email address to get a voucher. If an email address is required an email is sent to the address with a link to the voucher URL. Each voucher has a unique code, and vouchers that have an email address associated with them can only be used once, so once a registration-required voucher is downloaded it can't be downloaded again.

=== Hooks ===

From version 1.0 the plugin also offers a selection of hooks which you can use to run your own custom code. The hooks are:

==== gtvouchers_create ====

When a voucher is created, this hook returns the properties of the voucher. You can use it like this:

add_action( 'gtvouchers_create', 'my_gtvouchers_create_function' );
function my_gtvouchers_create_function( $id, $name, $text, $description, $template, $require_email, $limit, $startdate, $expiry ) {
	// do something here...
}

==== gtvouchers_edit ====

When a voucher is edited, this hook returns the properties of the voucher. You can use it like this:

add_action( 'gtvouchers_edit', 'my_gtvouchers_edit_function' );
function my_gtvouchers_edit_function( $id, $name, $text, $description, $template, $require_email, $limit, $startdate, $expiry ) {
	// do something here...
}

==== gtvouchers_register ====

When someone registers to download a voucher and an email is sent to them, this hook returns the voucher and the users details. You can use it like this:

add_action( 'gtvouchers_register', 'my_gtvouchers_register_function' );
function my_gtvouchers_register_function( $voucher_id, $voucher_name, $user_email, $user_name ) {
	// do something here...
}

==== gtvouchers_download ====

When someone downloads a voucher, this hook returns the voucher and the users details. You can use it like this:

add_action( 'gtvouchers_download', 'my_gtvouchers_download_function' );
function my_gtvouchers_download_function( $voucher_id, $voucher_name, $code ) {
	// do something here...
}

The plugin also makes use of the __() and _e() functions to allow for easy translation.

== Installation ==

The plugin should be placed in your /wp-content/plugins/ directory and activated in the plugin administration screen. The plugin is quite large as it includes the TCPDF class for creating the PDF file.

If you can't install it using the standard WordPress plugin page, you'll need to use an FTP program.

== Shortcodes ==

There are four shortcodes available. The first shows a link to a particular voucher, and is in the format:

[voucher id="123"]

The "id" parameter is the unique ID of the voucher. The correct ID to use is available in the screen where you edit the voucher.

You can also how the description after the link:

[voucher id="123" description="true"]

The second shows a link to a voucher, but with a preview of the voucher (just the background image, no text) and the voucher name as the image alternate text:

[voucher id="123" preview="true"]

And you can show the description after the preview as well:

[voucher id="123" preview="true" description="true"]

You can also show an unordered list of all your live vouchers using this shortcode:

[voucherlist]

And a list of all live vouchers with their descriptions:

[voucherlist description="true"]

And a list of all live vouchers with expiry dates:

[voucherlist showexpiry="true"]

And show the list with only vouchers in specific categories:

[voucherlist category=5,7]

And you can also show the form for people to enter their name and email address if they wish to register for a restricted voucher:

[voucherform id="123"]

The shortcodes for any voucher can be found on the edit screen for that voucher.

== Frequently Asked Questions ==

= My voucher PDF files are corrupted. Why? =

This is normally because PHP isn't given enough memory to create print-resolution PDF files. If you open one of your corrupted PDF files in a text editor it will say something like this:

Fatal error:  Out of memory (allocated 31981568) (tried to allocate 456135 bytes) in
/some/thing/here/wp-content/plugins/gt-vouchers/tcpdf/tcpdf.php

Speak to your hosts or system administrator to give PHP more memory. GT-Vouchers now has the option to show & download images which requires the commonly installed GD Image library and has no such memory issues.

== Screenshots ==

1. Creating or editing a voucher
2. Viewing the list of your vouchers, and the mot popular downloaded ones
3. A sample voucher
4. A Microsoft Windows print dialog showing the voucher on the paper
5. All the default templates

== Changelog ==
= 1.5.6.8 =
    Added: Attach to new hooks in Payments to properly clear transients on up/downgrade packages
    Updated: Adjust CSS on public voucher creation to work on slightly smaller width panels
    Updated: Change site URL in documentation.
= 1.5.6.7 =
    Fixed: Replace static reference to wp_ prefix with dynamic. Banners should show properly now.
= 1.5.6.6 =
    Update: Tweaks to filters to support GT-Vouchers MyCred integration better
= 1.5.6.5 =
    Update: Updated .PO (language file) for new custom "Voucher" word. This will fix translations broken in the last build.
    Fixed: Restricting voucher creation wasn't respecting limits entered by post type.
= 1.5.6.4 =
    Added: Added sidebar with links to documentation website, support ticket and add-ons
    Update: Added a note on Settings to visit Pricing when using the Payment plugin
    Fixed: Text for Logged Out Teaser wasn't being saved
    Fixed: Text was being output during custom voucher image upload
    Fixed: Custom image wasn't being added if the MEDIA URL option wasn't selected.
    Confirmed: If the Payment plugin is activated after GD-Vouchers, columns are updated properly
    Update: Removed code referring to custom GeoDirectory templates, since we use the originals & filters
= 1.5.6.3 =
    New: Added Tools menu which has the System Info report for debugging
    Update: Fixed the Price & Payment module table regeneration
    Update: Updated Widget code to comply with WordPress 4.3 deprecation of PHP 4 style constructors
    Update: Updated template layouts to be more compatible with WPGeoDirectory and other themes
