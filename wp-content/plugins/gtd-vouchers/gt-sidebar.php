
<div id="gtv-sidebar">
    <img src="http://gt-vouchers.com/wp-content/uploads/2015/10/gt-vouchers-Logo.jpg" style="width:240px;" />
	<h3>Documentation</h3>
	<a href="http://gt-vouchers.com/documentation/setting-up-gt-vouchers-for-the-first-time/" target="_blank">GT-Vouchers Documentation</a>
	<h3>Support</h3>
	<a href="http://gt-vouchers.com/new-ticket/" target="_blank">Open a support ticket</a>
	<h3>Add-ons</h3>
	<p><a href="http://gt-vouchers.com/downloads/gtv-mycred/" target="_blank">My Cred support for GT-Vouchers</a></p>
	<p><a href="http://gt-vouchers.com/downloads/gtv-mailchimp/" target="_blank">MailChimp support for GT-Vouchers</a></p>
</div>
