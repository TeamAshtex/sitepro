<?php
// If this file is called directly, abort.
if ( !defined( 'WPINC' ) ) {
	die;
}

class gtv_newest_vouchers extends WP_Widget{

	public function __construct() {
		parent::__construct( 'gtv_newest_vouchers', // Base ID
			__( 'GTV > Most Recent Vouchers', 'gtvouchers-d' ), // Name
			array( 'description' => __( 'List your newest vouchers.', 'gtvouchers-d' ), ) // Args
		);

		wp_enqueue_script( 'jQuery' );
	}

	public function form( $instance ) {
		$instance       = wp_parse_args( (array) $instance, array(
			'title' => '',
			'category' => array(),
			'category_title' => '',
			'city' => '',
			'how_many' => 5,
			'multi_city' => 'false',
			'post_type' => 'gd_place'
		) );
		$title          = strip_tags( $instance['title'] );
		$category       = (array) $instance['category'];
		$category_title = strip_tags( $instance['category_title'] );
		$city           = strip_tags( $instance['city'] );
		$how_many       = strip_tags( $instance['how_many'] );
		$multi_city     = strip_tags( $instance['multi_city'] );
		$post_type      = strip_tags( $instance['post_type'] );
		if ( isset( $instance['multi_city'] ) ) {
			$multi_city = strip_tags( $instance['multi_city'] );
			if ( 'true' !== $multi_city ) {
				$multi_city = 'false';
			}
		} else {
			$multi_city = 'false';
		}

		?>
		<p>
			<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:', 'gtvouchers-d' ); ?>
				<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>"/>
			</label>
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'post_type' ); ?>"><?php _e( 'Post Type:', GEODIRECTORY_TEXTDOMAIN ); ?>

				<?php $postypes = geodir_get_posttypes();
				$postypes       = apply_filters( 'geodir_post_type_list_in_p_widget', $postypes ); ?>

				<select class="widefat" id="<?php echo $this->get_field_id( 'post_type' ); ?>" name="<?php echo $this->get_field_name( 'post_type' ); ?>" onchange="geodir_change_category_list(this)">

					<?php foreach ( $postypes as $postypes_obj ) { ?>

						<option <?php if ( $post_type == $postypes_obj ) {
							echo 'selected="selected"';
						} ?> value="<?php echo $postypes_obj; ?>"><?php $extvalue = explode( '_', $postypes_obj );
							echo ucfirst( $extvalue[1] ); ?></option>

					<?php } ?>

				</select>
			</label>
		</p>


		<p id="post_type_cats">
			<label for="<?php echo $this->get_field_id( 'category' ); ?>"><?php _e( 'Post Category:', GEODIRECTORY_TEXTDOMAIN ); ?>

				<?php

				$post_type = ( $post_type != '' ) ? $post_type : 'gd_place';

				$all_postypes = geodir_get_posttypes();

				if ( !in_array( $post_type, $all_postypes ) ) {
					$post_type = 'gd_place';
				}

				$category_taxonomy = geodir_get_taxonomies( $post_type );
				$categories        = get_terms( $category_taxonomy, array( 'orderby' => 'count', 'order' => 'DESC' ) );

				?>

				<select multiple="multiple" class="widefat" name="<?php echo $this->get_field_name( 'category' ); ?>[]" onchange="geodir_popular_widget_cat_title(this)">

					<option <?php if ( is_array( $category ) && in_array( '0', $category ) ) {
						echo 'selected="selected"';
					} ?> value="0"><?php _e( 'All', GEODIRECTORY_TEXTDOMAIN ); ?></option>
					<?php foreach ( $categories as $category_obj ) {
						$selected = '';
						if ( is_array( $category ) && in_array( $category_obj->term_id, $category ) ) {
							echo $selected = 'selected="selected"';
						}

						?>

						<option <?php echo $selected; ?> value="<?php echo $category_obj->term_id; ?>"><?php echo ucfirst( $category_obj->name ); ?></option>

					<?php } ?>

				</select>


				<input type="hidden" name="<?php echo $this->get_field_name( 'category_title' ); ?>" id="<?php echo $this->get_field_id( 'category_title' ); ?>"
				       value="<?php if ( $category_title != '' ) {
					       echo $category_title;
				       } else {
					       echo __( 'All', GEODIRECTORY_TEXTDOMAIN );
				       } ?>"/>

			</label>
		</p>
		<p><strong>&nbsp;- OR -</strong></p>
		<p>
			<label for="<?php echo $this->get_field_id( 'city' ); ?>"><?php _e( 'City:', 'gtvouchers-d' ); ?>
				<?php
				echo $this->input_list_cities( esc_attr( $city ) );
				?>
			</label>
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'how_many' ); ?>"><?php _e( 'Number of vouchers to show:', 'gtvouchers-d' ); ?>
				<input class="widefat" id="<?php echo $this->get_field_id( 'how_many' ); ?>" name="<?php echo $this->get_field_name( 'how_many' ); ?>" type="text"
				       value="<?php echo esc_attr( $how_many ); ?>"/>
			</label>
		</p>
	<?php
	}

	public function update( $new_instance, $old_instance ) {
		$instance          = $old_instance;
		$instance['title'] = strip_tags( $new_instance['title'] );

		$instance['post_type'] = strip_tags( $new_instance['post_type'] );

		// If category is empty, set it to empty to ensure it's cleared.
		$instance['category']       = isset( $new_instance['category'] ) ? $new_instance['category'] : '';
		$instance['category_title'] = strip_tags( $new_instance['category_title'] );

		// If city is empty OR category is used, make sure to clear city.
		if ( "" == $new_instance['city'] || "" != $new_instance['category'] ) {
			$instance['city'] = '';
		} else {
			$instance['city'] = strip_tags( $new_instance['city'] );
		}
		$instance['how_many'] = strip_tags( $new_instance['how_many'] );

		if ( isset( $new_instance['multi_city'] ) && ( 'true' == $new_instance['multi_city'] ) ) {
			$instance['multi_city'] = 'true';
		} else {
			$instance['multi_city'] = 'false';
		}

		return $instance;
	}

	public function widget( $args, $instance ) {
		global $wpdb, $cache_enabled;
		extract( $args, EXTR_SKIP );

		$title      = empty( $instance['title'] ) ? '' : apply_filters( 'widget_title', $instance['title'] );
		$category   = empty( $instance['category'] ) ? '' : apply_filters( 'widget_category', $instance['category'] );
		$city       = empty( $instance['city'] ) ? '' : apply_filters( 'widget_city', $instance['city'] );
		$how_many   = empty( $instance['how_many'] ) ? '5' : apply_filters( 'widget_how_many', $instance['how_many'] );
		$multi_city = empty( $instance['multi_city'] ) ? 'false' : $instance['multi_city'];
		$post_type  = empty( $instance['post_type'] ) ? 'gd_place' : $instance['post_type'];

		include_once( ABSPATH . 'wp-admin/includes/plugin.php' );

		$ajax = false;
		if ( is_plugin_active( 'wp-super-cache/wp-cache.php' ) && $cache_enabled ) {
			$ajax = true;
		}

		echo $before_widget;
		if ( !empty( $title ) ) {
			echo $before_title . $title . $after_title;
		}

		if ( $ajax ) {
			$nonce = wp_create_nonce( "gtv-get-recent-vouchers" );
			$link  = admin_url( 'admin-ajax.php' );

			echo '<script type="text/javascript">
            jQuery(document).ready(function($){
                $.ajax({
                    type : "POST",
                    url : "' . $link . '",
                    data : { nonce     : "' . $nonce . '",
                             action : "gtv_recent_vouchers",
                             id : "' . esc_js( $args['widget_id'] ) . '",
                           },
                    success : function(response) {
                        // The server has finished executing PHP and has returned something,
                        // so display it!
                        $("#gtv-recent-vouchers").html(response);
                    }
                });
            });
        </script>';

			echo '<div id="gtv-recent-vouchers"></div>';
		} else {
			gtv_output_recent_vouchers( $args['widget_id'], $city, $how_many, $multi_city, $category, $post_type );
		}

		echo $after_widget;
	}

	private function input_list_cities( $selected ) {
		global $wpdb, $multicity_db_table_name;

		$city_sql = "SELECT location_id as city_id, city as cityname FROM {$wpdb->prefix}geodir_post_locations ORDER BY city, location_id";

		$cities = $wpdb->get_results( $city_sql );

		$city_list = '';
		if ( $cities ) {
			$name      = $this->get_field_name( 'city' );
			$city_list = '<select name=' . $name . '>';
			$city_list .= '<option value="">' . __( 'No city selected', 'gtvouchers-d' ) . '</option>';
			foreach ( $cities as $city ) {
				$city_list .= '<option value="' . $city->city_id . '"' . selected( $city->city_id, $selected, false ) . ' >' . $city->cityname . '</option>';
			}
			$city_list .= '</select>';
		}

		return $city_list;
	}

}

class gtv_popular_vouchers extends WP_Widget{

	public function __construct() {
		parent::__construct( 'gtv_popular_vouchers', // Base ID
			__( 'GTV > Popular Vouchers', 'gtvouchers-d' ), // Name
			array( 'description' => __( 'List your most downloaded vouchers.', 'gtvouchers-d' ), ) // Args
		);
		wp_enqueue_script( 'jQuery' );
	}

	public function form( $instance ) {
		$instance       = wp_parse_args( (array) $instance, array(
			'title' => '',
			'category' => array(),
			'how_many' => 5,
			'multi_city' => 'false',
			'category_title' => '',
			'post_type' => 'gd_place',
		) );
		$title          = strip_tags( $instance['title'] );
		$how_many       = strip_tags( $instance['how_many'] );
		$multi_city     = strip_tags( $instance['multi_city'] );
		$category       = (array) $instance['category'];
		$category_title = strip_tags( $instance['category_title'] );
		$post_type      = strip_tags( $instance['post_type'] );
		if ( isset( $instance['multi_city'] ) ) {
			$multi_city = strip_tags( $instance['multi_city'] );
			if ( 'true' !== $multi_city ) {
				$multi_city = 'false';
			}
		} else {
			$multi_city = 'false';
		}
		?>
		<p>
			<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:', 'gtvouchers-d' ); ?>
				<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>"/>
			</label>
		</p>

		<p>
			<label for="<?php echo $this->get_field_id( 'post_type' ); ?>"><?php _e( 'Post Type:', GEODIRECTORY_TEXTDOMAIN ); ?>

				<?php $postypes = geodir_get_posttypes();
				$postypes       = apply_filters( 'geodir_post_type_list_in_p_widget', $postypes ); ?>

				<select class="widefat" id="<?php echo $this->get_field_id( 'post_type' ); ?>" name="<?php echo $this->get_field_name( 'post_type' ); ?>" onchange="geodir_change_category_list(this)">

					<?php foreach ( $postypes as $postypes_obj ) { ?>

						<option <?php if ( $post_type == $postypes_obj ) {
							echo 'selected="selected"';
						} ?> value="<?php echo $postypes_obj; ?>"><?php $extvalue = explode( '_', $postypes_obj );
							echo ucfirst( $extvalue[1] ); ?></option>

					<?php } ?>

				</select>
			</label>
		</p>

		<p id="post_type_cats">
			<label for="<?php echo $this->get_field_id( 'category' ); ?>"><?php _e( 'Post Category:', GEODIRECTORY_TEXTDOMAIN ); ?>

				<?php

				$post_type = ( $post_type != '' ) ? $post_type : 'gd_place';

				$all_postypes = geodir_get_posttypes();

				if ( !in_array( $post_type, $all_postypes ) ) {
					$post_type = 'gd_place';
				}

				$category_taxonomy = geodir_get_taxonomies( $post_type );
				$categories        = get_terms( $category_taxonomy, array( 'orderby' => 'count', 'order' => 'DESC' ) );

				?>

				<select multiple="multiple" class="widefat" name="<?php echo $this->get_field_name( 'category' ); ?>[]" onchange="geodir_popular_widget_cat_title(this)">

					<option <?php if ( is_array( $category ) && in_array( '0', $category ) ) {
						echo 'selected="selected"';
					} ?> value="0"><?php _e( 'All', GEODIRECTORY_TEXTDOMAIN ); ?></option>
					<?php foreach ( $categories as $category_obj ) {
						$selected = '';
						if ( is_array( $category ) && in_array( $category_obj->term_id, $category ) ) {
							echo $selected = 'selected="selected"';
						}

						?>

						<option <?php echo $selected; ?> value="<?php echo $category_obj->term_id; ?>"><?php echo ucfirst( $category_obj->name ); ?></option>

					<?php } ?>

				</select>


				<input type="hidden" name="<?php echo $this->get_field_name( 'category_title' ); ?>" id="<?php echo $this->get_field_id( 'category_title' ); ?>"
				       value="<?php if ( $category_title != '' ) {
					       echo $category_title;
				       } else {
					       echo __( 'All', GEODIRECTORY_TEXTDOMAIN );
				       } ?>"/>

			</label>
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'multi_city' ); ?>"><?php _e( 'Show current city only - (requires multi-city to be on)', 'gtvouchers-d' ); ?>
				<input type="checkbox" value="true" name="<?php echo $this->get_field_name( 'multi_city' ); ?>" <?php checked( $multi_city, 'true' ); ?> />
			</label>
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'how_many' ); ?>"><?php _e( 'Number of vouchers to show:', 'gtvouchers-d' ); ?>
				<input class="widefat" id="<?php echo $this->get_field_id( 'how_many' ); ?>" name="<?php echo $this->get_field_name( 'how_many' ); ?>" type="text"
				       value="<?php echo esc_attr( $how_many ); ?>"/>
			</label>
		</p>
	<?php
	}

	public function update( $new_instance, $old_instance ) {
		$instance              = $old_instance;
		$instance['title']     = strip_tags( $new_instance['title'] );
		$instance['post_type'] = strip_tags( $new_instance['post_type'] );
		// If category is empty, set it to empty to ensure it's cleared.
		if ( "" == $new_instance['category'] ) {
			$instance['category'] = '';
		} else {
			$instance['category'] = implode( ',', $new_instance['category'] );
		}
		$instance['category_title'] = strip_tags( $new_instance['category_title'] );
		$instance['how_many']       = strip_tags( $new_instance['how_many'] );
		if ( isset( $new_instance['multi_city'] ) && ( 'true' == $new_instance['multi_city'] ) ) {
			$instance['multi_city'] = 'true';
		} else {
			$instance['multi_city'] = 'false';
		}

		return $instance;
	}

	public function widget( $args, $instance ) {
		global $cache_enabled, $wpdb;
		extract( $args, EXTR_SKIP );

		$title = empty( $instance['title'] ) ? '' : apply_filters( 'widget_title', $instance['title'] );

		$prefix = $wpdb->prefix;
//        if ( isset( $wpdb->base_prefix ) ){
//            $prefix = $wpdb->base_prefix;
//        }

		include_once( ABSPATH . 'wp-admin/includes/plugin.php' );
		$ajax = false;
		if ( is_plugin_active( 'wp-super-cache/wp-cache.php' ) && $cache_enabled ) {
			$ajax = true;
		}

		echo $before_widget;
		if ( !empty( $title ) ) {
			echo $before_title . $title . $after_title;
		}

		if ( $ajax ) {
			$nonce = wp_create_nonce( "gtv-get-top-vouchers" );
			$link  = admin_url( 'admin-ajax.php' );
			echo '<script type="text/javascript">
            jQuery(document).ready(function($){
                $.ajax({
                    type : "POST",
                    url : "' . $link . '",
                    data : { nonce     : "' . $nonce . '",
                             action : "gtv_top_vouchers",
                             id : "' . esc_js( $args['widget_id'] ) . '",
                           },
                    success : function(response) {
                        // The server has finished executing PHP and has returned something,
                        // so display it!
                        $("#popular-vouchers").html(response);
                    }
                });
            });
        </script>';

			echo '<div id="popular-vouchers"></div>';
		} else {
			gtv_output_popular_vouchers( $args['widget_id'] );
		}

		echo $after_widget;
	}

	private function input_list_cats( $selected, $tax ) {
		$cats = get_terms( $tax, array( 'hide_empty' => false ) );
		if ( $cats ) {
			$cat_list = '<ul style="margin-top:6px;">';
			foreach ( $cats as $one_cat ) {
				$name = $this->get_field_name( 'category' );
				$cat_list .= '<li style="margin-bottom:4px;">';
				$cat_list .= '<input style="margin: 0 7px;" type="checkbox" name="' . $name . '[]" value="' . $one_cat->term_id . '"';
				if ( in_array( $one_cat->term_id, $selected ) ) {
					$cat_list .= ' checked="checked"';
				}
				$cat_list .= '>' . $one_cat->name . '</li>';
			}
			$cat_list .= '</ul>';
		}

		return $cat_list;
	}
}

class gtv_download_count extends WP_Widget{
	public function __construct() {
		parent::__construct( 'gtv_download_count', __( 'GTV > Download Count', 'gtvouchers-d' ), // Name
			array(
				'classname' => 'widget gtv_download_count',
				'description' => __( 'Display a counter of how many vouchers have been downloaded' )
			) // Args
		);

//		$widget_ops = array(
//			'classname'   => 'widget gtv_download_count',
//			'description' => __( 'Display a counter of how many vouchers have been downloaded' )
//		);
//		$this->WP_Widget( 'gtv_download_count', __( 'GTV > Download Count' ), $widget_ops );

		wp_enqueue_script( 'jQuery' );
	}

	public function form( $instance ) {
		$instance     = wp_parse_args( (array) $instance, array(
			'title' => __( 'Download Counter', 'gtvouchers-d' ),
			'message_text' => __( 'There have been %d vouchers downloaded.', 'gtvouchers-d' )
		) );
		$title        = strip_tags( $instance['title'] );
		$message_text = strip_tags( $instance['message_text'] );
		?>
		<p>
			<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:', 'gtvouchers-d' ); ?>
				<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text"
				       value="<?php echo esc_attr( $title ); ?>"/>
			</label>
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'message_text' ); ?>"><?php _e( 'Text:', 'gtvouchers-d' ); ?>
				<input class="widefat" id="<?php echo $this->get_field_id( 'message_text' ); ?>" name="<?php echo $this->get_field_name( 'message_text' ); ?>" type="text"
				       value="<?php echo esc_attr( $message_text ); ?>"/>
			</label>
			<?php _e( 'Enter your text. Put a %d where you would like the number.', 'gtvouchers-d' ); ?>
			<br/><em><?php _e( 'eg: There have been %d coupons downloaded', 'gtvouchers-d' ); ?></em>
		</p>
	<?php
	}

	public function update( $new_instance, $old_instance ) {
		$instance                 = $old_instance;
		$instance['title']        = strip_tags( $new_instance['title'] );
		$instance['message_text'] = strip_tags( $new_instance['message_text'] );

		return $instance;
	}

	public function widget( $args, $instance ) {
		global $cache_enabled;
		extract( $args, EXTR_SKIP );

		$title        = empty( $instance['title'] ) ? '&nbsp;' : apply_filters( 'widget_title', $instance['title'] );
		$message_text = $instance['message_text'];

		$ajax = false;
		include_once( ABSPATH . 'wp-admin/includes/plugin.php' );
		if ( is_plugin_active( 'wp-super-cache/wp-cache.php' ) && $cache_enabled ) {
			$ajax = true;
		}

		echo $before_widget;
		?>
		<h3> <?php echo $title; ?> </h3>
		<?php
		if ( $ajax ) {
			$nonce = wp_create_nonce( "gtv-get-download-count" );
			$link  = admin_url( 'admin-ajax.php' );
			echo '<script type="text/javascript">
            jQuery(document).ready(function($){
                $.ajax({
                    type : "POST",
                    url : "' . $link . '",
                    data : { nonce     : "' . $nonce . '",
                             action : "gtv_download_count",
                             text : "' . esc_js( $message_text ) . '"
                           },
                    success : function(response) {
                        // The server has finished executing PHP and has returned something,
                        // so display it!
                        $("#download-count").html(response);
                    }
                });
            });
        </script>';

			echo '<div id="download-count"></div>';
		} else {
			gtv_get_download_count_text( $message_text );
		}

		echo $after_widget;
	}
}


/**
 * GT-Vouchers popular posts widget *
 **/
class gtv_popular_postview extends WP_Widget{

	function __construct() {
		//Constructor
		parent::__construct( 'voucher_popular_post_view', __( 'GTV > Popular Post View', 'gtvouchers-d' ), // Name
			array(
				'classname' => 'gtv_popular_post_view',
				'description' => __( 'GTV > Popular Post View', GEODIRECTORY_TEXTDOMAIN )
			) // Args
		);
	}


	function widget( $args, $instance ) {

		// prints the widget
		extract( $args, EXTR_SKIP );

		echo $before_widget;

		$title               = empty( $instance['title'] ) ? ucwords( $instance['category_title'] ) : apply_filters( 'widget_title', __( $instance['title'], GEODIRECTORY_TEXTDOMAIN ) );
		$post_type           = empty( $instance['post_type'] ) ? 'gd_place' : apply_filters( 'widget_post_type', $instance['post_type'] );
		$category            = empty( $instance['category'] ) ? '0' : apply_filters( 'widget_category', $instance['category'] );
		$post_number         = empty( $instance['post_number'] ) ? '5' : apply_filters( 'widget_post_number', $instance['post_number'] );
		$layout              = empty( $instance['layout'] ) ? 'gridview_onehalf' : apply_filters( 'widget_layout', $instance['layout'] );
		$add_location_filter = empty( $instance['add_location_filter'] ) ? '0' : apply_filters( 'widget_layout', $instance['add_location_filter'] );
//        $listing_width = empty( $instance['listing_width'] ) ? '' : apply_filters( 'widget_layout', $instance['listing_width'] );
		$list_sort = empty( $instance['list_sort'] ) ? 'latest' : apply_filters( 'widget_list_sort', $instance['list_sort'] );

		if ( isset( $instance['character_count'] ) ) {
			$character_count = apply_filters( 'widget_list_character_count', $instance['character_count'] );
		} else {
			$character_count = '';
		}

		if ( empty( $title ) || $title == 'All' ) {
			$title .= ' ' . get_post_type_plural_label( $post_type );
		}

		$location_url = array();
		$city         = get_query_var( 'gd_city' );
		if ( !empty( $city ) ) {

			if ( get_option( 'geodir_show_location_url' ) == 'all' ) {
				$country = get_query_var( 'gd_country' );
				$region  = get_query_var( 'gd_region' );
				if ( !empty( $country ) ) {
					$location_url[] = $country;
				}

				if ( !empty( $region ) ) {
					$location_url[] = $region;
				}
			}
			$location_url[] = $city;
		}

		$location_url = implode( "/", $location_url );


		if ( get_option( 'permalink_structure' ) ) {
			$viewall_url = get_post_type_archive_link( $post_type );
		} else {
			$viewall_url = get_post_type_archive_link( $post_type );
		}


		if ( !empty( $category ) && $category[0] != '0' ) {
			global $geodir_add_location_url;
			$geodir_add_location_url = '0';
			if ( $add_location_filter != '0' ) {
				$geodir_add_location_url = '1';
			}
			$viewall_url             = get_term_link( (int) $category[0], $post_type . 'category' );
			$geodir_add_location_url = null;
		}

		?>
		<div class="geodir_locations geodir_location_listing">
			<?php do_action( 'geodir_before_view_all_link_in_widget' ); ?>
			<div class="geodir_list_heading clearfix">
				<?php echo $before_title . $title . $after_title; ?>
				<a href="<?php echo $viewall_url; ?>" class="geodir-viewall">
					<?php _e( 'View all', GEODIRECTORY_TEXTDOMAIN ); ?>
				</a>
			</div>
			<?php do_action( 'geodir_after_view_all_link_in_widget' ); ?>
			<?php
			$query_args = array(
				'posts_per_page' => $post_number,
				'is_geodir_loop' => true,
				'gd_location' => ( $add_location_filter ) ? true : false,
				'post_type' => $post_type,
				'order_by' => $list_sort,
				'excerpt_length' => $character_count,
			);

			if ( !empty( $category ) && $category[0] != '0' ) {

				$category_taxonomy = geodir_get_taxonomies( $post_type );

				######### WPML #########
				if ( function_exists( 'icl_object_id' ) ) {
					$category = gd_lang_object_ids( $category, $category_taxonomy[0] );
				}
				######### WPML #########

				$tax_query = array(
					'taxonomy' => $category_taxonomy[0],
					'field' => 'id',
					'terms' => $category
				);

				$query_args['tax_query'] = array( $tax_query );
			}

			global $gridview_columns;

			query_posts( $query_args );

			if ( strstr( $layout, 'gridview' ) ) {

				$listing_view_exp = explode( '_', $layout );

				$gridview_columns = $layout;

				$layout = $listing_view_exp[0];

			}

			$template = apply_filters( "geodir_template_part-listing-listview", geodir_plugin_path() . '/geodirectory-templates/listing-listview.php' );

			include( $template );

			wp_reset_query();
			?>

		</div>


		<?php

		echo $after_widget;
	}

	function update( $new_instance, $old_instance ) {
		//save the widget
		$instance = $old_instance;

		if ( $new_instance['title'] == '' ) {
			$title = ucwords( strip_tags( $new_instance['category_title'] ) );
			//$instance['title'] = $title;
		}
		$instance['title'] = strip_tags( $new_instance['title'] );

		$instance['post_type'] = strip_tags( $new_instance['post_type'] );
		//$instance['category'] = strip_tags($new_instance['category']);
		$instance['category']        = isset( $new_instance['category'] ) ? $new_instance['category'] : '';
		$instance['category_title']  = strip_tags( $new_instance['category_title'] );
		$instance['post_number']     = strip_tags( $new_instance['post_number'] );
		$instance['layout']          = strip_tags( $new_instance['layout'] );
		$instance['listing_width']   = strip_tags( $new_instance['listing_width'] );
		$instance['list_sort']       = strip_tags( $new_instance['list_sort'] );
		$instance['character_count'] = $new_instance['character_count'];
		if ( isset( $new_instance['add_location_filter'] ) && $new_instance['add_location_filter'] != '' ) {
			$instance['add_location_filter'] = strip_tags( $new_instance['add_location_filter'] );
		} else {
			$instance['add_location_filter'] = '0';
		}


		return $instance;
	}

	function form( $instance ) {
		//widgetform in backend
		$instance = wp_parse_args( (array) $instance, array(
			'title' => '',
			'post_type' => '',
			'category' => array(),
			'category_title' => '',
			'list_sort' => '',
			'list_order' => '',
			'post_number' => '5',
			'layout' => 'gridview_onehalf',
			'listing_width' => '',
			'add_location_filter' => '1',
			'character_count' => '20'
		) );

		$title = strip_tags( $instance['title'] );

		$post_type = strip_tags( $instance['post_type'] );

		$category = $instance['category'];

		$category_title = strip_tags( $instance['category_title'] );

		$list_sort = strip_tags( $instance['list_sort'] );

		$list_order = strip_tags( $instance['list_order'] );

		$post_number = strip_tags( $instance['post_number'] );

		$layout = strip_tags( $instance['layout'] );

		$listing_width = strip_tags( $instance['listing_width'] );

		$add_location_filter = strip_tags( $instance['add_location_filter'] );

		$character_count = $instance['character_count'];

		?>

		<p>
			<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:', GEODIRECTORY_TEXTDOMAIN ); ?>

				<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>"/>
			</label>
		</p>

		<p>
			<label for="<?php echo $this->get_field_id( 'post_type' ); ?>"><?php _e( 'Post Type:', GEODIRECTORY_TEXTDOMAIN ); ?>

				<?php $postypes = geodir_get_posttypes();
				$postypes       = apply_filters( 'geodir_post_type_list_in_p_widget', $postypes ); ?>

				<select class="widefat" id="<?php echo $this->get_field_id( 'post_type' ); ?>" name="<?php echo $this->get_field_name( 'post_type' ); ?>" onchange="geodir_change_category_list(this)">

					<?php foreach ( $postypes as $postypes_obj ) { ?>

						<option <?php if ( $post_type == $postypes_obj ) {
							echo 'selected="selected"';
						} ?> value="<?php echo $postypes_obj; ?>"><?php $extvalue = explode( '_', $postypes_obj );
							echo ucfirst( $extvalue[1] ); ?></option>

					<?php } ?>

				</select>
			</label>
		</p>


		<p id="post_type_cats">
			<label for="<?php echo $this->get_field_id( 'category' ); ?>"><?php _e( 'Post Category:', GEODIRECTORY_TEXTDOMAIN ); ?>

				<?php

				$post_type = ( $post_type != '' ) ? $post_type : 'gd_place';

				$all_postypes = geodir_get_posttypes();

				if ( !in_array( $post_type, $all_postypes ) ) {
					$post_type = 'gd_place';
				}

				$category_taxonomy = geodir_get_taxonomies( $post_type );
				$categories        = get_terms( $category_taxonomy, array(
					'orderby' => 'count',
					'order' => 'DESC'
				) );

				?>

				<select multiple="multiple" class="widefat" name="<?php echo $this->get_field_name( 'category' ); ?>[]" onchange="geodir_popular_widget_cat_title(this)">

					<option <?php if ( is_array( $category ) && in_array( '0', $category ) ) {
						echo 'selected="selected"';
					} ?> value="0"><?php _e( 'All', GEODIRECTORY_TEXTDOMAIN ); ?></option>
					<?php foreach ( $categories as $category_obj ) {
						$selected = '';
						if ( is_array( $category ) && in_array( $category_obj->term_id, $category ) ) {
							echo $selected = 'selected="selected"';
						}

						?>

						<option <?php echo $selected; ?> value="<?php echo $category_obj->term_id; ?>"><?php echo ucfirst( $category_obj->name ); ?></option>

					<?php } ?>

				</select>


				<input type="hidden" name="<?php echo $this->get_field_name( 'category_title' ); ?>" id="<?php echo $this->get_field_id( 'category_title' ); ?>"
				       value="<?php if ( $category_title != '' ) {
					       echo $category_title;
				       } else {
					       echo __( 'All', GEODIRECTORY_TEXTDOMAIN );
				       } ?>"/>

			</label>
		</p>

		<p>
			<label for="<?php echo $this->get_field_id( 'list_sort' ); ?>"><?php _e( 'Sort by:', GEODIRECTORY_TEXTDOMAIN ); ?>

				<select class="widefat" id="<?php echo $this->get_field_id( 'list_sort' ); ?>" name="<?php echo $this->get_field_name( 'list_sort' ); ?>">

					<option <?php if ( $list_sort == 'latest' ) {
						echo 'selected="selected"';
					} ?> value="latest"><?php _e( 'Latest', GEODIRECTORY_TEXTDOMAIN ); ?></option>

					<option <?php if ( $list_sort == 'featured' ) {
						echo 'selected="selected"';
					} ?> value="featured"><?php _e( 'Featured', GEODIRECTORY_TEXTDOMAIN ); ?></option>

					<option <?php if ( $list_sort == 'high_review' ) {
						echo 'selected="selected"';
					} ?> value="high_review"><?php _e( 'Review', GEODIRECTORY_TEXTDOMAIN ); ?></option>

					<option <?php if ( $list_sort == 'high_rating' ) {
						echo 'selected="selected"';
					} ?> value="high_rating"><?php _e( 'Rating', GEODIRECTORY_TEXTDOMAIN ); ?></option>

					<option <?php if ( $list_sort == 'random' ) {
						echo 'selected="selected"';
					} ?> value="random"><?php _e( 'Random', GEODIRECTORY_TEXTDOMAIN ); ?></option>

				</select>
			</label>
		</p>

		<p>

			<label for="<?php echo $this->get_field_id( 'post_number' ); ?>"><?php _e( 'Number of posts:', GEODIRECTORY_TEXTDOMAIN ); ?>

				<input class="widefat" id="<?php echo $this->get_field_id( 'post_number' ); ?>" name="<?php echo $this->get_field_name( 'post_number' ); ?>" type="text"
				       value="<?php echo esc_attr( $post_number ); ?>"/>
			</label>
		</p>

		<p>
			<label for="<?php echo $this->get_field_id( 'layout' ); ?>">
				<?php _e( 'Layout:', GEODIRECTORY_TEXTDOMAIN ); ?>
				<select class="widefat" id="<?php echo $this->get_field_id( 'layout' ); ?>" name="<?php echo $this->get_field_name( 'layout' ); ?>">
					<option <?php if ( $layout == 'gridview_onehalf' ) {
						echo 'selected="selected"';
					} ?> value="gridview_onehalf"><?php _e( 'Grid View (Two Columns)', GEODIRECTORY_TEXTDOMAIN ); ?></option>
					<option <?php if ( $layout == 'gridview_onethird' ) {
						echo 'selected="selected"';
					} ?> value="gridview_onethird"><?php _e( 'Grid View (Three Columns)', GEODIRECTORY_TEXTDOMAIN ); ?></option>
					<option <?php if ( $layout == 'gridview_onefourth' ) {
						echo 'selected="selected"';
					} ?> value="gridview_onefourth"><?php _e( 'Grid View (Four Columns)', GEODIRECTORY_TEXTDOMAIN ); ?></option>
					<option <?php if ( $layout == 'gridview_onefifth' ) {
						echo 'selected="selected"';
					} ?> value="gridview_onefifth"><?php _e( 'Grid View (Five Columns)', GEODIRECTORY_TEXTDOMAIN ); ?></option>
					<option <?php if ( $layout == 'list' ) {
						echo 'selected="selected"';
					} ?> value="list"><?php _e( 'List view', GEODIRECTORY_TEXTDOMAIN ); ?></option>

				</select>
			</label>
		</p>

		<p>
			<label for="<?php echo $this->get_field_id( 'listing_width' ); ?>"><?php _e( 'Listing width:', GEODIRECTORY_TEXTDOMAIN ); ?>

				<input class="widefat" id="<?php echo $this->get_field_id( 'listing_width' ); ?>" name="<?php echo $this->get_field_name( 'listing_width' ); ?>" type="text"
				       value="<?php echo esc_attr( $listing_width ); ?>"/>
			</label>
		</p>

		<p>
			<label for="<?php echo $this->get_field_id( 'character_count' ); ?>"><?php _e( 'Post Content excerpt character count :', GEODIRECTORY_TEXTDOMAIN ); ?>
				<input class="widefat" id="<?php echo $this->get_field_id( 'character_count' ); ?>" name="<?php echo $this->get_field_name( 'character_count' ); ?>" type="text"
				       value="<?php echo esc_attr( $character_count ); ?>"/>
			</label>
		</p>

		<p>
			<label for="<?php echo $this->get_field_id( 'add_location_filter' ); ?>">
				<?php _e( 'Enable Location Filter:', GEODIRECTORY_TEXTDOMAIN ); ?>
				<input type="checkbox" id="<?php echo $this->get_field_id( 'add_location_filter' ); ?>"
				       name="<?php echo $this->get_field_name( 'add_location_filter' ); ?>" <?php if ( $add_location_filter ) {
					echo 'checked="checked"';
				} ?>  value="1"/>
			</label>
		</p>


		<script type="text/javascript">

			function geodir_popular_widget_cat_title(val) {

				jQuery(val).find("option:selected").each(function (i) {
					if (i == 0)
						jQuery(val).closest('form').find('#post_type_cats input').val(jQuery(this).html());

				});

			}

			function geodir_change_category_list(obj, selected) {
				var post_type = obj.value;

				var ajax_url = '<?php echo geodir_get_ajax_url(); ?>'

				var myurl = ajax_url + "&geodir_ajax=admin_ajax&ajax_action=get_cat_dl&post_type=" + post_type + "&selected=" + selected;

				jQuery.ajax({
					type: "GET",
					url: myurl,
					success: function (data) {

						jQuery(obj).closest('form').find('#post_type_cats select').html(data);

					}
				});

			}

			<?php if(is_active_widget( false, false, $this->id_base, true )){ ?>
			var post_type = jQuery('#<?php echo $this->get_field_id('post_type'); ?>').val();

			<?php } ?>

		</script>

	<?php
	}
}

register_widget( 'gtv_newest_vouchers' );
register_widget( 'gtv_popular_vouchers' );
register_widget( 'gtv_download_count' );
register_widget( 'gtv_popular_postview' );
