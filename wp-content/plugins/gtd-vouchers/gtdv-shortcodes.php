<?php
// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

// [voucher id="" preview=""]
add_shortcode( 'voucher', 'voucher_do_voucher_shortcode' );
// [voucherform id=""]
add_shortcode( 'voucherform', 'voucher_do_voucher_form_shortcode' );
// [voucherlist]
add_shortcode( 'voucherlist', 'voucher_do_list_shortcode' );

// process a shortcode for a voucher
function voucher_do_voucher_shortcode( $atts ) {
	extract( shortcode_atts( array(
		'id'          => '',
		'preview'     => '',
		'description' => 'false',
	), $atts ) );

	$r = '';

	$id = intval( $id );

	if ( '' != $id && 0 < $id ) {
		$voucher = gtvouchers_get_voucher( $id );

		if ( $voucher && ( ( '' == $voucher->expiry || 0 == intval( $voucher->expiry ) || time() < intval( $voucher->expiry ) ) ) && ( '' == $voucher->startdate || 0 == intval( $voucher->startdate ) || time() > intval( $voucher->startdate ) ) ) {
			if ( function_exists( 'gtvouchers_make_public_preview' ) ) {
				$r .= gtvouchers_make_public_preview( $voucher );
			}

			if ( 'true' == strtolower( $description ) ) {
				$r .= ' ' . htmlspecialchars( $voucher->description );
			}

			return $r;
		} else {
			$r = '<!-- The shortcode for voucher ' . $id . ' is displaying nothing because the voucher was not found, or the expiry date has passed, or the start date is still in the future';
			if ( $voucher ) {
				$r .= sprintf( __('. %s found, expiry: %s, start date: ', 'gtd-vouchers'), $options['replace_voucher_plural_word'], $voucher->expiry, $voucher->startdate );
			}
			$r .= ' -->';

			return $r;
		}
	}

	return $r;
}

// process a shortcode for a voucher form
function voucher_do_voucher_form_shortcode( $atts ) {
	extract( shortcode_atts( array( 'id' => '' ), $atts ) );
	if ( $id != '' ) {
		$id      = intval( $id );
		$voucher = gtvouchers_get_voucher( $id );
		if ( $voucher && ( ( '' == $voucher->expiry || 0 == intval( $voucher->expiry ) || time() < intval( $voucher->expiry ) ) ) && ( '' == $voucher->startdate || 0 == intval( $voucher->startdate ) || time() > intval( $voucher->startdate ) ) ) {
			return gtvouchers_register_form( $voucher->guid, true );
		} else {
			$r = '<!-- The form shortcode for voucher ' . $id . ' is displaying nothing because the voucher was not found, or the expiry date has passed, or the start date is still in the future';
			if ( $voucher ) {
				$r .= sprintf( __('. %s found, expiry: %s, start date: ', 'gtd-vouchers'), $options['replace_voucher_plural_word'], $voucher->expiry, $voucher->startdate );
			}
			$r .= ' -->';
		}
	}
}

// process a shortcode for a list of vouchers
function voucher_do_list_shortcode( $atts ) {
	extract( shortcode_atts( array(
		'description' => 'false',
		'category'    => '',
		'showexpiry'  => 'false',
		'sort'        => 'natural',
		'preview'     => 'false',
	), $atts ) );


	$options       = get_option( 'gtvouchers_options' );
	$date_format   = get_option( 'date_format' );
	$voucher_cache = plugins_url( 'cache', __FILE__ );

	/*
    * JR: Update - now use 3rd parameter to pass location to gtvouchers_get_vouchers
    * So we know if we are front or back. Obviously shortcode is always FRONT so we ignore
    * the author restriction.
    */

	$vouchers = gtvouchers_get_vouchers( 25, false, 'front', $category, $sort );

	if ( $vouchers && is_array( $vouchers ) && 0 < count( $vouchers ) ) {

		$r = "<div class=\"voucherlist\">\n";

		foreach ( $vouchers as $voucher ) {
			if ( 'show' == $options['maxxed_downloads'] || ( 0 == $voucher->limit ) || ( ( 0 < $voucher->limit ) && ( $voucher->limit > $voucher->downloads ) ) ) {
				if ( $voucher->require_like ) {
					$link = get_post_permalink( $voucher->place_id ) . '#v' . $voucher->id;
				} else {
					$link = gtvouchers_link( $voucher->guid );
				}
				$r .= '<div class="voucher-list-div">';

				if ( 'true' == strtolower( $preview ) ) {
					if ( ! ( isset( $voucher->use_custom_image ) ) || 0 == $voucher->use_custom_image ) {
						if ( ! file_exists( plugin_dir_path( __FILE__ ) . '/cache/' . $voucher->id . '_t_cache.jpg' ) ) {
							gtvouchers_generate_voucher_cache_image( $voucher, 'thumb' );
						}

						if ( ! gtv_is_voucher_cache_valid( $voucher->id, 'thumb' ) ) {
							gtvouchers_generate_voucher_cache_image( $voucher, 'thumb' );
						}
					}

					if ( ( isset( $voucher->use_custom_image ) ) && 1 == $voucher->use_custom_image ) {
						$r .= '<img src="' . esc_url( $template ) . '" class="voucherlist_preview" />';
					} else {
						$time = substr( ( '' . time() ), ( strlen( time() ) - 5 ), 5 );

						$r .= '<img src="' . $voucher_cache . '/' . $voucher->id . '_t_cache.jpg?ser=' . $time . '" class="voucherlist_preview" />';
					}
				}

				$r .= '<a href="' . $link . '" class="gtvlist-voucher">' . htmlspecialchars( $voucher->name ) . '</a>';
				if ( isset( $voucher->post_title ) ) {
					$r .= ' ' . __( 'from', 'gtvouchers-d' ) . ' ';
					$r .= '<a href="' . get_post_permalink( $voucher->place_id ) . '" class="gtvlist-link">' . $voucher->post_title . '</a><br />';
				}
				if ( 'true' == strtolower( $description ) ) {
					$r .= '<span class="gtvlist-description">' . htmlspecialchars( stripslashes( $voucher->description ) ) . '</span>';
				}

				if ( 'true' == strtolower( $showexpiry ) && 0 < $voucher->expiry ) {
					$r .= '<span class="gtvlist-expiry"> ' . __( 'Expires', 'gtvouchers-d' ) . ' ' . date_i18n( $date_format, $voucher->expiry ) . '</span>';
				}
				$r .= '</div>';
			}

		}

		$r .= '</div>';

		return $r;
	}

}
