<?php

function gtvouchers_verify_template_location(){

	$uploads = wp_upload_dir();
	$source = plugin_dir_path( __FILE__ ) . 'templates';
	$destination = $uploads['basedir'] . '/voucher_templates/';

	$check_first = get_option( 'gt-vouchers-moved' );

	if( '1' != $check_first ){
		gtvtm_move( $source, $destination, true );
	}

}

function gtvtm_exists($file) {
	return @file_exists($file);
}

function gtvtm_move($source, $destination, $overwrite = false) {
	if ( !$overwrite && gtvtm_exists( $destination ) ){
		add_action( 'admin_notices', 'gtvtm_destination_exists_message' );
		update_option( 'gt-vouchers-moved', true );
		return false;
	}

	// try using rename first. if that fails (for example, source is read only) try copy
	if ( @rename($source, $destination) ){
		add_action( 'admin_notices', 'gtvtm_transfer_worked_message' );
		update_option( 'gt-vouchers-moved', true );
		return true;
	}

	if ( gtvtm_copy_dir( $source, $destination ) && gtvtm_exists( $destination ) ) {
		add_action( 'admin_notices', 'gtvtm_transfer_worked_message' );
		update_option( 'gt-vouchers-moved', true );
		return true;
	} else {
		add_action( 'admin_notices', 'gtvtm_transfer_failed_message' );
		update_option( 'gt-vouchers-moved', true );
		return false;
	}
}

function gtvtm_copy_dir( $source, $destination ){

    if( is_dir( $source ) && $dir = opendir( $source ) ){
	    @mkdir( $destination );
	    while( false !== ( $file = readdir( $dir ) ) ) {
	        if ( ( $file != '.' ) && ( $file != '..' ) ) {
	            if ( is_dir( $source . '/' . $file ) ) {
	                gtvtm_copy_dir( $source . '/' . $file, $destination . '/' . $file );
	            }
	            else {
	                copy( $source . '/' . $file, $destination . '/' . $file );
	            }
	        }
	    }
	    closedir( $dir );
    	return true;
    }
    return false;
}

function gtvtm_destination_exists_message(){
	echo '<div class="error"><p>' . __( 'Destination folder exists. Transfer aborted.', 'gtvouchers-d' ) . '</p></div>';
}

function gtvtm_transfer_worked_message(){
	echo '<div class="updated"><p>' . __( 'Transfer complete.', 'gtvouchers-d' ) . '</p></div>';
}

function gtvtm_transfer_failed_message(){
	echo '<div class="error"><p>' . __( 'Transfer failed. Please move the templates directory to wp-content/uploads and rename it to voucher-templates.', 'gtvouchers-d' ) . '</p></div>';
}
