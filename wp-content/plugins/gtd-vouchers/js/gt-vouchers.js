jQuery(document).ready(function () {
    gtv_set_preview_font();
    gtv_set_preview_color();
    jQuery("#voucherthumbs img").on("click", gtv_set_preview);
    jQuery(".checkbox").on("click", gtv_set_template_deleted);
    jQuery("#font").on("change", gtv_set_preview_font);
    jQuery("#name").on("keyup", gtv_limit_text);
    jQuery("#text").on("keyup", gtv_limit_text);
    jQuery("#terms").on("keyup", gtv_limit_text);
    jQuery("#previewbutton").on("click", gtv_preview_voucher);
    jQuery("#savebutton").on("click", gtv_save_voucher);
    jQuery("a.templatepreview").on("click", gtv_new_window);
    jQuery("#randomcodes").on("click", gtv_check_random);
    jQuery("#sequentialcodes").on("click", gtv_check_sequential);
    jQuery("#customcodes").on("click", gtv_check_custom);
    jQuery("#singlecode").on("click", gtv_check_single);
    jQuery("#showshortcodes").on("click", gtv_toggle_shortcodes);
    jQuery("#delete").on("change", gtv_toggle_deletion);
    jQuery('#use_custom_image').on("click", gtv_toggle_design_section);
    jQuery('#delete-vouchers').on("click", gtvDeleteVouchers);

    gtv_hide_form_options();
    gtv_toggle_design_section();

    jQuery('#voucherpreview textarea').focus(function () {
        var $this = jQuery(this);
        var thisID = jQuery(this).attr('id');

        switch (thisID) {
            case 'name':
                if (gtvDefaultText.name === $this.val()) {
                    $this.val('');
                }
                break;
            case 'text':
                if (gtvDefaultText.text === $this.val()) {
                    $this.val('');
                }
                break;
            case 'terms':
                if (gtvDefaultText.terms === $this.val()) {
                    $this.val('');
                }
                break;
        }
    });

    jQuery('#voucherpreview textarea').blur(function () {
        var $this = jQuery(this);
        var thisID = jQuery(this).attr('id');

        switch (thisID) {
            case 'name':
                if ($this.val() === '') {
                    $this.val(gtvDefaultText.name);
                }
                break;
            case 'text':
                if ($this.val() === '') {
                    $this.val(gtvDefaultText.text);
                }
                break;
            case 'terms':
                if ($this.val() === '') {
                    $this.val(gtvDefaultText.terms);
                }
                break;
        }
    });

    if (jQuery('#singlecode').is(':checked')) {
        gtv_show_single();
    }

    if (jQuery('#sequentialcodes').is(':checked')) {
        gtv_show_sequential();
    }

    if (jQuery('#randomcodes').is(':checked')) {
        gtv_show_random();
    }

    if (jQuery('#customcodes').is(':checked')) {
        gtv_show_custom();
    }


    var editlink = jQuery(".company_info .edit-link");
    var borderColor = jQuery("#bordercolor");
    var allTextColor = jQuery("#alltextcolor");
    var voucherTextColor = jQuery("#vouchertextcolor");
    var headerBackgroundColor = jQuery("#headerbackgroundcolor");

//	if ( editlink.length ){
//		gtv_add_edit_link();
//	}

    if (borderColor.length) {
        if (typeof jQuery.wp === 'object' && typeof jQuery.wp.wpColorPicker === 'function') {
            jQuery('#bordercolor').wpColorPicker();
        }
        else {
            //We use farbtastic if the WordPress color picker widget doesn't exist
            jQuery('#colorpicker').farbtastic('#bordercolor');
        }
    }

    if (allTextColor.length) {
        if (typeof jQuery.wp === 'object' && typeof jQuery.wp.wpColorPicker === 'function') {
            jQuery('#alltextcolor').wpColorPicker();
        }
        else {
            //We use farbtastic if the WordPress color picker widget doesn't exist
            jQuery('#colorpicker2').farbtastic('#alltextcolor');
        }
    }

    if (headerBackgroundColor.length) {
        if (typeof jQuery.wp === 'object' && typeof jQuery.wp.wpColorPicker === 'function') {
            jQuery('#headerbackgroundcolor').wpColorPicker({
                change: function (event, ui) {
                    pickColor(ui.color.toString());
                }
            });
        }
    }

    if (voucherTextColor.length) {
        if (typeof jQuery.wp === 'object' && typeof jQuery.wp.wpColorPicker === 'function') {
            jQuery('#vouchertextcolor').wpColorPicker({
                change: function (event, ui) {
                    pickColor(ui.color.toString());
                }
            });
        }
        else {
            //We use farbtastic if the WordPress color picker widget doesn't exist
            jQuery.farbtastic('#colorpicker3', function (color) {
                pickColor(color);
            });
        }
        pickColor(jQuery('#vouchertextcolor').val());
    }

    // Prepare the variable that holds our custom media manager.
    var gtv_media_frame;

    if (typeof jQuery.wp === 'object' && typeof jQuery.wp.wpColorPicker === 'function') {
        // Bind to our click event in order to open up the new media experience.
        jQuery(document.body).on('click.gtvOpenMediaManager', '.gtv_icon_upload', function (e) {
            // Prevent the default action from occuring.
            e.preventDefault();

            // If the frame already exists, re-open it.
            if (gtv_media_frame) {
                gtv_media_frame.open();
                return;
            }

            gtv_media_frame = wp.media.frames.gtv_media_frame = wp.media({

                className: 'media-frame gtv-media-frame',

                frame: 'select',

                multiple: false,

                title: gtvNewMedia.title,

                library: {
                    type: 'image'
                },

                button: {
                    text: gtvNewMedia.button
                }
            });
            gtv_media_frame.on('select', function () {
                // Grab our attachment selection and construct a JSON representation of the model.
                var media_attachment = gtv_media_frame.state().get('selection').first().toJSON();

                // Send the attachment URL to our custom input field via jQuery.
                jQuery('#upload_image').val(media_attachment.url);
            });

            // Now that everything has been set, let's open up the frame.
            gtv_media_frame.open();
        });
    }
});
function gtv_toggle_deletion(e) {
    if (this.checked) {
        jQuery("#previewbutton").hide();
        jQuery("#savebutton").val("Delete voucher");
    } else {
        jQuery("#previewbutton").show();
        jQuery("#savebutton").val("Save");
    }
}
function gtv_toggle_shortcodes(e) {
    jQuery("#shortcodes").toggle();
    e.preventDefault();
    return false;
}
function gtv_hide_form_options() {
    jQuery(".hider").hide();
}
function gtv_check_random(e) {
    gtv_hide_form_options();
    if (this.checked) {
        gtv_show_random();
    }
}
function gtv_show_random() {
//	jQuery("#codelengthline").show();
    jQuery("#codeprefixline").show();
    jQuery("#codesuffixline").show();
}
function gtv_check_sequential(e) {
    gtv_hide_form_options();
    if (this.checked) {
        gtv_show_sequential();
    }
}
function gtv_show_sequential() {
    jQuery("#codeprefixline").show();
    jQuery("#codesuffixline").show();
}
function gtv_check_custom(e) {
    gtv_hide_form_options();
    if (this.checked) {
        gtv_show_custom();
    }
}
function gtv_show_custom() {
    jQuery("#customcodelistline").show();
    jQuery("#codeprefixline").hide();
    jQuery("#codesuffixline").hide();
}
function gtv_check_single(e) {
    gtv_hide_form_options();
    if (this.checked) {
        gtv_show_single();
    }
}
function gtv_show_single() {
    jQuery("#singlecodetextline").show();
    jQuery("#codeprefixline").hide();
    jQuery("#codesuffixline").hide();
}
function gtv_new_window(e) {
    jQuery(this).attr("target", "_blank");
}
function gtv_preview_voucher(e) {
    var form = jQuery("#voucherform");
    form.attr("action", form.attr("action") + "&preview=voucher");
    form.attr("target", "_blank");
    form.submit();
}
function gtv_save_voucher(e) {
    var form = jQuery("#voucherform");
    form.attr("action", form.attr("action").replace("&preview=voucher", ""));
    form.attr("target", "");
    form.submit();
}
function gtv_set_preview(e) {
    var id = this.id.replace("template_", "");
    var preview = "url(" + gtvTemplate.voucherPath + "/" + id + "_preview.jpg)";
    jQuery("#voucherpreview").css("background-image", preview);
    jQuery("#template").val(id);
}
function gtv_set_template_deleted(e) {
    var td = jQuery(this).parent().get(0);
    var tr = jQuery(td).parent().get(0);
    jQuery(tr).toggleClass("deleted");
}
function gtv_set_preview_color(color) {

    jQuery("#voucherpreview").css("color", color);
    jQuery("#voucherpreview h2 textarea").css("color", color);
    jQuery("#voucherpreview p textarea").css("color", color);
    jQuery("#voucherpreview p").css("color", color);
}
function gtv_set_preview_font(e) {
    var font = jQuery("#font :selected").val();
    jQuery("#voucherpreview h2 textarea").attr("class", font);
    jQuery("#voucherpreview p textarea").attr("class", font);
    jQuery("#voucherpreview p").attr("class", font);
}
function gtv_limit_text(e) {
    var limit = 30;
    var el = jQuery(this);
    if (el.attr("id") == "text") limit = 200;
    if (el.attr("id") == "terms") limit = 300;
    var length = el.val().length;
    if (parseFloat(length) >= parseFloat(limit)) {
        // if this is a character key, stop it being entered
        var key = gtv_keycode(e) || e.code;
        if (key != 8 && key != 46 && key != 37 && key != 39) {
            el.val(el.val().substr(0, limit));
            e.preventDefault();
            e.stopPropagation();
            return false;
        }
    }
}
// return the keycode for this event
function gtv_keycode(e) {
    if (window.event) {
        return window.event.keyCode;
    } else if (e) {
        return e.which;
    } else {
        return false;
    }
}

function gtv_add_edit_link() {
    if (typeof gtVouchers != "undefined") {
        var editLink = jQuery(".company_info .edit-link").filter(":first");

        editLink.after(gtVouchers.editCode);
    }
}

var ucinamefield = '<div id="labelNewName"><label for="name">Voucher Name:</label><input type="text" name="newname" id="newname" value="" /><br /><br /></div>';

function gtv_toggle_design_section() {
    if (jQuery('#use_custom_image').is(':checked')) {
        jQuery('#voucher_design').hide('slow');
        jQuery('#voucher_codes').hide('slow');
        jQuery("#custom_image").show('slow');
        jQuery("#uci").before(ucinamefield);
        jQuery('#newname').on('blur', copyName);
    } else {
        jQuery("#custom_image").hide('slow');
        jQuery('#labelNewName').remove();
        jQuery('#voucher_design').show('slow');
        jQuery('#voucher_codes').show('slow');
    }
}

function copyName() {
    var newName = jQuery('#newname').val();
    jQuery('#name').val(newName);
}

pickColor = function (color) {
//		farbtastic.setColor(color);
    jQuery('#vouchertextcolor').val(color);
    jQuery('#vouchertextcolor').css('background-color', color);
    gtv_set_preview_color(color);
};

function printPage(url) {
    printWin = window.open(url + "&print=print", "printWin", "left=0,top=0,width=800,height=800,toolbar=0,scrollbars=0,status=0");
    printWin.focus();
    setTimeout(function () {
        printWin.print()
    }, 3000);
    return false;
}

function gtvDeleteVouchers() {
    var delvouchers = jQuery('input[name^="delete"]:checked').serialize();
    if (0 < delvouchers.length) {
        jQuery('#voucherlist').submit();

    } else {
        alert("No vouchers selected");
        return false;
    }
//   alert("Deleting");
}
