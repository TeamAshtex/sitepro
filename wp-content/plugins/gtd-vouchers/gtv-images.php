<?php

// If this file is called directly, abort.
if ( !defined( 'WPINC' ) )
{
    die;
}

function gtv_is_voucher_cache_valid( $id, $size = 'full' )
{

    if ( !is_numeric( $id ) )
    {
	return false;
    }

    if ( 'full' == $size )
    {
	$changetime = filemtime( plugin_dir_path( __FILE__ ) . 'cache/' . $id . '_cache.jpg' );
    }
    else
    {
	$changetime = filemtime( plugin_dir_path( __FILE__ ) . 'cache/' . $id . '_t_cache.jpg' );
    }

    if ( time() - $changetime >= ( GTV_CACHE_TTL ) )
    {
	return false;
    }

    return true;
}

function gtvouchers_generate_voucher_cache_image( $voucher, $image_size = 'full' )
{
    if ( empty( $voucher ) )
    {
	return;
    }

    $options = get_option( 'gtvouchers_options' );

    require_once( 'lib/WideImage.php' );

    $template = GTV_TEMPLATE_PATH . '/1_preview.jpg'; // ' . $voucher->template . '

  
    /* Attempt to open */
    try
    {
	$v_image = @WideImage::load( $template )->resize( 800, 360 );
    }
    catch ( Exception $e )
    {
	$v_image = false;
    }

    /* See if it failed */
    if ( false != $v_image )
    {

	$sizes = array(
	    'title' => 26,
	    'body' => 14,
	    'registered' => 12,
	    'terms' => 9,
	    'expiry' => 12,
	);

	switch ( $voucher->font )
	{
	    case 'times':
		$font = '/fonts/DejaVuSerif.ttf';
		break;
	    case 'timesb':
		$font = '/fonts/DejaVuSerif-Bold.ttf';
//				$sizes['body']  = 14;
//				$sizes['terms'] = 7;
		break;
	    case 'aefurat':
		$font = '/fonts/ae_Furat.ttf';
//				$sizes['body'] = 12;
		break;
	    case 'helvetica':
		$font = '/fonts/DejaVuSans.ttf';
		break;
	    case 'helveticab':
		$font = '/fonts/DejaVuSans-Bold.ttf';
//				$sizes['body']  = 12;
//				$sizes['terms'] = 7;
		break;
	    case 'dejavusans':
		$font = '/fonts/DejaVuSans.ttf';
		break;
	    case 'dejavusansb':
		$font = '/fonts/DejaVuSans-Bold.ttf';
//				$sizes['body']  = 12;
//				$sizes['terms'] = 7;
		break;
	    case 'courier':
		$font = '/fonts/DejaVuSansMono.ttf';
//				$sizes['body'] = 12;
		break;
	}

	if ( isset( $options[ 'voucher_text_color' ] ) )
	{
	    $rgb = gtv_html2rgb( $options[ 'voucher_text_color' ] );
	    $text_color = $v_image->allocateColor( $rgb[ 0 ], $rgb[ 1 ], $rgb[ 2 ] );
	}

	if ( isset( $voucher->textcolor ) && '' != $voucher->textcolor )
	{
	    $rgb = gtv_html2rgb( $voucher->textcolor );
	    $text_color = $v_image->allocateColor( $rgb[ 0 ], $rgb[ 1 ], $rgb[ 2 ] );
	}

	if ( !file_exists( plugin_dir_path( __FILE__ ) . 'cache/watermark.png' ) )
	{
	    gtv_generate_watermark_image();
	}

	if ( 'true' == $options[ 'listing_voucher_header' ] )
	{
	    $top_bar_image = WideImage::createPaletteImage( 800, 25 );

	    if ( isset( $options[ 'listing_voucher_header_color' ] ) && '' != $options[ 'listing_voucher_header_color' ] )
	    {
		$rgb = gtv_html2rgb( $options[ 'listing_voucher_header_color' ] );
	    }

	    $sample_canvas = $top_bar_image->getCanvas();
	    $sample_canvas->filledRectangle( 0, 0, 1181, 360, $top_bar_image->allocateColor( $rgb[ 0 ], $rgb[ 1 ], $rgb[ 2 ] ) );

	    //Output the text onto our sample image
	    $text = strtoupper( esc_attr( ltrim( rtrim( $options[ 'listing_header_text' ] ) ) ) );
	    if ( empty( $text ) )
	    {
		$text = get_post_field( 'post_title', $voucher->place_id );
	    }

	    $header_text_color = gtv_choose_text_color( $rgb );

	    $sample_canvas->useFont( plugin_dir_path( __FILE__ ) . '/fonts/DejaVuSans-Bold.ttf', 14, $top_bar_image->allocateColor( $header_text_color[ 0 ], $header_text_color[ 1 ], $header_text_color[ 2 ] ) );
	    $sample_canvas->writeText( 'center', 'middle', strtoupper( esc_attr( $text ) ) );

	    $v_image = $v_image->merge( $top_bar_image, 0, 0, 100 );
	}

	$watermark = false;
	if ( file_exists( plugin_dir_path( __FILE__ ) . 'cache/watermark.png' ) )
	{
	    $watermark = @WideImage::load( plugin_dir_path( __FILE__ ) . 'cache/watermark.png' );
	}

	if ( $watermark )
	{
	    $v_image = $v_image->merge( $watermark );
	}

	$canvas = $v_image->getCanvas();

	// Output the voucher title
	$canvas->useFont( plugin_dir_path( __FILE__ ) . $font, $sizes[ 'title' ], $text_color );
	if ( true == $options[ 'listing_voucher_header' ] )
	{
	    $canvas->writeText( 'center', 55, stripslashes( $voucher->name ) );
	}
	else
	{
	    $canvas->writeText( 'center', 55, stripslashes( $voucher->name ) );
	}

	$canvas->useFont( plugin_dir_path( __FILE__ ) . $font, $sizes[ 'body' ], $text_color );
	$canvas->writeText( 'center', 140, wordwrap( stripslashes( $voucher->text ), 68, "\n" ) );


	// $canvas->writeText( 'center', 'bottom - 170', wordwrap( file_get_contents( plugin_dir_path( __FILE__ ) . "cache/{$code}.png" ), 120, "\n" ) );

	$canvas->useFont( plugin_dir_path( __FILE__ ) . $font, $sizes[ 'terms' ], $text_color );
	$canvas->writeText( 'center', 'bottom - 40', wordwrap( stripslashes( $voucher->terms ), 100, "\n" ) );

	$expiry = '';
	if ( '' != $voucher->expiry && 0 < ( int ) $voucher->expiry )
	{
	    $expiry = ' ' . __( 'Expiry:', 'gtvouchers-d' ) . ' ' . date( get_option( "date_format" ), $voucher->expiry );
	    $canvas->useFont( plugin_dir_path( __FILE__ ) . $font, $sizes[ 'expiry' ], $text_color );
	    $canvas->writeText( 'center', 'bottom - 10', wordwrap( $expiry, 120, "\n" ) );
	}
    }
    else
    {
	/* Create a black image */
	$v_image = WideImage::createTrueColorImage( 150, 30 );
	$bgc = $v_image->allocateColor( 0, 0, 0 );
	$tc = $v_image->allocateColor( 255, 255, 255 );

	$canvas = $v_image->getCanvas();

	$canvas->useFont( plugin_dir_path( __FILE__ ) . '/fonts/DejaVuSerif.ttf', 5, $v_image->allocateColor( 255, 250, 250 ) );

	/* Output an error message */
	$canvas->writeText( "right", "bottom", __( 'Error loading ', 'gtvouchers-d' ) . $imgname );
    }

    if ( !is_dir( plugin_dir_path( __FILE__ ) . '/cache' ) )
    {
	mkdir( plugin_dir_path( __FILE__ ) . '/cache' );
    }

    if ( 'full' == $image_size )
    {
	$v_image->saveToFile( plugin_dir_path( __FILE__ ) . 'cache/' . $voucher->id . '_cache.jpg' );
    }
    else
    {
	$new_image = $v_image->resize( 200, 90 );
	$new_image->saveToFile( plugin_dir_path( __FILE__ ) . 'cache/' . $voucher->id . '_t_cache.jpg' );
    }

    return true;
}

function gtv_generate_watermark_image()
{
    //Create sample text watermark image
    $options = get_option( 'gtvouchers_options' );

    if ( empty( $options[ 'preview_watermark_text' ] ) )
    {
	return;
    }

    //Approximate scaled font size
    $len = strlen( $options[ 'preview_watermark_text' ] );
    if ( 13 > $len )
    {
	$size = ( 75 + ( ( 13 - $len ) * 8 ) );
    }
    else
    {
	$size = ( 75 + ( ( 13 - $len ) * 3 ) );
    }

    //Create the blank image to match our final size
    $sample_image = WideImage::createPaletteImage( 800, 360 );
    $sample_image->setTransparentColor( $sample_image->allocateColor( 255, 255, 255 ) );
    $sample_canvas = $sample_image->getCanvas();

    //Output the text onto our sample image
    $sample_canvas->useFont( plugin_dir_path( __FILE__ ) . '/fonts/DejaVuSans.ttf', $size, $sample_image->allocateColor( 240, 240, 240 ) );
    $sample_canvas->writeText( 'left', 'bottom', esc_attr( $options[ 'preview_watermark_text' ] ), - 17 );

    //AutoCrop the image, then resize it to fit - this fakes a "fit to size" function for smaller amounts of text.
    $sample_image = $sample_image->autoCrop( 10 )->resize( 800, 360, 'fill' );
    $sample_image->saveToFile( plugin_dir_path( __FILE__ ) . 'cache/watermark.png' );

    return;
}

function gtvouchers_generate_voucher_full_image( $voucher, $code = 'ABC123', $dest = 'save' )
{


    if ( empty( $voucher ) )
    {
	return;
    }

    require_once( 'lib/WideImage.php' );
    $options = get_option( 'gtvouchers_options' );

    $slug = $voucher->name;

    if ( 1 == $voucher->use_custom_image )
    {

	$v_image = WideImage::loadFromFile( $voucher->voucher_image );
      
	$canvas = $v_image->getCanvas();

	if ( 'print' != $dest )
	{
	    header( 'Content-Disposition: attachment; filename="' . $slug . '.jpg"' );
	}

	$v_image->output( 'jpg' );

	return true;
    }

    $template = GTV_TEMPLATE_PATH . '/' . $voucher->template . '.jpg';

    /* Attempt to open */
    try
    {
	$v_image = @WideImage::load( $template ); //->resize( 1181, 532 );
    }
    catch ( Exception $e )
    {
	$v_image = false;
    }

    /* See if it failed */
    if ( false != $v_image )
    {

	$sizes = array(
	    'title' => 36,
	    'body' => 20,
	    'registered' => 20,
	    'terms' => 12,
	    'expiry' => 14,
	);

	switch ( $voucher->font )
	{
	    case 'times':
		$font = '/fonts/DejaVuSerif.ttf';
		break;
	    case 'timesb':
		$font = '/fonts/DejaVuSerif-Bold.ttf';
		$sizes[ 'body' ] = 16;
		break;
	    case 'aefurat':
		$font = '/fonts/ae_Furat.ttf';
		$sizes[ 'body' ] = 16;
		break;
	    case 'helvetica':
		$font = '/fonts/DejaVuSans.ttf';
		break;
	    case 'helveticab':
		$font = '/fonts/DejaVuSans-Bold.ttf';
		$sizes[ 'body' ] = 16;
		break;
	    case 'dejavusans':
		$font = '/fonts/DejaVuSans.ttf';
		break;
	    case 'dejavusansb':
		$font = '/fonts/DejaVuSans-Bold.ttf';
		$sizes[ 'body' ] = 16;
		break;
	    case 'courier':
		$font = '/fonts/DejaVuSansMono.ttf';
		$sizes[ 'body' ] = 16;
		break;
	}

	if ( isset( $options[ 'voucher_text_color' ] ) )
	{
	    $rgb = gtv_html2rgb( $options[ 'voucher_text_color' ] );
	    $text_color = $v_image->allocateColor( $rgb[ 0 ], $rgb[ 1 ], $rgb[ 2 ] );
	}

	if ( isset( $voucher->textcolor ) && '' != $voucher->textcolor )
	{
	    $rgb = gtv_html2rgb( $voucher->textcolor );
	    $text_color = $v_image->allocateColor( $rgb[ 0 ], $rgb[ 1 ], $rgb[ 2 ] );
	}
         
         /*QRcode::png( $code, plugin_dir_path( __FILE__ ) . "/{$code}" );
       $qr_pic = WideImage::load(plugin_dir_path( __FILE__ ) . "/{$code}");
       $v_image = $v_image->merge( $qr_pic, 'center', 'bottom – 15', 100);*/

	$canvas = $v_image->getCanvas();

	// Output the voucher title
	$canvas->useFont( plugin_dir_path( __FILE__ ) . $font, $sizes[ 'title' ], $text_color );
	if ( true == $options[ 'listing_voucher_header' ] )
	{
	    $canvas->writeText( 'center', 85, stripslashes( $voucher->name ) );
	}
	else
	{
	    $canvas->writeText( 'center', 85, stripslashes( $voucher->name ) );
	}

	$canvas->useFont( plugin_dir_path( __FILE__ ) . $font, $sizes[ 'body' ], $text_color );
	$canvas->writeText( 'center', 185, wordwrap( stripslashes( $voucher->text ), 70, "\n" ) );

	$registered_name = '';
	if ( '' != $voucher->registered_name )
	{
	    $registered_name = __( 'Registered to:', 'gtvouchers-d' ) . ' ' . stripslashes( $voucher->registered_name ) . ' / ' . $code;
	    $canvas->useFont( plugin_dir_path( __FILE__ ) . $font, $sizes[ 'registered' ], $text_color );
	    $canvas->writeText( 'center', 'bottom - 170', wordwrap( $registered_name, 120, "\n" ) );
	}
	else
	{
	    $canvas->useFont( plugin_dir_path( __FILE__ ) . $font, $sizes[ 'registered' ], $text_color );
	    $canvas->writeText( 'center', 'bottom - 170', wordwrap( $code, 120, "\n" ) );
	}

	$canvas->useFont( plugin_dir_path( __FILE__ ) . $font, $sizes[ 'terms' ], $text_color );
	$canvas->writeText( 'center', 'bottom - 60', wordwrap( stripslashes( $voucher->terms ), 100, "\n" ) );

	$expiry = '';
	if ( '' != $voucher->expiry && 0 < ( int ) $voucher->expiry )
	{
	    $expiry = ' ' . __( 'Expiry:', 'gtvouchers-d' ) . ' ' . date_i18n( get_option( "date_format" ), $voucher->expiry );
	    $canvas->useFont( plugin_dir_path( __FILE__ ) . $font, $sizes[ 'expiry' ], $text_color );
	    $canvas->writeText( 'center', 'bottom - 10', wordwrap( $expiry, 120, "\n" ) );
	}
    }
    else
    {
	/* Create a black image */
	$v_image = WideImage::createTrueColorImage( 150, 30 );
	$bgc = $v_image->allocateColor( 0, 0, 0 );
	$tc = $v_image->allocateColor( 255, 255, 255 );

	$canvas = $v_image->getCanvas();

	$canvas->useFont( plugin_dir_path( __FILE__ ) . '/fonts/DejaVuSerif.ttf', 5, $v_image->allocateColor( 255, 250, 250 ) );

	/* Output an error message */
	$canvas->writeText( "right", "bottom", __( 'Error loading ', 'gtvouchers-d' ) . $imgname );
    }

    if ( true == $options[ 'listing_voucher_header' ] )
    {
	$top_bar_image = WideImage::createPaletteImage( 1181, 30 );

	if ( isset( $options[ 'listing_voucher_header_color' ] ) && '' != $options[ 'listing_voucher_header_color' ] )
	{
	    $rgb = gtv_html2rgb( $options[ 'listing_voucher_header_color' ] );
	}

	$sample_canvas = $top_bar_image->getCanvas();
	$sample_canvas->filledRectangle( 0, 0, 1181, 360, $top_bar_image->allocateColor( $rgb[ 0 ], $rgb[ 1 ], $rgb[ 2 ] ) );

	//Output the text onto our sample image
	$text = strtoupper( esc_attr( ltrim( rtrim( $options[ 'listing_header_text' ] ) ) ) );
	if ( empty( $text ) )
	{
	    $text = get_post_field( 'post_title', $voucher->place_id );
	}

	$header_text_color = gtv_choose_text_color( $rgb );

	$sample_canvas->useFont( plugin_dir_path( __FILE__ ) . '/fonts/DejaVuSans-Bold.ttf', 14, $top_bar_image->allocateColor( $header_text_color[ 0 ], $header_text_color[ 1 ], $header_text_color[ 2 ] ) );
	$sample_canvas->writeText( 'center', 'middle', strtoupper( esc_attr( $text ) ) );

	$v_image = $v_image->merge( $top_bar_image, 0, 0, 100 );
    }

    if ( 'print' != $dest )
    {
//	header_remove( 'Content-type' );
	header( 'Content-Disposition: attachment; filename="' . $slug . '.jpg"' );
	header( 'Content-type: image/jpeg' );
    }
    else
    {
	header( 'Content-type: image/jpeg' );
    }

    $v_image->output( 'jpeg' );

    return true;
}

function delete_voucher_cache()
{
    $files = glob( plugin_dir_path( __FILE__ ) . 'cache/*.jpg' ); // get all file names
    foreach ( $files as $file )
    { // iterate files
	if ( is_file( $file ) )
	{
	    @unlink( $file );
	} // delete file
    }

    $files = glob( plugin_dir_path( __FILE__ ) . 'cache/*.png' ); // get all file names
    foreach ( $files as $file )
    { // iterate files
	if ( is_file( $file ) )
	{
	    @unlink( $file );
	} // delete file
    }
}

function delete_single_cache( $id )
{

    $file_jpg = plugin_dir_path( __FILE__ ) . 'cache/' . $id . '_cache.jpg'; // get all file names
    if ( is_file( $file_jpg ) )
    {
	@unlink( $file_jpg ); // delete file
    }

    $file_png = plugin_dir_path( __FILE__ ) . 'cache/' . $id . '_cache.png'; // get all file names
    if ( is_file( $file_png ) )
    {
	@unlink( $file_png ); // delete file
    }

    $file_jpg = plugin_dir_path( __FILE__ ) . 'cache/' . $id . '_t_cache.jpg'; // get all file names
    if ( is_file( $file_jpg ) )
    {
	@unlink( $file_jpg ); // delete file
    }

    $file_png = plugin_dir_path( __FILE__ ) . 'cache/' . $id . '_t_cache.png'; // get all file names
    if ( is_file( $file_png ) )
    {
	@unlink( $file_png ); // delete file
    }
}

function gtv_choose_text_color( $rgb )
{
    $color = ( $rgb[ 0 ] + $rgb[ 1 ] + $rgb[ 2 ] ) / 3;

    if ( 128 > $color )
    {
	return array( 255, 255, 255 );
    }
    else
    {
	return array( 0, 0, 0 );
    }
}

//[view_v]