<?php
/**
 * @package GT-Vouchers - GeoDirectory Version
 * @author Jeff Rose
 * @version 1.5.6.8
 */
/*
  Plugin Name: GT-Vouchers - GeoDirectory Version
  Plugin URI: http://www.gt-vouchers.com/
  Description: GT-Vouchers - GeoDirectory Version allows you to offer downloadable, printable vouchers from your WordPress site.
  Vouchers can be available to anyone, or may require a name and email address before they can be downloaded.
  This version is customized for GeoDirectory users. It allows a Place Owner to generate a voucher for their place/business
  and to continue editing that.
  Author: Jeff Rose
  Version: 1.5.6.8
  Author URI: http://www.gt-vouchers.com/
  Text Domain: gtvouchers-d
  Domain Path: /languages
 */
// If this file is called directly, abort.
if ( !defined( 'WPINC' ) )
{
    die;
}

include_once('lib/QRcode/qrlib.php');

$uploads = wp_upload_dir();

DEFINE( 'GTV_TEMPLATE_PATH', $uploads[ 'basedir' ] . '/voucher_templates' );
DEFINE( 'GTV_TEMPLATE_URL', $uploads[ 'baseurl' ] . '/voucher_templates' );
DEFINE( 'GTV_CACHE_TTL', 7 * 24 * 60 * 60 ); // One Week
// Plugin Folder Path
if ( !defined( 'GTV_PLUGIN_DIR' ) )
{
    define( 'GTV_PLUGIN_DIR', plugin_dir_path( __FILE__ ) );
}

// Plugin Folder URL
if ( !defined( 'GTV_PLUGIN_URL' ) )
{
    define( 'GTV_PLUGIN_URL', plugin_dir_url( __FILE__ ) );
}

if ( extension_loaded( 'gd' ) && function_exists( 'gd_info' ) )
{
    DEFINE( 'GTV_GD', true );
    require_once( 'gtv-images.php' );
}
else
{
    DEFINE( 'GTV_GD', false );
}

/*
 * NOTE: Setting GTV_DEBUG to TRUE will enable logging of certain functions, queries and other data
 * This data is logged using the error_log command which will write to wp-content/debug.log (by default)
 * if the wp_config.php has DEBUG_LOG enabled.
 *
 * Email is also sent to jeff@jeffrose.ca for analysis.
 */
DEFINE( 'GTV_DEBUG', false );
/*
 * You should only activate the above is requested by Jeff directly.
 */

// set the current version
function gtvouchers_current_version()
{
    return "1.5.6.8";
}

// set activation hook
register_activation_hook( __FILE__, "gtvouchers_activate" );
register_deactivation_hook( __FILE__, "gtvouchers_deactivate" );

// initialise the plugin
add_action( 'plugins_loaded', 'gtvouchers_init' );

// ==========================================================================================
// initialisation functions

function gtvouchers_init()
{
    if ( function_exists( 'add_action' ) )
    {
	$current = gtvouchers_current_version();
	if ( version_compare( get_option( 'gtvouchers_version' ), $current ) == - 1 )
	{
	    gtvouchers_check_install();
	}

	// Verify GeoTheme is running
	add_action( 'admin_init', 'gtvouchers_setup' );
	// add init action
	add_action( 'init', 'gtvouchers_prepare' );
	// add template redirect action
	add_action( 'template_redirect', 'gtvouchers_template' );
	// add the admin menu
	add_action( 'admin_menu', 'gtvouchers_add_admin' );
	// add the create voucher function
	add_action( 'admin_menu', 'gtvouchers_check_create_voucher' );
	// add the edit voucher function
	add_action( 'admin_menu', 'gtvouchers_check_edit_voucher' );
	// add the debug voucher function
	add_action( 'admin_menu', 'gtvouchers_check_debug_voucher' );
	// add the admin preview function
	add_action( 'admin_menu', 'gtvouchers_check_preview' );
	// add the admin email download function
	add_action( 'admin_menu', 'gtvouchers_check_download' );
	// add the admin head includes
	if ( isset( $_GET[ 'page' ] ) && ( substr( $_GET[ 'page' ], 0, 8 ) == 'vouchers' || $_GET[ 'page' ] == 'gtv-settings' ) )
	{
	    add_action( 'admin_head', 'gtvouchers_admin_css' );
//			add_action( 'admin_head', 'gtvouchers_admin_js' );
	    add_action( 'admin_head', 'gtvouchers_jq' );
	}
//        add_action( 'admin_head', 'gtvouchers_jq' );
	// Ensure jQuery is loaded
	add_action( 'wp_enqueue_scripts', 'gtvouchers_jq' );
	// add custom action for our Edit link on the front-end
	add_action( 'gt_vouchers_edit', 'gt_vouchers_show_edit', 10, 1 );
	// add redirect for custom actions. Only the above for now.
	add_action( 'template_redirect', 'gt_vouchers_redirect' );
	// setup shortcodes
	require_once( 'gtdv-shortcodes.php' );

	add_action( 'gtvouchers_reports', 'gtvouchers_run_daily_reports' );

	add_action( 'widgets_init', 'gtvouchers_widgets' );

	add_action( 'admin_init', 'gtv_late_plugins_active' );

//		$options = get_option( 'gtvouchers_options' );
	// add_filter( 'contextual_help', 'gtv_contextual_help', 10, 3 );
	add_action( 'admin_enqueue_scripts', 'gtv_pointer_load' );
	add_action( 'wp_head', 'gtv_load_public_css' );
	add_action( 'init', 'gtv_check_payment_manager', 99 );
	add_action( 'geodir_after_badge_on_image', 'after_badge_on_image' );

	do_action( 'gtv_loaded' );
    }
}

function gtv_late_plugins_active()
{
    // Add stats to my Dashboard plugin.
    if ( function_exists( 'is_plugin_active' ) && is_plugin_active( 'gd-dashboard/gd-dashboard.php' ) )
    {
	add_action( 'gd_dashboard_finish', 'gtvouchers_dash_integrated' );
    }
}

function gtv_load_public_css()
{
    wp_enqueue_style( 'gtv-banners', plugins_url( 'gtv-public.css', __FILE__ ) );
}

if ( !function_exists( 'ify_geodirectory_check' ) )
{

    function ify_geodirectory_check()
    {
	if ( function_exists( 'is_plugin_active' ) && is_plugin_active( 'geodirectory/geodirectory.php' ) )
	{
	    return true;
	}
	else
	{
	    return false;
	}
    }

}

if ( !function_exists( 'ify_table_exists' ) )
{

    function ify_table_exists( $table_name )
    {
	global $wpdb;

	if ( $wpdb->get_var( "SHOW TABLES LIKE '$table_name'" ) != $table_name )
	{
	    return false;
	}
	else
	{
	    return true;
	}
    }

}


if ( !function_exists( 'gtvouchers_jq' ) )
{

    function gtvouchers_jq()
    {
	global $post, $wp_version;
	$options = get_option( 'gtvouchers_options' );

	wp_enqueue_script( 'jquery' );

	$text_strings = array(
	    'name' => sprintf( __( '%s name (30 characters)', 'gtvouchers-d' ), $options[ 'replace_voucher_singular_word' ] ),
	    'text' => sprintf( __( 'Type the %s text here (200 characters)', 'gtvouchers-d' ), strtolower( $options[ 'replace_voucher_singular_word' ] ) ),
	    'terms' => sprintf( __( 'Type the %s terms and conditions here (300 characters)', 'gtvouchers-d' ), strtolower( $options[ 'replace_voucher_singular_word' ] ) ),
	    'novouchers' => sprintf( __( 'No %s selected', 'gtvouchers-d' ), strtolower( $options[ 'replace_voucher_plural_word' ] ) ),
	);

	wp_enqueue_script( 'gt-vouchers', plugins_url( 'js/gt-vouchers.js', __FILE__ ), array( 'jquery' ) );

	if ( is_admin() || ( isset( $_GET[ 'gtv-page' ] ) ) )
	{
	    wp_localize_script( 'gt-vouchers', 'gtvTemplate', array( 'voucherPath' => GTV_TEMPLATE_URL ) );

	    wp_localize_script( 'gt-vouchers', 'gtvDefaultText', $text_strings );
	    //If the WordPress version is greater than or equal to 3.5, then load the new WordPress color picker.
	    if ( 3.5 <= $wp_version )
	    {
		//Both the necessary css and javascript have been registered already by WordPress, so all we have to do is load them with their handle.
		wp_enqueue_style( 'wp-color-picker' );
		wp_enqueue_script( 'wp-color-picker' );
		wp_enqueue_style( 'farbtastic' );
		wp_enqueue_script( 'farbtastic', plugins_url( 'js/farbtastic.js', __FILE__ ), array( 'jquery' ) );

		if ( is_admin() )
		{
		    wp_enqueue_media();
		}

		wp_localize_script( 'gt-vouchers', 'gtvNewMedia', array(
		    'title' => __( 'Upload or Choose Your Custom Icon File', 'gtvouchers-d' ),
		    // This will be used as the default title
		    'button' => __( 'Use this custom image', 'gtvouchers-d' )
			// This will be used as the default button text
		) );
	    } //If the WordPress version is less than 3.5 load the older farbtasic color picker.
	    else
	    {
		//As with wp-color-picker the necessary css and javascript have been registered already by WordPress, so all we have to do is load them with their handle.
		wp_enqueue_script( 'farbtastic', plugins_url( 'js/farbtastic.js', __FILE__ ), array( 'jquery' ) );

		wp_enqueue_script( 'media-upload' );
		wp_enqueue_script( 'thickbox' );
		wp_enqueue_style( 'thickbox' );
	    }
	}

	if ( !isset( $post ) )
	{
	    return;
	}

	if ( !is_user_logged_in() )
	{
	    return;
	}

	if ( ( get_current_user_id() != $post->post_author ) )
	{
	    if ( !current_user_can( 'manage_options' ) )
	    {
		return;
	    }
	}
    }

}

add_filter( 'the_content', 'gtvouchers_show_place_vouchers', 999, 1 );

function gtvouchers_widgets()
{
    global $geodir_sidebars;
    /**
     * Filter the `$before_widget` widget opening HTML tag.
     *
     * @since 1.0.0
     * @param string $var The HTML string to filter. Default = '<section id="%1$s" class="widget geodir-widget %2$s">'.
     * @see 'geodir_after_widget'
     */
    $before_widget = apply_filters( 'gtv_before_widget', '<section id="%1$s" class="widget geodir-widget %2$s">' );
    /**
     * Filter the `$after_widget` widget closing HTML tag.
     *
     * @since 1.0.0
     * @param string $var The HTML string to filter. Default = '</section>'.
     * @see 'geodir_before_widget'
     */
    $after_widget = apply_filters( 'gtv_after_widget', '</section>' );
    /**
     * Filter the `$before_title` widget title opening HTML tag.
     *
     * @since 1.0.0
     * @param string $var The HTML string to filter. Default = '<h3 class="widget-title">'.
     * @see 'geodir_after_title'
     */
    $before_title = apply_filters( 'gtv_before_title', '<h3 class="widget-title">' );
    /**
     * Filter the `$after_title` widget title closing HTML tag.
     *
     * @since 1.0.0
     * @param string $var The HTML string to filter. Default = '</h3>'.
     * @see 'geodir_before_title'
     */
    $after_title = apply_filters( 'gtv_after_title', '</h3>' );

    register_sidebars( 1, array(
	'id' => 'gt_vouchers_sidebar',
	'name' => __( 'GT Vouchers Create/Edit Right Sidebar', 'gtvouchers-d' ),
	'before_widget' => $before_widget,
	'after_widget' => $after_widget,
	'before_title' => $before_title,
	'after_title' => $after_title
    ) );
    $geodir_sidebars[] = 'gt_vouchers_sidebar';
    require_once( 'gtv-widgets.php' );
}

function gtvouchers_prepare()
{
    $domain = 'gtvouchers-d';
    // The "plugin_locale" filter is also used in load_plugin_textdomain()
    $locale = apply_filters( 'plugin_locale', get_locale(), $domain );

    load_textdomain( $domain, WP_LANG_DIR . '/gtd-vouchers/' . $domain . '-' . $locale . '.mo' );
    load_plugin_textdomain( $domain, false, dirname( plugin_basename( __FILE__ ) ) . '/languages/' );

    // this is the URL our updater / license checker pings. This should be the URL of the site with EDD installed
    define( 'GTV_STORE_URL', 'http://www.gt-vouchers.com' ); // you should use your own CONSTANT name, and be sure to replace it throughout this file
    // the name of your product. This should match the download name in EDD exactly
    define( 'GTV_PRODUCT_NAME', 'GT-Vouchers - GeoDirectory Version' ); // you should use your own CONSTANT name, and be sure to replace it throughout this file

    if ( !class_exists( 'EDD_SL_Plugin_Updater' ) )
    {
	// load our custom updater
	include( dirname( __FILE__ ) . '/EDD_SL_Plugin_Updater.php' );
    }

    // retrieve our license key from the DB
    $license_key = trim( get_option( 'gtv_license_key' ) );

    // setup the updater
    $edd_updater = new EDD_SL_Plugin_Updater( GTV_STORE_URL, __FILE__, array(
	'version' => gtvouchers_current_version(),
	// current version number
	'license' => $license_key,
	// license key (used get_option above to retrieve from DB)
	'item_name' => GTV_PRODUCT_NAME,
	// name of this plugin
	'author' => 'Jeff Rose'
	    // author of this plugin
	    ) );
}

function gtvouchers_template()
{
    // if requesting a voucher
    if ( isset( $_GET[ 'voucher' ] ) && $_GET[ 'voucher' ] != '' )
    {
	// get the details
	$voucher_guid = $_GET[ 'voucher' ];
	$download_guid = '';
	if ( isset( $_GET[ 'guid' ] ) )
	{
	    $download_guid = $_GET[ 'guid' ];
	}

	// check the template exists
	if ( gtvouchers_voucher_exists( $voucher_guid ) )
	{
	    // if the email address supplied is valid
	    if ( gtvouchers_download_guid_is_valid( $voucher_guid, $download_guid ) != 'unregistered' )
	    {
		// download the voucher
		gtvouchers_download_voucher( $voucher_guid, $download_guid );
	    }
	    else
	    {
		// show the form
		gtvouchers_register_form( $voucher_guid );
	    }
	    exit();
	}
	else
	{
	    gtvouchers_404();
	}
    }

    if ( is_search() || is_archive() )
    {
	global $wp_query;

	foreach ( $wp_query->posts as $one_post )
	{
	    if ( gtv_place_has_visible_vouchers( $one_post->ID ) )
	    {
		$voucher_list[] = array(
		    $one_post->ID,
		    post_permalink( $one_post->ID )
		);
	    }
	}

	if ( empty( $voucher_list ) )
	{
	    return;
	}

	$options = get_option( 'gtvouchers_options' );
	$voucher_available_text = empty( $options[ 'voucher_available_text' ] ) ? sprintf( __( '%s Available', 'gtvouchers-d' ), $options[ 'replace_voucher_singular_word' ] ) : $options[ 'voucher_available_text' ];

	if ( 'true' == $options[ "voucher_available_no_label" ] )
	{
	    $voucher_available_text = '';
	}

	if ( empty( $options[ 'voucher_available_icon' ] ) )
	{
	    $image = plugins_url() . "/gt-vouchers/images/use_coupon_icon.gif";
	}
	else
	{
	    $image = $options[ 'voucher_available_icon' ];
	}

	$text = $voucher_available_text;

	$data = array(
	    "theImage" => $image,
	    "theText" => $text,
	    "theList" => $voucher_list
	);

	wp_enqueue_script( 'gt-vouchers', plugins_url( 'js/gtv-public.js', __FILE__ ), array( 'jquery' ) );
	wp_localize_script( 'gt-vouchers', 'hasVouchers', $data );
    }
}

// show a 404 page
function gtvouchers_404( $found = true )
{
    global $wp_query;
    $wp_query->set_404();
    if ( $found )
    {
	wp_die( __( 'Sorry, that item is not available', 'gtvouchers-d' ) );
    }
    else
    {
	wp_die( __( 'Sorry, that item was not found', 'gtvouchers-d' ) );
    }
    exit();
}

// show an expired voucher page
function gtvouchers_expired()
{
    global $wp_query;
    $wp_query->set_404();

    wp_die( __( 'Sorry, that item has expired', 'gtvouchers-d' ) );

    exit();
}

// show an expired voucher page
function gtvouchers_notyetavailable()
{
    global $wp_query;
    $wp_query->set_404();

    wp_die( __( 'Sorry, that item is not yet available', 'gtvouchers-d' ) );

    exit();
}

// show a run out voucher page
function gtvouchers_runout()
{
    global $wp_query;
    $wp_query->set_404();

    wp_die( __( 'Sorry, that item has run out', 'gtvouchers-d' ) );

    exit();
}

// show a downloaded voucher page
function gtvouchers_downloaded()
{
    global $wp_query;
    $wp_query->set_404();

    $options = get_option( 'gtvouchers_options' );

    wp_die( sprintf( __( 'You have already downloaded this %s', 'gtvouchers-d' ), strtolower( $options[ 'replace_voucher_plural_word' ] ) ) );

    exit();
}

// ==========================================================================================
// activation functions
// activate the plugin
function gtvouchers_activate()
{
    // if PHP is less than version 5
    if ( ( version_compare( PHP_VERSION, '5.0.0' ) != 1 ) || !ify_geodirectory_check() )
    {
	echo PHP_VERSION;
	echo '
        <div id="message" class="error">
            <p><strong>' . __( 'Sorry, your PHP version must be 5 or above. Please contact your server administrator for help.', 'gtvouchers-d' ) . '</strong></p>
        </div>
        ';
    }
    else
    {
	//check install
	gtvouchers_check_install();
	add_option( 'gtvouchers_version', gtvouchers_current_version() );
    }
}

// deactivate the plugin
function gtvouchers_deactivate()
{
    // delete options
    delete_option( 'gtvouchers_data' );
    delete_option( 'gtvouchers_version' );
}

// insert the default templates
function gtvouchers_insert_templates()
{
    global $wpdb;
    $prefix = $wpdb->prefix;

    $templates = $wpdb->get_var( "select count(name) from {$prefix}gtvouchers_templates;" );
    if ( $templates == 0 )
    {
	$wpdb->query( "insert into " . $prefix . "gtvouchers_templates (name, live, blog_id) values ('Plain Black Border', 1, 1);" );
	$wpdb->query( "insert into " . $prefix . "gtvouchers_templates (name, live, blog_id) values ('Car Key', 1, 1);" );
	$wpdb->query( "insert into " . $prefix . "gtvouchers_templates (name, live, blog_id) values ('Hair Dryer', 1, 1);" );
	$wpdb->query( "insert into " . $prefix . "gtvouchers_templates (name, live, blog_id) values ('Towel & Stones', 1, 1);" );
	$wpdb->query( "insert into " . $prefix . "gtvouchers_templates (name, live, blog_id) values ('Weights and Apple', 1, 1);" );
	$wpdb->query( "insert into " . $prefix . "gtvouchers_templates (name, live, blog_id) values ('Wine Glass', 1, 1);" );
    }
}

// check gtvouchers is installed correctly
function gtvouchers_check_install()
{
    // create tables
    gtvouchers_create_tables();

    // sleep for 1 second to allow the tables to be created
    sleep( 1 );

//	$options = get_option( 'gtvouchers_options' );
    // if there are no templates saved
    $templates = gtvouchers_get_templates();
    if ( !$templates || !is_array( $templates ) || count( $templates ) == 0 )
    {
	// insert the default templates
	gtvouchers_insert_templates();
    }

    if ( '1' != get_option( 'gt-vouchers-moved' ) )
    {
	require_once( 'template_mover.php' );
	gtvouchers_verify_template_location();
    }

    // check the templates directory is writeable
    if ( !@is_writable( GTV_TEMPLATE_PATH ) && current_user_can( 'update_core' ) )
    {
	echo '
        <div id="message" class="warning">
            <p><strong>' . __( 'The system does not have write permissions on the folder (' . GTV_TEMPLATE_PATH . ') where your custom templates are stored. You may not be able to upload your own templates. Please set permissions to 755 or contact your system administrator for more information.', 'gtvouchers-d' ) . '</strong></p>
        </div>
        ';
    }

    gtv_check_payment_manager();
}

function gtvouchers_migration_message()
{
    echo '
    <div class="updated">
        <p><strong>' . __( 'GT-Vouchers now uses the GT-VOUCHERS SETTINGS page to choose which packages get Vouchers. Your Special Offers selections have been migrated to Settings. Please visit Settings to verify your choices.', 'gtvouchers-d' ) . '</strong></p>
    </div>';
}

function gtvouchers_migrate_packages_to_options()
{
    global $wpdb;

    $options = get_option( 'gtvouchers_options' );

    $prefix = $wpdb->prefix;

    $packages = "SELECT * FROM {$prefix}price WHERE property_feature_pkg = 1";
    $packages = $wpdb->get_results( $packages );

    if ( empty( $packages ) )
    {
	return;
    }

    $voucher_packages = [ ];

    foreach ( $packages as $a_package )
    {
	$voucher_packages[] = $a_package->pid;
    }

    $options[ 'voucher_packages' ] = $voucher_packages;

    update_option( 'gtvouchers_options', $options );
}

// get the currently installed version
function gtvouchers_get_version()
{
    if ( function_exists( 'get_site_option' ) )
    {
	return get_site_option( 'gtvouchers_version' );
    }
    else
    {
	return get_option( 'gtvouchers_version' );
    }
}

// update the currently installed version
function gtvouchers_update_version()
{
    $version = gtvouchers_current_version();
    if ( function_exists( 'get_site_option' ) )
    {
	update_site_option( 'gtvouchers_version', $version );
    }
    else
    {
	return update_option( 'gtvouchers_version', $version );
    }

    return false;
}

// delete the currently installed version flag
function gtvouchers_delete_version()
{
    if ( function_exists( 'get_site_option' ) )
    {
	delete_site_option( 'gtvouchers_version' );
    }
    else
    {
	return delete_option( 'gtvouchers_version' );
    }

    return false;
}

// create the tables
function gtvouchers_create_tables()
{
    // check the current version
    if ( version_compare( gtvouchers_get_version(), gtvouchers_current_version() ) <= 0 || defined( 'gtvouchersDEV' ) )
    {
	global $wpdb;
	$prefix = $wpdb->prefix;

	require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );

	// table to store the vouchers
	$table_name = $prefix . 'gtvouchers_vouchers';
	if ( !ify_table_exists( $table_name ) )
	{
	    $sql = "CREATE TABLE " . $table_name . " (
                  id mediumint(9) NOT NULL AUTO_INCREMENT,
                  blog_id mediumint NOT NULL,
                  author_id mediumint NOT NULL,
                  place_id int DEFAULT '0',
                  time bigint(11) DEFAULT '0' NOT NULL,
                  name VARCHAR(50) NOT NULL,
                  `text` varchar(250) NOT NULL,
                  `description` TEXT NULL,
                  terms varchar(500) NOT NULL,
                  template varchar(55) NOT NULL,
                  font varchar(55) DEFAULT 'helvetica' NOT NULL,
                  textcolor varchar(10) DEFAULT '' NOT NULL,
                  require_email TINYINT DEFAULT 1 NOT NULL,
                  liketextcta VARCHAR(500) NULL,
                  `limit` MEDIUMINT(9) NOT NULL DEFAULT 0,
                  guid varchar(36) NOT NULL,
                  live TINYINT DEFAULT '0',
                  startdate int DEFAULT '0',
                  expiry int DEFAULT '0',
                  codestype varchar(12) DEFAULT 'random',
                  codeprefix varchar(6) DEFAULT '',
                  codesuffix varchar(6) DEFAULT '',
                  codelength int DEFAULT 6,
                  codes MEDIUMTEXT NOT NULL DEFAULT '',
                  deleted tinyint DEFAULT '0',
                  require_like TINYINT DEFAULT 0 NOT NULL,
                  require_phone TINYINT DEFAULT 0 NOT NULL,
                  voucher_image VARCHAR(500) NULL,
                  use_custom_image INT( 4 ) NULL DEFAULT 0,
                  category_id INT( 4 ) NULL DEFAULT 0,
                  PRIMARY KEY  id (id),
                  KEY `voucher-blog-place` (`id`, `place_id`, `blog_id`),
                  KEY `live` (`live`),
                  KEY `deleted` (`deleted`),
                  KEY `expiry` (`expiry`),
                  KEY `startdate` (`startdate`)
                ) ENGINE=InnoDB DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;";
	    dbDelta( $sql );
	}

	$sql = "SELECT COUNT(1) IndexIsThere FROM INFORMATION_SCHEMA.STATISTICS WHERE table_schema=DATABASE() AND table_name='{$table_name}' AND index_name='voucher-blog-place';";
	$is_indexed = $wpdb->get_col( $sql );

	if ( 0 == $is_indexed[ 0 ] )
	{
	    $sql = "ALTER TABLE `{$table_name}` ADD INDEX `voucher-blog-place` (`id`, `place_id`, `blog_id`);";
	    $wpdb->query( $sql );
	}

	$sql = "SELECT COUNT(1) IndexIsThere FROM INFORMATION_SCHEMA.STATISTICS WHERE table_schema=DATABASE() AND table_name='{$table_name}' AND index_name='live';";
	$is_indexed = $wpdb->get_col( $sql );

	if ( 0 == $is_indexed[ 0 ] )
	{
	    $sql = "ALTER TABLE `{$table_name}` ADD INDEX `live` (`live`);";
	    $wpdb->query( $sql );

	    $sql = "ALTER TABLE `{$table_name}` ADD INDEX `deleted` (`deleted`);";
	    $wpdb->query( $sql );

	    $sql = "ALTER TABLE `{$table_name}` ADD INDEX `startdate` (`startdate`);";
	    $wpdb->query( $sql );

	    $sql = "ALTER TABLE `{$table_name}` ADD INDEX `expiry` (`expiry`);";
	    $wpdb->query( $sql );
	}

	// table to store downloads
	$table_name = $prefix . 'gtvouchers_downloads';
	if ( !ify_table_exists( $table_name ) )
	{
	    $sql = "CREATE TABLE " . $table_name . " (
              id mediumint(9) NOT NULL AUTO_INCREMENT,
              voucherid mediumint(9) NOT NULL,
              time bigint(11) DEFAULT '0' NOT NULL,
              ip VARCHAR(15) NOT NULL,
              name VARCHAR(55) NULL,
              email varchar(255) NULL,
              phone varchar(55) NULL,
              user_id int NULL,
              guid varchar(36) NOT NULL,
              code varchar(255) NOT NULL,
              downloaded TINYINT DEFAULT '0',
              PRIMARY KEY  id (id),
              KEY `viddl` (`voucherid`,`downloaded`)
            ) ENGINE=InnoDB DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;";
	    dbDelta( $sql );
	}

	if ( function_exists( 'add_column_if_not_exist' ) )
	{
	    $table_name = $prefix . 'gtvouchers_downloads';
	    add_column_if_not_exist( $table_name, 'phone', " VARCHAR(55) NULL" );
	    add_column_if_not_exist( $table_name, 'user_id', " INT(4) DEFAULT 0 NULL" );
	}

	$sql = "SELECT COUNT(1) IndexIsThere FROM INFORMATION_SCHEMA.STATISTICS WHERE table_schema=DATABASE() AND table_name='{$table_name}' AND index_name='viddl';";
	$is_indexed = $wpdb->get_col( $sql );

	if ( 0 == $is_indexed[ 0 ] )
	{
	    $sql = "ALTER TABLE `{$table_name}` ADD INDEX `viddl` (`voucherid`, `downloaded`)";
	    $wpdb->query( $sql );
	}

	// table to store templates
	$table_name = $prefix . 'gtvouchers_templates';
	if ( !ify_table_exists( $table_name ) )
	{
	    $sql = "CREATE TABLE " . $table_name . " (
              id mediumint(9) NOT NULL AUTO_INCREMENT,
              blog_id mediumint NOT NULL,
              time bigint(11) DEFAULT '0' NOT NULL,
              name VARCHAR(55) NOT NULL,
              live tinyint DEFAULT '1',
              PRIMARY KEY  id (id)
            ) ENGINE=InnoDB DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;";
	    dbDelta( $sql );
	}

	// table to store templates
	$table_name = $prefix . 'gtvouchers_categories';
	if ( !ify_table_exists( $table_name ) )
	{
	    $sql = "CREATE TABLE " . $table_name . " (
              id mediumint(9) NOT NULL AUTO_INCREMENT,
              cat_name VARCHAR(55) NOT NULL,
              status TINYINT DEFAULT 1 NOT NULL,
              PRIMARY KEY  id (id)
            ) ENGINE=InnoDB DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;";
	    dbDelta( $sql );
	}

	// update the version
	gtvouchers_update_version();
    }
}

// ==========================================================================================
// general admin function
// add the menu items
function gtvouchers_add_admin()
{
    global $gtv_main, $gtv_create, $gtv_templates, $gtv_categories, $gtv_tools;
    $options = get_option( 'gtvouchers_options' );
    $gtv_main = add_menu_page( $options[ 'replace_voucher_plural_word' ], $options[ 'replace_voucher_plural_word' ], 'publish_posts', 'vouchers', 'vouchers_admin', 'dashicons-cart' );
    $gtv_create = add_submenu_page( 'vouchers', __( 'Create a voucher', 'gtvouchers-d' ), __( 'Create', 'gtvouchers-d' ), 'publish_posts', 'vouchers-create', 'gtvouchers_create_voucher_page' );
    $gtv_categories = add_submenu_page( 'vouchers', __( 'Voucher categories', 'gtvouchers-d' ), __( 'Categories', 'gtvouchers-d' ), 'publish_posts', 'vouchers-categories', 'gtvouchers_categories_page' );
    $gtv_templates = add_submenu_page( 'vouchers', __( 'Voucher templates', 'gtvouchers-d' ), __( 'Templates', 'gtvouchers-d' ), 'publish_posts', 'vouchers-templates', 'gtvouchers_templates_page' );
    $gtv_tools = add_submenu_page( 'vouchers', __( 'Tools', 'gtvouchers-d' ), __( 'Tools', 'gtvouchers-d' ), 'manage_options', 'voucher-tools', 'gtvouchers_tools_page' );

    add_submenu_page( 'vouchers', __( 'Settings', 'gtvouchers-d' ), __( 'Settings', 'gtvouchers-d' ), 'manage_options', 'gtv-settings', 'gtvouchers_settings_page' );
}

function gtvouchers_help_file()
{
//	gtv_show_help_options();
}

// show the general site admin page
function gtvouchers_site_admin()
{
    gtvouchers_report_header();
    $options = get_option( 'gtvouchers_options' );

    echo '<h2>' . $options[ 'replace_voucher_plural_word' ] . '</h2>';

    echo '<div class="gtvouchers_col1">
    <h3>' . sprintf( __( '25 most recent %s', 'gtvouchers-d' ), strtolower( $options[ 'replace_voucher_plural_word' ] ) ) . '</h3>';
    $vouchers = gtvouchers_get_all_vouchers( 25, 0 );
    if ( $vouchers && is_array( $vouchers ) && count( $vouchers ) > 0 )
    {
	gtvouchers_table_header( array(
	    __( 'Blog', 'gtvouchers-d' ),
	    __( 'Name', 'gtvouchers-d' ),
	    _( 'Downloads', 'gtvouchers-d' )
	) );
	foreach ( $vouchers as $voucher )
	{
	    echo '
            <tr>
                <td><a href="//' . $voucher->domain . $voucher->path . '">' . $voucher->domain . $voucher->path . '</a></td>
                <td><a href="//' . $voucher->domain . $voucher->path . '?voucher=' . $voucher->guid . '">' . $voucher->name . '</a></td>
                <td>' . $voucher->downloads . '</td>
            </tr>
            ';
	}
	gtvouchers_table_footer();
    }
    else
    {
	echo '
        <p>' . sprintf( __( 'No %s found. <a href="admin.php?page=vouchers-create">Create your first %s here.</a>', 'gtvouchers-d' ), strtolower( $options[ 'replace_voucher_plural_word' ] ), strtolower( $options[ 'replace_voucher_singular_word' ] ) ) . '</p>
        ';
    }
    echo '
    </div>';

    echo '<div class="gtvouchers_col2">
    <h3>' . sprintf( __( '25 most popular %s', 'gtvouchers-d' ), strtolower( $options[ 'replace_voucher_plural_word' ] ) ) . '</h3>
    ';
    $vouchers = gtvouchers_get_all_popular_vouchers( 25, 0 );
    if ( $vouchers && is_array( $vouchers ) && count( $vouchers ) > 0 )
    {
	gtvouchers_table_header( array(
	    __( 'Blog', 'gtvouchers-d' ),
	    __( 'Name', 'gtvouchers-d' ),
	    _( 'Downloads', 'gtvouchers-d' )
	) );
	foreach ( $vouchers as $voucher )
	{
	    echo '
            <tr>
                <td><a href="//' . $voucher->domain . $voucher->path . '">' . $voucher->domain . $voucher->path . '</a></td>
                <td><a href="//' . $voucher->domain . $voucher->path . '?voucher=' . $voucher->guid . '">' . $voucher->name . '</a></td>
                <td>' . $voucher->downloads . '</td>
            </tr>
            ';
	}
	gtvouchers_table_footer();
    }
    else
    {
	echo '
        <p>' . sprintf( __( 'No %s found. <a href="admin.php?page=vouchers-create">Create your first %s here.</a>', 'gtvouchers-d' ), strtolower( $options[ 'replace_voucher_plural_word' ] ), strtolower( $options[ 'replace_voucher_singular_word' ] ) ) . '</p>
        ';
    }
    echo '
    </div>';

    gtvouchers_report_footer();
}

// show the general admin page
function vouchers_admin()
{
    gtvouchers_report_header();
    $options = get_option( 'gtvouchers_options' );

    if ( isset( $_POST[ "action" ] ) && ( 'deletevouchers' == $_POST[ "action" ] ) && check_admin_referer( 'deletevouchers' ) )
    {
	$delete_list = $_POST[ 'delete' ];
	if ( !empty( $delete_list ) && current_user_can( 'manage_options' ) )
	{
	    foreach ( $delete_list as $voucher_id )
	    {
		gtvouchers_delete_voucher( $voucher_id );
	    }
	}
	echo '<div id="message" class="updated">
            <p><strong>' . sprintf( __( 'Your %s were marked as deleted.', 'gtvouchers-d' ), strtolower( $options[ 'replace_voucher_plural_word' ] ) ) . '</strong></p>
            </div>';
    }

    // if a voucher has not been chosen
    if ( !isset( $_GET[ 'id' ] ) )
    {
	echo '<h2>' . $options[ 'replace_voucher_plural_word' ] . '
        <span style="float:right;font-size:80%"><a href="admin.php?page=vouchers-create" class="button">' . sprintf( __( 'Create a %s', 'gtvouchers-d' ), strtolower( $options[ 'replace_voucher_singular_word' ] ) ) . '</a></span></h2>';

	if ( isset( $_GET[ 'reset' ] ) && $_GET[ 'reset' ] == 'true' )
	{
	    gtvouchers_delete_version();
	    echo '
            <div id="message" class="updated">
                <p><strong>' . __( 'Your gtvouchers database will be reset next time you create or edit a voucher. You will not lose any data, the tables will just be checked for all the correct fields.', 'gtvouchers-d' ) . '</strong></p>
            </div>
            ';
	}

	echo '<div class="gtvouchers_col1">
        <h3>' . sprintf( __( 'Your %s', 'gtvouchers-d' ), $options[ 'replace_voucher_plural_word' ] ) . '</h3>
        ';
	$vouchers = gtvouchers_get_vouchers( 50, true );
	if ( $vouchers && is_array( $vouchers ) && count( $vouchers ) > 0 )
	{
	    echo '<form name="voucherlist" id="voucherlist" method="post">';
	    echo '<input type="hidden" name="action" value="deletevouchers" />';
	    wp_nonce_field( 'deletevouchers' );
	    if ( current_user_can( 'manage_options' ) )
	    {
		gtvouchers_table_header( array(
		    __( 'Delete', 'gtvouchers-d' ),
		    __( 'Name', 'gtvouchers-d' ),
		    __( 'Downloads', 'gtvouchers-d' ),
		    __( 'Email required', 'gtvouchers-d' ),
		    __( 'Place', 'gtvouchers-d' )
		) );
	    }
	    else
	    {
		gtvouchers_table_header( array(
		    __( 'Name', 'gtvouchers-d' ),
		    __( 'Downloads', 'gtvouchers-d' ),
		    __( 'Email required', 'gtvouchers-d' ),
		    __( 'Place', 'gtvouchers-d' )
		) );
	    }
	    foreach ( $vouchers as $voucher )
	    {
		echo '
                <tr>';
		if ( current_user_can( 'manage_options' ) )
		{
		    echo '<td><input type="checkbox" name="delete[]" value=' . $voucher->id . ' />';
		}
		echo '
                    <td><a href="admin.php?page=vouchers&amp;id=' . $voucher->id . '">' . $voucher->name . '</a></td>
                    <td>' . $voucher->downloads . '</td>
                    <td>' . gtvouchers_yes_no( $voucher->require_email ) . '</td>
                    <td>' . $voucher->post_title . '</td>
                </tr>
                ';
	    }
	    gtvouchers_table_footer();
	    if ( current_user_can( 'manage_options' ) )
	    {
		echo '<br />';
		echo '<a href="#" class="button-secondary" id="delete-vouchers">' . sprintf( __( 'Delete selected %s', 'gtvouchers-d' ), strtolower( $options[ 'replace_voucher_plural_word' ] ) ) . '</a>';
		echo '</form>';
	    }
	}
	else
	{
	    echo '
            <p>' . sprintf( __( 'No %s found. <a href="admin.php?page=vouchers-create">Create your first %s here.</a>', 'gtvouchers-d' ), strtolower( $options[ 'replace_voucher_plural_word' ] ), strtolower( $options[ 'replace_voucher_singular_word' ] ) ) . '</p>
            ';
	}
	echo '
        </div>';

	echo '<div class="gtvouchers_col2">
        <h3>' . sprintf( __( 'Popular %s', 'gtvouchers-d' ), strtolower( $options[ 'replace_voucher_plural_word' ] ) ) . '</h3>
        ';
	$vouchers = gtvouchers_get_popular_vouchers();
	if ( $vouchers && is_array( $vouchers ) && count( $vouchers ) > 0 )
	{
	    gtvouchers_table_header( array(
		__( 'Name', 'gtvouchers-d' ),
		__( 'Downloads', 'gtvouchers-d' ),
		__( 'Email required', 'gtvouchers-d' ),
		__( 'Place', 'gtvouchers-d' )
	    ) );
	    foreach ( $vouchers as $voucher )
	    {
		echo '
                <tr>
                    <td><a href="admin.php?page=vouchers&amp;id=' . $voucher->id . '">' . $voucher->name . '</a></td>
                    <td>' . $voucher->downloads . '</td>
                    <td>' . gtvouchers_yes_no( $voucher->require_email ) . '</td>
                    <td>' . $voucher->post_title . '</td>
                </tr>
                ';
	    }
	    gtvouchers_table_footer();
	}
	else
	{
	    echo '
            <p>' . sprintf( __( 'No %s found. <a href="admin.php?page=vouchers-create">Create your first %s here.</a>', 'gtvouchers-d' ), strtolower( $options[ 'replace_voucher_plural_word' ] ), strtolower( $options[ 'replace_voucher_singular_word' ] ) ) . '</p>
            ';
	}
	echo '
        <p><a href="' . wp_nonce_url( 'admin.php?page=vouchers&amp;download=emails', 'gtvouchers_download_csv' ) . '" class="button">' . __( 'Download all registered email addresses', 'gtvouchers-d' ) . '</a></p>
        </div>';

	// if a voucher has been chosen
    }
    else
    {

	gtvouchers_edit_voucher_page();
    }

    gtvouchers_report_footer();
}

// show the create voucher page
function gtvouchers_create_voucher_page( $location = 'backend' )
{
    if ( '' == $location )
    {
	$location = 'backend';
    }

    if ( !is_user_logged_in() )
    {
	return;
    }

    $options = get_option( 'gtvouchers_options' );
    wp_enqueue_script( 'thickbox' );
    wp_enqueue_style( 'thickbox' );

    if ( 'front' === $location || !current_user_can( 'manage_options' ) )
    {
	$place_id = isset( $_GET[ 'place_id' ] ) ? intval( $_GET[ 'place_id' ] ) : 0;

	if ( 0 < $place_id )
	{
	    $post_info = geodir_get_post_info( $place_id );

	    $vouchers = gtv_package_allows_vouchers( $post_info->package_id );

	    if ( 0 == $vouchers )
	    {
		$vouchers = gtv_vouchers_on_post_type( $place_id );
	    }

	    if ( (!$vouchers ) && ( get_current_user_id() != $post_info->post_author || !current_user_can( 'manage_options' ) ) )
	    {
		return;
	    }

//			if ( is_plugin_active( 'geodir_custom_posts/geodir_custom_posts.php' ) ) {
//				$is_right_post_type = gtv_vouchers_on_post_type( $place_id );
//
//				if ( ! ( $is_right_post_type ) ) {
//					die("913");
//					return;
//				}
//			}
	}
    }

    //gtvouchers_report_header();

    if ( 'front' != $location )
    {
	echo '
        <h2>' . sprintf( __( 'Create a %s', 'gtvouchers-d' ), strtolower( $options[ 'replace_voucher_singular_word' ] ) ) . '</h2>';
    }

    if ( isset( $_GET[ 'result' ] ) && $_GET[ 'result' ] != '' )
    {
	if ( $_GET[ 'result' ] == '1' )
	{
	    echo '
            <div id="message" class="error">
                <p><strong>' . sprintf( __( 'Sorry, your %s could not be created. Please click back and try again.', 'gtvouchers-d' ), strtolower( $options[ 'replace_voucher_singular_word' ] ) ) . '</strong></p>
            </div>
            ';
	}
    }
    if ( 'backend' == $location )
    {
	echo '<form action="admin.php?page=vouchers-create" method="post" id="voucherform">';
    }
    else
    {
	echo '<form action="?gtv-page=save-voucher" method="post" id="voucherform" enctype="multipart/form-data">';
	echo '<input type="hidden" name="place_id" value="' . intval( $_GET[ 'place_id' ] ) . '" />';
    }

    $listings = gtvouchers_choose_listing( intval( isset( $_GET[ 'place_id' ] ) ? $_GET[ 'place_id' ] : 0  ) );
    $success = $listings[ 'success' ];
    $listings = $listings[ 'listings' ];

    echo '<label for="place_id">' . sprintf( __( 'Attach %s to: ', 'gtvouchers-d' ), strtolower( $options[ 'replace_voucher_singular_word' ] ) ) . '</label>' . $listings;
    echo '<br />';

    if ( false === $success )
    {
	return;
    }

    if ( 'true' == $options[ 'vouchers_require_approval' ] && !current_user_can( 'update_core' ) )
    {
	echo '<br /><p class="warning"><strong>';
	printf( __( 'This %s will not be shown on the site until it is approved.', 'gtvouchers-d' ), strtolower( $options[ 'replace_voucher_singular_word' ] ) );
	echo '</strong></p>';
    }

    if ( 'true' == $options[ 'use_categories' ] || current_user_can( 'update_core' ) )
    {
	gtvouchers_select_voucher_category();
    }

    if ( 'true' != $options[ 'hide_email_required_option' ] || 'true' == $options[ 'admin_require_email' ] )
    {
	echo '<p>' . sprintf( __( '%s description (optional): enter a longer description here which will go in the email sent to a user registering for this %s.', 'gtvouchers-d' ), $options[ 'replace_voucher_singular_word' ], strtolower( $options[ 'replace_voucher_singular_word' ] ) ) . '</p>
        <p><textarea name="description" id="description" rows="3" cols="100"></textarea></p>';
    }

    do_action( 'gtv_post_voucher_description' );

    if ( current_user_can( 'manage_options' ) || ( isset( $options[ 'user_image_upload' ] ) && ( 'true' == $options[ 'user_image_upload' ] ) ) )
    {
	if ( 'backend' == $location )
	{
//			global $wp_version;
	    ?>
	    <script language="JavaScript">
	        jQuery( document ).ready( function () {
	    	jQuery( '#voucher_image_button' ).click( function () {
	    	    var formfield = jQuery( '#voucher_image' ).attr( 'name' );
	    	    tb_show( '', 'media-upload.php?type=image&TB_iframe=true' );
	    	    return false;
	    	} );

	    	window.send_to_editor = function ( html ) {
	    	    var imgurl = jQuery( 'img', html ).attr( 'src' );
	    	    if ( undefined == imgurl ) {
	    		imgurl = jQuery( html ).attr( 'src' );
	    	    }
	    	    jQuery( 'input#voucher_image' ).val( imgurl );
	    	    tb_remove();
	    	};

	        } );
	    </script>
	    <?php
	    echo '<div id="hide_custom_image_option"><label for="use_custom_image" id="uci">' . sprintf( __( 'Use a custom %s image? ', 'gtvouchers-d' ), strtolower( $options[ 'replace_voucher_singular_word' ] ) ) . '</label>';
	    echo '<input type="checkbox" name="use_custom_image" id="use_custom_image" value="true" /> ' . __( 'Yes', 'gtvouchers-d' );
	    echo '<br /><br /></div>';

	    echo '<p id="custom_image">
        ' . sprintf( __( 'Enter a URL or upload an image for the %s.', 'gtvouchers-d' ), strtolower( $options[ 'replace_voucher_singular_word' ] ) ) . '
        <label for="voucher_image">' . sprintf( __( '%s image:', 'gtvouchers-d' ), $options[ 'replace_voucher_singular_word' ] ) . '</label>
        <input id="voucher_image" type="text" size="36" name="voucher_image" value="" />
        <input id="voucher_image_button" type="button" value="' . __( 'Select Image', 'gtvouchers-d' ) . '" />
        </p>
    ';
	}
	else
	{
	    echo '<div id="hide_custom_image_option"><label for="use_custom_image" id="uci">' . sprintf( __( 'Use a custom %s image? ', 'gtvouchers-d' ), strtolower( $options[ 'replace_voucher_singular_word' ] ) ) . '</label>';
	    echo '<input type="checkbox" name="use_custom_image" id="use_custom_image" value="true" /> ' . __( 'Yes', 'gtvouchers-d' );
	    echo '<br /><br /></div>';

	    echo '<p id="custom_image">
        ' . sprintf( __( 'Enter a URL or upload an image for the %s.', 'gtvouchers-d' ), strtolower( $options[ 'replace_voucher_singular_word' ] ) ) . '
        <label for="voucher_image">' . sprintf( __( '%s image:', 'gtvouchers-d' ), $options[ 'replace_voucher_singular_word' ] ) . '</label>
        <input id="voucher_image" type="file" size="36" name="voucher_image" value="" />';
	}
    }

    echo '
    <div id="voucher_design">
        <div id="voucherpreview">

            <h2><textarea name="name" id="name" rows="1" cols="100">' . sprintf( __( '%s name (30 characters)', 'gtvouchers-d' ), $options[ 'replace_voucher_singular_word' ] ) . '</textarea></h2>

            <p><textarea name="text" id="text" rows="2" cols="100">' . sprintf( __( 'Type the %s text here (200 characters)', 'gtvouchers-d' ), strtolower( $options[ 'replace_voucher_singular_word' ] ) ) . '</textarea></p>

            <p>[' . sprintf( __( '%s code inserted here', 'gtvouchers-d' ), $options[ 'replace_voucher_singular_word' ] ) . ']</p>

            <p id="voucherterms"><textarea name="terms" id="terms" rows="3" cols="100">' . sprintf( __( 'Type the %s terms and conditions here (300 characters)', 'gtvouchers-d' ), strtolower( $options[ 'replace_voucher_singular_word' ] ) ) . '</textarea></p>

        </div>';

    $fonts = gtvouchers_fonts();
    echo '
    <h3>' . __( 'Font', 'gtvouchers-d' ) . '</h3>
    <p><label for="font">' . __( 'Font', 'gtvouchers-d' ) . '</label>
    <select name="font" id="font">
    ';
    foreach ( $fonts as $font )
    {
	echo '
        <option value="' . $font[ 0 ] . '">' . $font[ 1 ] . '</option>
        ';
    }
    echo '
    </select> <span class="geodir_message_note">' . sprintf( __( 'Set the font for this %s', 'gtvouchers-d' ), strtolower( $options[ 'replace_voucher_singular_word' ] ) ) . '</span></p>
    ';

    //Both the necessary css and javascript have been registered already by WordPress, so all we have to do is load them with their handle.
    wp_enqueue_style( 'wp-color-picker' );

    echo '
    <p>
    <label for="textcolor">' . __( 'Text color', 'gtvouchers-d' ) . '</label>
    <input type="text" name="textcolor" id="vouchertextcolor" value="#000000" />
    <span class="geodir_message_note">' . sprintf( __( 'Set the color of text for this %s', 'gtvouchers-d' ), strtolower( $options[ 'replace_voucher_singular_word' ] ) ) . '</span>
    ';
    echo '<div style="left: 350px;position:relative; top: -17px;" id="colorpicker3"></div></p>';

    $templates = gtvouchers_get_templates();
    if ( $templates && is_array( $templates ) && count( $templates ) > 0 )
    {
	echo '
        <div id="voucherthumbs">
        <h3>' . __( 'Template', 'gtvouchers-d' ) . '</h3>
        ';
	foreach ( $templates as $template )
	{
	    echo '
            <span><img src="' . GTV_TEMPLATE_URL . '/' . $template->id . '_thumb.jpg" id="template_' . $template->id . '" alt="' . $template->name . '" /></span>
            ';
	}
	echo '
        </div>
        ';
    }
    else
    {
	echo '
        <p id="gtv-no-templates">' . __( 'Sorry, no templates found', 'gtvouchers-d' ) . '</p>
        ';
    }

    echo '</div>'; // voucher_design
    echo '
    <h3>' . __( 'Settings', 'gtvouchers-d' ) . '</h3>

    ';

    if ( 'true' != $options[ 'hide_email_required_option' ] )
    {
	echo '
        <p><label for="requireemail">' . __( 'Require email address', 'gtvouchers-d' ) . '</label>
        <input type="checkbox" name="requireemail" id="requireemail" value="1"';

	if ( 'true' == $options[ 'admin_require_email' ] )
	{
	    echo ' checked="checked" ';
	}

	echo ' /> <span class="gtv_message_note">' . sprintf( __( 'Check this box to require a valid email address to be given before this %s can be downloaded', 'gtvouchers-d' ), strtolower( $options[ 'replace_voucher_singular_word' ] ) ) . '</span></p>
        ';
    }

    if ( 'true' == $options[ 'admin_require_email' ] && 'true' == $options[ 'hide_email_required_option' ] )
    {
	echo '<input type="hidden" name="requireemail" value="1" />';
    }

    if ( isset( $voucher ) && is_object( $voucher ) )
    {
	do_action( 'gtvouchers_after_require_email', $voucher->id, $voucher->place_id );
    }

    if ( 'true' != $options[ 'hide_phone_required_option' ] )
    {
	echo '
        <p><label for="requirephone">' . __( 'Require phone number', 'gtvouchers-d' ) . '</label>
        <input type="checkbox" name="requirephone" id="requirephone" value="1"';

	if ( 'true' == $options[ 'admin_require_phone' ] )
	{
	    echo ' checked="checked" ';
	}

	echo ' /> <span class="gtv_message_note">' . sprintf( __( 'Check this box to require a phone number be given before this %s can be downloaded. <strong>Require Email must also be checked.</strong>', 'gtvouchers-d' ), strtolower( $options[ 'replace_voucher_singular_word' ] ) ) . '</span></p>
        ';
    }

    if ( 'true' != $options[ 'hide_facebook_option' ] )
    {
	echo '<p><label for="requirelike">' . __( 'Require Facebook Like', 'gtvouchers-d' ) . '</label>
    <input type="checkbox" name="requirelike" id="requirelike" value="1"';

	if ( 'true' == $options[ 'admin_require_like' ] )
	{
	    echo ' checked="checked" ';
	}

	echo '/> <span class="gtv_message_note">' . sprintf( __( 'Check this box to require the user to Like the listing page before this %s can be downloaded', 'gtvouchers-d' ), strtolower( $options[ 'replace_voucher_singular_word' ] ) ) . '</span></p>

    <p id="liketextcta"><label for="liketextcta">' . __( 'Facebook call to action', 'gtvouchers-d' ) . '</label>
    <input type="text" name="liketextcta" id="liketextcta" style="width:200px;" /> <span class="gtv_message_note">' . __( 'Text shown above the Facebook Like icon. eg: "Like us on Facebook to download a valuable coupon."', 'gtvouchers-d' ) . '</span></p>';
    }

    if ( 'true' == $options[ 'admin_require_like' ] && 'true' == $options[ 'hide_facebook_option' ] )
    {
	echo '<input type="hidden" name="requirelike" value="1" />';
    }

    echo '<p><label for="limit">' . sprintf( __( 'Number of %s available', 'gtvouchers-d' ), strtolower( $options[ 'replace_voucher_singular_word' ] ) ) . '</label>
    <input type="text" name="limit" id="limit" class="num" value="" /> <span  class="gtv_message_note">' . sprintf( __( 'Set the number of times this %s can be downloaded (leave blank or 0 for unlimited)', 'gtvouchers-d' ), strtolower( $options[ 'replace_voucher_singular_word' ] ) ) . '</span></p>

    <br /><p><label for="startyear">' . sprintf( __( 'Date %s starts being available', 'gtvouchers-d' ), strtolower( $options[ 'replace_voucher_singular_word' ] ) ) . '</label>
    ' . __( 'Year:', 'gtvouchers-d' ) . ' <input type="text" name="startyear" id="startyear" class="num" value="" />
    ' . __( 'Month:', 'gtvouchers-d' ) . ' <input type="text" name="startmonth" id="startmonth" class="num" value="" />
    ' . __( 'Day:', 'gtvouchers-d' ) . ' <input type="text" name="startday" id="startday" class="num" value="" />
    <span class="gtv_message_note">' . sprintf( __( 'Enter the date on which this %s will become available (leave blank if this %s is immediately available)', 'gtvouchers-d' ), strtolower( $options[ 'replace_voucher_singular_word' ] ), strtolower( $options[ 'replace_voucher_singular_word' ] ) ) . '</span></p>

    <p><label for="expiryyear">' . sprintf( __( 'Date %s expires', 'gtvouchers-d' ), strtolower( $options[ 'replace_voucher_singular_word' ] ) ) . '</label>
    ' . __( 'Year:', 'gtvouchers-d' ) . ' <input type="text" name="expiryyear" id="expiryyear" class="num" value="" />
    ' . __( 'Month:', 'gtvouchers-d' ) . ' <input type="text" name="expirymonth" id="expirymonth" class="num" value="" />
    ' . __( 'Day:', 'gtvouchers-d' ) . ' <input type="text" name="expiryday" id="expiryday" class="num" value="" />
    <span class="gtv_message_note">' . sprintf( __( 'Enter the date on which this %s will expire (leave blank for never)', 'gtvouchers-d' ), strtolower( $options[ 'replace_voucher_singular_word' ] ) ) . '</span></p>

    <p><label for="expirydays">' . sprintf( __( 'Or enter the number of days the %s will stay live', 'gtvouchers-d' ), strtolower( $options[ 'replace_voucher_singular_word' ] ) ) . '</label>
    <input type="text" class="num" name="expirydays" id="expirydays" value="" /></p>';

    if ( "true" != $options[ 'vouchers_require_approval' ] || current_user_can( 'update_core' ) )
    {
	echo '<p><strong>' . sprintf( __( 'This box MUST be checked for this %s to be available.', 'gtvouchers-d' ), strtolower( $options[ 'replace_voucher_singular_word' ] ) ) . '</strong></p>
        <p><label for="live">' . sprintf( __( '%s available', 'gtvouchers-d' ), $options[ 'replace_voucher_singular_word' ] ) . '</label>
        <input type="checkbox" name="live" id="live" value="1" checked="checked" /> <span class="gtv_message_note">' . sprintf( __( 'Check this box to allow this %s to be downloaded', 'gtvouchers-d' ), strtolower( $options[ 'replace_voucher_singular_word' ] ) ) . '</span></p>';
    }
    else
    {
	echo '<input type="hidden" name="live" id="live" value="0" />';
	echo '<br />';
    }

    echo '<div id="voucher_codes">
    <h3>' . sprintf( __( '%s codes', 'gtvouchers-d' ), $options[ 'replace_voucher_singular_word' ] ) . '</h3>

    <p>' . __( 'The code prefix and suffix will only be used on random and sequential codes.', 'gtvouchers-d' ) . '</p>

    <p id="codeprefixline"><label for="codeprefix">' . __( 'Code prefix', 'gtvouchers-d' ) . '</label>
    <input type="text" name="codeprefix" id="codeprefix" /> <span class="gtv_message_note">' . __( 'Text to show before the code (eg <strong>ABC</strong>123XYZ)', 'gtvouchers-d' ) . '</span></p>

    <p id="codesuffixline"><label for="codesuffix">' . __( 'Code suffix', 'gtvouchers-d' ) . '</label>
    <input type="text" name="codesuffix" id="codesuffix" /> <span class="gtv_message_note">' . __( 'Text to show after the code (eg ABC123<strong>XYZ</strong>)', 'gtvouchers-d' ) . '</span></p>

    <p><label for="randomcodes">' . __( 'Use random codes', 'gtvouchers-d' ) . '</label>
    <input type="radio" name="codestype" id="randomcodes" value="random" checked="checked" /> <span class="gtv_message_note">' . __( 'Check this box to use a random character code each time', 'gtvouchers-d' ) . '</span></p>

    <p class="hider" id="codelengthline"><label for="codelength">' . __( 'Random code length', 'gtvouchers-d' ) . '</label>
    <select name="codelength" id="codelength">
    <option value="6">6</option>
    <option value="7">7</option>
    <option value="8">8</option>
    <option value="9">9</option>
    <option value="10">10</option>
    </select> <span class="gtv_message_note">' . __( 'How long would you like the random code to be?', 'gtvouchers-d' ) . '</span></p>

    <p><label for="sequentialcodes">' . __( 'Use sequential codes', 'gtvouchers-d' ) . '</label>
    <input type="radio" name="codestype" id="sequentialcodes" value="sequential" /> <span class="gtv_message_note">' . __( 'Check this box to use sequential codes (1, 2, 3 etc)', 'gtvouchers-d' ) . '</span></p>

    <p><label for="customcodes">' . __( 'Use custom codes', 'gtvouchers-d' ) . '</label>
    <input type="radio" name="codestype" id="customcodes" value="custom" /> <span class="gtv_message_note">' . sprintf( __( 'Check this box to use your own codes on each download of this %s. You must enter all the codes you want to use below:', 'gtvouchers-d' ), strtolower( $options[ 'replace_voucher_singular_word' ] ) ) . '</span></p>

    <p class="hider" id="customcodelistline"><label for="customcodelist">' . __( 'Custom codes (one per line)', 'gtvouchers-d' ) . '</label>
    <textarea name="customcodelist" id="customcodelist" rows="6" cols="100"></textarea></p>

    <p><label for="singlecode">' . __( 'Use a single code', 'gtvouchers-d' ) . '</label>
    <input type="radio" name="codestype" id="singlecode" value="single" /> <span class="gtv_message_note">' . __( 'Check this box to use one code on all downloads. Enter the code you want to use below:', 'gtvouchers-d' ) . '</span></p>

    <p class="hider" id="singlecodetextline"><label for="singlecodetext">' . __( 'Single code', 'gtvouchers-d' ) . '</label>
    <input type="text" name="singlecodetext" id="singlecodetext" /></p>
    </div>
    <p>';

    // Hide the "Preview" button from the front until I get it working.
    if ( 'backend' == $location )
    {
	echo '<input type="button" name="preview" id="previewbutton" class="button" value="' . __( 'Preview', 'gtvouchers-d' ) . '" />';
    }
    echo '
    <input type="submit" name="save" id="savebutton" class="button-primary" value="' . __( 'Save', 'gtvouchers-d' ) . '" />
    <input type="hidden" name="template" id="template" value="1" />';
    wp_nonce_field( 'gtvouchers_create' );
    echo '</p>

    </form>

    <script type="text/javascript">
    //jQuery(document).ready(gtv_show_random);
    </script>
    ';
}

// show the edit voucher page
function gtvouchers_edit_voucher_page( $location = 'backend' )
{
    wp_enqueue_script( 'thickbox' );
    $options = get_option( 'gtvouchers_options' );

    if ( isset( $_GET[ 'id' ] ) && ( 'new' != $_GET[ 'id' ] ) )
    {
	$voucher = gtvouchers_get_voucher( intval( $_GET[ 'id' ] ), 0 );
    }
    elseif ( isset( $_GET[ 'place_id' ] ) && !( isset( $_GET[ 'id' ] ) && 'new' == $_GET[ 'id' ] ) )
    {
	gtvouchers_choose_voucher();
    }

    // Admin level gets a pass on security check
    if ( !current_user_can( 'administrator' ) )
    {
	if ( isset( $voucher ) && ( get_current_user_id() != $voucher->author_id ) )
	{
	    echo '<h2>';
	    printf( __( 'You are not authorized to edit this %s.', 'gtvouchers-d' ), $options[ 'replace_voucher_plural_word' ] );
	    echo '</h2>';

	    return;
	}
    }

    if ( isset( $voucher ) && is_object( $voucher ) )
    {
	echo '
        <h2>' . sprintf( __( 'Edit %s:', 'gtvouchers-d' ), $options[ 'replace_voucher_plural_word' ] ) . ' ' . htmlspecialchars( stripslashes( $voucher->name ) ) . ' <span class="r">';

	if ( $voucher->downloads > 0 )
	{
	    echo __( 'Downloads:', 'gtvouchers-d' ) . ' ' . $voucher->downloads;
	    $options = get_option( 'gtvouchers_options' );
	    if ( 'true' != $options[ 'hide_csv_download_report' ] || current_user_can( 'administrator' ) )
	    {
		$csv_url = admin_url( 'admin.php?page=vouchers&amp;download=emails&amp;voucher=' . $voucher->id );
		echo ' | <a href="' . wp_nonce_url( $csv_url, 'gtvouchers_download_csv' ) . '" class="button">' . __( 'CSV', 'gtvouchers-d' ) . '</a>';
	    }
	}

	echo '</span></h2>';

	if ( 'backend' == $location && current_user_can( 'administrator' ) )
	{
	    echo '
            <div class="hiders" id="shortcodes">

            <h3>' . sprintf( __( 'Shortcode for this %s:', 'gtvouchers-d' ), $options[ 'replace_voucher_singular_word' ] ) . ' <input type="text" value="[voucher id=&quot;' . $voucher->id . '&quot;]" /></h3>
            <p><a href="' . gtvouchers_link( $voucher->guid ) . '">' . htmlspecialchars( stripslashes( $voucher->name ) ) . '</a></p>

            <h3>' . sprintf( __( 'Shortcode for this %s with description:', 'gtvouchers-d' ), $options[ 'replace_voucher_plural_word' ] ) . ' <input type="text" value="[voucher id=&quot;' . $voucher->id . '&quot; description=&quot;true&quot;]" /></h3>
            <p><a href="' . gtvouchers_link( $voucher->guid ) . '">' . htmlspecialchars( stripslashes( $voucher->name ) ) . '</a> ' . htmlspecialchars( stripslashes( $voucher->description ) ) . '</p>

            ';

	    if ( '1' == $voucher->require_email )
	    {
		echo '
                <h3>' . sprintf( __( 'Shortcode for this %s registration form:', 'gtvouchers-d' ), $options[ 'replace_voucher_plural_word' ] ) . '</h3>
                <p><input type="text" value="[voucherform id=&quot;' . $voucher->id . '&quot;]" /></p>
                ';
	    }

	    echo '</div>';
	}

	if ( isset( $_GET[ 'result' ] ) && '' != $_GET[ 'result' ] )
	{
	    gtvouchers_clear_gtv_transients();
	    if ( '1' == $_GET[ 'result' ] )
	    {
		echo '
                <div id="message" class="updated fade">
                    <p style="color: black;background-color:#FFFFE0;border:1px solid #E6DB55;font-weight:bold;width:80%;>">';
		printf( __( 'Your %s has been saved', 'gtvouchers-d' ), $options[ 'replace_voucher_singular_word' ] );
		delete_single_cache( $_GET[ 'id' ] );
		if ( 'true' == $options[ 'vouchers_require_approval' ] && !current_user_can( 'update_core' ) )
		{
		    _e( ' and will be reviewed before being available', 'gtvouchers-d' );
		}
		echo '.</p>
                </div>
                ';
	    }
	    if ( '2' == $_GET[ 'result' ] )
	    {
		echo '
                <div id="message" class="error">
                    <p><strong>' . sprintf( __( 'Sorry, your %s could not be edited.', 'gtvouchers-d' ), $options[ 'replace_voucher_singular_word' ] ) . '</strong></p>
                </div>
                ';
	    }
	    if ( '3' == $_GET[ 'result' ] )
	    {
		echo '
                <div id="message" class="updated fade">
                    <p><strong>' . sprintf( __( 'Your %s has been edited.', 'gtvouchers-d' ), $options[ 'replace_voucher_singular_word' ] ) . '</strong></p>
                </div>
                ';
		delete_single_cache( $_GET[ 'id' ] );
	    }
	    if ( '4' == $_GET[ 'result' ] )
	    {
		echo '
                <div id="message" class="updated fade">
                    <p><strong>' . sprintf( __( 'The %s has been deleted.', 'gtvouchers-d' ), $options[ 'replace_voucher_singular_word' ] ) . '</strong></p>
                </div>
                ';
	    }
	    if ( '5' == $_GET[ 'result' ] )
	    {
		echo '
                <div id="message" class="error">
                    <p><strong>' . sprintf( __( 'The %s could not be deleted.', 'gtvouchers-d' ), $options[ 'replace_voucher_singular_word' ] ) . '</strong></p>
                </div>
                ';
	    }
	}

	// if this voucher has an expiry date which has passed
	if ( '' != $voucher->expiry && 0 != intval( $voucher->expiry ) && time() >= intval( $voucher->expiry ) )
	{
	    echo '
            <div id="message" class="updated fade">
                <p><strong>' . sprintf( __( 'This %s expired on %s. Change the expiry date below to allow this %s to be downloaded.', 'gtvouchers-d' ), $options[ 'replace_voucher_singular_word' ], date( get_option( "date_format" ), $voucher->expiry ), $options[ 'replace_voucher_plural_word' ] ) . '</strong></p>
            </div>
            ';
	}

	// if this voucher has a start date not yet reached
	if ( '' != $voucher->startdate && 0 != intval( $voucher->startdate ) && time() < intval( $voucher->startdate ) )
	{
	    echo '
            <div id="message" class="updated fade">
                <p><strong>' . sprintf( __( 'This %s is not yet available. Change the start date below to allow this %s to be downloaded.', 'gtvouchers-d' ), $options[ 'replace_voucher_plural_word' ], $options[ 'replace_voucher_plural_word' ] ) . '</strong></p>
            </div>
            ';
	}

	echo '<p><a href="' . get_post_permalink( intval( $voucher->place_id ) ) . '" style="text-decoration:underline;">' . __( 'Return to listing', 'gtvouchers-d' ) . '</a></p>';

	if ( 'backend' == $location )
	{
	    echo '<form action="admin.php?page=vouchers&amp;id=' . intval( $_GET[ 'id' ] ) . '" method="post" id="voucherform">';
	}
	else
	{
	    echo '<form action="?gtv-page=save-voucher&amp;id=' . intval( $_GET[ 'id' ] ) . '" method="post" id="voucherform">';
	    echo '<input type="hidden" name="place_id" value="' . intval( $_GET[ 'place_id' ] ) . '" />';
	}

	if ( 'true' == $options[ 'vouchers_require_approval' ] && !current_user_can( 'update_core' ) )
	{
	    if ( '1' != $_GET[ 'result' ] )
	    {
		echo '<p class="warning"><strong>';
		printf( __( 'Editing this %s will make it unavailable until the site admin approves the changes.', 'gtvouchers-d' ), $options[ 'replace_voucher_plural_word' ] );
		echo '</strong></p>';
	    }
	}

	if ( 0 != $voucher->use_custom_image )
	{
	    echo '<label for="name">' . sprintf( __( '%s name:', 'gtvouchers-d' ), $options[ 'replace_voucher_singular_word' ] ) . '</label><input type="text" name="name" id="name" value="' . $voucher->name . '" /><br /><br />';
	}

	if ( 'true' == $options[ 'use_categories' ] || current_user_can( 'update_core' ) )
	{
	    gtvouchers_select_voucher_category( $voucher->category_id );
	}

	echo '<div class="geodir_form_row clearfix">' . sprintf( __( '%s description (optional): enter a longer description here which will go in the email sent to a user registering for this %s.', 'gtvouchers-d' ), $options[ 'replace_voucher_singular_word' ], strtolower( $options[ 'replace_voucher_singular_word' ] ) ) . '</div>
    <div class="geodir_form_row clearfix"><textarea name="description" id="description" rows="3" cols="100" class="geodir_textarea">' . stripslashes( $voucher->description ) . '</textarea></div>

        ';

	do_action( 'gtv_post_voucher_description' );

	if ( 0 == $voucher->use_custom_image )
	{

	    echo '
            <div id="voucherpreview" style="background-image:url(' . GTV_TEMPLATE_URL . '/' . $voucher->template . '_preview.jpg)">

                <h2><textarea name="name" id="name" rows="2" cols="100">' . stripslashes( $voucher->name ) . '</textarea></h2>

                <p><textarea name="text" id="text" rows="3" cols="100">' . stripslashes( $voucher->text ) . '</textarea></p>

                <p>[' . sprintf( __( 'The %s code will be inserted automatically here', 'gtvouchers-d' ), strtolower( $options[ 'replace_voucher_singular_word' ] ) ) . ']</p>

                <p id="voucherterms"><textarea name="terms" id="terms" rows="4" cols="100">' . stripslashes( $voucher->terms ) . '</textarea></p>

            </div>';

	    $fonts = gtvouchers_fonts();
	    echo '
            <h3>' . __( 'Font', 'gtvouchers-d' ) . '</h3>
            <div class="geodir_form_row clearfix"><label for="font">' . __( 'Font', 'gtvouchers-d' ) . '</label>
            <select name="font" id="font" class="geodir_select">
            ';
	    foreach ( $fonts as $font )
	    {
		echo '
                <option value="' . $font[ 0 ] . '"' . selected( $voucher->font, $font[ 0 ], false ) . '>' . $font[ 1 ] . '</option>
                ';
	    }
	    echo '
            </select> <span class="geodir_message_note">' . sprintf( __( 'Set the font for this %s', 'gtvouchers-d' ), strtolower( $options[ 'replace_voucher_singular_word' ] ) ) . '</span></div>
            ';

	    if ( empty( $voucher->textcolor ) )
	    {
		$voucher->textcolor = '#000000';
	    }

	    global $wp_version;
	    //If the WordPress version is greater than or equal to 3.5, then load the new WordPress color picker.
	    if ( 3.5 <= $wp_version && 'backend' == $location )
	    {
		//Both the necessary css and javascript have been registered already by WordPress, so all we have to do is load them with their handle.
		wp_enqueue_style( 'wp-color-picker' );
	    } //If the WordPress version is less than 3.5 load the older farbtasic color picker.
	    else
	    {
		//As with wp-color-picker the necessary css and javascript have been registered already by WordPress, so all we have to do is load them with their handle.
		wp_enqueue_script( 'farbtastic' );
		wp_enqueue_style( 'farbtastic' );
	    }

	    echo '
            <div class="geodir_form_row clearfix">
            <label for="textcolor">' . __( 'Text color', 'gtvouchers-d' ) . '</label>
            <input type="text" name="textcolor" id="vouchertextcolor" value="' . $voucher->textcolor . '" />
            <span class="geodir_message_note">' . sprintf( __( 'Set the color of text for this %s', 'gtvouchers-d' ), strtolower( $options[ 'replace_voucher_singular_word' ] ) ) . '</span>
            ';
	    echo '<div style="left: 350px;position:relative;" id="colorpicker3"></div></div>';

	    $templates = gtvouchers_get_templates();
	    if ( $templates && is_array( $templates ) && count( $templates ) > 0 )
	    {
		echo '
                <div id="voucherthumbs">
                <h3>' . __( 'Template', 'gtvouchers-d' ) . '</h3>
                ';
		foreach ( $templates as $template )
		{
		    echo '
                    <span><img src="' . GTV_TEMPLATE_URL . '/' . $template->id . '_thumb.jpg" id="template_' . $template->id . '" alt="' . $template->name . '" /></span>
                    ';
		}
		echo '
                </div>
                ';
	    }
	    else
	    {
		echo '
                <p id="gtv-no-templates">' . __( 'Sorry, no templates found', 'gtvouchers-d' ) . '</p>
                ';
	    }
	}
	else
	{
	    echo '<input type="hidden" name="use_custom_image" id="use_custom_image" value="true" />';
	    echo '<div class="geodir_form_row clearfix">
                <label for="voucher_image">' . sprintf( __( '%s image:', 'gtvouchers-d' ), $options[ 'replace_voucher_singular_word' ] ) . '</label>
                <input id="voucher_image" type="text" size="80" name="voucher_image" value="' . $voucher->voucher_image . '" />
                <input id="voucher_image_button" type="button" value="' . __( 'Select Image', 'gtvouchers-d' ) . '" />
                </div>
            ';
	    echo "<img src='{$voucher->voucher_image}' style='width:500px;' />";
	}

	echo '
        <h3>' . __( 'Settings', 'gtvouchers-d' ) . '</h3>';

	if ( 'true' != $options[ 'hide_email_required_option' ] )
	{
	    echo '
        <div class="geodir_form_row clearfix"><label for="requireemail">' . __( 'Require email address', 'gtvouchers-d' ) . '</label>
        <input type="checkbox" name="requireemail" id="requireemail" value="1"';
	    checked( $voucher->require_email, '1', true );
	    echo '/> <span class="geodir_message_note">' . sprintf( __( 'Check this box to require a valid email address to be given before this %s can be downloaded', 'gtvouchers-d' ), strtolower( $options[ 'replace_voucher_singular_word' ] ) ) . '</span></div>
        ';
	}

	do_action( 'gtvouchers_after_require_email', $voucher->id, $voucher->place_id );

	if ( 'true' != $options[ 'hide_phone_required_option' ] )
	{
	    echo '
        <div class="geodir_form_row clearfix"><label for="requirephone">' . __( 'Require phone number', 'gtvouchers-d' ) . '</label>
        <input type="checkbox" name="requirephone" id="requirephone" value="1"';
	    checked( $voucher->require_phone, '1', true );
	    echo '/> <span class="geodir_message_note">' . sprintf( __( 'Check this box to require a phone number to be given before this %s can be downloaded. <strong>Require Email must also be checked.</strong>', 'gtvouchers-d' ), strtolower( $options[ 'replace_voucher_singular_word' ] ) ) . '</span></div>
        ';
	}

	if ( 'true' != $options[ 'hide_facebook_option' ] )
	{
	    echo '<p><label for="requirelike">' . __( 'Require Facebook Like', 'gtvouchers-d' ) . '</label>
        <input type="checkbox" name="requirelike" id="requirelike" value="1"';
	    checked( $voucher->require_like, '1', true );
	    echo '/> <span>' . sprintf( __( 'Check this box to require the user to Like the listing page before this %s can be downloaded', 'gtvouchers-d' ), strtolower( $options[ 'replace_voucher_singular_word' ] ) ) . '</span></p>
        ';

	    echo '<p id="liketextcta"><label for="liketextcta">' . __( 'Facebook call to action', 'gtvouchers-d' ) . '</label>
        <input type="text" name="liketextcta" id="liketextcta" style="width:200px;" value="' . esc_attr( $voucher->liketextcta ) . '" /> <span>' . __( 'Text shown above the Facebook Like icon. eg: "Like us on Facebook to download a valuable coupon."', 'gtvouchers-d' ) . '</span></p>';
	}

	echo '
        <div class="geodir_form_row clearfix"><label for="limit">' . sprintf( __( 'Number of %s available', 'gtvouchers-d' ), strtolower( $options[ 'replace_voucher_plural_word' ] ) ) . '</label>
        <input type="text" name="limit" id="limit" class="num" value="' . $voucher->limit . '" /> <span class="geodir_message_note">' . sprintf( __( 'Set the number of times this %s can be downloaded (leave blank for unlimited)', 'gtvouchers-d' ), strtolower( $options[ 'replace_voucher_singular_word' ] ) ) . '</span><br /><br /></div>
        <div class="geodir_form_row clearfix"><label for="startyear">' . sprintf( __( 'Date %s starts being available', 'gtvouchers-d' ), strtolower( $options[ 'replace_voucher_singular_word' ] ) ) . '</label>
        ' . __( 'Year:', 'gtvouchers-d' ) . '<input type="text" name="startyear" id="startyear" class="num geodir-textfield" value="';
	if ( '' != $voucher->startdate && 0 < $voucher->startdate )
	{
	    echo date( 'Y', $voucher->startdate );
	}
	echo '" />
        ' . __( 'Month:', 'gtvouchers-d' ) . ' <input type="text" name="startmonth" id="startmonth" class="num" value="';
	if ( '' != $voucher->startdate && 0 < $voucher->startdate )
	{
	    echo date( 'n', $voucher->startdate );
	}
	echo '" />
        ' . __( 'Day:', 'gtvouchers-d' ) . ' <input type="text" name="startday" id="startday" class="num" value="';
	if ( '' != $voucher->startdate && 0 < $voucher->startdate )
	{
	    echo date( 'j', $voucher->startdate );
	}
	echo '" />
        <span class="geodir_message_note">' . sprintf( __( 'Enter the date on which this %s will become available (leave blank if this voucher is available immediately)', 'gtvouchers-d' ), strtolower( $options[ 'replace_voucher_singular_word' ] ) ) . '</span>
		</div>
		<br />
        <div class="geodir_form_row clearfix"><label for="expiry">' . sprintf( __( 'Date %s expires', 'gtvouchers-d' ), strtolower( $options[ 'replace_voucher_singular_word' ] ) ) . '</label>
        ' . __( 'Year:', 'gtvouchers-d' ) . ' <input type="text" name="expiryyear" id="expiryyear" class="num" value="';
	if ( '' != $voucher->expiry && 0 < $voucher->expiry )
	{
	    echo date( 'Y', $voucher->expiry );
	}
	echo '" />
        ' . __( 'Month:', 'gtvouchers-d' ) . ' <input type="text" name="expirymonth" id="expirymonth" class="num" value="';
	if ( '' != $voucher->expiry && 0 < $voucher->expiry )
	{
	    echo date( 'n', $voucher->expiry );
	}
	echo '" />
        ' . __( 'Day:', 'gtvouchers-d' ) . ' <input type="text" name="expiryday" id="expiryday" class="num" value="';
	if ( '' != $voucher->expiry && 0 < $voucher->expiry )
	{
	    echo date( 'j', $voucher->expiry );
	}
	echo '" />
        <span class="geodir_message_note">' . sprintf( __( 'Enter the date on which this %s will expire (leave blank for never)', 'gtvouchers-d' ), strtolower( $options[ 'replace_voucher_singular_word' ] ) ) . '</span></div>

        <div class="geodir_form_row clearfix"><label for="expirydays">' . sprintf( __( 'Or enter the number of days the %s will stay live', 'gtvouchers-d' ), strtolower( $options[ 'replace_voucher_singular_word' ] ) ) . '</label>
    <input type="text" class="num" name="expirydays" id="expirydays" value="" />';

	if ( "true" != $options[ 'vouchers_require_approval' ] || current_user_can( 'update_core' ) )
	{
	    echo '<p><strong>' . sprintf( __( 'This box MUST be checked for this %s to be available.', 'gtvouchers-d' ), strtolower( $options[ 'replace_voucher_singular_word' ] ) ) . '</strong></div>
        <div class="geodir_form_row clearfix"><label for="live">' . sprintf( __( '%s available', 'gtvouchers-d' ), $options[ 'replace_voucher_singular_word' ] ) . '</label>
        <input type="checkbox" name="live" id="live" value="1"';
	    checked( $voucher->live, '1', true );
	    echo '/> <span class="geodir_message_note">' . sprintf( __( 'Check this box to allow this %s to be downloaded', 'gtvouchers-d' ), strtolower( $options[ 'replace_voucher_singular_word' ] ) ) . '</span></div>';
	}
	else
	{
	    echo '<input type="hidden" name="live" id="live" value="0" />';
	    echo '<br />';
	}

	if ( 0 == $voucher->use_custom_image )
	{
	    echo '<h3>' . sprintf( __( '%s codes', 'gtvouchers-d' ), $options[ 'replace_voucher_singular_word' ] ) . '</h3>

        <p>' . __( 'The code prefix and suffix will only be used on random and sequential codes.', 'gtvouchers-d' ) . '</p>

        <div class="geodir_form_row clearfix" id="codeprefixline"><label for="codeprefix">' . __( 'Code prefix', 'gtvouchers-d' ) . '</label>
    <input type="text" name="codeprefix" id="codeprefix" value="' . $voucher->codeprefix . '" /> <span class="geodir_message_note">' . __( 'Text to show before the sequential code (eg <strong>ABC</strong>123XYZ)', 'gtvouchers-d' ) . '</span></div>

        <div class="geodir_form_row clearfix" id="codesuffixline"><label for="codesuffix">' . __( 'Code suffix', 'gtvouchers-d' ) . '</label>
    <input type="text" name="codesuffix" id="codesuffix" value="' . $voucher->codesuffix . '" /> <span class="geodir_message_note">' . __( 'Text to show after the sequential code (eg ABC123<strong>XYZ</strong>)', 'gtvouchers-d' ) . '</span></div>

        <div class="geodir_form_row clearfix"><label for="randomcodes">' . __( 'Use random codes', 'gtvouchers-d' ) . '</label>
        <input type="radio" name="codestype" id="randomcodes" value="random"';
	    checked( $voucher->codestype, 'random' );
	    checked( $voucher->codestype, '' );
	    echo ' /> <span class="geodir_message_note">' . __( 'Check this box to use a random 6-character code each time', 'gtvouchers-d' ) . '</span></div>

        <div class="geodir_form_row clearfix hider" id="codelengthline"><label for="codelength">' . __( 'Random code length', 'gtvouchers-d' ) . '</label>
        <select name="codelength" id="codelength">';

	    for ( $i = 6; $i < 11; $i ++ )
	    {
		echo '<option value="' . $i . '" ' . selected( $voucher->codelength, $i, false ) . '>' . $i . '</option>';
	    }

	    echo '</select> <span class="geodir_message_note">' . __( 'How long would you like the random code to be?', 'gtvouchers-d' ) . '</span></div>

        <div class="geodir_form_row clearfix"><label for="sequentialcodes">' . __( 'Use sequential codes', 'gtvouchers-d' ) . '</label>
        <input type="radio" name="codestype" id="sequentialcodes" value="sequential"';
	    checked( $voucher->codestype, 'sequential' );
	    echo ' /> <span class="geodir_message_note">' . __( 'Check this box to use sequential codes (1, 2, 3 etc)', 'gtvouchers-d' ) . '</span></div>

        <div class="geodir_form_row clearfix"><label for="customcodes">' . __( 'Use custom codes', 'gtvouchers-d' ) . '</label>
        <input type="radio" name="codestype" id="customcodes" value="custom"';
	    checked( $voucher->codestype, 'custom' );
	    echo ' /> <span class="geodir_message_note">' . __( 'Check this box to use your own codes. You must enter all the codes you want to use below:', 'gtvouchers-d' ) . '</span></div>

        <div class="geodir_form_row clearfix hider" id="customcodelistline"><label for="customcodelist">' . __( 'Custom codes (one per line)', 'gtvouchers-d' ) . '</label>
        <textarea name="customcodelist" id="customcodelist" rows="6" cols="100">';
	    if ( $voucher->codestype == 'custom' )
	    {
		echo $voucher->codes;
	    }
	    echo '</textarea></div>

        <div class="geodir_form_row clearfix"><label for="singlecode">' . __( 'Use a single code', 'gtvouchers-d' ) . '</label>
        <input type="radio" name="codestype" id="singlecode" value="single"';
	    checked( $voucher->codestype, 'single' );
	    echo ' /> <span class="geodir_message_note">' . sprintf( __( 'Check this box to use one code on all downloads of this %s. Enter the code you want to use below:', 'gtvouchers-d' ), strtolower( $options[ 'replace_voucher_singular_word' ] ) ) . '</span></div>

        <div class="geodir_form_row clearfix hider" id="singlecodetextline"><label for="singlecodetext">' . __( 'Single code', 'gtvouchers-d' ) . '</label>
        <input type="text" name="singlecodetext" id="singlecodetext" value="';
	    if ( $voucher->codestype == 'single' )
	    {
		echo $voucher->codes;
	    }
	    echo '" /></div>';
	}
	echo '<h3>' . sprintf( __( 'Delete %s', 'gtvouchers-d' ), strtolower( $options[ 'replace_voucher_singular_word' ] ) ) . '</h3>

        <div class="geodir_form_row clearfix"><label for="delete">' . sprintf( __( 'Delete %s', 'gtvouchers-d' ), $options[ 'replace_voucher_plural_word' ] ) . '</label>
        <input type="checkbox" name="delete" id="delete" value="1" /> <span class="geodir_message_note">' . sprintf( __( 'Check this box to delete this %s', 'gtvouchers-d' ), strtolower( $options[ 'replace_voucher_singular_word' ] ) ) . '</span></div>

        <p>';

	// Hide the "Preview" button from the front until I get it working.
	if ( 'backend' == $location )
	{
	    echo '<input type="button" name="preview" id="previewbutton" class="button" value="' . __( 'Preview', 'gtvouchers-d' ) . '" />';
	}
	echo '
        <input type="submit" name="save" id="savebutton" class="button-primary" value="' . __( 'Save', 'gtvouchers-d' ) . '" />
        <input type="hidden" name="template" id="template" value="' . $voucher->template . '" />';
	wp_nonce_field( 'gtvouchers_create' );
	echo '</p>

        </form>

        <script type="text/javascript">
        //jQuery(document).ready(gtv_show_' . $voucher->codestype . ');
        </script>
        ';
    }
    else
    {

	if ( isset( $_GET[ 'result' ] ) && '4' == $_GET[ 'result' ] )
	{
	    echo '
            <h2>' . sprintf( __( '%s deleted', 'gtvouchers-d' ), strtolower( $options[ 'replace_voucher_singular_word' ] ) ) . '</h2>
            <div id="message" class="updated fade">
                <p><strong>' . sprintf( __( 'The %s has been deleted.', 'gtvouchers-d' ), strtolower( $options[ 'replace_voucher_singular_word' ] ) ) . '</strong></p>
            </div>
            ';
	}
	elseif ( isset( $_GET[ "ID" ] ) )
	{
	    echo '
            <h2>' . sprintf( __( '%s not found', 'gtvouchers-d' ), $options[ 'replace_voucher_singular_word' ] ) . '</h2>
            <p>' . sprintf( __( 'Sorry, that %s was not found.', 'gtvouchers-d' ), strtolower( $options[ 'replace_voucher_singular_word' ] ) ) . '</p>
            ';
	}
    }
}

add_action( 'geodir_save_listing_payment', 'gtvouchers_clear_gtv_transients' );
add_action( 'geodir_downgrade_price', 'gtvouchers_clear_gtv_transients' );

function gtvouchers_clear_gtv_transients()
{
    delete_transient( 'gtv_visible_vouchers_places' );
}

// show the voucher reports page
function gtvouchers_reports_page()
{
    gtvouchers_report_header();
    $options = get_option( 'gtvouchers_options' );

    echo '
    <h2>' . sprintf( __( '%s reports', 'gtvouchers-d' ), $options[ 'replace_voucher_singular_word' ] ) . '</h2>

    ';

    gtvouchers_report_footer();
}

function gtvouchers_categories_page()
{
    $options = get_option( 'gtvouchers_options' );
    if ( !empty( $_POST ) && check_admin_referer( 'save_voucher_cats' ) )
    {
	gtvouchers_save_categories();
    }

    gtvouchers_report_header();
    echo '<h2>' . sprintf( __( '%s categories', 'gtvouchers-d' ), $options[ 'replace_voucher_singular_word' ] ) . '</h2>';

    $categories = gtv_get_category_list();
    echo '<form name="vcats" method="post">';
    wp_nonce_field( 'save_voucher_cats' );
    echo '<table class="wp-list-table widefat fixed" style="width:400px;">';
    echo '  <thead>
                <tr>
                    <th style="width:100px;">ID</th>
                    <th>' . __( 'Category name', 'gtvouchers-d' ) . '</th>
            </thead>';
    echo '  <tfoot>
                <tr>
                    <th>ID</th>
                    <th>' . __( 'Category name', 'gtvouchers-d' ) . '</th>
            </tfoot>';
    echo '<tr>
            <td>' . __( 'New category', 'gtvouchers-d' ) . '</td>
            <td><input type="text" name="vcategory[]" value="" maxlength="55" style="width:240px;" /></td>
          </tr>';
    if ( !( empty( $categories ) ) )
    {
	foreach ( $categories as $category )
	{
	    echo '<tr>';
	    echo '<td>' . $category->id . '</td>';
	    echo '<td><input type="text" name="vcategory[' . $category->id . ']" value="' . $category->cat_name . '"  maxlength="55" style="width:240px;" /></td>';
	    echo '</tr>';
	}
    }
    echo '</table>';
    echo '<input type="submit" class="button-primary" value="' . __( 'Save categories', 'gtvouchers-d' ) . '" style="margin-top:15px;" />';
    echo '</form>';

    gtvouchers_report_footer();
}

function gtvouchers_save_categories()
{
    global $wpdb;

    $prefix = $wpdb->prefix;

    if ( !empty( $_POST ) && check_admin_referer( 'save_voucher_cats' ) )
    {
	foreach ( $_POST[ 'vcategory' ] as $id => $name )
	{
	    if ( 0 == $id && '' !== $name )
	    {
		$wpdb->insert( $prefix . 'gtvouchers_categories', array(
		    'cat_name' => $name,
		    'status' => 1,
			), array(
		    '%s',
		    '%d'
		) );
	    }
	    else
	    {
		if ( '' !== $name )
		{
		    $wpdb->update( $prefix . 'gtvouchers_categories', array( 'cat_name' => $name ), array( 'id' => $id ), array( '%s' ) );
		}
		else
		{
		    $wpdb->delete( $prefix . 'gtvouchers_categories', array( 'id' => $id ), array( '%d' ) );
		}
	    }
	}
    }
}

function gtv_get_category_list()
{
    global $wpdb;

    $prefix = $wpdb->prefix;

    $sql = "SELECT id, cat_name FROM {$prefix}gtvouchers_categories WHERE status=1 ORDER BY cat_name";

    $status_list = $wpdb->get_results( $sql );

    return $status_list;
}

// show the templates page
function gtvouchers_templates_page()
{
    gtvouchers_report_header();
    $options = get_option( 'gtvouchers_options' );

    echo '
    <h2>' . sprintf( __( '%s templates', 'gtvouchers-d' ), $options[ 'replace_voucher_singular_word' ] ) . '</h2>
    ';

    // get templates
    $templates = gtvouchers_get_templates();

    // if submitting a form
    if ( $_POST && is_array( $_POST ) && 0 < count( $_POST ) )
    {
	// if updating templates
	if ( isset( $_POST[ '_wpnonce' ] ) )
	{
	    if ( wp_verify_nonce( $_POST[ '_wpnonce' ], 'gtvouchers_edit_template' ) && ( isset( $_POST[ 'action' ] ) && 'update' == $_POST[ 'action' ] ) )
	    {
		// loop templates
		foreach ( $templates as $template )
		{
		    $live = 1;
		    if ( isset( $_POST[ 'delete' . $template->id ] ) && '1' == $_POST[ 'delete' . $template->id ] )
		    {
			$live = 0;
		    }
		    // edit this template
		    gtvouchers_edit_template( $template->id, $_POST[ 'name' . $template->id ], $live );
		}

		// get the new templates
		$templates = gtvouchers_get_templates();

		echo '
                <div id="message" class="updated fade">
                    <p><strong>' . __( 'Templates updated', 'gtvouchers-d' ) . '</strong></p>
                </div>
                ';
	    }
	}
	// if adding a template
	if ( ( isset( $_POST[ 'action' ] ) && 'add' == $_POST[ 'action' ] ) )
	{

	    if ( isset( $_POST[ '_wpnonce' ] ) && wp_verify_nonce( $_POST[ '_wpnonce' ], 'gtvouchers_add_template' ) && $_FILES && is_array( $_FILES ) && 0 < count( $_FILES ) && '' != $_FILES[ 'file' ][ 'name' ] && 0 < intval( $_FILES[ 'file' ][ 'size' ] ) )
	    {
		// check the GD functions exist
		if ( function_exists( 'imagecreatetruecolor' ) && function_exists( 'getimagesize' ) && function_exists( 'imagejpeg' ) )
		{

		    $name = $_POST[ 'name' ];
		    if ( $name == '' )
		    {
			$name = 'New template ' . date( 'F j, Y, g:i a' );
		    }

		    // try to save the template name
		    $id = gtvouchers_add_template( $name );

		    // if the id can be fetched
		    if ( $id )
		    {

			$uploaded = gtvouchers_upload_template( $id, $_FILES[ 'file' ] );

			if ( $uploaded )
			{

			    echo '
                            <div id="message" class="updated fade">
                                <p><strong>' . __( 'Your template has been uploaded.', 'gtvouchers-d' ) . '</strong></p>
                            </div>
                            ';

			    // get templates
			    $templates = gtvouchers_get_templates();
			}
			else
			{

			    echo '
                            <div id="message" class="error">
                                <p><strong>' . __( 'Sorry, the template file you uploaded was not in the correct format (JPEG), or was not the correct size (1181 x 532 pixels). Please upload a correct template file.', 'gtvouchers-d' ) . '</strong></p>
                            </div>
                            ';
			}
		    }
		    else
		    {

			echo '
                        <div id="message" class="error">
                            <p><strong>' . __( 'Sorry, your template could not be saved. Please try again.', 'gtvouchers-d' ) . '</strong></p>
                        </div>
                        ';
		    }
		}
		else
		{
		    echo '
                    <div id="message" class="error">
                        <p><strong>' . __( 'Sorry, your host does not support GD image functions, so you cannot add your own templates.', 'gtvouchers-d' ) . '</strong></p>
                    </div>
                    ';
		}
	    }
	    else
	    {
		echo '
                <div id="message" class="error">
                    <p><strong>' . __( 'Please attach a template file', 'gtvouchers-d' ) . '</strong></p>
                </div>
                ';
	    }
	}
    }

    if ( function_exists( 'imagecreatetruecolor' ) && function_exists( 'getimagesize' ) && function_exists( 'imagejpeg' ) )
    {
	echo '
        <h3>' . __( 'Add a template', 'gtvouchers-d' ) . '</h3>

        <form action="admin.php?page=vouchers-templates" method="post" enctype="multipart/form-data" id="templateform">

        <p>' . __( sprintf( 'To create your own templates use <a href="%s">this empty template</a>.', GTV_TEMPLATE_URL . '/1.jpg' ), 'gtvouchers-d' ) . '</p>

        <p><label for="file">' . __( 'Template file', 'gtvouchers-d' ) . '</label>
        <input type="file" name="file" id="file" /></p>

        <p><label for="name">' . __( 'Template name', 'gtvouchers-d' ) . '</label>
        <input type="text" name="name" id="name" /></p>

        <p><input type="submit" class="button-primary" value="' . __( 'Add template', 'gtvouchers-d' ) . '" />
        <input type="hidden" name="action" value="add" />';
	wp_nonce_field( 'gtvouchers_add_template' );
	echo '</p>

        </form>
        ';
    }
    else
    {
	echo '
        <p>' . __( 'Sorry, your host does not support GD image functions, so you cannot add your own templates.', 'gtvouchers-d' ) . '</p>
        ';
    }

    if ( $templates && is_array( $templates ) && 0 < count( $templates ) )
    {
	echo '
        <form id="templatestable" method="post" action="">
        ';
	gtvouchers_table_header( array(
	    'Preview',
	    'Name',
	    'Delete'
	) );
	foreach ( $templates as $template )
	{
	    echo '
            <tr>
                <td><a href="' . GTV_TEMPLATE_URL . '/' . intval( $template->id ) . '_preview.jpg" class="templatepreview"><img src="' . GTV_TEMPLATE_URL . '/' . intval( $template->id ) . '_thumb.jpg" alt="' . $template->name . '" /></a></td>
                ';
	    // if this is not a multisite-wide template
	    if ( '0' != $template->blog_id || (!defined( 'VHOST' ) && (!defined( 'MULTISITE' ) || '' == MULTISITE || false == MULTISITE ) ) )
	    {
		echo '
                    <td><input type="text" name="name' . intval( $template->id ) . '" value="' . $template->name . '" /></td>
                    <td><input class="checkbox" type="checkbox" value="1" name="delete' . intval( $template->id ) . '" /></td>
                    ';
	    }
	    else
	    {
		echo '
                    <td colspan="2">' . __( 'This template cannot be edited', 'gtvouchers-d' ) . '</td>
                    ';
	    }
	    echo '
            </tr>
            ';
	}
	gtvouchers_table_footer();
	echo '
        <p><input type="submit" class="button-primary" value="' . __( 'Save templates', 'gtvouchers-d' ) . '" />
        <input type="hidden" name="action" value="update" />';
	wp_nonce_field( 'gtvouchers_edit_template' );
	echo '</p>
        </form>
        ';
    }
    else
    {
	echo '
        <p id="gtv-no-templates">' . __( 'Sorry, no templates found', 'gtvouchers-d' ) . '</p>
        ';
    }

    gtvouchers_report_footer();
}

// include the gtvouchers CSS file
function gtvouchers_admin_css()
{
    wp_enqueue_style( 'gtv-styles', plugins_url( 'gt-vouchers.css', __FILE__ ) );
//	echo '
//    <link rel="stylesheet" href="' . plugins_url( 'gt-vouchers.css', __FILE__ ) . '" type="text/css" media="all" />
//    ';
}

// include the gtvouchers JS file
//function gtvouchers_admin_js() {
//	echo '
//    <script type="text/javascript">
//        var gtv_siteurl = "' . get_option( 'siteurl' ) . '";
//    </script>
//    ';
//}
// to display above every report
function gtvouchers_report_header()
{
    echo '
    <div id="gtvouchers">
    ';
}

// to display below every report
function gtvouchers_report_footer()
{
    echo '
    </div>
    ';
}

// display the header of a data table
function gtvouchers_table_header( $headings )
{
    echo '
    <table class="widefat post fixed">
    <thead>
    <tr>
    ';
    foreach ( $headings as $heading )
    {
	if ( __( 'Delete', 'gtvouchers-d' ) == $heading )
	{
	    echo '<th style="width:50px;">';
	}
	else
	{
	    echo '<th>';
	}
	echo __( $heading, 'gtvouchers-d' ) . '</th>
        ';
    }
    echo '
    </tr>
    </thead>
    <tbody>
    ';
}

// display the footer of a data table
function gtvouchers_table_footer()
{
    echo '
    </tbody>
    </table>
    ';
}

// ==========================================================================================
// general functions
// return a list of safe fonts
function gtvouchers_fonts()
{
    return array(
	array(
	    'times',
	    'Serif 1'
	),
	array(
	    'timesb',
	    'Serif 1 (bold)'
	),
	array(
	    'aefurat',
	    'Serif 2'
	),
	array(
	    'helvetica',
	    'Sans-serif 1'
	),
	array(
	    'helveticab',
	    'Sans-serif 1 (bold)'
	),
	array(
	    'dejavusans',
	    'Sans-serif 2'
	),
	array(
	    'dejavusansb',
	    'Sans-serif 2 (bold)'
	),
	array(
	    'courier',
	    'Monotype'
	),
	    //array( 'courierb', 'Monotype (bold)'),
    );
}

// Redundant function for now. Core fonts can't handle the array of arrays.
function gtvouchers_core_fonts()
{
    return array(
	'times' => 'Serif 1',
	'timesb' => 'Serif 1 (bold)',
	'aefurat' => 'Serif 2',
	'helvetica' => 'Sans-serif 1',
	'helveticab' => 'Sans-serif 1 (bold)',
	'dejavusans' => 'Sans-serif 2',
	'dejavusansb' => 'Sans-serif 2 (bold)',
	'courier' => 'Monotype',
	    //array( 'courierb' => 'Monotype (bold)'),
    );
}

// check if the site is using pretty URLs
function gtvouchers_pretty_urls()
{
    $structure = get_option( 'permalink_structure' );
    if ( '' !== $structure || false === strpos( $structure, '?' ) )
    {
	return true;
    }

    return false;
}

// create a URL to a gtvouchers page
function gtvouchers_link( $voucher_guid, $download_guid = '', $encode = true )
{
    if ( gtvouchers_pretty_urls() )
    {
	if ( '' != $download_guid )
	{
	    if ( $encode )
	    {
		$download_guid = '&amp;guid=' . urlencode( $download_guid );
	    }
	    else
	    {
		$download_guid = '&guid=' . urlencode( $download_guid );
	    }
	}

	return home_url() . '/?voucher=' . $voucher_guid . $download_guid;
    }
    if ( $download_guid != '' )
    {
	if ( $encode )
	{
	    $download_guid = '&amp;guid=' . urlencode( $download_guid );
	}
	else
	{
	    $download_guid = '&guid=' . urlencode( $download_guid );
	}
    }

    return home_url() . '/?voucher=' . $voucher_guid . $download_guid;
}

// create an md5 hash of a guid
// from http://php.net/manual/en/function.com-create-guid.php
function gtvouchers_guid( $length = 6 )
{
    if ( function_exists( 'com_create_guid' ) )
    {
	return substr( md5( str_replace( '{', '', str_replace( '}', '', com_create_guid() ) ) ), 0, $length );
    }
    else
    {
	mt_srand( ( double ) microtime() * 10000 );
	$charid = strtoupper( md5( uniqid( rand(), true ) ) );
	$hyphen = chr( 45 );
	$uuid = substr( $charid, 0, 8 ) . $hyphen . substr( $charid, 8, 4 ) . $hyphen . substr( $charid, 12, 4 ) . $hyphen . substr( $charid, 16, 4 ) . $hyphen . substr( $charid, 20, 12 );

	return substr( md5( str_replace( '{', '', str_replace( '}', '', $uuid ) ) ), 0, $length );
    }
}

// get the users IP address
// from http://roshanbh.com.np/2007/12/getting-real-ip-address-in-php.html
function gtvouchers_ip()
{
    //check ip from share internet
    if ( !empty( $_SERVER[ 'HTTP_CLIENT_IP' ] ) )
    {
	$ip = $_SERVER[ 'HTTP_CLIENT_IP' ];
    }
    elseif ( !empty( $_SERVER[ 'HTTP_X_FORWARDED_FOR' ] ) )
    {
	//to check ip is pass from proxy
	$ip = $_SERVER[ 'HTTP_X_FORWARDED_FOR' ];
    }
    else
    {
	$ip = $_SERVER[ 'REMOTE_ADDR' ];
    }

    return $ip;
}

// get the current blog ID (for WP Multisite) or '1' for standard WP
function gtvouchers_blog_id()
{
    global $current_blog;
    if ( is_object( $current_blog ) && $current_blog->blog_id != '' )
    {
	return $current_blog->blog_id;
    }
    else
    {
	return 1;
    }
}

// return yes or no
function gtvouchers_yes_no( $val )
{
    if ( !$val || $val == '' || $val == '0' )
    {
	return __( 'No', 'gtvouchers-d' );
    }
    else
    {
	return __( 'Yes', 'gtvouchers-d' );
    }
}

// create slug
// Bramus! pwnge! : simple method to create a post slug (http://www.bram.us/)
function gtvouchers_slug( $string )
{
    $slug = preg_replace( "/[^a-zA-Z0-9 -]/", "", $string );
    $slug = str_replace( " ", "-", $slug );
    $slug = strtolower( $slug );

    return $slug;
}

// ==========================================================================================
// voucher administration functions
// listen for downloads of email addresses
function gtvouchers_check_download()
{
    if ( isset( $_GET[ '_wpnonce' ] ) && isset( $_GET[ 'page' ] ) )
    {
	// download all unique email addresses
	if ( wp_verify_nonce( $_GET[ '_wpnonce' ], 'gtvouchers_download_csv' ) && ( isset( $_GET[ 'page' ] ) && 'vouchers' == $_GET[ 'page' ] ) && ( isset( $_GET[ 'download' ] ) && 'emails' == $_GET[ 'download' ] ) && (!( isset( $_GET[ 'voucher' ] ) ) ) )
	{
	    if ( !gtvouchers_download_emails() )
	    {
		wp_die( __( 'Sorry, the list could not be downloaded. Please click back and try again.', 'gtvouchers-d' ) );
	    }
	}
	// download unique email addresses for a voucher
	if ( wp_verify_nonce( $_GET[ '_wpnonce' ], 'gtvouchers_download_csv' ) && ( isset( $_GET[ 'page' ] ) && 'vouchers' == $_GET[ 'page' ] ) && ( isset( $_GET[ 'download' ] ) && 'emails' == $_GET[ 'download' ] ) && isset( $_GET[ 'voucher' ] ) )
	{
	    if ( !gtvouchers_download_emails( $_GET[ 'voucher' ] ) )
	    {
		wp_die( __( 'Sorry, the list could not be downloaded. Please click back and try again.', 'gtvouchers-d' ) );
	    }
	}
    }
}

// listen for previews of a voucher
function gtvouchers_check_preview()
{
    if ( isset( $_GET[ 'page' ] ) )
    {
	if ( ( 'vouchers' == $_GET[ 'page' ] || 'vouchers-create' == $_GET[ 'page' ] ) && ( isset( $_GET[ 'preview' ] ) && 'voucher' == $_GET[ 'preview' ] ) )
	{
	    gtvouchers_preview_voucher( $_POST[ 'template' ], $_POST[ 'font' ], $_POST[ 'textcolor' ], $_POST[ 'name' ], $_POST[ 'text' ], $_POST[ 'terms' ] );
	}
    }
}

// listen for creation of a voucher
function gtvouchers_check_create_voucher()
{
    if ( isset( $_POST[ '_wpnonce' ] ) && isset( $_GET[ 'page' ] ) )
    {
	if ( wp_verify_nonce( $_POST[ '_wpnonce' ], 'gtvouchers_create' ) && 'vouchers-create' == $_GET[ 'page' ] && !isset( $_GET[ 'preview' ] ) && $_POST && is_array( $_POST ) && 0 < count( $_POST ) )
	{

	    $done = gtvouchers_check_voucher( 'create', 'backend' );

	    if ( $done && is_array( $done ) && true == $done[ 0 ] && 0 < $done[ 1 ] )
	    {
		header( 'Location: admin.php?page=vouchers&id=' . $done[ 1 ] . '&result=1' );
		exit();
	    }
	    else
	    {
		header( 'Location: admin.php?page=vouchers-create&result=1' );
		exit();
	    }
	}
    }
}

// listen for editing of a voucher
function gtvouchers_check_edit_voucher()
{
    if ( isset( $_POST[ '_wpnonce' ] ) && isset( $_GET[ 'page' ] ) )
    {
	if ( wp_verify_nonce( $_POST[ '_wpnonce' ], 'gtvouchers_create' ) && 'vouchers' == $_GET[ 'page' ] && (!( isset( $_GET[ 'preview' ] ) ) || '' == $_GET[ 'preview' ] ) && $_POST && is_array( $_POST ) && 0 < count( $_POST ) )
	{
	    $id = intval( $_GET[ 'id' ] );
	    if ( isset( $_POST[ 'delete' ] ) )
	    {
		$done = gtvouchers_delete_voucher( $id );
		if ( $done )
		{
		    header( 'Location: admin.php?page=vouchers&id=' . $id . '&result=4' );
		    exit();
		}
		else
		{
		    header( 'Location: admin.php?page=vouchers&id=' . $id . '&result=5' );
		    exit();
		}
	    }

	    $done = gtvouchers_check_voucher( 'edit', 'backend' );

	    if ( $done )
	    {
		header( 'Location: admin.php?page=vouchers&id=' . $id . '&result=3' );
		exit();
	    }
	    else
	    {
		header( 'Location: admin.php?page=vouchers&id=' . $id . '&result=2' );
		exit();
	    }
	}
    }
}

function gtvouchers_check_voucher( $vaction, $location = 'backend' )
{
    if ( isset( $_POST[ '_wpnonce' ] ) && ( isset( $_GET[ 'page' ] ) || isset( $_GET[ 'gtv-page' ] ) ) )
    {
	if ( wp_verify_nonce( $_POST[ '_wpnonce' ], 'gtvouchers_create' ) && ( ( isset( $_GET[ 'page' ] ) && ( 'vouchers-create' == $_GET[ 'page' ] || 'vouchers' == $_GET[ 'page' ] ) ) || 'save-voucher' == $_GET[ 'gtv-page' ] ) && !isset( $_GET[ 'preview' ] ) && $_POST && is_array( $_POST ) && 0 < count( $_POST ) )
	{

	    $use_custom_image = false;
	    if ( isset( $_POST[ 'use_custom_image' ] ) )
	    {
		$use_custom_image = true;
	    }

	    $voucher_image = '';

	    if ( !( empty( $_FILES ) ) && !( empty( $_FILES[ 'voucher_image' ][ 'name' ] ) ) )
	    {
		if ( !function_exists( 'wp_handle_upload' ) )
		{
		    require_once( ABSPATH . 'wp-admin/includes/file.php' );
		}
		$uploadedfile = $_FILES[ 'voucher_image' ];
		$upload_overrides = array( 'test_form' => false );
		$voucher_image = wp_handle_upload( $uploadedfile, $upload_overrides );
		if ( $voucher_image )
		{
		    $voucher_image = $voucher_image[ 'url' ];
		}
		else
		{
		    $voucher_image = '';
		}
	    }

	    if ( isset( $_POST[ 'voucher_image' ] ) && !empty( $_POST[ 'voucher_image' ] ) )
	    {
		$voucher_image = esc_url_raw( $_POST[ 'voucher_image' ], array(
		    'http',
		    'https'
			) );
	    }

	    $require_email = 0;
	    if ( isset( $_POST[ 'requireemail' ] ) && '1' == $_POST[ 'requireemail' ] )
	    {
		$require_email = 1;
	    }

	    $require_phone = 0;
	    if ( isset( $_POST[ 'requirephone' ] ) && '1' == $_POST[ 'requirephone' ] )
	    {
		$require_phone = 1;
	    }

	    $require_like = 0;
	    if ( isset( $_POST[ 'requirelike' ] ) && '1' == $_POST[ 'requirelike' ] )
	    {
		$require_like = 1;
	    }

	    $liketextcta = '';
	    if ( isset( $_POST[ 'liketextcta' ] ) )
	    {
		$liketextcta = trim( $_POST[ 'liketextcta' ] );
	    }

	    $live = 0;
	    if ( isset( $_POST[ 'live' ] ) && '1' == $_POST[ 'live' ] )
	    {
		$live = 1;
	    }

	    $limit = 0;
	    if ( '' != $_POST[ 'limit' ] && '0' != $_POST[ 'limit' ] )
	    {
		$limit = intval( $_POST[ 'limit' ] );
	    }

	    $startdate = 0;
	    if ( '' != $_POST[ 'startyear' ] && '0' != $_POST[ 'startyear' ] && '' != $_POST[ 'startmonth' ] && '0' != $_POST[ 'startmonth' ] && '' != $_POST[ 'startday' ] && '0' != $_POST[ 'startday' ] )
	    {
		$startdate = strtotime( $_POST[ 'startyear' ] . '/' . $_POST[ 'startmonth' ] . '/' . $_POST[ 'startday' ] );
	    }

	    $expiry = 0;
	    if ( '' != $_POST[ 'expiryyear' ] && '0' != $_POST[ 'expiryyear' ] && '' != $_POST[ 'expirymonth' ] && '0' != $_POST[ 'expirymonth' ] && '' != $_POST[ 'expiryday' ] && '0' != $_POST[ 'expiryday' ] )
	    {
		$expiry = strtotime( $_POST[ 'expiryyear' ] . '/' . $_POST[ 'expirymonth' ] . '/' . $_POST[ 'expiryday' ] );
	    }
	    if ( '' != $_POST[ 'expirydays' ] && 0 < is_numeric( $_POST[ 'expirydays' ] ) )
	    {
		$expiry = time() + ( intval( $_POST[ 'expirydays' ] ) * 24 * 60 * 60 );
	    }
	    if ( '' != $_POST[ 'expirydays' ] && 0 < is_numeric( $_POST[ 'expirydays' ] ) && 0 < $startdate )
	    {
		$expiry = $startdate + ( intval( $_POST[ 'expirydays' ] ) * 24 * 60 * 60 );
	    }

	    if ( isset( $_POST[ 'codestype' ] ) && ( 'random' == $_POST[ 'codestype' ] || 'sequential' == $_POST[ 'codestype' ] || 'custom' == $_POST[ 'codestype' ] || 'single' == $_POST[ 'codestype' ] ) )
	    {
		$codestype = trim( sanitize_text_field( $_POST[ 'codestype' ] ) );
	    }
	    else
	    {
		$codestype = 'random';
	    }

	    $codelength = 6;
	    if ( ( isset( $_POST[ 'codelength' ] ) ) && ( '' != $_POST[ 'codelength' ] ) )
	    {
		$codelength = intval( $_POST[ 'codelength' ] );
	    }
	    if ( '' == $codelength || 0 == $codelength )
	    {
		$codelength = 6;
	    }

	    $codeprefix = '';
	    if ( isset( $_POST[ 'codeprefix' ] ) )
	    {
		$codeprefix = trim( sanitize_text_field( $_POST[ 'codeprefix' ] ) );
	    }
	    if ( 6 < strlen( $codeprefix ) )
	    {
		$codeprefix = substr( $codeprefix, 6 );
	    }

	    $codesuffix = '';
	    if ( isset( $_POST[ 'codesuffix' ] ) )
	    {
		$codesuffix = trim( sanitize_text_field( $_POST[ 'codesuffix' ] ) );
	    }
	    if ( 6 < strlen( $codesuffix ) )
	    {
		$codesuffix = substr( $codesuffix, 6 );
	    }

	    $codes = '';
	    if ( isset( $_POST[ 'codestype' ] ) && $_POST[ 'codestype' ] == 'custom' )
	    {
		$codes = trim( esc_textarea( $_POST[ 'customcodelist' ] ) );
	    }
	    if ( isset( $_POST[ 'codestype' ] ) && $_POST[ 'codestype' ] == 'single' )
	    {
		$codes = trim( sanitize_text_field( $_POST[ 'singlecodetext' ] ) );
	    }

	    $voucher_category = '';
	    if ( isset( $_POST[ 'voucher_category' ] ) )
	    {
		$voucher_category = absint( $_POST[ 'voucher_category' ] );
	    }

	    $live = 0;
	    if ( isset( $_POST[ 'live' ] ) )
	    {
		$live = intval( $_POST[ "live" ] );
	    }

	    if ( 'edit' == $vaction )
	    {
		$done = gtvouchers_edit_voucher( intval( $_GET[ 'id' ] ), $_POST[ 'name' ], $require_email, $require_like, $require_phone, $liketextcta, $limit, $_POST[ 'text' ], $_POST[ 'description' ], $_POST[ 'template' ], $_POST[ 'font' ], $_POST[ 'textcolor' ], $_POST[ 'terms' ], $live, $startdate, $expiry, $codestype, $codelength, $codeprefix, $codesuffix, $codes, $voucher_image, $use_custom_image, $voucher_category );
		do_action( 'gtvouchers_after_save', intval( $_GET[ 'id' ] ) );

		return array(
		    $done,
		    intval( $_GET[ 'id' ] )
		);
	    }
	    elseif ( 'create' == $vaction )
	    {
		$array = gtvouchers_create_voucher( intval( $_POST[ 'place_id' ] ), $_POST[ 'name' ], $require_email, $require_like, $require_phone, $liketextcta, $limit, $_POST[ 'text' ], $_POST[ 'description' ], $_POST[ 'template' ], $_POST[ 'font' ], $_POST[ 'textcolor' ], $_POST[ 'terms' ], $live, $startdate, $expiry, $codestype, $codelength, $codeprefix, $codesuffix, $codes, $voucher_image, $use_custom_image, $voucher_category );
		do_action( 'gtvouchers_after_save', $array[ 1 ] );

		return $array;
	    }
	}
    }

    return false;
}

// listen for debugging of a voucher
function gtvouchers_check_debug_voucher()
{
    if ( isset( $_GET[ 'page' ] ) && isset( $_GET[ 'debug' ] ) && isset( $_GET[ 'id' ] ) )
    {
	if ( 'vouchers' == $_GET[ 'page' ] && 'true' == $_GET[ 'debug' ] && '' != $_GET[ 'id' ] )
	{
	    $voucher = gtvouchers_get_voucher( intval( $_GET[ 'id' ] ), 0 );
	    if ( $voucher )
	    {
		header( 'Content-type: application/octet-stream' );
		header( 'Content-Disposition: attachment; filename="voucher-debug.csv"' );
		echo "ID,Name,Text,Terms,Font,Template,Require Email,Limit,Expiry,GUID,Live\n";
		echo $voucher->id . ',"' . $voucher->name . '","' . $voucher->text . '","' . $voucher->terms . '","' . $voucher->font . '",' . $voucher->template . ',' . $voucher->require_email . ',' . $voucher->require_like . ',' . $voucher->require_phone . ',' . $voucher->limit . ',"' . $voucher->expiry . '","' . $voucher->guid . '",' . $voucher->live . "\n\n";
		$downloads = gtvouchers_voucher_downloads( intval( $_GET[ 'id' ] ) );
		if ( $downloads )
		{
		    echo "Datestamp,Email,Name,Code,GUID,Downloaded\n";
		    foreach ( $downloads as $download )
		    {
			echo '"' . date( "r", $download->time ) . '","' . $download->email . '","' . $download->name . '","' . $download->code . '",' . $download->guid . ',' . $download->downloaded . "\n";
		    }
		}
		exit();
	    }
	    else
	    {
		gtvouchers_404();
	    }
	}
    }
}

function gtvouchers_voucher_downloads( $voucherid = 0 )
{
    global $wpdb, $current_blog;
    $blog_id = 1;
    if ( is_object( $current_blog ) )
    {
	$blog_id = $current_blog->blog_id;
    }

    $prefix = $wpdb->prefix;

    $sql = $wpdb->prepare( "select v.name as voucher, d.time, d.downloaded, d.email, d.name, d.code, d.guid from " . $prefix . "gtvouchers_downloads d inner join " . $prefix . "gtvouchers_vouchers v on v.id = d.voucherid
    where (%d = 0 or voucherid = %d)
    and deleted = 0
    and v.blog_id = %d;", $voucherid, $voucherid, $blog_id );
    $emails = $wpdb->get_results( $sql );

    return $emails;
}

// download a list of email addresses
function gtvouchers_download_emails( $voucherid = 0 )
{
    $emails = gtvouchers_voucher_downloads( $voucherid );
    $options = get_option( 'gtvouchers_options' );
    if ( $emails && is_array( $emails ) && 0 < count( $emails ) )
    {
	header( 'Content-type: application/octet-stream' );
	header( 'Content-Disposition: attachment; filename="voucher-emails.csv"' );
//		echo "Voucher,Datestamp,Name,Email,Code\n";
	echo $options[ 'replace_voucher_singular_word' ] . ',' . __( 'Datestamp', 'gtvouchers-d' ) . ',' . __( 'Name', 'gtvouchers-d' ) . ',' . __( 'Code', 'gtvouchers-d' ) . "\n";
	foreach ( $emails as $email )
	{
	    echo htmlspecialchars( $email->voucher ) . "," . str_replace( ",", "", date( "r", $email->time ) ) . "," . htmlspecialchars( $email->name ) . "," . htmlspecialchars( $email->email ) . "," . htmlspecialchars( $email->code ) . "\n";
	}
	exit();
    }
    else
    {
	return false;
    }
}

// preview a voucher
// @todo
function gtvouchers_preview_voucher( $template, $font, $vouchertextcolor, $name, $text, $terms )
{
//	$options = get_option( 'gtvouchers_options' );
//	$voucher->template  = $template;
//	$voucher->font      = $font;
//	$voucher->textcolor = $vouchertextcolor;
//	$voucher->name      = $name;
//	$voucher->text      = $text;
//	$voucher->terms     = $terms;
//	gtvouchers_render_voucher( $voucher, '[' . sprintf( __( '%s code inserted here', 'gtvouchers-d' ), strtolower( $options['replace_voucher_singular_word'] ) ) . ']' );
}

// create a new voucher
function gtvouchers_create_voucher( $place_id, $name, $require_email, $require_like, $require_phone, $liketextcta, $limit, $text, $description, $template, $font, $vouchertextcolor, $terms, $live, $startdate, $expiry, $codestype, $codelength, $codeprefix, $codesuffix, $codes, $voucher_image, $use_custom, $voucher_category )
{
    $options = get_option( 'gtvouchers_options' );

    $blog_id = gtvouchers_blog_id();
    $guid = gtvouchers_guid( 36 );
    global $wpdb;
    $prefix = $wpdb->prefix;

    if ( 'true' == $options[ 'vouchers_require_approval' ] && !current_user_can( 'update_core' ) )
    {
	$live = 0;
    }

    $sql = $wpdb->prepare( "insert into " . $prefix . "gtvouchers_vouchers
    (blog_id, author_id, place_id, name, `text`, `description`, terms, template, font, textcolor, require_email, require_like, require_phone, liketextcta, `limit`, guid, time, live, startdate, expiry, codestype, codelength, codeprefix, codesuffix, codes, deleted, voucher_image, use_custom_image, category_id )
    values
    (%d, %d, %d, %s, %s, %s, %s, %d, %s, %s, %d, %d, %d, %s, %d, %s, %d, %d, %d, %d, %s, %d, %s, %s, %s, 0, %s, %s, %d);", $blog_id, get_current_user_id(), $place_id, $name, $text, $description, $terms, $template, $font, $vouchertextcolor, $require_email, $require_like, $require_phone, $liketextcta, $limit, $guid, time(), $live, $startdate, $expiry, $codestype, $codelength, $codeprefix, $codesuffix, $codes, $voucher_image, $use_custom, $voucher_category );

    $id = 0;

    $done = $wpdb->query( $sql );
    $id = $wpdb->insert_id;

    if ( GTV_DEBUG )
    {
	wp_mail( 'jeff@jeffrose.ca', 'Debug log', 'Create (2553) called:' . $sql );
	wp_mail( 'jeff@jeffrose.ca', 'Debug log', 'Create (2560) done:' . $done );
    }

    if ( $done )
    {
	gtvouchers_clear_gtv_transients();
	delete_single_cache( $id );
	do_action( 'gtvouchers_create', $id, $name, $text, $description, $template, $require_email, $require_like, $require_phone, $liketextcta, $limit, $startdate, $expiry );
    }

    return array(
	$done,
	$id
    );
}

// create a new voucher thumbnail
function gtvouchers_create_voucher_thumb( $id, $name, $require_email, $limit, $text, $template, $font, $vouchertextcolor, $terms, $startdate, $expiry )
{
    // do nothing
}

// delete a voucher
function gtvouchers_delete_voucher( $id )
{
    global $wpdb;

    $blog_id = gtvouchers_blog_id();

    $prefix = $wpdb->prefix;

    $sql = $wpdb->prepare( "update " . $prefix . "gtvouchers_vouchers
    set deleted = 1, live = 0
    where id = %d and blog_id = %d;", $id, $blog_id );

    delete_single_cache( $id );
    gtvouchers_clear_gtv_transients();

    return $wpdb->query( $sql );
}

// edit a voucher
function gtvouchers_edit_voucher( $id, $name, $require_email, $require_like, $require_phone, $liketextcta, $limit, $text, $description, $template, $font, $vouchertextcolor, $terms, $live, $startdate, $expiry, $codestype, $codelength, $codeprefix, $codesuffix, $codes, $voucher_image = '', $use_custom = false, $voucher_category = '' )
{
    global $wpdb;
    $options = get_option( 'gtvouchers_options' );

    $blog_id = gtvouchers_blog_id();

    // $old_voucher = gtvouchers_get_voucher( $id );

    $prefix = $wpdb->prefix;

    if ( 'true' == $options[ 'vouchers_require_approval' ] && !current_user_can( 'update_core' ) )
    {
	$live = 0;
    }

    $sql = $wpdb->prepare( "update " . $prefix . "gtvouchers_vouchers set
    time = %d,
    name = %s,
    `text` = %s,
    `description` = %s,
    terms = %s,
    template = %d,
    font = %s,
    textcolor = %s,
    require_email = %d,
    require_like = %d,
    require_phone = %d,
    liketextcta = %s,
    `limit` = %d,
    live = %d,
    startdate = %d,
    expiry = %d,
    codestype = %s,
    codelength = %d,
    codeprefix = %s,
    codesuffix = %s,
    codes = %s,
    voucher_image = %s,
    use_custom_image = %s,
    category_id = %d
    where id = %d
    and blog_id = %d;", time(), $name, $text, $description, $terms, $template, $font, $vouchertextcolor, $require_email, $require_like, $require_phone, $liketextcta, $limit, $live, $startdate, $expiry, $codestype, $codelength, $codeprefix, $codesuffix, $codes, $voucher_image, $use_custom, $voucher_category, $id, $blog_id );

    if ( GTV_DEBUG )
    {
	wp_mail( 'jeff@jeffrose.ca', 'Debug log', 'Edit (2653) called:' . $sql );
    }

    $done = $wpdb->query( $sql );

    if ( $done )
    {
	do_action( 'gtvouchers_edit', $id, $name, $text, $description, $template, $require_email, $require_like, $require_phone, $liketextcta, $limit, $startdate, $expiry );
	gtvouchers_clear_gtv_transients();
	delete_single_cache( $id );
    }

    return $done;
}

// ==========================================================================================
// template functions

function gtvouchers_upload_template( $id, $file )
{

    $file = $file[ 'tmp_name' ];

    // get the image size
    $imagesize = getimagesize( $file );
    $width = $imagesize[ 0 ];
    $height = $imagesize[ 1 ];
    $imagetype = $imagesize[ 2 ];

    // if the imagesize could be fetched and is JPG, PNG or GIF
    if ( 2 == $imagetype && 1181 == $width && 532 == $height )
    {

	// check gtvouchers is installed correctly
	// move the temporary file to the full-size image (1181 x 532 px @ 150dpi)
	$fullpath = GTV_TEMPLATE_PATH . '/' . $id . '.jpg';
	move_uploaded_file( $file, $fullpath );

	// get the image
	$image = imagecreatefromjpeg( $fullpath );

	// create the preview image (800 x 360 px @ 72dpi)
	$preview = imagecreatetruecolor( 800, 360 );
	imagecopyresampled( $preview, $image, 0, 0, 0, 0, 800, 360, $width, $height );
	$previewpath = GTV_TEMPLATE_PATH . '/' . $id . '_preview.jpg';
	imagejpeg( $preview, $previewpath, 80 );

	// create the thumbnail image (200 x 90 px @ 72dpi)
	$thumb = imagecreatetruecolor( 200, 90 );
	imagecopyresampled( $thumb, $image, 0, 0, 0, 0, 200, 90, $width, $height );
	$thumbpath = GTV_TEMPLATE_PATH . '/' . $id . '_thumb.jpg';
	imagejpeg( $thumb, $thumbpath, 70 );

	return true;
    }
    else
    {

	return false;
    }
}

// add a new template
function gtvouchers_add_template( $name )
{
    global $wpdb;

    $blog_id = gtvouchers_blog_id();
    $prefix = $wpdb->prefix;

    $sql = $wpdb->prepare( "insert into " . $prefix . "gtvouchers_templates
    (blog_id, name, time)
    values
    (%d, %s, %d);", $blog_id, $name, time() );
    if ( $wpdb->query( $sql ) )
    {
	return $wpdb->insert_id;
    }
    else
    {
	return false;
    }
}

// edit a template
function gtvouchers_edit_template( $id, $name, $live )
{
    global $wpdb;

    $blog_id = gtvouchers_blog_id();

    $prefix = $wpdb->prefix;

    $sql = $wpdb->prepare( "update " . $prefix . "gtvouchers_templates set
    name = %s,
    live = %d
    where id = %d and blog_id = %d;", $name, $live, $id, $blog_id );

    return $wpdb->query( $sql );
}

// get a list of templates
function gtvouchers_get_templates()
{
    global $wpdb;

    $blog_id = gtvouchers_blog_id();

    $prefix = $wpdb->prefix;

    $sql = $wpdb->prepare( "select id, blog_id, name from " . $prefix . "gtvouchers_templates where live = 1 and (blog_id = 0 or blog_id = %d);", $blog_id );

    return $wpdb->get_results( $sql );
}

// ==========================================================================================
// vouchers functions
// get a list of all blog vouchers
//
// JR: Removed this line: inner join " . $wpdb->base_prefix . "blogs b on b.blog_id = v.blog_id
// JR: Removed references to blogs: select b.domain, b.path,
function gtvouchers_get_all_vouchers( $num = 25, $start = 0 )
{
    global $wpdb;

    $blog_id = gtvouchers_blog_id();

    $showall = '0';
    if ( $all )
    {
	$showall = '1';
    }

    $limit = 'limit ' . ( int ) $start . ', ' . ( int ) $num;
    if ( $num == 0 )
    {
	$limit = '';
    }

    $prefix = $wpdb->prefix;

    $sql = "select v.id, v.name, v.`text`, v.terms, v.require_email, v.require_like, v.require_phone, v.liketextcta, v.`limit`, v.live, v.startdate, v.expiry, v.guid, pl.post_title, v.category_id,
(select count(d.id) from " . $prefix . "gtvouchers_downloads d where d.voucherid = v.id and d.downloaded > 0) as downloads
from " . $prefix . "gtvouchers_vouchers v
LEFT JOIN {$wpdb->posts} as pl ON pl.ID = v.place_id
where v.live = 1
and v.deleted = 0
order by v.time desc
" . $limit . ";";

    return $wpdb->get_results( $sql );
}

// get a list of vouchers
/*
 * JR: Update - now use 3rd parameter to pass location to gtvouchers_get_vouchers
 * So we know if we are front or back. Obviously shortcode is always FRONT so we ignore
 * the author restriction.
 */
function gtvouchers_get_vouchers( $num = 25, $all = false, $location = 'back', $category = '', $sort = 'natural' )
{
    global $wpdb;

    $options = get_option( 'gtvouchers_options' );

    $blog_id = gtvouchers_blog_id();

    $showall = '0';
    if ( $all )
    {
	$showall = '1';
    }

    $limit = 'limit ' . ( int ) $num;
    if ( $num == 0 )
    {
	$limit = '';
    }

    $prefix = $wpdb->prefix;

    $sql = "select v.*, pl.post_title,
(select count(d.id) from " . $prefix . "gtvouchers_downloads d where d.voucherid = v.id and d.downloaded > 0) as `downloads`, if(expiry=0, 999999999999, expiry) as 'sortorder'
from " . $prefix . "gtvouchers_vouchers v
LEFT JOIN {$wpdb->posts} as pl ON pl.ID = v.place_id ";

    if ( !empty( $category ) )
    {
	$sql .= "
INNER JOIN $wpdb->term_relationships ON (pl.ID = $wpdb->term_relationships.object_id)
INNER JOIN $wpdb->term_taxonomy ON ($wpdb->term_relationships.term_taxonomy_id = $wpdb->term_taxonomy.term_taxonomy_id)
";
    }

    $sql .= " where (%s = '1' or v.live = 1)
and (%s = '1' or (expiry = '' or expiry = 0 or expiry > %d))
and (%s = '1' or (startdate = '' or startdate = 0 or startdate <= %d))
and v.blog_id = %d
and v.deleted = 0 ";

    if ( !current_user_can( 'administrator' ) && 'back' == $location )
    {
	$sql .= ' and author_id = %d';
    }

    if ( !empty( $category ) )
    {
	$sql .= "AND $wpdb->term_taxonomy.term_id IN ($category)";
    }

    if ( 'hide' == $options[ 'maxxed_downloads' ] && 'back' != $location )
    {
	$sql .= ' HAVING ( `downloads` < v.limit OR v.limit = 0  )';
    }

    if ( 'expiry' == $sort )
    {
	$sql .= " ORDER BY 'sortorder' ASC, v.time DESC {$limit};";
    }
    else
    {
	$sql .= ' ORDER BY v.time DESC ' . $limit . ';';
    }
    $sql = $wpdb->prepare( $sql, $showall, $showall, time(), $showall, time(), $blog_id, intval( get_current_user_id() ) );

    return $wpdb->get_results( $sql );
}

// get a list of all popular vouchers by download
function gtvouchers_get_all_popular_vouchers( $num = 25, $start = 0 )
{
    global $wpdb;
    $blog_id = gtvouchers_blog_id();

    $limit = 'limit ' . ( int ) $start . ', ' . ( int ) $num;
    if ( $num == 0 )
    {
	$limit = '';
    }

    $prefix = $wpdb->prefix;

    $sql = "select v.id, v.name, v.`text`, v.`description`, v.terms, v.require_email, v.require_like, v.require_phone, v.liketextcta, v.`limit`, v.live, v.startdate, v.expiry, v.guid, pl.post_title, pl.id as post_id,
count(d.id) as downloads
from " . $prefix . "gtvouchers_downloads d
inner join " . $prefix . "gtvouchers_vouchers v on v.id = d.voucherid
LEFT JOIN {$wpdb->posts} as pl ON pl.ID = v.place_id
where v.deleted = 0
and d.downloaded > 0";
    if ( !current_user_can( 'administrator' ) )
    {
	$sql .= ' and author_id = ' . intval( get_current_user_id() );
    }
    $sql .= ' group by v.id, v.name, v.`text`, v.terms, v.require_email, v.require_like, v.require_phone, v.liketextcta, v.`limit`, v.live, v.expiry, v.guid ';
    $sql .= ' order by count(d.id) desc %s;';

    return $wpdb->get_results( $wpdb->prepare( $sql, $limit ) );
}

// get a list of popular vouchers by download
function gtvouchers_get_popular_vouchers( $num = 25 )
{
    global $wpdb;

    $blog_id = gtvouchers_blog_id();

    $limit = 'limit ' . ( int ) $num;
    if ( $num == 0 )
    {
	$limit = '';
    }

    $prefix = $wpdb->prefix;

    $sql = "select v.id, v.name, v.`text`, v.`description`, v.terms, v.require_email, v.require_like, v.require_phone, v.liketextcta, v.`limit`, v.live, v.startdate, v.expiry, v.guid, pl.post_title,
count(d.id) as downloads
from " . $prefix . "gtvouchers_downloads d
inner join " . $prefix . "gtvouchers_vouchers v on v.id = d.voucherid
JOIN {$wpdb->posts} as pl ON pl.ID = v.place_id
where v.blog_id = %d
and v.deleted = 0
and d.downloaded > 0";
    if ( !current_user_can( 'administrator' ) )
    {
	$sql .= ' and author_id = %d ';
    }
    $sql .= ' group by v.id, v.name, v.`text`, v.terms, v.require_email, v.require_like, v.require_phone, v.liketextcta, v.`limit`, v.live, v.expiry, v.guid
order by count(d.id) desc
' . $limit . ';';

    return $wpdb->get_results( $wpdb->prepare( $sql, intval( $blog_id ), intval( get_current_user_id() ) ) );
}

// ==========================================================================================
// individual voucher functions
// get a voucher by id or guid
function gtvouchers_get_voucher( $voucher, $live = 1, $code = '', $unexpired = 0 )
{
    global $wpdb;

    $blog_id = gtvouchers_blog_id();

    $prefix = $wpdb->prefix;

    // get by id
    if ( is_numeric( $voucher ) )
    {
	$sql = $wpdb->prepare( "select v.id, v.author_id, v.place_id, v.name, v.`text`, v.`description`, v.terms, v.font, v.textcolor, v.template, v.require_email, v.require_like, v.require_phone, v.liketextcta, v.`limit`, v.startdate, v.expiry, v.guid, v.live, '' as registered_email, '' as registered_name,
        v.codestype, v.codeprefix, v.codesuffix, v.codelength, v.codes, v.place_id, v.voucher_image, v.use_custom_image, v.category_id,
        (select count(d.id) from " . $prefix . "gtvouchers_downloads d where d.voucherid = v.id and d.downloaded > 0) as downloads
        from " . $prefix . "gtvouchers_vouchers v
        where
        (%s = '0' or v.live = 1)
        and (%s = '0' or (expiry = '' or expiry = 0 or expiry > %d))
        and (%s = '0' or (startdate = '' or startdate = 0 or startdate <= %d))
        and v.id = %d
        and v.deleted = 0
        and v.blog_id = %d", $live, $live, time(), $live, time(), $voucher, $blog_id );

	// get by guid
    }
    else
    {
	// if a download code has been specified
	if ( '' != $code )
	{
	    $sql = $wpdb->prepare( "select v.id, v.name, v.`text`, v.`description`, v.terms, v.font, v.textcolor, v.template, v.require_email, v.require_like, v.require_phone, v.`limit`, v.startdate, v.expiry, v.guid, v.live, r.email as registered_email, r.name as registered_name,
            v.codestype, v.codeprefix, v.codesuffix, v.codelength, v.codes, v.place_id, v.voucher_image, v.use_custom_image, v.category_id,
            (select count(d.id) from " . $prefix . "gtvouchers_downloads d where d.voucherid = v.id and d.downloaded > 0) as downloads
            from " . $prefix . "gtvouchers_vouchers v
            left outer join " . $prefix . "gtvouchers_downloads r on r.voucherid = v.id and r.guid = %s
            where
            (%s = '0' or v.live = 1)
            and (%s = '0' or (expiry = '' or expiry = 0 or expiry > %d))
            and (%s = '0' or (startdate = '' or startdate = 0 or startdate <= %d))
            and v.deleted = 0
            and v.guid = %s
            and v.blog_id = %d", $code, $live, $live, time(), $live, time(), $voucher, $blog_id );
	}
	else
	{
	    $sql = $wpdb->prepare( "select v.id, v.name, v.`text`, v.`description`, v.terms, v.font, v.textcolor, v.template, v.require_email, v.require_like, v.require_phone, v.`limit`, v.startdate, v.expiry, v.guid, v.live, '' as registered_email, '' as registered_name,
            v.codestype, v.codeprefix, v.codesuffix, v.codelength, v.codes, v.place_id, v.voucher_image, v.use_custom_image, v.category_id,
            (select count(d.id) from " . $prefix . "gtvouchers_downloads d where d.voucherid = v.id and d.downloaded > 0) as downloads
            from " . $prefix . "gtvouchers_vouchers v
            where
            (%s = '0' or v.live = 1)
            and (%s = '0' or (expiry = '' or expiry = 0 or expiry > %d))
            and (%s = '0' or (startdate = '' or startdate = 0 or startdate <= %d))
            and v.deleted = 0
            and v.guid = %s
            and v.blog_id = %d", $live, $live, time(), $live, time(), $voucher, $blog_id );
	}
    }

    $row = $wpdb->get_row( $sql );
    if ( is_object( $row ) && '' != $row->id )
    {
	if ( "0" == $row->limit )
	{
	    $row->limit = '';
	}

	return $row;
    }
    else
    {
	return false;
    }
}

// check a voucher exists and can be downloaded
function gtvouchers_voucher_exists( $guid )
{

    global $wpdb;

    $blog_id = gtvouchers_blog_id();

    $prefix = $wpdb->prefix;

    $sql = $wpdb->prepare( "select v.id, v.`limit`,
    (select count(d.id) from " . $prefix . "gtvouchers_downloads d where d.voucherid = v.id and d.downloaded > 0) as downloads
    from " . $prefix . "gtvouchers_vouchers v
    where
    v.guid = %s
    and v.deleted = 0
    and v.blog_id = %d", $guid, $blog_id );
    $row = $wpdb->get_row( $sql );
    if ( $row )
    {
	return true;
    }

    return false;
}

// download a voucher
function gtvouchers_download_voucher( $voucher_guid, $download_guid = '' )
{

    $voucher = gtvouchers_get_voucher( $voucher_guid, 1, $download_guid );
    if ( is_object( $voucher ) && 1 == $voucher->live && '' != $voucher->id && '' != $voucher->name && ( // If not using a custom image, then all text & template is required
	    ( '' != $voucher->text && '' != $voucher->terms && '' != $voucher->template && gtvouchers_template_exists( $voucher->template ) ) || ( 1 == $voucher->use_custom_image // Using a custom image, we don't need the text but we do need the image
	    && ( @fopen( $voucher->voucher_image, "r" ) == true ) ) )
    )
    {
	// see if this voucher can be downloaded
	$valid = gtvouchers_download_guid_is_valid( $voucher_guid, $download_guid );

	if ( 'valid' === $valid )
	{


	    // set this download as completed
	    $code = gtvouchers_create_download_code( $voucher->id, $download_guid );

	    do_action( 'gtvouchers_download', $voucher->id, $voucher->name, $code );

	    global $wpdb;

	    $prefix = $wpdb->prefix;

	    // set this voucher as being downloaded
	    $sql = $wpdb->prepare( "update " . $prefix . "gtvouchers_downloads set downloaded = 1 where voucherid = %d and guid = %s;", $voucher->id, $download_guid );
	    $wpdb->query( $sql );


	    // render the voucher
	    gtvouchers_render_voucher( $voucher, $code );
	}
	else
	{
	    if ( 'unavailable' === $valid )
	    {

		// this voucher is not available
		//print '<!-- The voucher is not available for download -->';
		gtvouchers_404();
	    }
	    else
	    {
		if ( 'runout' === $valid )
		{

		    // this voucher has run out
//					print '<!-- The voucher has run out -->';
		    gtvouchers_runout();
		}
		else
		{
		    if ( 'downloaded' === $valid )
		    {

			// this voucher has been downloaded already
//						print '<!-- The voucher has already been downloaded by this person -->';
			gtvouchers_downloaded();
		    }
		    else
		    {
			if ( 'expired' === $valid )
			{

			    // this voucher has expired
//							print '<!-- The voucher has expired -->';
			    gtvouchers_expired();
			}
			else
			{
			    if ( 'notyetavailable' === $valid )
			    {

				// this voucher is not yet available
//								print '<!-- The voucher is not yet available -->';
				gtvouchers_notyetavailable();
			    }
			}
		    }
		}
	    }
	}
    }
    else
    {
	// this voucher is not available
//		print '<!-- The voucher could not be found -->';
	gtvouchers_404( false );
    }
    //wp_redirect('http://link1n1.com/DEV/use-incentive/');	
}

// render a voucher
function gtvouchers_render_voucher( $voucher, $code )
{


    global $current_user;
    $options = get_option( 'gtvouchers_options' );

    // get the voucher template image
    if ( gtvouchers_template_exists( $voucher->template ) || 1 == $voucher->use_custom_image )
    {

	if ( 'true' == $options[ 'listing_voucher_preview' ] || 1 == $voucher->use_custom_image )
	{
	    $dest = 'save';
	    if ( isset( $_GET[ 'print' ] ) && ( 'print' == $_GET[ 'print' ] ) )
	    {
		$dest = 'print';
	    }

	    gtvouchers_generate_voucher_full_image( $voucher, $code, $dest );	}
	else
	{
	    $slug = gtvouchers_slug( $voucher->name );
	    header( 'Expires: Mon, 26 Jul 1997 05:00:00 GMT' );
	    header( 'Last-Modified: ' . gmdate( 'D, d M Y H:i:s' ) . ' GMT' );
	    header( 'Cache-Control: no-store, no-cache, must-revalidate' );
	    header( 'Cache-Control: post-check=0, pre-check=0', false );
	    header( 'Pragma: no-cache' );
	    header( 'Content-type: application/octet-stream' );
	    header( 'Content-Disposition: attachment; filename="' . $slug . '.pdf"' );

	    // include the TCPDF class and gtvouchers PDF class
	    require_once( 'gt-vouchers_pdf.php' );

	    // create new PDF document
	    $pdf = new gtvouchers_pdf( PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false );

	    // set the properties
	    $pdf->voucher_image = GTV_TEMPLATE_PATH . '/' . $voucher->template . '.jpg';
	    $pdf->voucher_image_w = 200;
	    $pdf->voucher_image_h = 90;
	    $pdf->voucher_image_dpi = 150;
            
	    // set document information
	    $pdf->SetCreator( PDF_CREATOR );
	    $pdf->SetAuthor( $current_user->user_nicename );
	    $pdf->SetTitle( $voucher->name );

	    // set header and footer fonts
	    $pdf->setHeaderFont( Array(
		PDF_FONT_NAME_MAIN,
		'',
		PDF_FONT_SIZE_MAIN
	    ) );

	    // set default monospaced font
	    $pdf->SetDefaultMonospacedFont( PDF_FONT_MONOSPACED );

	    //set margins
	    $pdf->SetMargins( PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT );
	    $pdf->SetHeaderMargin( 0 );
	    $pdf->SetFooterMargin( 0 );

	    // remove default footer
	    $pdf->setPrintFooter( false );

	    //set auto page breaks
	    $pdf->SetAutoPageBreak( true, PDF_MARGIN_BOTTOM );

	    //set image scale factor
	    $pdf->setImageScale( PDF_IMAGE_SCALE_RATIO );

	    //set some language-dependent strings
	    $pdf->setLanguageArray( $l );

	    // set top margin
	    $pdf->SetTopMargin( 12 );

	    if ( isset( $options[ 'voucher_text_color' ] ) )
	    {
		$rgb = gtv_html2rgb( $options[ 'voucher_text_color' ] );
		$pdf->SetColor( 'text', $rgb[ 0 ], $rgb[ 1 ], $rgb[ 2 ] );
	    }

	    if ( isset( $voucher->textcolor ) && '' != $voucher->textcolor )
	    {
		$rgb = gtv_html2rgb( $voucher->textcolor );
		$pdf->SetColor( 'text', $rgb[ 0 ], $rgb[ 1 ], $rgb[ 2 ] );
	    }

	    // add a page
	    $pdf->AddPage( 'L', array(
		200,
		90
	    ) );

	    // set title font
	    $pdf->SetFont( $voucher->font, '', 30 );
	    // print title
	    $pdf->writeHTML( stripslashes( $voucher->name ), $ln = true, $fill = false, $reseth = false, $cell = false, $align = 'C' );

	    // set text font
	    $pdf->SetFont( $voucher->font, '', 14 );
	    // print text
	    $pdf->Write( 5, '', $link = '', $fill = 0, $align = 'L', $ln = true );
	    $pdf->Write( 5, stripslashes( $voucher->text ), $link = '', $fill = 0, $align = 'L', $ln = true );
	    $pdf->Write( 2, '', $link = '', $fill = 0, $align = 'L', $ln = true );

	    $registered_name = '';
	    if ( '' != $voucher->registered_name )
	    {
		$registered_name = __( 'Registered to:', 'gtvouchers-d' ) . ' ' . stripslashes( $voucher->registered_name ) . ': ';
	    }

	    // set code font
	    $pdf->SetFont( $voucher->font, '', 14 );
	    // outputs image directly into browser, as PNG stream 
	    QRcode::png( $code, plugin_dir_path( __FILE__ ) . "cache/{$code}" );
	    // print code
	    $pdf->Image( plugin_dir_path( __FILE__ ) . "cache/{$code}", $x = '', $y = '', $w = 0, $h = 0, $type = '', $link = '', $align = 'N', $resize = false, $dpi = 300, $palign = 'C', $ismask = false, $imgmask = false, $border = 0, $fitbox = false, $hidden = false, $fitonpage = false, $alt = false, $altimgs = array() );
	    $pdf->Write( 7, $registered_name . $code, $link = '', $fill = 0, $align = 'C', $ln = true );

	    // get the expiry, if it exists
	    $expiry = '';
	    if ( '' != $voucher->expiry && 0 < ( int ) $voucher->expiry )
	    {
		$expiry = ' ' . __( 'Expiry:', 'gtvouchers-d' ) . ' ' . date_i18n( get_option( "date_format" ), $voucher->expiry );
	    }

	    // set terms font
	    $pdf->SetFont( $voucher->font, '', 10 );
	    // print terms
	    $pdf->Write( 2, '', $link = '', $fill = 0, $align = 'L', $ln = true );
	    $pdf->Write( 5, stripslashes( $voucher->terms ) . $expiry, $link = '', $fill = 0, $align = 'L', $ln = true );

	    // close and output PDF document
	    $pdf->Output( $slug . '.pdf', 'D' );
	    //ob_clean();
	    //header("location: http://link1n1.com/DEV/use-incentive/");
	    //ob_start();

	    exit();
	}
    }
    else
    {

	return false;
    }
}

// render a voucher
function gtvouchers_render_voucher_thumb( $voucher, $code )
{
    // do nothing
}

// check a template exists
function gtvouchers_template_exists( $template )
{
    $file = GTV_TEMPLATE_PATH . '/' . $template . '.jpg';
    if ( file_exists( $file ) )
    {
	return true;
    }

    return false;
}

// ==========================================================================================
// person functions
// show the registration form
function gtvouchers_register_form( $voucher_guid, $plain = false )
{

    load_template( dirname( __FILE__ ) . '/gd-templates/voucher-registration.php' );

    exit();
}

function gtvouchers_page_url()
{
    $pageURL = 'http';
    if ( isset( $_SERVER[ 'HTTPS' ] ) && 'on' == $_SERVER[ 'HTTPS' ] )
    {
	$pageURL .= 's';
    }
    $pageURL .= '://';
    if ( '80' != $_SERVER[ 'SERVER_PORT' ] )
    {
	$pageURL .= $_SERVER[ 'SERVER_NAME' ] . ':' . $_SERVER[ 'SERVER_PORT' ] . $_SERVER[ 'REQUEST_URI' ];
    }
    else
    {
	$pageURL .= $_SERVER[ 'SERVER_NAME' ] . $_SERVER[ 'REQUEST_URI' ];
    }

    return $pageURL;
}

// register a persons name and email address
function gtvouchers_register_person( $voucher_guid, $email, $name, $phone )
{
    global $wpdb;
    $prefix = $wpdb->prefix;

    // get the voucher id
    $sql = $wpdb->prepare( "select id from " . $prefix . "gtvouchers_vouchers where guid = %s and deleted = 0;", $voucher_guid );
    $voucherid = $wpdb->get_var( $sql );

    // if the id has been found
    if ( '' != $voucherid )
    {

	// if the email address has already been registered
	$sql = $wpdb->prepare( "select guid from " . $prefix . "gtvouchers_downloads where voucherid = %d and email = %s;", $voucherid, $email );
	$guid = $wpdb->get_var( $sql );

	if ( $guid == '' )
	{

	    // get the IP address
	    $ip = gtvouchers_ip();

	    // create the code
	    $code = gtvouchers_create_code( $voucherid );

	    // create the guid
	    $guid = gtvouchers_guid( 36 );

	    // insert the new download
	    $sql = $wpdb->prepare( "insert into " . $prefix . "gtvouchers_downloads
            (voucherid, time, email, name, phone, ip, code, guid, downloaded, user_id)
            values
            (%d, %d, %s, %s, %s, %s, %s, %s, 0, %d)", $voucherid, time(), $email, $name, $phone, $ip, $code, $guid, get_current_user_id() );

	    $default = $wpdb->query( $sql );

	    return $guid;
	}
    }

    return false;
}

// check a code address is valid for a voucher
function gtvouchers_download_guid_is_valid( $voucher_guid, $download_guid )
{
    if ( '' == $voucher_guid && '' == $download_guid )
    {
	return false;
    }
    else
    {
	global $wpdb;
	$prefix = $wpdb->prefix;

	$blog_id = gtvouchers_blog_id();

	$sql = $wpdb->prepare( "select v.id, v.require_email, v.require_like, v.require_phone, ifnull( d.email, '' ) as email, ifnull( d.downloaded, 0 ) as downloaded, v.`limit`, v.startdate, v.expiry from
                " . $prefix . "gtvouchers_vouchers v
                left outer join " . $prefix . "gtvouchers_downloads d on d.voucherid = v.id and d.guid = %s
                where v.guid = %s
                and v.blog_id = %d
                and v.deleted = 0;", $download_guid, $voucher_guid, $blog_id );
	$row = $wpdb->get_row( $sql );
	// if the voucher has been found
	if ( $row )
	{
	    // a limit has been set
	    if ( 0 != intval( $row->limit ) )
	    {
		$sql = $wpdb->prepare( "select count(id) from " . $prefix . "gtvouchers_downloads where voucherid = %d and downloaded > 0", $row->id );
		$downloads = $wpdb->get_var( $sql );
		// if the limit has been reached
		if ( intval( $downloads ) >= intval( $row->limit ) )
		{
		    return 'runout';
		}
	    }

	    // if there is an expiry and the expiry is in the past
	    if ( 0 != intval( $row->expiry ) && time() >= intval( $row->expiry ) )
	    {
		return 'expired';
	    }

	    // if there is a start date and the tart date is in the future
	    if ( 0 != intval( $row->startdate ) && time() < intval( $row->startdate ) )
	    {
		return 'notyetavailable';
	    }

	    // if emails are not required
	    if ( '1' != $row->require_email )
	    {
		return 'valid';
	    }
	    else
	    {
		// if the voucher has been downloaded - this allows the user multiple downloads now
		if ( '' != $download_guid && '' != $row->email && '0' != $row->downloaded )
		{
		    return 'valid';
		}
		// if the voucher has not been downloaded
		if ( '' != $download_guid && '' != $row->email && '0' == $row->downloaded )
		{
		    return 'valid';
		}

		return 'unregistered';
	    }
	}

	return 'unavailable';
    }
}

// get the next custom code in a list
function gtvouchers_get_custom_code( $codes )
{
    if ( '' != trim( $codes ) )
    {
	$codelist = explode( "\n", $codes );
	if ( is_array( $codelist ) && 0 < count( $codelist ) )
	{
	    return trim( $codelist[ 0 ] );
	}
    }

    return gtvouchers_guid();
}

// create a download code for a voucher
function gtvouchers_create_download_code( $voucherid, $download_guid = '' )
{
    global $wpdb;
    $prefix = $wpdb->prefix;

    if ( '' != $download_guid )
    {
	// get the code
	$sql = $wpdb->prepare( "select code from " . $prefix . "gtvouchers_downloads where voucherid = %d and guid = %s;", $voucherid, $download_guid );
	$code = $wpdb->get_var( $sql );
    }
    else
    {

	// get the IP address
	$ip = gtvouchers_ip();

	$code = gtvouchers_create_code( $voucherid );

	// insert the download
	$sql = $wpdb->prepare( "insert into " . $prefix . "gtvouchers_downloads
        (voucherid, time, ip, code, downloaded, user_id)
        values
        (%d, %d, %s, %s, 0, %d)", $voucherid, time(), $ip, $code, get_current_user_id() );
	$wpdb->query( $sql );
    }

    // return this code
    return $code;
}

// create a code for a voucher
function gtvouchers_create_code( $voucherid )
{
    global $wpdb;
    $prefix = $wpdb->prefix;

    // get the codes type for this voucher
    $sql = $wpdb->prepare( "select v.codestype, v.codeprefix, v.codesuffix, v.codelength, v.codes, count(d.id) as downloads from " . $prefix . "gtvouchers_vouchers v left outer join " . $prefix . "gtvouchers_downloads d on d.voucherid = v.id and d.downloaded > 0 where v.id = %d and v.deleted = 0 group by v.codestype, v.codeprefix, v.codesuffix, v.codelength, v.codes;", $voucherid );
    $voucher_codestype = $wpdb->get_row( $sql );
    // using custom codes
    if ( 'custom' == $voucher_codestype->codestype )
    {

	// use the next one of the custom codes
	$code = gtvouchers_get_custom_code( $voucher_codestype->codes );

	// set the remaining codes by removing this code
	$remaining_codes = trim( str_replace( $code, '', $voucher_codestype->codes ) );

	// update the codes to set this one as being used
	$sql = $wpdb->prepare( "update " . $prefix . "gtvouchers_vouchers set codes = %s where id = %d;", $remaining_codes, $voucherid );
	$wpdb->query( $sql );

	// using sequential codes
    }
    else
    {
	if ( 'sequential' == $voucher_codestype->codestype )
	{

	    // add one to the number of vouchers already downloaded
	    $code = $voucher_codestype->codeprefix . ( ( int ) $voucher_codestype->downloads + 1 ) . $voucher_codestype->codesuffix;

	    // using a single code
	}
	else
	{
	    if ( 'single' == $voucher_codestype->codestype )
	    {

		// get the code
		$code = $voucher_codestype->codes;

		// using random codes
	    }
	    else
	    {

		// create the random code
		$code = $voucher_codestype->codeprefix . gtvouchers_guid( $voucher_codestype->codelength ) . $voucher_codestype->codesuffix;
	    }
	}
    }

    return $code;
}

function gtvouchers_choose_listing( $selected = 0 )
{
    $options = get_option( 'gtvouchers_options' );
    $listings = gtvoucher_get_all_listings_for_user();

    if ( !( empty( $listings ) ) )
    {
	$select_list = '<select name="place_id">';
	if ( current_user_can( 'administrator' ) )
	{
	    $select_list .= '<option value="0"></option>';
	}

	foreach ( $listings as $place )
	{
	    $vouchers = gtvouchers_get_vouchers_by_place( $place->ID, 'true' );

	    if ( (!isset( $place->max_vouchers ) ) || ( isset( $place->max_vouchers ) && ( ( '' == $place->max_vouchers ) || ( count( $vouchers ) < $place->max_vouchers ) ) ) )
	    {
		$success = true;
		$select_list .= '<option value="' . $place->ID . '" ' . selected( $selected, $place->ID, false ) . '>' . $place->post_title . '</option>';
	    }
	}
	$select_list .= '</select>';
    }

    if ( empty( $listings ) || ( '<select name="place_id"></select>' == $select_list ) )
    {
	$success = false;
	$select_list = sprintf( __( 'You have no listings to which to attach a %s', 'gtvouchers-d' ), strtolower( $options[ 'replace_voucher_singular_word' ] ) );
    }
    return array( 'success' => $success, 'listings' => $select_list );
}

function gtvoucher_get_all_listings_for_user()
{
    if ( current_user_can( 'manage_options' ) || is_plugin_active( 'geodir_payment_manager/geodir_payment_manager.php' ) )
    {
	$post_types = geodir_get_posttypes();
    }
    else
    {
	$options = get_option( 'gtvouchers_options' );
	$post_types = isset( $options[ 'voucher_post_types' ] ) ? $options[ 'voucher_post_types' ] : array();
    }

    if ( empty( $post_types ) )
    {
	return;
    }

    global $wpdb, $current_user;

//	$prefix    = $wpdb->prefix;
    $gd_prefix = gtv_get_gd_prefix();

    $sql = '';
    foreach ( $post_types as $slug )
    {
	//Build up a union statement, taking into consideration payment options & post types
	if ( current_user_can( "manage_options" ) )
	{
	    // Admin can do anything
	    $sql .= "SELECT DISTINCT post_id as ID, post_title, 9999 as max_vouchers FROM {$gd_prefix}geodir_{$slug}_detail AS gp_{$slug} ";
	}

	if ( (!( current_user_can( "manage_options" ) ) ) && !is_plugin_active( 'geodir_payment_manager/geodir_payment_manager.php' ) )
	{
	    // Regular user, and no payment manager. Not sure what the limit should be.
	    $max_vouchers = isset( $options[ 'voucher_post_type_qty' ][ $slug ] ) ? $options[ 'voucher_post_type_qty' ][ $slug ] : 0;
	    $sql .= "SELECT DISTINCT post_id as ID, gp_{$slug}.post_title, " . $max_vouchers . " as max_vouchers FROM {$gd_prefix}geodir_{$slug}_detail AS gp_{$slug} JOIN {$wpdb->posts} ON post_id = ID AND post_author=" . $current_user->ID;
	}

	if ( (!( current_user_can( "manage_options" ) ) ) && is_plugin_active( 'geodir_payment_manager/geodir_payment_manager.php' ) )
	{
	    // Regular user with payment manager, so we get the limit from geodir_price
	    $sql .= "SELECT DISTINCT post_id as ID, gp_{$slug}.post_title, max_vouchers FROM {$gd_prefix}geodir_{$slug}_detail AS gp_{$slug} ";
	    $sql .= " JOIN {$wpdb->posts} ON post_id = ID AND post_author=" . $current_user->ID;
	    $sql .= " JOIN " . GEODIR_PRICE_TABLE . " gp ON gp_{$slug}.package_id = pid AND gp.allow_vouchers = 1 AND gp.status=1 ";
	}

	if ( end( $post_types ) !== $slug )
	{
	    $sql .= "UNION ";
	}
    }

    if ( GTV_DEBUG )
    {
	error_log( $sql );
    }
    $results = $wpdb->get_results( $sql );

    return $results;
}

function gtvouchers_get_vouchers_by_place( $post_id, $only_author = false )
{
    global $wpdb, $current_user;
//	global $wp_filter;
//	echo '<pre>';
//	print_r($wp_filter['the_content']);
//	echo '</pre>';
    if ( function_exists( 'geodir_get_post_package_info' ) )
    {
	$package = geodir_get_post_package_info( '', $post_id );
    }
    if ( isset( $package[ 'pid' ] ) )
    {
	$pid = $package[ 'pid' ];
    }
    else
    {
	$pid = 0;
//		return;
    }

    $current_user = wp_get_current_user();

    $options = get_option( 'gtvouchers_options' );

    $prefix = $wpdb->prefix;

    $vouchers = '';

    if ( gtv_vouchers_on_post_type( $post_id ) || gtv_package_allows_vouchers( $pid ) )
    {
	$sql = "
            SELECT DISTINCT v.*,
                            (SELECT Count(d.id)
                             FROM   {$prefix}gtvouchers_downloads d
                             WHERE  d.voucherid = v.id) AS downloads
                            , v.limit
            FROM   {$prefix}gtvouchers_vouchers v
            WHERE  deleted = 0
                   AND place_id = %d";

	if ( !current_user_can( "administrator" ) )
	{
	    $sql .= "           AND ( From_unixtime(startdate) < Now()
                             AND ( expiry = 0
                                    OR From_unixtime(expiry) > Now() ) )
            ";
	}

	if ( !$only_author && 'hide' == $options[ 'maxxed_downloads' ] && !current_user_can( "administrator" ) )
	{
	    $sql .= '           HAVING downloads < v.limit OR v.limit = 0 OR v.author_id = %d';
	}

	$vouchers = $wpdb->get_results( $wpdb->prepare( $sql, $post_id, $current_user->ID ) );
    }

    return $vouchers;
}

function gtvouchers_show_place_vouchers( $the_content )
{
    global $post;

    if ( !( is_single() ) || ( 'page' == $post->post_type ) )
    {
	return $the_content;
    }

    $options = get_option( 'gtvouchers_options' );

    $the_vouchers = gtvouchers_get_vouchers_by_place( $post->ID );

    $voucher_list = '';

    if ( !empty( $the_vouchers ) )
    {
	if ( ( 'true' == $options[ 'logged_in_only' ] ) && !is_user_logged_in() )
	{
	    if ( !empty( $options[ 'logged_out_teaser' ] ) )
	    {
		$the_content .= '<div id="gtv-teaser-text">' . $options[ 'logged_out_teaser' ] . '</div><br />';
	    }

	    return $the_content;
	}

	$voucher_list .= '<br />';

	foreach ( $the_vouchers as $voucher )
	{
	    if ( 1 == $voucher->live )
	    {
		$voucher_list .= '<a name="v' . $voucher->id . '"></a><br />';
		if ( 1 == $voucher->require_like )
		{
		    $the_content = gtv_add_like_headers( $the_content );
		}

		$image_args = array(
		    'numberposts' => 1,
		    'order' => 'ASC',
		    'post_mime_type' => 'image',
		    'post_parent' => $post->ID,
		    'post_status' => null,
		    'post_type' => 'attachment',
		);

		$arrImages = get_children( $image_args );

		$show_me = array_shift( $arrImages );

		if ( function_exists( 'gtvouchers_make_public_preview' ) )
		{
		    $voucher_list .= gtvouchers_make_public_preview( $voucher );
		}
	    }
	}
    }

    $voucher_list = apply_filters( 'place_voucher_list', $voucher_list );

    return $the_content . $voucher_list . '<br />';
}

function gt_vouchers_show_edit( $post_id )
{
    $options = get_option( 'gtvouchers_options' );
    if ( !is_user_logged_in() )
    {
	return false;
    }

    echo '<p class="edit-link"><a href="' . site_url( '?gtv-page=edit-voucher&place_id=' . $post_id ) . '">' . sprintf( __( 'Edit %s', 'gtvouchers-d' ), $options[ 'replace_voucher_singular_word' ] ) . '</a></p>';
}

function gt_vouchers_redirect()
{
    if ( !isset( $_GET[ 'gtv-page' ] ) )
    {
	return;
    }

    switch ( $_GET[ 'gtv-page' ] )
    {
	case 'edit-voucher':
	    gtv_display_voucher_edit();
	    exit;
	case 'save-voucher':
	    gtvouchers_save_front_voucher();
	    exit;
    }
}

function gtv_display_voucher_edit()
{
    $options = get_option( "gtvouchers_options" );
    $place_id = $_GET[ 'place_id' ];

    if ( !is_numeric( $place_id ) && ( 'new' != $_GET[ "id" ] ) )
    {
	return;
    }

    gtvouchers_admin_css();

// call header
    get_header();

###### WRAPPER OPEN ######

    /**
     * Outputs the opening HTML wrappers for most template pages.
     *
     * This adds the opening html tags to the primary div, this required the closing tag below :: ($type='',$id='',$class='').
     *
     * @since 1.1.0
     * @param string $type Page type.
     * @param string $id The id of the HTML element.
     * @param string $class The class of the HTML element.
     * @see 'geodir_wrapper_close'
     */
    do_action( 'geodir_wrapper_open', 'voucher-page', 'geodir-wrapper', '' );

###### TOP CONTENT ######

    /**
     * Called before the main content and the page specific content.
     *
     * @since 1.1.0
     * @param string $type Page type.
     */
    do_action( 'geodir_top_content', 'add-listing-page' );

    /**
     * Calls the top section widget area and the breadcrumbs on the add listing page.
     *
     * @since 1.1.0
     */
    do_action( 'geodir_add_listing_before_main_content' );

    /**
     * Called before the main content of a template page.
     *
     * @since 1.1.0
     * @see 'geodir_after_main_content'
     */
    do_action( 'geodir_before_main_content', 'voucher-page' );

###### MAIN CONTENT WRAPPERS OPEN ######
    /**
     * Outputs the opening HTML wrappers for the content.
     *
     * This adds the opening html tags to the content div, this required the closing tag below :: ($type='',$id='',$class='')
     *
     * @since 1.1.0
     * @param string $type Page type.
     * @param string $id The id of the HTML element.
     * @param string $class The class of the HTML element.
     * @see 'geodir_wrapper_content_close'
     */
    do_action( 'geodir_wrapper_content_open', 'voucher-page', 'geodir-wrapper-content', '' );
    ?>
    <?php
    if ( gtv_has_vouchers( $place_id ) || ( isset( $_GET[ 'id' ] ) && ( 'new' != $_GET[ "id" ] ) ) )
    {
	echo '<h1>' . sprintf( __( 'Edit %s', 'gtvouchers-d' ), $options[ 'replace_voucher_singular_word' ] ) . '</h1>';
    }
    else
    {
	echo '<h1>' . sprintf( __( 'Create %s', 'gtvouchers-d' ), $options[ 'replace_voucher_singular_word' ] ) . '</h1>';
    }
    ?>
    <?php
    if ( isset( $_GET[ 'result' ] ) && ( '4' == $_GET[ 'result' ] ) )
    {
	echo '
                            <div id="message" class="updated fade">
                                <p><strong>' . sprintf( __( 'The %s has been deleted.', 'gtvouchers-d' ), strtolower( $options[ 'replace_voucher_singular_word' ] ) ) . '</strong></p>
                            </div>
                            ';
    }
    if ( isset( $_GET[ 'result' ] ) && ( '5' == $_GET[ 'result' ] ) )
    {
	echo '
                            <div id="message" class="error">
                                <p><strong>' . sprintf( __( 'The %s could not be deleted.', 'gtvouchers-d' ), strtolower( $options[ 'replace_voucher_singular_word' ] ) ) . '</strong></p>
                            </div>
                            ';
    }
    if ( ( ( isset( $_GET[ 'id' ] ) ) && 'new' == $_GET[ 'id' ] ) || !gtv_has_vouchers( $place_id ) )
    {
	gtvouchers_create_voucher_page( 'front' );
    }
    else
    {
	gtvouchers_edit_voucher_page( 'front' );
    }
    ?>
    <!-- In between -->
    <?php
    ###### MAIN CONTENT WRAPPERS CLOSE ######

    /**
     * Called after the main content of a template page.
     *
     * @see 'geodir_before_main_content'
     * @since 1.1.0
     */
    do_action( 'geodir_after_main_content' );

    /**
     * Outputs the closing HTML wrappers for the content.
     *
     * This adds the closing html tags to the wrapper_content div :: ($type='')
     *
     * @since 1.1.0
     * @param string $type Page type.
     * @see 'geodir_wrapper_content_open'
     */
    do_action( 'geodir_wrapper_content_close', 'voucher-page' );

###### SIDEBAR ######
    /**
     * This action adds the sidebar to the add listing page template.
     *
     * @since 1.1.0
     */
    do_action( 'gt_vouchers_sidebar' );

###### WRAPPER CLOSE ######

    /**
     * Outputs the closing HTML wrappers for most template pages.
     *
     * This adds the closing html tags to the wrapper div :: ($type='')
     *
     * @since 1.1.0
     * @param string $type Page type.
     * @see 'geodir_wrapper_open'
     */
    do_action( 'geodir_wrapper_close', 'voucher-page' );

// call footer
    get_footer();
}

function gt_vouchers_sidebar_inside()
{
    dynamic_sidebar( 'gt_vouchers_sidebar' );
}

//add_action( 'gt_vouchers_sidebar', 'gtv_output_edit_page_sidebar' );
add_action( 'gt_vouchers_sidebar_inside', 'gt_vouchers_sidebar_inside', 10 );
add_action( 'gt_vouchers_sidebar', 'gt_vouchers_action_sidebar', 10 );

function gt_vouchers_action_sidebar()
{
    /** This action is documented in geodirectory_template_actions.php */
    do_action( 'geodir_sidebar_right_open', 'gt-vouchers', 'geodir-sidebar', 'geodir-sidebar-right', 'http://schema.org/WPSideBar' );
    /**
     * This is used to add the content to the add listing page sidebar.
     *
     * @since 1.0.0
     */
    do_action( 'gt_vouchers_sidebar_inside' );
    /** This action is documented in geodirectory_template_actions.php */
    do_action( 'geodir_sidebar_right_close', 'gt-vouchers' );
}

function gtv_has_vouchers( $place_id )
{
    if ( !is_numeric( $place_id ) )
    {
	return;
    }

    global $wpdb;

    $prefix = $wpdb->prefix;

    $voucher_sql = "SELECT place_id as vouchers FROM {$prefix}gtvouchers_vouchers WHERE deleted = 0 AND place_id = %d";
    $params[ 0 ] = $place_id;

    if ( !current_user_can( 'administrator' ) )
    {
	$voucher_sql .= " AND author_id = %d";
	$params[ 1 ] = get_current_user_id();
    }

    $voucher_sql .= "  LIMIT 1";

    $count = $wpdb->get_col( $wpdb->prepare( $voucher_sql, $params ) );

    if ( !empty( $count ) )
    {
	return true;
    }
    else
    {
	return false;
    }
}

function gtv_place_has_visible_vouchers( $place_id )
{
    if ( false === ( $visible_vouchers_places = get_transient( 'gtv_visible_vouchers_places' ) ) )
    {
	$visible_vouchers_places = gtvouchers_get_all_visible_vouchers();
	set_transient( 'gtv_visible_vouchers_places', $visible_vouchers_places, 24 * 60 * 60 );
    }

    if ( empty( $visible_vouchers_places ) )
    {
	$visible_vouchers_places = gtvouchers_get_all_visible_vouchers();
	set_transient( 'gtv_visible_vouchers_places', $visible_vouchers_places, 24 * 60 * 60 );
    }

    if ( in_array( $place_id, $visible_vouchers_places ) )
    {
	return true;
    }

    return false;
}

function gtvouchers_get_all_visible_vouchers()
{
    global $wpdb, $plugin_prefix;
    $options = get_option( 'gtvouchers_options' );

    $prefix = $wpdb->prefix;

    $vouchers = '';

    $voucher_packages = $wpdb->get_col( "SELECT pid FROM {$prefix}geodir_price WHERE allow_vouchers = 1" );
    $voucher_packages = implode( ',', $voucher_packages );

    $select_clause = "SELECT DISTINCT v.place_id, (
		SELECT Count(d.id)
		FROM {$prefix}gtvouchers_downloads d
		WHERE d.voucherid = v.id) as downloads,
		v.limit";

    $from_clause = " FROM {$prefix}gtvouchers_vouchers v";

    $where_clause = " WHERE  deleted = 0
            AND ( live = 1 )
            AND ( From_unixtime(startdate) < Now()
            AND ( expiry = 0
                OR From_unixtime(expiry) > Now() ) )";


    if ( 'hide' == $options[ 'maxxed_downloads' ] )
    {
	$where_clause .= '           HAVING (downloads < v.limit OR v.limit = 0)';
    }

    if ( is_plugin_active( 'geodir_payment_manager/geodir_payment_manager.php' ) )
    {
	$from_clause .= "
		 JOIN {$wpdb->posts} AS wpp
		    ON v.place_id = wpp.id
		";

	$geodir_post_types = geodir_get_posttypes( 'array' );
	$geodir_post_types = array_keys( $geodir_post_types );

	foreach ( $geodir_post_types as $post_type )
	{
	    $table = $plugin_prefix . $post_type . '_detail';
	    $from_clause .= " LEFT JOIN $table AS $post_type ON wpp.id = $post_type.post_id";

	    $packages[] = "$post_type.package_id";
	}

	$packages = implode( ',', $packages );

	$select_clause .= ",
		Coalesce($packages) as pid";

	if ( 'hide' == $options[ 'maxxed_downloads' ] )
	{
	    $where_clause .= " AND pid IN ($voucher_packages)";
	}
	else
	{
	    $where_clause .= " HAVING pid IN ($voucher_packages)";
	}
    }

    $vouchers = $wpdb->get_col( $select_clause . $from_clause . $where_clause );

    return $vouchers;
}

function gtv_save_voucher_edit( $id, $name, $require_email, $require_like, $require_phone, $liketextcta, $limit, $text, $description, $template, $font, $vouchertextcolor, $terms, $live, $startdate, $expiry, $codestype, $codelength, $codeprefix, $codesuffix, $codes, $voucher_category )
{
    global $wpdb;
    $blog_id = gtvouchers_blog_id();

    $prefix = $wpdb->prefix;
//    if ( isset( $wpdb->base_prefix ) ){
//        $prefix = $wpdb->base_prefix;
//    }
    $sql = $wpdb->prepare( "update " . $prefix . "gtvouchers_vouchers set
    time = %d,
    name = %s,
    `text` = %s,
    `description` = %s,
    terms = %s,
    template = %d,
    font = %s,
    textcolor = %
    require_email = %d,
    require_like = %d,
    require_phone = %d,
    liketextcta = %s,
    `limit` = %d,
    live = %d,
    startdate = %d,
    expiry = %d,
    codestype = %s,
    codelength = %d,
    codeprefix = %s,
    codesuffix = %s,
    codes = %s
    category_id = %d,
    where id = %d
    and blog_id = %d;", time(), $name, $text, $description, $terms, $template, $font, $vouchertextcolor, $require_email, $require_like, $require_phone, $liketextcta, $limit, $live, $startdate, $expiry, $codestype, $codelength, $codeprefix, $codesuffix, $codes, $voucher_category, $id, $blog_id );

    if ( GTV_DEBUG )
    {
	wp_mail( 'jeff@jeffrose.ca', 'Debug log', 'Edit (4106) called:' . $sql );
    }

    $done = $wpdb->query( $sql );

    if ( $done )
    {
	do_action( 'gtvouchers_edit', $id, $name, $text, $description, $template, $require_email, $require_like, $require_phone, $liketextcta, $limit, $startdate, $expiry );
	gtvouchers_clear_gtv_transients();
	delete_single_cache( $id );
    }

    return $done;
}

function gtvouchers_choose_voucher()
{
    $options = get_option( 'gtvouchers_options' );
    $place_id = intval( $_GET[ 'place_id' ] );

    if ( 0 == $place_id )
    {
	return;
    }

    $place_vouchers = gtvouchers_get_vouchers_by_place( $place_id, 'edit' );

    if ( empty( $place_vouchers ) )
    {
	gtvouchers_edit_voucher_page();
    }

    $max_vouchers = gtvouchers_get_max_vouchers_for_listing( $place_id );

    echo '<form name="select-voucher" method="get" action="' . $_SERVER[ 'PHP_SELF' ] . '">';
    echo sprintf( __( 'Select which %s to edit:', 'gtvouchers-d' ), $options[ 'replace_voucher_plural_word' ] );
    echo '<input type="hidden" name="place_id" value="' . $place_id . '" />';
    echo '<input type="hidden" name="gtv-page" value="edit-voucher" />';
    echo '  <select name="id">';
    if ( ( '' == $max_vouchers ) || ( count( $place_vouchers ) < $max_vouchers ) || current_user_can( 'update_core' ) )
    {
	echo '      <option value="new">' . sprintf( __( 'Create new %s', 'gtvouchers-d' ), strtolower( $options[ 'replace_voucher_singular_word' ] ) ) . '</option>';
    }
    foreach ( $place_vouchers as $voucher )
    {
	echo '      <option value="' . $voucher->id . '">' . $voucher->name . '</option>';
    }
    echo '  </select>';
    echo '  <input type="submit" name="get-voucher" value="' . sprintf( __( 'Edit %s', 'gtvouchers-d' ), strtolower( $options[ 'replace_voucher_singular_word' ] ) ) . '" />';
    echo '</form>';
}

function gtvouchers_get_max_vouchers_for_package( $package_id )
{
    global $wpdb;

    if ( !( is_plugin_active( 'geodir_payment_manager/geodir_payment_manager.php' ) ) )
    {
	return 0;
    }

    if ( 0 == $package_id )
    {
	return 0;
    }

    $sql = "SELECT max_vouchers FROM " . GEODIR_PRICE_TABLE . " WHERE pid = %d";

    $result = $wpdb->get_var( $wpdb->prepare( $sql, $package_id ) );

    return $result;
}

function gtvouchers_get_max_vouchers_for_listing( $place_id )
{
    global $wpdb;
    $gd_prefix = gtv_get_gd_prefix();

    $post_type = get_post_type( $place_id );

    if ( ( is_plugin_active( 'geodir_payment_manager/geodir_payment_manager.php' ) ) )
    {
	$sql = "SELECT package_id FROM {$gd_prefix}geodir_{$post_type}_detail WHERE post_id=%d";

	$package_id = $wpdb->get_var( $wpdb->prepare( $sql, $place_id ) );

	return gtvouchers_get_max_vouchers_for_package( $package_id );
    }
    else
    {
	$options = get_option( 'gtvouchers_options' );

	if ( isset( $options[ 'voucher_post_types' ] ) )
	{
	    if ( in_array( $post_type, $options[ 'voucher_post_types' ] ) )
	    {
		$max_vouchers = $options[ 'voucher_post_type_qty' ][ $post_type ];

		return $max_vouchers;
	    }
	}
    }

    return 0;
}

function gtvouchers_save_front_voucher()
{

    if ( isset( $_POST[ 'delete' ] ) && ( '1' == $_POST[ 'delete' ] ) && isset( $_GET[ 'id' ] ) )
    {

	$listing_author = get_post( $_POST[ 'place_id' ] )->post_author;
	if ( current_user_can( 'administrator' ) || get_current_user_id() == $listing_author )
	{
	    $done = gtvouchers_delete_voucher( $_GET[ 'id' ] );
	}
	if ( $done )
	{
	    header( 'Location: ' . site_url() . '?gtv-page=edit-voucher&place_id=' . intval( $_POST[ 'place_id' ] ) . '&result=4' );
	    exit();
	}
	else
	{
	    header( 'Location: ' . site_url() . '?gtv-page=edit-voucher&place_id=' . intval( $_POST[ 'place_id' ] ) . '&result=5' );
	    exit();
	}
    }

    $require_email = 0;
    if ( isset( $_POST[ 'requireemail' ] ) && '1' == $_POST[ 'requireemail' ] )
    {
	$require_email = 1;
    }

    $require_phone = 0;
    if ( isset( $_POST[ 'requirephone' ] ) && '1' == $_POST[ 'requirephone' ] )
    {
	$require_phone = 1;
    }

    $require_like = 0;
    if ( isset( $_POST[ 'requirelike' ] ) && '1' == $_POST[ 'requirelike' ] )
    {
	$require_like = 1;
    }

    $liketextcta = '';
    if ( isset( $_POST[ 'liketextcta' ] ) )
    {
	$liketextcta = trim( $_POST[ 'liketextcta' ] );
    }

    $live = 0;
    if ( isset( $_POST[ 'live' ] ) && '1' == $_POST[ 'live' ] )
    {
	$live = 1;
    }

    $limit = 0;
    if ( '' != $_POST[ 'limit' ] && '0' != $_POST[ 'limit' ] )
    {
	$limit = ( int ) $_POST[ 'limit' ];
    }

    $startdate = 0;
    if ( '' != $_POST[ 'startyear' ] && '0' != $_POST[ 'startyear' ] && '' != $_POST[ 'startmonth' ] && '0' != $_POST[ 'startmonth' ] && '' != $_POST[ 'startday' ] && '0' != $_POST[ 'startday' ] )
    {
	$startdate = strtotime( $_POST[ 'startyear' ] . '/' . $_POST[ 'startmonth' ] . '/' . $_POST[ 'startday' ] );
    }

    $expiry = 0;
    if ( '' != $_POST[ 'expiryyear' ] && '0' != $_POST[ 'expiryyear' ] && '' != $_POST[ 'expirymonth' ] && '0' != $_POST[ 'expirymonth' ] && '' != $_POST[ 'expiryday' ] && '0' != $_POST[ 'expiryday' ] )
    {
	$expiry = strtotime( $_POST[ 'expiryyear' ] . '/' . $_POST[ 'expirymonth' ] . '/' . $_POST[ 'expiryday' ] );
    }
    if ( '' != $_POST[ 'expirydays' ] && 0 < is_numeric( $_POST[ 'expirydays' ] ) )
    {
	$expiry = time() + ( intval( $_POST[ 'expirydays' ] ) * 24 * 60 * 60 );
    }
    if ( '' != $_POST[ 'expirydays' ] && 0 < is_numeric( $_POST[ 'expirydays' ] ) && 0 < $startdate )
    {
	$expiry = $startdate + ( intval( $_POST[ 'expirydays' ] ) * 24 * 60 * 60 );
    }

    if ( 'random' == $_POST[ 'codestype' ] || 'sequential' == $_POST[ 'codestype' ] || 'custom' == $_POST[ 'codestype' ] || 'single' == $_POST[ 'codestype' ] )
    {
	$codestype = $_POST[ 'codestype' ];
    }
    else
    {
	$codestype = 'random';
    }
    if ( '' != $_POST[ 'codelength' ] )
    {
	$codelength = ( int ) $_POST[ 'codelength' ];
    }
    if ( '' == $codelength || 0 == $codelength )
    {
	$codelength = 6;
    }

    $codeprefix = trim( $_POST[ 'codeprefix' ] );
    if ( 6 < strlen( $codeprefix ) )
    {
	$codeprefix = substr( $codeprefix, 6 );
    }

    $codesuffix = trim( $_POST[ 'codesuffix' ] );
    if ( 6 < strlen( $codesuffix ) )
    {
	$codesuffix = substr( $codesuffix, 6 );
    }

    $codes = '';
    if ( 'custom' == $_POST[ 'codestype' ] )
    {
	$codes = trim( $_POST[ 'customcodelist' ] );
    }
    if ( 'single' == $_POST[ 'codestype' ] )
    {
	$codes = trim( $_POST[ 'singlecodetext' ] );
    }

    if ( isset( $_GET[ 'id' ] ) )
    {
	$done = gtvouchers_edit_voucher( intval( $_GET[ 'id' ] ), $_POST[ 'name' ], $require_email, $require_like, $require_phone, $liketextcta, $limit, $_POST[ 'text' ], $_POST[ 'description' ], $_POST[ 'template' ], $_POST[ 'font' ], $_POST[ 'textcolor' ], $_POST[ 'terms' ], $live, $startdate, $expiry, $codestype, $codelength, $codeprefix, $codesuffix, $codes );
	$return_id = intval( $_GET[ 'id' ] );
    }
    else
    {
	$done = gtvouchers_check_voucher( 'create', 'front' );
	$return_id = intval( $done[ 1 ] );
    }
    if ( $done )
    {
	gtvouchers_clear_gtv_transients();
	header( 'Location: ' . get_site_url() . '?place_id=' . intval( $_POST[ 'place_id' ] ) . '&gtv-page=edit-voucher&result=1&id=' . $return_id );
	exit();
    }
    else
    {
	gtvouchers_clear_gtv_transients();
	delete_single_cache( $return_id );
	header( 'Location: ' . get_site_url() . '?place_id=' . intval( $_POST[ 'place_id' ] ) . '&gtv-page=edit-voucher&result=2&id=' . $return_id );
	exit();
    }
}

function gtvouchers_settings_page()
{
    $active_tab = isset( $_GET[ 'tab' ] ) ? $_GET[ 'tab' ] : 'main_settings';
    $options = get_option( 'gtvouchers_options' );
    ?>
    <style type="text/css">
        .form-table th {
    	width: 350px;
        }
    </style>
    <div class="wrap">
        <div id="icon-plugins" class="icon32"></div>
        <h2>GT-Vouchers Settings</h2>
    </div>
    <div>
	<?php gtv_output_promo_sidebar(); ?>
        <div style="float: left; max-width:750px;">
    	<h2 class="nav-tab-wrapper">
    	    <a href="?page=gtv-settings&tab=main_settings"
    	       class="nav-tab <?php echo $active_tab == 'main_settings' ? 'nav-tab-active' : ''; ?>"><?php _e( 'Main Settings', 'gtvouchers-d' ); ?></a>
    	    <a href="?page=gtv-settings&tab=text_settings"
    	       class="nav-tab <?php echo $active_tab == 'text_settings' ? 'nav-tab-active' : ''; ?>"><?php _e( 'Text Settings', 'gtvouchers-d' ); ?></a>
    	    <a href="?page=gtv-settings&tab=voucher_settings"
    	       class="nav-tab <?php echo $active_tab == 'voucher_settings' ? 'nav-tab-active' : ''; ?>"><?php printf( __( '%s Settings', 'gtvouchers-d' ), $options[ 'replace_voucher_singular_word' ] ); ?></a>
    	    <a href="?page=gtv-settings&tab=license_settings"
    	       class="nav-tab <?php echo $active_tab == 'license_settings' ? 'nav-tab-active' : ''; ?>"><?php _e( 'License Settings', 'gtvouchers-d' ); ?></a>
		   <?php do_action( 'after_settings_tab_list', $active_tab ); ?>
    	</h2>

    	<p>
		<?php _e( 'Did you know there are shortcodes for voucher functions? Check the website reference <a href="http://gt-vouchers.com/documentation/shortcodes/" target="gt_vouchers">here</a>', 'gtvouchers-d' ); ?>
    	    .
    	</p>
	    <?php
	    if ( ( is_plugin_active( 'geodir_payment_manager/geodir_payment_manager.php' ) ) )
	    {
		echo '<p>';
		printf( __( 'Visit the <a href="%s">Geodirectory settings for prices</a> to activate Vouchers on select packages.', 'gtvouchers-d' ), 'admin.php?page=geodirectory&tab=paymentmanager_fields&subtab=geodir_payment_general_options' );
		echo '</p>';
	    }
	    ?>

    	<form action="options.php" method="post">
		<?php
		if ( 'main_settings' == $active_tab )
		{
		    settings_fields( 'gtvouchers-options' );
		    do_settings_sections( 'gtvo-main' );
		}
		elseif ( 'text_settings' == $active_tab )
		{
		    settings_fields( 'gtvouchers-options' );
		    do_settings_sections( 'gtvo-text' );
		}
		elseif ( 'voucher_settings' == $active_tab )
		{
		    settings_fields( 'gtvouchers-options' );
		    do_settings_sections( 'gtvo-vouchers' );
		}
		elseif ( 'license_settings' == $active_tab )
		{
		    settings_fields( 'gtvouchers-license' );
		    do_settings_sections( 'gtvo-license' );
		}
		do_action( 'after_settings_tab_functions' );
		?>
		<?php submit_button(); ?>
    	</form>
        </div>
    </div>
    <?php
}

function gtvouchers_setup()
{
    $options = get_option( 'gtvouchers_options' );
    register_setting( 'gtvouchers-options', 'gtvouchers_options', 'gtvouchers_options_validate' );
    register_setting( 'gtvouchers-license', 'gtv_license_key', 'gtvouchers_license_validate' );

    add_settings_section( 'gtvo-license', __( 'License Settings', 'gtvouchers-d' ), 'gtvouchers_options_license_text', 'gtvo-license' );
    add_settings_field( 'gtvouchers_license_key', __( 'Enter your license key', 'gtvouchers-d' ), 'gtvo_license_key', 'gtvo-license', 'gtvo-license' );
    add_settings_field( 'gtvouchers_license_status', __( 'License status', 'gtvouchers-d' ), 'gtvo_activate_license', 'gtvo-license', 'gtvo-license' );

    add_settings_section( 'gtvo-main', __( 'Main Settings', 'gtvouchers-d' ), 'gtvouchers_options_main_text', 'gtvo-main' );
    if ( !( is_plugin_active( 'geodir_payment_manager/geodir_payment_manager.php' ) ) )
    {
	add_settings_field( 'voucher_cpts', __( 'Allow vouchers on these Post Types', 'gtvouchers-d' ), 'gtvo_voucher_cpts', 'gtvo-main', 'gtvo-main' );
    }
    add_settings_field( 'email_from_name', sprintf( __( 'Name from which to send %s', 'gtvouchers-d' ), strtolower( $options[ 'replace_voucher_plural_word' ] ) ), 'gtvo_from_name', 'gtvo-main', 'gtvo-main' );
    add_settings_field( 'email_from_email', sprintf( __( 'Email from which to send %s', 'gtvouchers-d' ), strtolower( $options[ 'replace_voucher_plural_word' ] ) ), 'gtvo_from_email', 'gtvo-main', 'gtvo-main' );
    add_settings_field( 'replace_voucher_singular_word', __( 'Replace the word "Voucher" with:', 'gtvouchers-d' ), 'gtvo_voucher_singular_word', 'gtvo-main', 'gtvo-main' );
    add_settings_field( 'replace_voucher_plural_word', __( 'Replace the word "Vouchers" with:', 'gtvouchers-d' ), 'gtvo_voucher_plural_word', 'gtvo-main', 'gtvo-main' );
    add_settings_field( 'use_voucher_tab', sprintf( __( 'Display %s in separate tab', 'gtvouchers-d' ), strtolower( $options[ 'replace_voucher_plural_word' ] ) ), 'gtvo_use_voucher_tab', 'gtvo-main', 'gtvo-main' );
    add_settings_field( 'voucher_tab_text', sprintf( __( 'Name of the tab for %s', 'gtvouchers-d' ), strtolower( $options[ 'replace_voucher_plural_word' ] ) ), 'gtvo_voucher_tab_text', 'gtvo-main', 'gtvo-main' );
    add_settings_field( 'send_daily_downloads_report', __( 'Send daily download counts to listing owners', 'gtvouchers-d' ), 'gtvo_send_daily_downloads_report', 'gtvo-main', 'gtvo-main' );
    add_settings_field( 'hide_csv_download_report', __( 'Block download of email lists by listing owners', 'gtvouchers-d' ), 'gtvo_hide_csv_download_report', 'gtvo-main', 'gtvo-main' );
    add_settings_field( 'hide_maxxed_downloads', sprintf( __( 'Hide %s after max downloads', 'gtvouchers-d' ), strtolower( $options[ 'replace_voucher_plural_word' ] ) ), 'gtvo_max_download', 'gtvo-main', 'gtvo-main' );
    add_settings_field( 'show_countdown', sprintf( __( 'Show a counter of how many %s remain', 'gtvouchers-d' ), strtolower( $options[ 'replace_voucher_plural_word' ] ) ), 'gtvo_show_countdown', 'gtvo-main', 'gtvo-main' );
    add_settings_field( 'logged_in_only', sprintf( __( 'Only show %s to logged in users', 'gtvouchers-d' ), strtolower( $options[ 'replace_voucher_plural_word' ] ) ), 'gtvo_logged_in_only', 'gtvo-main', 'gtvo-main' );
    add_settings_field( 'teaser_text', sprintf( __( 'Text to display to non-logged in users in place of %s', 'gtvouchers-d' ), strtolower( $options[ 'replace_voucher_singular_word' ] ) ), 'gtvo_logged_out_teaser', 'gtvo-main', 'gtvo-main' );
    add_settings_field( 'border_thickness', sprintf( __( 'When displaying the %s on the Place page, how thick is the border', 'gtvouchers-d' ), strtolower( $options[ 'replace_voucher_plural_word' ] ) ), 'gtvo_border_thickness', 'gtvo-main', 'gtvo-main' );
    add_settings_field( 'border_color', sprintf( __( 'When displaying the %s on the Place page, what color is the border?', 'gtvouchers-d' ), strtolower( $options[ 'replace_voucher_plural_word' ] ) ), 'gtvo_border_color', 'gtvo-main', 'gtvo-main' );
    add_settings_field( 'border_style', sprintf( __( 'When displaying the %s on the Place page, what style is the border', 'gtvouchers-d' ), strtolower( $options[ 'replace_voucher_plural_word' ] ) ), 'gtvo_border_style', 'gtvo-main', 'gtvo-main' );
    add_settings_field( 'use_voucher_banner', sprintf( __( 'Show a %s available banner instead of the icon.', 'gtvouchers-d' ), strtolower( $options[ 'replace_voucher_singular_word' ] ) ), 'gtvo_use_voucher_banner', 'gtvo-main', 'gtvo-main' );
    add_settings_field( 'voucher_banner_text', __( 'Text to display in the banner on listings.', 'gtvouchers-d' ), 'gtvo_voucher_banner_text', 'gtvo-main', 'gtvo-main' );
    add_settings_field( 'voucher_available_text', sprintf( __( 'Text to display as title and alt tag for %s icon.', 'gtvouchers-d' ), strtolower( $options[ 'replace_voucher_singular_word' ] ) ), 'gtvo_voucher_avail_text', 'gtvo-main', 'gtvo-main' );
    add_settings_field( 'voucher_available_no_label', __( 'Hide the text, just use the icon.', 'gtvouchers-d' ), 'gtvo_voucher_avail_text_hidden', 'gtvo-main', 'gtvo-main' );
    add_settings_field( 'voucher_available_icon', sprintf( __( 'Custom icon to indicate %s on a listing', 'gtvouchers-d' ), strtolower( $options[ 'replace_voucher_plural_word' ] ) ), 'gtvo_voucher_icon_upload', 'gtvo-main', 'gtvo-main' );

    do_action( 'gtv_after_main_settings' );

    add_settings_section( 'gtvo-text', __( 'Text Settings', 'gtvouchers-d' ), 'gtvouchers_options_text_text', 'gtvo-text' );
    add_settings_field( 'download_text', sprintf( __( 'Prompt to download %s', 'gtvouchers-d' ), strtolower( $options[ 'replace_voucher_singular_word' ] ) ), 'gtvo_download_text', 'gtvo-text', 'gtvo-text' );
    if ( GTV_GD )
    {
	add_settings_field( 'print_button_text', sprintf( __( 'Prompt to print %s', 'gtvouchers-d' ), strtolower( $options[ 'replace_voucher_singular_word' ] ) ), 'gtvo_print_button_text', 'gtvo-text', 'gtvo-text' );
    }
    add_settings_field( 'pre_download_text', sprintf( __( 'Text to display above the %s registration fields. Only {listing} and {voucher} tokens work here.', 'gtvouchers-d' ), strtolower( $options[ 'replace_voucher_singular_word' ] ) ), 'gtvo_pre_reg_text', 'gtvo-text', 'gtvo-text' );
    add_settings_field( 'thank_you_text', __( 'Text to display with successful registration', 'gtvouchers-d' ), 'gtvo_thank_you_text', 'gtvo-text', 'gtvo-text' );
    add_settings_field( 'cannot_reg_text', __( 'Text to display with failed registration', 'gtvouchers-d' ), 'gtvo_failed_text', 'gtvo-text', 'gtvo-text' );
    add_settings_field( 'email_body', __( 'Email message content', 'gtvouchers-d' ), 'gtvo_email_text', 'gtvo-text', 'gtvo-text' );
    add_settings_field( 'daily_report_message', __( 'Text of the daily report message', 'gtvouchers-d' ), 'gtvo_daily_report_text', 'gtvo-text', 'gtvo-text' );

    add_settings_section( 'gtvo-vouchers', sprintf( __( '%s Settings', 'gtvouchers-d' ), $options[ 'replace_voucher_singular_word' ] ), 'gtvouchers_options_voucher_text', 'gtvo-vouchers' );
    add_settings_field( 'use_categories', sprintf( __( 'Allow users to set category for %s', 'gtvouchers-d' ), strtolower( $options[ 'replace_voucher_plural_word' ] ) ), 'gtvo_use_categories', 'gtvo-vouchers', 'gtvo-vouchers' );
    add_settings_field( 'user_image_upload', sprintf( __( 'Allow users to upload custom %s images', 'gtvouchers-d' ), strtolower( $options[ 'replace_voucher_plural_word' ] ) ), 'gtvo_user_image_upload', 'gtvo-vouchers', 'gtvo-vouchers' );
    if ( GTV_GD )
    {
	add_settings_field( 'listing_voucher_header', __( 'Show text on solid top bar', 'gtvouchers-d' ), 'gtvo_listing_header', 'gtvo-vouchers', 'gtvo-vouchers' );
	add_settings_field( 'listing_voucher_header_text', __( 'Custom text for top bar', 'gtvouchers-d' ), 'gtvo_listing_header_text', 'gtvo-vouchers', 'gtvo-vouchers' );
	add_settings_field( 'listing_voucher_header_color', __( 'Bar background color', 'gtvouchers-d' ), 'gtvo_listing_header_color', 'gtvo-vouchers', 'gtvo-vouchers' );
	add_settings_field( 'listing_voucher_preview', sprintf( __( 'Show %s preview on listing with real image', 'gtvouchers-d' ), strtolower( $options[ 'replace_voucher_plural_word' ] ) ), 'gtvo_listing_preview', 'gtvo-vouchers', 'gtvo-vouchers' );
	add_settings_field( 'preview_watermark_text', sprintf( __( 'Watermark text for %s preview', 'gtvouchers-d' ), strtolower( $options[ 'replace_voucher_singular_word' ] ) ), 'gtvo_preview_watermark_text', 'gtvo-vouchers', 'gtvo-vouchers' );
    }
    add_settings_field( 'voucher-thank-you', __( 'Custom Thank You page', 'gtvouchers-d' ), 'gtvo_voucher_thank_you_page', 'gtvo-vouchers', 'gtvo-vouchers' );
    add_settings_field( 'voucher-thank-you-url', __( 'Custom Thank You url', 'gtvouchers-d' ), 'gtvo_voucher_thank_you_url', 'gtvo-vouchers', 'gtvo-vouchers' );
    add_settings_field( 'voucher-text-color', sprintf( __( '%s text color', 'gtvouchers-d' ), $options[ 'replace_voucher_singular_word' ] ), 'gtvo_voucher_text_color', 'gtvo-vouchers', 'gtvo-vouchers' );
    add_settings_field( 'hide_email_required_option', __( 'Hide Email Required option', 'gtvouchers-d' ), 'gtvo_hide_email_required_option', 'gtvo-vouchers', 'gtvo-vouchers' );
    add_settings_field( 'admin_require_email', __( 'Pre-check the Email Required box', 'gtvouchers-d' ), 'gtvo_admin_require_email', 'gtvo-vouchers', 'gtvo-vouchers' );

    add_settings_field( 'hide_phone_required_option', __( 'Hide Phone Required option', 'gtvouchers-d' ), 'gtvo_hide_phone_required_option', 'gtvo-vouchers', 'gtvo-vouchers' );
    add_settings_field( 'admin_require_phone', __( 'Pre-check the Phone Required box', 'gtvouchers-d' ), 'gtvo_admin_require_phone', 'gtvo-vouchers', 'gtvo-vouchers' );

    add_settings_field( 'hide_facebook_option', __( 'Hide Facebook Like option', 'gtvouchers-d' ), 'gtvo_hide_facebook_option', 'gtvo-vouchers', 'gtvo-vouchers' );
    add_settings_field( 'admin_require_like', __( 'Pre-check the Like Required box', 'gtvouchers-d' ), 'gtvo_admin_require_like', 'gtvo-vouchers', 'gtvo-vouchers' );
    add_settings_field( 'vouchers_require_approval', sprintf( __( '%s require approval before going live', 'gtvouchers-d' ), $options[ 'replace_voucher_plural_word' ] ), 'gtvo_vouchers_require_approval', 'gtvo-vouchers', 'gtvo-vouchers' );

    do_action( 'gtv_after_voucher_settings' );
}

function gtvo_options_license_text()
{
    _( 'Using a valid license key ensures access to future updates', 'gtvouchers-d' );
}

function gtvo_license_key()
{
    $license = get_option( 'gtv_license_key' );

    echo '<input id="gtv_license" name="gtv_license_key" type="text" class="regular-text" value="' . esc_attr( $license ) . '" />';
}

function gtvo_activate_license()
{
    $license = get_option( 'gtv_license_key' );
    $status = get_option( 'gtv_license_status' );

    if ( false !== $license )
    {
	if ( $status !== false && $status == 'valid' )
	{
	    ?>
	    <span style="color:green;"><?php _e( 'Active', 'gtvouchers-d' ); ?></span>
	    <?php
	}
	else
	{
	    wp_nonce_field( 'gtv_activation', 'gtv_activation' );
	    ?>
	    <input type="submit" class="button-secondary" name="edd_license_activate" value="<?php _e( 'Activate License', 'gtvouchers-d' ); ?>"/>
	    <?php
	}
    }
}

function gtvouchers_options_main_text()
{
//	echo '<style>
//        .new_icon{
//            background: url( ' . plugins_url( '/images/new.png', __FILE__ ) . ');
//            background-repeat:no-repeat;
//            padding-left:28px;
//            padding-bottom:10px;
//            line-height:24px;
//            font-weight: bold;
//            display:block;
//        }
//        </style>
//    ';
}

function gtvo_voucher_cpts()
{
    $options = get_option( 'gtvouchers_options' );
    $chosen_types = isset( $options[ 'voucher_post_types' ] ) ? $options[ 'voucher_post_types' ] : array();
    if ( !( is_array( $chosen_types ) ) )
    {
	$chosen_types = array( $chosen_types );
    }

    $post_types = array_unique( geodir_vouchers_type_setting() );

    echo '<table id="gtv_post_types_settings">';
    echo '<tr><td>' . __( 'Type', 'gtvouchers-d' ) . '</td><td>' . __( 'Allow', 'gtvouchers-d' ) . '</td><td>' . __( 'How many?', 'gtvouchers-d' ) . '</td></tr>';
    foreach ( $post_types as $value => $type )
    {
	echo '<tr><td>' . esc_attr( $type ) . '</td>';
	echo '<td><input type="checkbox" name="gtvouchers_options[voucher_post_types][' . esc_attr( $value ) . ']"';
	if ( in_array( $value, array_values( $chosen_types ) ) )
	{
	    echo ' checked="checked"';
	}
	echo '/></td>';
	$qty = isset( $options[ 'voucher_post_type_qty' ][ $value ] ) ? $options[ 'voucher_post_type_qty' ][ $value ] : 0;
	echo '<td><input type="number" min="0" max="20" style="width:40px;" value="' . intval( $qty ) . '" maxlength="2" name="gtvouchers_options[voucher_post_type_qty][' . esc_attr( $value ) . ']" /></td></tr>';
    }
    echo '</table>';
}

function geodir_vouchers_type_setting()
{

    $post_type_arr = array();

    $post_types = geodir_get_posttypes( 'object' );

    foreach ( $post_types as $key => $post_types_obj )
    {
	$post_type_arr[ $key ] = $post_types_obj->labels->singular_name;
    }

    return $post_type_arr;
}

function gtvo_from_name()
{
    $options = get_option( 'gtvouchers_options' );
    $default = get_option( 'blogname' );

    if ( isset( $options[ 'email_from_name' ] ) && !empty( $options[ 'email_from_name' ] ) )
    {
	$default = $options[ 'email_from_name' ];
    }
    echo '<input type="hidden" name="tab" value="main_settings" />';
    echo '<input type="text" name="gtvouchers_options[email_from_name]" value="' . esc_attr( $default ) . '" />';
}

function gtvo_from_email()
{
    $options = get_option( 'gtvouchers_options' );
    $default = get_option( 'admin_email' );

    if ( isset( $options[ 'email_from_email' ] ) && !empty( $options[ 'email_from_email' ] ) )
    {
	$default = $options[ 'email_from_email' ];
    }

    echo '<input type="text" name="gtvouchers_options[email_from_email]" value="' . esc_attr( $default ) . '" />';
}

function gtvouchers_options_text_text()
{
    $options = get_option( 'gtvouchers_options' );
    _e( 'Enter your custom text below. This is shown to users registering to download vouchers.', 'gtvouchers-d' );
    echo '<br />';
    _e( 'Optional tokens to customize the message:', 'gtvouchers-d' );
    echo '<ul>';
    echo '<li>&nbsp;&nbsp;{name} - ' . __( 'Registered name', 'gtvouchers-d' ) . '</li>';
    echo '<li>&nbsp;&nbsp;{email} - ' . __( 'Email address', 'gtvouchers-d' ) . '</li>';
    echo '<li>&nbsp;&nbsp;{listing} - ' . __( 'Listing name', 'gtvouchers-d' ) . '</li>';
    echo '<li>&nbsp;&nbsp;{voucher} - ' . sprintf( __( '%s name', 'gtvouchers-d' ), $options[ 'replace_voucher_singular_word' ] ) . '</li>';
    echo '<li>&nbsp;&nbsp;{sitename} - ' . __( 'Your site name', 'gtvouchers-d' ) . ' - ' . get_option( 'blogname' ) . '</li>';
    echo '</ul>';
}

function gtvouchers_options_voucher_text()
{
    
}

function gtvouchers_options_license_text()
{
    echo '<p>';
    _e( 'GT-Vouchers requires a license for upgrade and support purposes.', 'gtvouchers-d' );
    echo '</p>';
    echo '<p>';
    _e( 'Licenses are issued by email usually within 24 hours of purchase but are not required for full function of the plugin.', 'gtvouchers-d' );
    echo '</p>';
}

function gtvo_send_daily_downloads_report()
{
    $options = get_option( 'gtvouchers_options' );
    echo "<input id='send_daily_downloads_report' name='gtvouchers_options[send_daily_downloads_report]' type='checkbox' value='true' " . checked( $options[ 'send_daily_downloads_report' ], 'true', false ) . " />";
}

function gtvo_voucher_singular_word()
{
    $options = get_option( 'gtvouchers_options' );
    if ( empty( $options[ 'replace_voucher_singular_word' ] ) )
    {
	$options[ 'replace_voucher_singular_word' ] = __( 'Voucher', 'gtvouchers-d' );
    }
    echo '<input type="text" name="gtvouchers_options[replace_voucher_singular_word]" value="' . esc_attr( $options[ 'replace_voucher_singular_word' ] ) . '" />';
}

function gtvo_voucher_plural_word()
{
    $options = get_option( 'gtvouchers_options' );
    if ( empty( $options[ 'replace_voucher_plural_word' ] ) )
    {
	$options[ 'replace_voucher_plural_word' ] = __( 'Vouchers', 'gtvouchers-d' );
    }
    echo '<input type="text" name="gtvouchers_options[replace_voucher_plural_word]" value="' . esc_attr( $options[ 'replace_voucher_plural_word' ] ) . '" />';
}

function gtvo_use_voucher_tab()
{
    $options = get_option( 'gtvouchers_options' );
    echo "<input id='use_voucher_tab' name='gtvouchers_options[use_voucher_tab]' type='checkbox' value='true' " . checked( $options[ 'use_voucher_tab' ], 'true', false ) . " />";
}

function gtvo_voucher_tab_text()
{
    $options = get_option( 'gtvouchers_options' );
    echo '<input type="text" name="gtvouchers_options[voucher_tab_text]" value="' . esc_attr( $options[ 'voucher_tab_text' ] ) . '" />';
}

function gtvo_hide_csv_download_report()
{
    $options = get_option( 'gtvouchers_options' );
    echo "<input id='hide_csv_download_report' name='gtvouchers_options[hide_csv_download_report]' type='checkbox' value='true' " . checked( $options[ 'hide_csv_download_report' ], 'true', false ) . " />";
}

function gtvo_max_download()
{
    $options = get_option( 'gtvouchers_options' );
    echo "<input id='maxxed_downloads' name='gtvouchers_options[maxxed_downloads]' type='checkbox' value='hide' " . checked( $options[ 'maxxed_downloads' ], 'hide', false ) . " />";
}

function gtvo_logged_in_only()
{
    $options = get_option( 'gtvouchers_options' );
    echo "<input id='logged_in_only' name='gtvouchers_options[logged_in_only]' type='checkbox' value='true' " . checked( $options[ 'logged_in_only' ], 'true', false ) . " />";
}

function gtvo_logged_out_teaser()
{
    $options = get_option( 'gtvouchers_options' );
    echo "<textarea name='gtvouchers_options[logged_out_teaser]' id='logged_out_teaser' style='width:400px;height:55px;' >\n";
    echo esc_textarea( $options[ 'logged_out_teaser' ] );
    echo "</textarea>\n";
    echo "<br />" . __( 'This will be wrapped in a div with an id="gtv-teaser-text" so you can apply styling.', 'gtvouchers-d' );
    echo "<br /><em>" . __( 'Leave blank for no teaser text. Users not logged in will not know what\'s missing.', 'gtvouchers-d' ) . '</em>';
}

function gtvo_use_categories()
{
    $options = get_option( 'gtvouchers_options' );
    if ( !( isset( $options[ 'use_categories' ] ) ) )
    {
	$options[ 'use_categories' ] = 'true';
    }
    echo "<input id='use_categories' name='gtvouchers_options[use_categories]' type='checkbox' value='true' " . checked( $options[ 'use_categories' ], 'true', false ) . " />&nbsp;";
    printf( __( 'Allow %s to be assigned to a category. Some ideas: "2 for 1", "%% off", "Sale" or "Food". Always shows to admin.', 'gtvouchers-d' ), strtolower( $options[ 'replace_voucher_plural_word' ] ) );
}

function gtvo_user_image_upload()
{
    $options = get_option( 'gtvouchers_options' );
    if ( !( isset( $options[ 'user_image_upload' ] ) ) )
    {
	$options[ 'user_image_upload' ] = 'false';
    }
    echo "<input id='user_image_upload' name='gtvouchers_options[user_image_upload]' type='checkbox' value='true' " . checked( $options[ 'user_image_upload' ], 'true', false ) . " />&nbsp;";
    printf( __( 'Allow users to upload custom images for %s. No text or codes are overlaid on these images.', 'gtvouchers-d' ), strtolower( $options[ 'replace_voucher_plural_word' ] ) );
}

function gtvo_listing_preview()
{
    $options = get_option( 'gtvouchers_options' );
    if ( empty( $options[ 'listing_voucher_preview' ] ) )
    {
	$options[ 'listing_voucher_preview' ] = 'false';
    }
    echo "<input id='listing_voucher_preview' name='gtvouchers_options[listing_voucher_preview]' type='checkbox' value='true' " . checked( $options[ 'listing_voucher_preview' ], 'true', false ) . " />&nbsp;";
    sprintf( __( 'Also activates the print option and downloads %s as images, not PDFs', 'gtvouchers-d' ), strtolower( $options[ 'replace_voucher_plural_word' ] ) );
}

function gtvo_preview_watermark_text()
{
    $options = get_option( 'gtvouchers_options' );
    if ( empty( $options[ 'preview_watermark_text' ] ) )
    {
	$options[ 'preview_watermark_text' ] = '';
    }
    echo "<input id='preview_watermark_text' name='gtvouchers_options[preview_watermark_text]' type='text' value='" . $options[ 'preview_watermark_text' ] . "' />&nbsp;" . sprintf( __( 'Leave blank for no watermark. Does not appear on downloaded/printed %s.', 'gtvouchers-d' ), strtolower( $options[ 'replace_voucher_plural_word' ] ) );
}

function gtvo_show_countdown()
{
    $options = get_option( 'gtvouchers_options' );
    if ( empty( $options[ 'show_countdown' ] ) )
    {
	$options[ 'show_countdown' ] = 'false';
    }
    echo "<input id='show_countdown' name='gtvouchers_options[show_countdown]' type='checkbox' value='true' " . checked( $options[ 'show_countdown' ], 'true', false ) . " />";
}

function gtvo_border_thickness()
{
    $options = get_option( 'gtvouchers_options' );
    echo '<div class="voucher_preview">';
    echo '<select name="gtvouchers_options[border_thickness]">';
    for ( $i = 0; $i <= 6; $i ++ )
    {
	echo '<option value="' . $i . '"';
	selected( $i, $options[ 'border_thickness' ], true );
	echo '>' . $i . ' px</option>';
    }
    echo '</select>';
    echo '</div>';
}

function gtvo_border_style()
{
    $options = get_option( 'gtvouchers_options' );
    $styles = array(
	'none' => __( 'None', 'gtvouchers-d' ),
	'solid' => __( 'Solid', 'gtvouchers-d' ),
	'dotted' => __( 'Dotted', 'gtvouchers-d' ),
	'dashed' => __( 'Dashed', 'gtvouchers-d' ),
    );
    echo '<div class="voucher_preview">';
    echo '<select name="gtvouchers_options[border_style]">';
    foreach ( $styles as $value => $title )
    {
	echo '<option value="' . $value . '"';
	selected( $value, $options[ 'border_style' ], true );
	echo '>' . esc_attr( $title ) . '</option>';
    }
    echo '</select>';
    echo '</div>';
}

function gtvo_border_color()
{
    global $wp_version;

    //If the WordPress version is greater than or equal to 3.5, then load the new WordPress color picker.
    if ( 3.5 <= $wp_version )
    {
	//Both the necessary css and javascript have been registered already by WordPress, so all we have to do is load them with their handle.
	wp_enqueue_style( 'wp-color-picker' );
	wp_enqueue_script( 'wp-color-picker' );
    } //If the WordPress version is less than 3.5 load the older farbtasic color picker.
    else
    {
	//As with wp-color-picker the necessary css and javascript have been registered already by WordPress, so all we have to do is load them with their handle.
	wp_enqueue_style( 'farbtastic' );
	wp_enqueue_script( 'farbtastic' );
    }

    $options = get_option( 'gtvouchers_options' );
    if ( '' == $options[ 'border_color' ] )
    {
	$options[ 'border_color' ] = '#ffffff';
    }
    echo '<div class="voucher_preview">';
    echo '<input type="text" id="bordercolor" name="gtvouchers_options[border_color]" value="' . esc_attr( $options[ 'border_color' ] ) . '" />';
    echo '<div style="left: 150px;position:relative; top: -30px;" id="colorpicker"></div>';
    echo '</div>';
}

function gtvo_use_voucher_banner()
{
    $options = get_option( 'gtvouchers_options' );

    echo "<input id='use_voucher_banner' name='gtvouchers_options[use_voucher_banner]' type='checkbox' value='true' " . checked( $options[ 'use_voucher_banner' ], 'true', false ) . " />&nbsp;";
    printf( __( 'Use the banner when showing a %s is available, instead of the icon. Only in GTV Listing Widget and GTV Grid Widget for now.', 'gtvouchers-d' ), strtolower( $options[ 'replace_voucher_plural_word' ] ) );
}

function gtvo_voucher_banner_text()
{
    $options = get_option( 'gtvouchers_options' );
    echo '<input style="width:150px;" type="text" name="gtvouchers_options[voucher_banner_text]" value="' . esc_attr( $options[ 'voucher_banner_text' ] ) . '" />';
    printf( __( '<em>Default</em>: %s', 'gtvouchers-d' ), $options[ 'replace_voucher_singular_word' ] );
}

function gtvo_voucher_avail_text()
{
    $options = get_option( 'gtvouchers_options' );
    echo '<input style="width:150px;" type="text" name="gtvouchers_options[voucher_available_text]" value="' . esc_attr( $options[ 'voucher_available_text' ] ) . '" />';
    printf( __( '<em>Default</em>: %s Available', 'gtvouchers-d' ), $options[ 'replace_voucher_singular_word' ] );
}

function gtvo_voucher_avail_text_hidden()
{
    $options = get_option( 'gtvouchers_options' );

    echo "<input id='voucher_available_no_label' name='gtvouchers_options[voucher_available_no_label]' type='checkbox' value='true' " . checked( $options[ 'voucher_available_no_label' ], 'true', false ) . " />&nbsp;";
    printf( __( 'Show no text for "%s Available," just the icon.', 'gtvouchers-d' ), $options[ 'replace_voucher_singular_word' ] );
}

function gtvo_voucher_icon_upload()
{
    global $wp_version;
    $options = get_option( 'gtvouchers_options' );
    $icon = $options[ 'voucher_available_icon' ];
    if ( 3.5 > $wp_version )
    {
	?>
	<script language="JavaScript">
	    jQuery( document ).ready( function () {
		jQuery( '#upload_image_button' ).click( function () {
		    formfield = jQuery( '#upload_image' ).attr( 'name' );
		    tb_show( '', 'media-upload.php?type=image&TB_iframe=true' );
		    return false;
		} );

		window.send_to_editor = function ( html ) {
		    imgurl = jQuery( 'img', html ).attr( 'src' );
		    jQuery( 'input#upload_image' ).val( imgurl );
		    tb_remove();
		};

	    } );
	</script>
    <?php } ?>
    <label for="upload_image">
        <input id="upload_image" type="text" size="36" name="gtvouchers_options[voucher_available_icon]" value="<?php echo esc_url( $icon ); ?>"/>
        <input id="upload_image_button" class="gtv_icon_upload" type="button" value="<?php _e( 'Upload or choose icon', 'gtvouchers-d' ); ?>"/>
        <br/><?php _e( 'Enter an URL or upload an image for the icon.', 'gtvouchers-d' ); ?>
    </label>
    <?php
}

function gtvo_download_text()
{
    echo '<input type="hidden" name="tab" value="text_settings" />';
    $options = get_option( 'gtvouchers_options' );
    if ( empty( $options[ 'download_text' ] ) )
    {
	$options[ 'download_text' ] = '';
    }
    echo "<textarea name='gtvouchers_options[download_text]' id='download_text' style='width:400px;height:55px;' >";
    echo esc_textarea( $options[ 'download_text' ] );
    echo "</textarea>";
    echo "<br /><em>" . __( 'Original', 'gtvouchers-d' ) . ":</em> ";
    printf( __( 'Download this %s', 'gtvouchers-d' ), strtolower( $options[ 'replace_voucher_singular_word' ] ) );
}

function gtvo_print_button_text()
{
    $options = get_option( 'gtvouchers_options' );
    if ( empty( $options[ 'print_button_text' ] ) )
    {
	$options[ 'print_button_text' ] = '';
    }
    echo "<textarea name='gtvouchers_options[print_button_text]' id='print_button_text' style='width:400px;height:55px;' >";
    echo esc_textarea( $options[ 'print_button_text' ] );
    echo "</textarea>";
    echo "<br /><em>" . __( 'Original', 'gtvouchers-d' ) . ":</em> ";
    printf( __( 'Print this %s', 'gtvouchers-d' ), strtolower( $options[ 'replace_voucher_singular_word' ] ) );
}

function gtvo_pre_reg_text()
{
    $options = get_option( 'gtvouchers_options' );
    if ( empty( $options[ 'pre_download_text' ] ) )
    {
	$options[ 'pre_download_text' ] = '';
    }
    echo "<textarea name='gtvouchers_options[pre_download_text]' id='pre_download_text' style='width:400px;height:55px;' >";
    echo esc_textarea( $options[ 'pre_download_text' ] );
    echo "</textarea>";
    echo "<br /><em>" . __( 'Original', 'gtvouchers-d' ) . ":</em> ";
    printf( __( 'To download this %s you must provide your name and email address.', 'gtvouchers-d' ), strtolower( $options[ 'replace_voucher_singular_word' ] ) );
    echo '<br />';
    printf( __( 'You will then receive a link by email to download your personalised %s.', 'gtvouchers-d' ), strtolower( $options[ 'replace_voucher_singular_word' ] ) );
}

function gtvo_thank_you_text()
{
    $options = get_option( 'gtvouchers_options' );
    if ( empty( $options[ 'thank_you_text' ] ) )
    {
	$options[ 'thank_you_text' ] = '';
    }
    echo "<textarea name='gtvouchers_options[thank_you_text]' id='thank_you_text' style='width:400px;height:55px;' >";
    echo esc_textarea( $options[ 'thank_you_text' ] );
    echo '</textarea>';
    echo "<br /><em>" . __( 'Original', 'gtvouchers-d' ) . ":</em> ";
    printf( __( "Thank you for registering. You will shortly receive an email sent to '{email}' with a link to your personalised %s.", 'gtvouchers-d' ), strtolower( $options[ 'replace_voucher_singular_word' ] ) );
}

function gtvo_failed_text()
{
    $options = get_option( 'gtvouchers_options' );
    if ( empty( $options[ 'cannot_reg_text' ] ) )
    {
	$options[ 'cannot_reg_text' ] = '';
    }
    echo "<textarea name='gtvouchers_options[cannot_reg_text]' id='cannot_reg_text' style='width:400px;height:55px;' >";
    echo esc_textarea( $options[ 'cannot_reg_text' ] );
    echo "</textarea>";
    echo "<br /><em>" . __( 'Original', 'gtvouchers-d' ) . ":</em> ";
    printf( __( 'Sorry, your email address and name could not be registered. Have you already registered for this %s? Please try again.', 'gtvouchers-d' ), strtolower( $options[ 'replace_voucher_singular_word' ] ) );
}

function gtvo_email_text()
{
    $options = get_option( 'gtvouchers_options' );
    if ( empty( $options[ 'email_body' ] ) )
    {
	$options[ 'email_body' ] = '';
    }
    echo "<textarea name='gtvouchers_options[email_body]' id='email_body' style='width:400px;height:55px;' >";
    echo esc_textarea( $options[ 'email_body' ] );
    echo "</textarea>";
    echo "<br /><em>" . __( 'Original', 'gtvouchers-d' ) . ":</em> ";
    printf( __( 'You have successfully registered to download this %s, please download it from here:', 'gtvouchers-d' ), strtolower( $options[ 'replace_voucher_singular_word' ] ) );
}

function gtvo_daily_report_text()
{
    $options = get_option( 'gtvouchers_options' );
    if ( empty( $options[ 'daily_report_message' ] ) )
    {
	$options[ 'daily_report_message' ] = '';
    }
    echo "<textarea name='gtvouchers_options[daily_report_message]' id='daily_report_message' style='width:400px;height:85px;' >";
    echo esc_textarea( $options[ 'daily_report_message' ] );
    echo "</textarea>";
    echo "<br /><em>" . __( 'Template options', 'gtvouchers-d' ) . ":</em> ";
    echo " {sitename}, {firstname}";
}

function gtvo_voucher_thank_you_page()
{
    echo '<input type="hidden" name="tab" value="voucher_settings" />';
    $options = get_option( 'gtvouchers_options' );

    $pages = get_pages();
    if ( !empty( $pages ) )
    {
	?>
	<select name="gtvouchers_options[thank_you_page]">
	    <option value="">
		<?php echo esc_attr( __( 'Select a page...', 'gtvouchers-d' ) ); ?></option>
	    <?php
	    foreach ( $pages as $page )
	    {
		$option = '<option value="' . intval( $page->ID ) . '" ';
		if ( isset( $options[ 'thank_you_page' ] ) )
		{
		    $option .= selected( $options[ 'thank_you_page' ], $page->ID );
		}
		$option .= '>';
		$option .= esc_attr( $page->post_title );
		$option .= '</option>';
		echo $option;
	    }
	    ?>
	</select>
	<?php
	echo '&nbsp;';
	_e( 'Choose a WordPress page to send users to after they register their email.', 'gtvouchers-d' );
    }
}

function gtvo_voucher_thank_you_url()
{
    $options = get_option( 'gtvouchers_options' );

    echo '<input type="text" id="gtvouchers_options[thank_you_url]" name="gtvouchers_options[thank_you_url]" value="' . esc_attr( isset( $options[ 'thank_you_url' ] ) ? $options[ 'thank_you_url' ] : ''  ) . '" />&nbsp;';
    _e( 'Enter a full URL to send users to after they register. The page chosen above will be used if both are set.', 'gtvouchers-d' );
}

function gtvo_voucher_text_color()
{
    $options = get_option( 'gtvouchers_options' );
    if ( !( isset( $options[ 'voucher_text_color' ] ) ) || '' == $options[ 'voucher_text_color' ] )
    {
	$options[ 'voucher_text_color' ] = '#000000';
    }
    echo '<input type="text" id="alltextcolor" name="gtvouchers_options[voucher_text_color]" value="' . esc_attr( $options[ 'voucher_text_color' ] ) . '" />';
    echo '<div style="left: 150px;position:relative; top: -30px;" id="colorpicker2"></div>';
}

function gtvo_hide_email_required_option()
{
    $options = get_option( 'gtvouchers_options' );
    if ( empty( $options[ 'hide_email_required_option' ] ) )
    {
	$options[ 'hide_email_required_option' ] = 'false';
    }
    echo '<input type="checkbox" name="gtvouchers_options[hide_email_required_option]" value="true" ' . checked( "true", $options[ 'hide_email_required_option' ], false ) . ' style="margin-right:10px;" />';
    _e( 'Prevent users from choosing the "Require Email Address" option.', 'gtvouchers-d' );
}

function gtvo_admin_require_email()
{
    $options = get_option( 'gtvouchers_options' );
    if ( empty( $options[ 'admin_require_email' ] ) )
    {
	$options[ 'admin_require_email' ] = 'false';
    }
    echo '<input type="checkbox" name="gtvouchers_options[admin_require_email]" value="true" ' . checked( "true", $options[ 'admin_require_email' ], false ) . ' style="margin-right:10px;" />';
    printf( __( 'Using both the "Hide email required option" above and this option, you can basically force all %s to require emails.', 'gtvouchers-d' ), strtolower( $options[ 'replace_voucher_plural_word' ] ) );
}

function gtvo_hide_phone_required_option()
{
    $options = get_option( 'gtvouchers_options' );
    if ( empty( $options[ 'hide_phone_required_option' ] ) )
    {
	$options[ 'hide_phone_required_option' ] = 'false';
    }
    echo '<input type="checkbox" name="gtvouchers_options[hide_phone_required_option]" value="true" ' . checked( "true", $options[ 'hide_phone_required_option' ], false ) . ' style="margin-right:10px;" />';
    _e( 'Prevent users from choosing the "Require Phone Number" option.', 'gtvouchers-d' );
}

function gtvo_admin_require_phone()
{
    $options = get_option( 'gtvouchers_options' );
    if ( empty( $options[ 'admin_require_phone' ] ) )
    {
	$options[ 'admin_require_phone' ] = 'false';
    }
    $require = isset( $options[ 'admin_require_phone' ] ) ? $options[ 'admin_require_phone' ] : false;
    echo '<input type="checkbox" name="gtvouchers_options[admin_require_phone]" value="true" ' . checked( "true", $require, false ) . ' style="margin-right:10px;" />';
    printf( __( 'Using both the "Hide phone required option" above and this option, you can basically force all %s to require phone numbers.', 'gtvouchers-d' ), strtolower( $options[ 'replace_voucher_plural_word' ] ) );
}

function gtvo_hide_facebook_option()
{
    $options = get_option( 'gtvouchers_options' );
    $hide = isset( $options[ 'hide_facebook_option' ] ) ? $options[ 'hide_facebook_option' ] : false;
    echo '<input type="checkbox" name="gtvouchers_options[hide_facebook_option]" value="true" ' . checked( "true", $hide ) . ' style="margin-right:10px;" />';
    _e( 'Prevent users from choosing the "Require Facebook Like" option.', 'gtvouchers-d' );
}

function gtvo_admin_require_like()
{
    $options = get_option( 'gtvouchers_options' );
    if ( empty( $options[ 'admin_require_like' ] ) )
    {
	$options[ 'admin_require_like' ] = 'false';
    }
    $require = isset( $options[ 'admin_require_like' ] ) ? $options[ 'admin_require_like' ] : false;
    echo '<input type="checkbox" name="gtvouchers_options[admin_require_like]" value="true" ' . checked( "true", $require, false ) . ' style="margin-right:10px;" />';
    printf( __( 'Using both the "Hide Like required option" above and this option, you can basically force all %s to require Facebook Likes.', 'gtvouchers-d' ), strtolower( $options[ 'replace_voucher_plural_word' ] ) );
}

function gtvo_vouchers_require_approval()
{
    $options = get_option( 'gtvouchers_options' );
    if ( empty( $options[ 'vouchers_require_approval' ] ) )
    {
	$options[ 'vouchers_require_approval' ] = 'false';
    }
    echo '<input type="checkbox" name="gtvouchers_options[vouchers_require_approval]" value="true" ' . checked( "true", $options[ 'vouchers_require_approval' ], false ) . ' style="margin-right:10px;" />';
    printf( __( 'Admin receives an email when a %s is created or changed and must activate all %s manually.', 'gtvouchers-d' ), strtolower( $options[ 'replace_voucher_singular_word' ] ), strtolower( $options[ 'replace_voucher_plural_word' ] ) );
}

function gtvouchers_license_validate( $new )
{
    $old = get_option( 'gtv_license_key' );
    if ( $old && $old != $new )
    {
	delete_option( 'gtv_license_status' ); // new license has been entered, so must reactivate
    }

    return $new;
}

function gtvouchers_options_validate( $post_options )
{
    $old_options = get_option( 'gtvouchers_options' );
    $styles = array(
	'solid',
	'dotted',
	'dashed'
    );
    $tab = $_POST[ 'tab' ];

    $post_options = stripslashes_deep( $post_options );

    if ( isset( $tab ) && 'main_settings' == $tab )
    {
	$new_input[ 'send_daily_downloads_report' ] = isset( $post_options[ 'send_daily_downloads_report' ] ) ? $post_options[ 'send_daily_downloads_report' ] : '';
	if ( empty( $new_input[ 'send_daily_downloads_report' ] ) )
	{
	    $new_input[ 'send_daily_downloads_report' ] = 'false';
	    // Remove all future scheduled reports
	    wp_clear_scheduled_hook( 'gtvouchers_reports' );
	}
	else
	{
	    // Add a task to WP_Cron
	    gtvouchers_add_scheduled_reports();
	}

	$new_input[ 'use_voucher_tab' ] = isset( $post_options[ 'use_voucher_tab' ] ) ? $post_options[ 'use_voucher_tab' ] : 'false';
	if ( empty( $new_input[ 'use_voucher_tab' ] ) )
	{
	    $new_input[ 'use_voucher_tab' ] = 'false';
	}

	$new_input[ 'voucher_tab_text' ] = isset( $post_options[ 'voucher_tab_text' ] ) ? sanitize_text_field( $post_options[ 'voucher_tab_text' ] ) : '';

	$new_input[ 'email_from_name' ] = esc_attr( $post_options[ 'email_from_name' ] );

	$post_types = geodir_get_posttypes();

	if ( isset( $post_options[ 'voucher_post_types' ] ) )
	{
	    if ( empty( $post_options[ 'voucher_post_types' ] ) )
	    {
		$new_input[ 'voucher_post_types' ] = array();
	    }
	    else
	    {
		$good_types = array();
		foreach ( $post_options[ 'voucher_post_types' ] as $this_type => $status )
		{
		    if ( in_array( $this_type, array_values( $post_types ) ) )
		    {
			$good_types[] = $this_type;
		    }
		}
		$new_input[ 'voucher_post_types' ] = $good_types;
	    }
	}
	else
	{
	    $new_input[ 'voucher_post_types' ] = array();
	}

	$type_quantities = isset( $post_options[ 'voucher_post_type_qty' ] ) ? $post_options[ 'voucher_post_type_qty' ] : 0;
	foreach ( $post_types as $value => $type )
	{
	    if ( isset( $type_quantities[ $type ] ) && ( in_array( $type, array_values( $new_input[ 'voucher_post_types' ] ) ) ) )
	    {
		$limits[ $type ] = intval( $type_quantities[ $type ] );
		// If Vouchers are allowed, you must allow at least 1.
		if ( 0 == intval( $type_quantities[ $type ] ) )
		{
		    $limits[ $type ] = 1;
		}
	    }
	    else
	    {
		$limits[ $type ] = 0;
	    }
	}

	$new_input[ 'voucher_post_type_qty' ] = $limits;

	$email = is_email( $post_options[ 'email_from_email' ] );
	if ( !$email )
	{
	    $email = get_option( 'admin_email' );
	}
	$new_input[ 'email_from_email' ] = $email;

	$new_input[ 'hide_csv_download_report' ] = isset( $post_options[ 'hide_csv_download_report' ] ) ? $post_options[ 'hide_csv_download_report' ] : 'false';
	if ( empty( $new_input[ 'hide_csv_download_report' ] ) )
	{
	    $new_input[ 'hide_csv_download_report' ] = 'false';
	}

	$new_input[ 'maxxed_downloads' ] = isset( $post_options[ 'maxxed_downloads' ] ) ? $post_options[ 'maxxed_downloads' ] : 'show';
	if ( empty( $new_input[ 'maxxed_downloads' ] ) )
	{
	    $new_input[ 'maxxed_downloads' ] = 'show';
	}

	$new_input[ 'show_countdown' ] = isset( $post_options[ 'show_countdown' ] ) ? $post_options[ 'show_countdown' ] : 'false';
	if ( empty( $new_input[ 'show_countdown' ] ) )
	{
	    $new_input[ 'show_countdown' ] = 'false';
	}

	$new_input[ 'replace_voucher_singular_word' ] = isset( $post_options[ 'replace_voucher_singular_word' ] ) ? $post_options[ 'replace_voucher_singular_word' ] : __( 'Voucher', 'gtvouchers-d' );
	$new_input[ 'replace_voucher_plural_word' ] = isset( $post_options[ 'replace_voucher_plural_word' ] ) ? $post_options[ 'replace_voucher_plural_word' ] : __( 'Vouchers', 'gtvouchers-d' );


	$new_input[ 'logged_in_only' ] = isset( $post_options[ 'logged_in_only' ] ) ? $post_options[ 'logged_in_only' ] : 'false';
	if ( empty( $new_input[ 'logged_in_only' ] ) )
	{
	    $new_input[ 'logged_in_only' ] = 'false';
	}

	$new_input[ 'logged_out_teaser' ] = isset( $post_options[ 'logged_out_teaser' ] ) ? esc_textarea( $post_options[ 'logged_out_teaser' ] ) : '';
	$new_input[ 'border_thickness' ] = isset( $post_options[ 'border_thickness' ] ) ? intval( $post_options[ 'border_thickness' ] ) : '';
	$new_input[ 'border_color' ] = isset( $post_options[ 'border_color' ] ) ? sanitize_text_field( $post_options[ 'border_color' ] ) : '';
	if ( in_array( $post_options[ 'border_style' ], $styles ) )
	{
	    $new_input[ 'border_style' ] = $post_options[ 'border_style' ];
	}
	else
	{
	    $new_input[ 'border_style' ] = '';
	}

	if ( isset( $post_options[ 'use_voucher_banner' ] ) )
	{
	    $new_input[ 'use_voucher_banner' ] = 'true';
	}
	else
	{
	    $new_input[ 'use_voucher_banner' ] = 'false';
	}

	$new_input[ 'voucher_banner_text' ] = esc_attr( $post_options[ 'voucher_banner_text' ] );

	$new_input[ 'voucher_available_text' ] = esc_html( wp_kses_post( $post_options[ 'voucher_available_text' ] ) );
	$new_input[ 'voucher_available_icon' ] = $post_options[ 'voucher_available_icon' ];

	if ( isset( $post_options[ 'voucher_available_no_label' ] ) )
	{
	    $new_input[ 'voucher_available_no_label' ] = $post_options[ 'voucher_available_no_label' ];
	}
	else
	{
	    $new_input[ 'voucher_available_no_label' ] = 'false';
	}
    }

    if ( isset( $tab ) && 'text_settings' == $tab )
    {
	$new_input[ 'download_text' ] = esc_html( wp_kses( $post_options[ 'download_text' ], array() ) );
	$new_input[ 'print_button_text' ] = esc_html( wp_kses( $post_options[ 'print_button_text' ], array() ) );
	$new_input[ 'pre_download_text' ] = esc_textarea( wp_kses( $post_options[ 'pre_download_text' ], array() ) );
	$new_input[ 'thank_you_text' ] = esc_html( wp_kses( $post_options[ 'thank_you_text' ], array() ) );
	$new_input[ 'cannot_reg_text' ] = esc_html( wp_kses( $post_options[ 'cannot_reg_text' ], array() ) );
	$new_input[ 'email_body' ] = esc_html( wp_kses( $post_options[ 'email_body' ], array() ) );
	$new_input[ 'daily_report_message' ] = esc_html( wp_kses( $post_options[ 'daily_report_message' ], array() ) );
    }

    if ( isset( $tab ) && 'voucher_settings' == $tab )
    {

	$new_input[ 'use_categories' ] = isset( $post_options[ 'use_categories' ] ) ? $post_options[ 'use_categories' ] : 'false';
	if ( empty( $new_input[ 'use_categories' ] ) )
	{
	    $new_input[ 'use_categories' ] = 'false';
	}

	$new_input[ 'user_image_upload' ] = isset( $post_options[ 'user_image_upload' ] ) ? $post_options[ 'user_image_upload' ] : 'false';
	if ( empty( $new_input[ 'user_image_upload' ] ) )
	{
	    $new_input[ 'user_image_upload' ] = 'false';
	}

	$new_input[ 'listing_voucher_preview' ] = isset( $post_options[ 'listing_voucher_preview' ] ) ? $post_options[ 'listing_voucher_preview' ] : 'false';
	if ( empty( $new_input[ 'listing_voucher_preview' ] ) )
	{
	    $new_input[ 'listing_voucher_preview' ] = 'false';
	}

	if ( isset( $post_options[ 'preview_watermark_text' ] ) )
	{
	    $new_input[ 'preview_watermark_text' ] = esc_attr( $post_options[ 'preview_watermark_text' ] );
	}
	else
	{
	    $new_input[ 'preview_watermark_text' ] = '';
	}

	if ( GTV_GD )
	{
	    if ( $new_input[ 'preview_watermark_text' ] != $old_options[ 'preview_watermark_text' ] )
	    {
		delete_voucher_cache();
	    }
	}

	if ( isset( $post_options[ 'listing_voucher_header' ] ) )
	{
	    $new_input[ 'listing_voucher_header' ] = 'true';
	}
	else
	{
	    $new_input[ 'listing_voucher_header' ] = '';
	}

	if ( isset( $post_options[ 'listing_header_text' ] ) )
	{
	    $new_input[ 'listing_header_text' ] = esc_attr( $post_options[ 'listing_header_text' ] );
	}
	else
	{
	    $new_input[ 'listing_header_text' ] = '';
	}

	$new_input[ 'voucher_text_color' ] = esc_html( wp_kses( $post_options[ 'voucher_text_color' ], array() ) );
	$new_input[ 'listing_voucher_header_color' ] = esc_html( wp_kses( $post_options[ 'listing_voucher_header_color' ], array() ) );
	$new_input[ 'thank_you_page' ] = intval( $post_options[ 'thank_you_page' ] );
	$new_input[ 'thank_you_url' ] = esc_url_raw( $post_options[ 'thank_you_url' ] );

	if ( isset( $post_options[ 'hide_email_required_option' ] ) )
	{
	    $new_input[ 'hide_email_required_option' ] = $post_options[ 'hide_email_required_option' ];
	}
	else
	{
	    $new_input[ 'hide_email_required_option' ] = 'false';
	}

	if ( isset( $post_options[ 'admin_require_email' ] ) )
	{
	    $new_input[ 'admin_require_email' ] = $post_options[ 'admin_require_email' ];
	}
	else
	{
	    $new_input[ 'admin_require_email' ] = 'false';
	}

	if ( isset( $post_options[ 'hide_phone_required_option' ] ) )
	{
	    $new_input[ 'hide_phone_required_option' ] = $post_options[ 'hide_phone_required_option' ];
	}
	else
	{
	    $new_input[ 'hide_phone_required_option' ] = 'false';
	}

	if ( isset( $post_options[ 'admin_require_phone' ] ) )
	{
	    $new_input[ 'admin_require_phone' ] = $post_options[ 'admin_require_phone' ];
	}
	else
	{
	    $new_input[ 'admin_require_phone' ] = 'false';
	}

	if ( isset( $post_options[ 'hide_facebook_option' ] ) )
	{
	    $new_input[ 'hide_facebook_option' ] = $post_options[ 'hide_facebook_option' ];
	}
	else
	{
	    $new_input[ 'hide_facebook_option' ] = 'false';
	}

	if ( isset( $post_options[ 'admin_require_like' ] ) )
	{
	    $new_input[ 'admin_require_like' ] = $post_options[ 'admin_require_like' ];
	}
	else
	{
	    $new_input[ 'admin_require_like' ] = 'false';
	}

	if ( isset( $post_options[ 'vouchers_require_approval' ] ) )
	{
	    $new_input[ 'vouchers_require_approval' ] = $post_options[ 'vouchers_require_approval' ];
	}
	else
	{
	    $new_input[ 'vouchers_require_approval' ] = 'false';
	}
    }

    $new_input = apply_filters( 'gtv_more_settings', $new_input, $post_options, $tab );

    $new_input = wp_parse_args( $new_input, $old_options );

    return $new_input;
}

function gtvouchers_customize_string( $string, $tokens, $replacements )
{
    if ( empty( $string ) || empty( $tokens ) || empty( $replacements ) )
    {
	return $string;
    }

    foreach ( $tokens as $key => $find )
    {
	$string = str_replace( $find, $replacements[ $key ], $string );
    }

    return $string;
}

function gtvouchers_add_scheduled_reports()
{
    if ( !wp_next_scheduled( 'gtvouchers_reports' ) )
    {
	wp_schedule_event( current_time( 'timestamp' ), 'daily', 'gtvouchers_reports' );
    }
}

function gtvouchers_run_daily_reports()
{
    global $wpdb;
    $options = get_option( 'gtvouchers_options' );
    $site = get_option( 'blogname' );

    if ( 'true' != $options[ 'send_daily_downloads_report' ] )
    {
	wp_clear_scheduled_hook( 'gtvouchers_reports' );

	return;
    }

    $prefix = $wpdb->prefix;

    $sql = "
    SELECT count(voucherid) as vcount, v.id, v.name, v.require_email, v.require_like, v.require_phone, post_title, user_email, meta_value as username
    FROM {$prefix}gtvouchers_downloads d
    JOIN {$prefix}gtvouchers_vouchers v ON d.voucherid = v.id
    JOIN {$wpdb->posts} wp ON wp.ID = v.place_id
    JOIN {$wpdb->users} u ON v.author_id = u.ID
    JOIN {$wpdb->usermeta} um ON u.ID = um.user_id and meta_key = 'first_name'
     WHERE d.time BETWEEN %d AND %d AND deleted=0 group by user_email, place_id, d.voucherid";
    $downloads = $wpdb->get_results( $wpdb->prepare( $sql, ( current_time( 'timestamp' ) - 86400 ), current_time( 'timestamp' ) ) );

    $base_message = $options[ 'daily_report_message' ];
    if ( empty( $base_message ) )
    {
	$base_message = "
Hello {firstname}:
Below is your daily download report from {sitename}:

";
    }
    else
    {
	$base_message .= "\n";
    }

    $base_message = $full_message . $base_message . "\n\n";

    $templates = array(
	'{firstname}',
	'{sitename}',
    );

    $last_email = '';

    $subject = $site . ' ' . sprintf( __( '%s download report', 'gtvouchers-d' ), strtolower( $options[ 'replace_voucher_singular_word' ] ) );

    if ( !isset( $options[ 'email_from_email' ] ) )
    {
	$sender = get_option( 'admin_email' );
    }
    else
    {
	$sender = $options[ 'email_from_email' ];
    }

    $headers = "From: $site <$sender>" . "\r\n";

    foreach ( $downloads as $d_count )
    {
	if ( $last_email->user_email != $d_count->user_email )
	{
	    if ( !empty( $last_email ) )
	    {
		$values = array(
		    $last_email->username . '',
		    $site,
		);
		$message = str_replace( $templates, $values, $message );

		$result = wp_mail( $last_email->user_email, $subject, $message, $headers );
	    }

	    $last_email = $d_count;
	    $message = $base_message;
	}

	$message = $message . $d_count->post_title . ' - ' . $d_count->name . ' : ' . $d_count->vcount . ' ' . __( 'downloads', 'gtvouchers-d' ) . "\n";
    }

    // This is here to catch the FINAL email in the loop, since we fall out before it needs to send
    // There might be a better way to do this. It's duplicate code.
    $values = array(
	$d_count->username . '',
	$site,
    );
    $message = str_replace( $templates, $values, $message );

    $result = wp_mail( $d_count->user_email, $subject, $message, $headers );
}

function gtv_html2rgb( $color )
{
    if ( $color[ 0 ] == '#' )
    {
	$color = substr( $color, 1 );
    }

    if ( strlen( $color ) == 6 )
    {
	list( $r, $g, $b ) = array(
	    $color[ 0 ] . $color[ 1 ],
	    $color[ 2 ] . $color[ 3 ],
	    $color[ 4 ] . $color[ 5 ],
	);
    }
    elseif ( strlen( $color ) == 3 )
    {
	list( $r, $g, $b ) = array(
	    $color[ 0 ] . $color[ 0 ],
	    $color[ 1 ] . $color[ 1 ],
	    $color[ 2 ] . $color[ 2 ],
	);
    }
    else
    {
	return false;
    }

    $r = hexdec( $r );
    $g = hexdec( $g );
    $b = hexdec( $b );

    return array(
	$r,
	$g,
	$b
    );
}

function gtv_add_like_headers( $buffer )
{
    global $post;
    $post_id = $post->ID;

    $buffer = str_replace( '<html dir="ltr" lang="en-US">', '<html dir="ltr" lang="en-US" xmlns:fb="//www.facebook.com/2008/fbml">', $buffer );

    if ( stripos( $buffer, '#appId=' ) === false )
    {
	$cookie_domain = $_SERVER[ 'HTTP_HOST' ];
	$cookie_domain = preg_replace( '#(www*\.)#si', '', $cookie_domain ); // rm www.

	$locale = apply_filters( 'plugin_locale', get_locale(), 'gtvouchers-d' );

	$buffer .= '<script src="//connect.facebook.net/' . $locale . '/all.js#appId=127835810690843&amp;xfbml=1"></script>';
	$buffer .= <<<JS_EOF
<script>
var cookie_domain = '$cookie_domain';

if (typeof jQuery.cookie == 'undefined') {
    /**
     * Cookie plugin
     *
     * Copyright (c) 2006 Klaus Hartl (stilbuero.de)
     * Dual licensed under the MIT and GPL licenses:
     * http://www.opensource.org/licenses/mit-license.php
     * http://www.gnu.org/licenses/gpl.html
     *
     */

    /**
     * Create a cookie with the given name and value and other optional parameters.
     *
     * @example $.cookie('the_cookie', 'the_value');
     * @desc Set the value of a cookie.
     * @example $.cookie('the_cookie', 'the_value', { expires: 7, path: '/', domain: 'jquery.com', secure: true });
     * @desc Create a cookie with all available options.
     * @example $.cookie('the_cookie', 'the_value');
     * @desc Create a session cookie.
     * @example $.cookie('the_cookie', null);
     * @desc Delete a cookie by passing null as value. Keep in mind that you have to use the same path and domain
     *       used when the cookie was set.
     *
     * @param String name The name of the cookie.
     * @param String value The value of the cookie.
     * @param Object options An object literal containing key/value pairs to provide optional cookie attributes.
     * @option Number|Date expires Either an integer specifying the expiration date from now on in days or a Date object.
     *                             If a negative value is specified (e.g. a date in the past), the cookie will be deleted.
     *                             If set to null or omitted, the cookie will be a session cookie and will not be retained
     *                             when the the browser exits.
     * @option String path The value of the path atribute of the cookie (default: path of page that created the cookie).
     * @option String domain The value of the domain attribute of the cookie (default: domain of page that created the cookie).
     * @option Boolean secure If true, the secure attribute of the cookie will be set and the cookie transmission will
     *                        require a secure protocol (like HTTPS).
     * @type undefined
     *
     * @name $.cookie
     * @cat Plugins/Cookie
     * @author Klaus Hartl/klaus.hartl@stilbuero.de
     */

    /**
     * Get the value of a cookie with the given name.
     *
     * @example $.cookie('the_cookie');
     * @desc Get the value of a cookie.
     *
     * @param String name The name of the cookie.
     * @return The value of the cookie.
     * @type String
     *
     * @name $.cookie
     * @cat Plugins/Cookie
     * @author Klaus Hartl/klaus.hartl@stilbuero.de
     */
    jQuery.cookie = function(name, value, options) {
        if (typeof value != 'undefined') { // name and value given, set cookie
            options = options || {};
            if (value === null) {
                value = '';
                options.expires = -1;
            }
            var expires = '';
            if (options.expires && (typeof options.expires == 'number' || options.expires.toUTCString)) {
                var date;
                if (typeof options.expires == 'number') {
                    date = new Date();
                    date.setTime(date.getTime() + (options.expires * 24 * 60 * 60 * 1000));
                } else {
                    date = options.expires;
                }
                expires = '; expires=' + date.toUTCString(); // use expires attribute, max-age is not supported by IE
            }
            // CAUTION: Needed to parenthesize options.path and options.domain
            // in the following expressions, otherwise they evaluate to undefined
            // in the packed version for some reason...
            var path = options.path ? '; path=' + (options.path) : '';
            var domain = options.domain ? '; domain=' + (options.domain) : '';
            var secure = options.secure ? '; secure' : '';
            document.cookie = [name, '=', encodeURIComponent(value), expires, path, domain, secure].join('');
        } else { // only name given, get cookie
            var cookieValue = null;
            if (document.cookie && document.cookie != '') {
                var cookies = document.cookie.split(';');
                for (var i = 0; i < cookies.length; i++) {
                    var cookie = jQuery.trim(cookies[i]);
                    // Does this cookie string begin with the name we want?
                    if (cookie.substring(0, name.length + 1) == (name + '=')) {
                        cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                        break;
                    }
                }
            }
            return cookieValue;
        }
    };
 }

 // credits: phpjs, Jonas Raoni Soares Silva (http://www.jsfromhell.com), Ates Goral (http://magnetiq.com), Onno Marsman, RafaÅ‚ Kukawski (http://blog.kukawski.pl)
 function like_gate_decrypt(str, pwd) {
    return (str + '').replace(/[a-z]/gi, function (s) {
        return String.fromCharCode(s.charCodeAt(0) + (s.toLowerCase() < 'n' ? 13 : -13));
    });
 }

 // handles like and unlike events
 function like_gate_handle_event(pars) {
   pars = pars || {};

   if (pars.event == 'unlike') {
        jQuery('.like-gate-result').hide('slow').html('');
        jQuery.cookie('like_gate_lp_{$post_id}', null, { path: '/' });
        return true;
   }

   // , domain: '$cookie_domain' // chrome doesn't like the domain !?! doesn't delete the cookie.
   jQuery.cookie('like_gate_lp_{$post_id}', 1, { expires: 365, path: '/' });

   var decrypted_hidden = like_gate_decrypt(jQuery('.like-gate').html());
   jQuery('.like-gate-result').html(decrypted_hidden).show('slow');
 }

 jQuery(document).ready(function() {
    jQuery('.posts').css('overflow', 'visible');
    var like_status = jQuery.cookie('like_gate_lp_{$post_id}');

    // let's reveal if the user has already liked the page.
    if (like_status > 0) {
        like_gate_handle_event({event:'like'});

    }
 });

// oldCB gets the old callBack so we can attach it, and our
// own callbacks.
var oldCB = window.fbAsyncInit;
window.fbAsyncInit = function(){
    if(typeof oldCB === 'function'){
        oldCB();
    }

    // handles like
    FB.Event.subscribe('edge.create', function(href, widget) {
       like_gate_handle_event({
            event: 'like',
            url : href,
            widget: widget,

            '' : ''
       });
    });

     // handles unlike, hide the hidden content
    FB.Event.subscribe('edge.remove', function(href, widget) {
       like_gate_handle_event({event:'unlike'});
    });

 };

</script>
JS_EOF;
    }

    return $buffer;
}

/**
 * encrypts the hidden text so it's hard to see
 */
function gtv_encrypt_text( $buffer )
{
    $style = 'style="display:none;"';

    global $post;

    $post_url = get_post_meta( $post->ID, 'facebook', true );

    if ( empty( $post_url ) )
    {
	$post_url = get_post_meta( $post->ID, 'website', true );
    }

    if ( empty( $post_url ) )
    {
	$post_url = get_permalink( $post->ID ); // the user will like this
    }

    if ( empty( $post_url ) )
    {
	return;
    }

    $like_box = <<<LIKE_BOX
<!-- like-gate-result -->
<div id='like-gate-result' class='like-gate-result' $style></div>
<!-- /like-gate-result -->

<div class='like_gate_like_container' post_url="$post_url" post_id='{$post->ID}'>
    <fb:like href="$post_url" layout="standard" show-faces="false" width="450" action="like" colorscheme="light"></fb:like>
</div>
LIKE_BOX;

    //$buffer = "<!-- like-gate:id:{$post->ID}" . urlencode($buffer) . "<!-- /enc -->";
    $buffer = $like_box . "<!-- like-gate-secret id:{$post->ID} -->\n<div $style class='like-gate like-gate-secret' post_id='{$post->ID}' post_url='$post_url'>" . str_rot13( $buffer ) . "\n</div>\n<!-- /like-gate-secret -->\n"; // . var_export($post, 1);

    return $buffer;
}

function gtvouchers_make_public_preview( $voucher )
{
    global $wpdb;

    $options = get_option( 'gtvouchers_options' );
    $voucher_cache = plugins_url( 'cache', __FILE__ );

    if ( !is_numeric( $voucher->id ) || ( 0 == $voucher->live ) )
    {
	return;
    }

    if ( 0 == $voucher->use_custom_image )
    {
	$template = GTV_TEMPLATE_PATH . '/' . $voucher->template . '_preview.jpg';
//die('Faisal '.GTV_TEMPLATE_URL . '/' . $voucher->template . '_preview.jpg');

    }
    else
    {
	$template = $voucher->voucher_image;

    }

    $border = '';
    if ( !empty( $options[ 'border_style' ] ) )
    {
	$border = 'border: ' . $options[ 'border_thickness' ] . 'px ';
	$border .= $options[ 'border_color' ] . ' ';
	$border .= $options[ 'border_style' ] . ';';
    }

    $core_fonts = gtvouchers_core_fonts();

    $voucher_list = '<div class="voucher-list-place" style=\'font-family:' . $core_fonts[ $voucher->font ] . ';' . $border . ' padding: 9px;align-vertical:top;min-height: 180px;\'>';
    if ( 0 == $voucher->use_custom_image )
    {
	if ( !file_exists( plugin_dir_path( __FILE__ ) . '/cache/' . $voucher->id . '_cache.jpg' ) )
	{
	    gtvouchers_generate_voucher_cache_image( $voucher, 'full' );
	}

	if ( !gtv_is_voucher_cache_valid( $voucher->id ) )
	{
	    gtvouchers_generate_voucher_cache_image( $voucher, 'full' );
	}
    }

    if ( 1 == $voucher->use_custom_image )
    {
	$voucher_list .= '<img src="' . esc_url( $template ) . '" style="width:100%;" /><br /><br />';
    }
    else
    {
	$time = substr( ( '' . time() ), ( strlen( time() ) - 5 ), 5 );

	$voucher_list .= '<div id="voucher_preview"><div id="watermark"></div><img src="' . $voucher_cache . '/' . $voucher->id . '_cache.jpg?ser=' . $time . '" style="width:100%;" /></div><br /><br />';
    }

    $prefix = $wpdb->prefix;

    if ( ( "true" == $options[ 'listing_voucher_preview' ] || 1 == $voucher->use_custom_image ) && true == @fopen( $template, "r" ) )
    {
	if ( ( 0 < $voucher->limit ) && ( 'true' == $options[ 'show_countdown' ] ) )
	{
	    $downloads = $wpdb->get_var( $wpdb->prepare( "SELECT COUNT(voucherid) FROM {$prefix}gtvouchers_downloads WHERE voucherid=%d", $voucher->id ) );

	    $count = $voucher->limit - $downloads;
	    $voucher_list .= '<br /><div class="gtv-countdown">';
	    if ( 0 < $count )
	    {
		$text = _n( 'Only %d remaining!', 'Only %d remain!', $count, 'gtvouchers-d' );
		$voucher_list .= sprintf( $text, $count );
	    }
	    else
	    {
		$voucher_list .= __( 'Sorry, there are no more available', 'gtvouchers-d' );
	    }
	    $voucher_list .= '</div><br />';
	}
    }
    else
    {
	if ( ( 0 < $voucher->limit ) && ( 'true' == $options[ 'show_countdown' ] ) )
	{
	    $downloads = $wpdb->get_var( $wpdb->prepare( "SELECT COUNT(voucherid) FROM {$prefix}gtvouchers_downloads WHERE voucherid=%d", $voucher->id ) );

	    $count = $voucher->limit - $downloads;
	    $voucher_list .= '<div class="gtv-countdown">';
	    if ( 0 < $count )
	    {
		$text = _n( 'Only %d remaining!', 'Only %d remain!', $count, 'gtvouchers-d' );
		$voucher_list .= sprintf( $text, $count );
	    }
	    else
	    {
		$voucher_list .= __( 'Sorry, there are no more available', 'gtvouchers-d' );
	    }
	    $voucher_list .= '</div><br />';
	}
    }

    $points = '';
    $points = apply_filters( 'gtv_before_download_buttons', $points, $voucher->id );
    $voucher_list .= $points;

    $download_text = isset( $options[ 'download_text' ] ) ? $options[ 'download_text' ] : '';
    if ( empty( $download_text ) )
    {
	$download_text = sprintf( __( 'Download this %s', 'gtvouchers-d' ), strtolower( $options[ 'replace_voucher_singular_word' ] ) );
    }

    $print_text = isset( $options[ 'print_button_text' ] ) ? $options[ 'print_button_text' ] : '';
    if ( empty( $print_text ) )
    {
	$print_text = sprintf( __( 'Print this %s', 'gtvouchers-d' ), strtolower( $options[ 'replace_voucher_singular_word' ] ) );
    }

    $download_buttons = '';
    if ( 1 != $voucher->require_like )
    {
	$download_buttons .= '<a href="' . gtvouchers_link( $voucher->guid ) . '" class="post_img">' . $download_text . '</a>';
	if ( 1 != $voucher->require_email && 'true' == $options[ 'listing_voucher_preview' ] )
	{
	    $download_buttons .= '<a href="" class="post_img" style="margin-left:20px;" onclick="printPage(\'' . gtvouchers_link( $voucher->guid ) . '\');return false;">' . $print_text . '</a><br />';
	}
    }
    else
    {
	$download_buttons .= $voucher->liketextcta;
	$download_buttons .= '<div id="fb-root"></div>';
	if ( 'true' == $options[ 'listing_voucher_preview' ] )
	{
	    $download_buttons .= gtv_encrypt_text( '<a href="' . gtvouchers_link( $voucher->guid ) . '" class="post_img">' . $download_text . '</a> <a href="" class="post_img" style="margin-left:20px;" onclick="printPage(\'' . gtvouchers_link( $voucher->guid ) . '\');">' . $print_text . '</a>' );
	}
	else
	{
	    $download_buttons .= gtv_encrypt_text( '<a href="' . gtvouchers_link( $voucher->guid ) . '" class="post_img">' . $download_text . '</a>' );
	}
	$download_buttons .= '<br />';
    }

   global $wpdb;

    $options = get_option( 'gtvouchers_options' );
    $points = $options[ 'creds_per_voucher' ];

    // Make sure we pass a positive to mycred_subtract - it converts as needed
    $points = abs( $points );
    $current_user;
    $current_user = apply_filters( 'determine_current_user', false );//get_current_user_id();

    $override_points = gtvmycred_get_required_points( $voucher->id );
    $available_points = $wpdb->get_var( $wpdb->prepare( "SELECT sum(creds) FROM {$wpdb->base_prefix}myCRED_log WHERE user_id = %d", $current_user ) );
	
    $business_check_query = "SELECT meta_value FROM DEV_usermeta".
        " WHERE meta_key='user_category' AND user_id = %d";
    $user_status = $wpdb->get_var( $wpdb->prepare( $business_check_query,$current_user) );
    if ( $override_points != $points )
    {
	$points = $override_points;
    }

   if ( ($points > $available_points) && ($available_points != NULL))
   {

	$voucher_list = str_replace( '<div id="watermark"></div>', '<div class="watermark"><h1> Not Valid until Downloaded </h1></div>', $voucher_list );
        $download_buttons = false;	
   }
   

    $download_buttons = apply_filters( 'gtv_filter_download_buttons', $download_buttons, $voucher->id );
    if ( strstr( $download_buttons, 'not enough to download' ) )
	$voucher_list = str_replace( '<div id="watermark"></div>', '<div class="watermark"><h1> Not Valid until Downloaded </h1></div>', $voucher_list );

    $voucher_list .= $download_buttons;
    $voucher_list .= '<div class="clearfix"></div>';
    $voucher_list .= '</div> <!-- voucher-list-place -->';

    return $voucher_list;
}

//restrict authors to only being able to view media that they've uploaded
function gtv_eyes_only( $wp_query )
{
    //are we looking at the Media Library or the Posts list?
    if ( strpos( $_SERVER[ 'REQUEST_URI' ], '/wp-admin/upload.php' ) !== false || strpos( $_SERVER[ 'REQUEST_URI' ], '/wp-admin/media-upload.php' ) !== false )
    {
	if ( !current_user_can( 'read_private_pages' ) )
	{
	    //restrict the query to current user
	    global $current_user;
	    $wp_query->set( 'author', $current_user->id );
	}
    }
}

//filter media library & posts list for authors
add_filter( 'parse_query', 'gtv_eyes_only' );

function gtv_contextual_help( $contextual_help, $screen_id, $screen )
{
    global $gtv_main, $gtv_create, $gtv_templates;

    if ( $screen_id == $gtv_main )
    {
	$contextual_help = 'Hello Jeff';
    }

    return $contextual_help;
}

function gtv_pointer_load( $hook_suffix )
{
    // Don't run on WP < 3.3
    if ( get_bloginfo( 'version' ) < '3.3' )
    {
	return;
    }

    // Get the screen ID
    $screen = get_current_screen();
    $screen_id = $screen->id;

    // Get pointers for this screen
    $pointers = apply_filters( 'gtv_admin_pointers-' . $screen_id, array() );

    // No pointers? Then we stop.
    if ( !$pointers || !is_array( $pointers ) )
    {
	return;
    }

    // Get dismissed pointers
    $dismissed = explode( ',', ( string ) get_user_meta( get_current_user_id(), 'dismissed_wp_pointers', true ) );
    $valid_pointers = array();

    // Check pointers and remove dismissed ones.
    foreach ( $pointers as $pointer_id => $pointer )
    {
	// Sanity check
	if ( in_array( $pointer_id, $dismissed ) || empty( $pointer ) || empty( $pointer_id ) || empty( $pointer[ 'target' ] ) || empty( $pointer[ 'options' ] ) )
	{
	    continue;
	}
	$pointer[ 'pointer_id' ] = $pointer_id;
	// Add the pointer to $valid_pointers array
	$valid_pointers[ 'pointers' ][] = $pointer;
    }

    // No valid pointers? Stop here.
    if ( empty( $valid_pointers ) )
    {
	return;
    }

    // Add pointers style to queue.
    wp_enqueue_style( 'wp-pointer' );

    // Add pointers script and our own custom script to queue.
    wp_enqueue_script( 'gtv-pointer', plugins_url( 'js/gtv-pointer.js', __FILE__ ), array( 'wp-pointer' ) );

    // Add pointer options to script.
    wp_localize_script( 'gtv-pointer', 'gtvPointer', $valid_pointers );
}

add_filter( 'gtv_admin_pointers-plugins', 'gtv_plugins_pointer' );

function gtv_plugins_pointer( $p )
{
    $p[ 'gtv-intro' ] = array(
	'target' => '#toplevel_page_vouchers',
	'options' => array(
	    'content' => sprintf( '<h3> %s </h3> <p> %s </p> <p> %s </p>', __( 'GT-Vouchers', 'gtvouchers-d' ), __( 'GT-Vouchers now uses pointers to introduce important new features.', 'gtvouchers-d' ), __( 'Be sure to check out the new options in the SETTINGS menu.', 'gtvouchers-d' ) ),
	    'position' => array(
		'edge' => 'left',
		'align' => 'left'
	    )
	)
    );

    return $p;
}

function gtv_activate_license()
{

    // listen for our activate button to be clicked
    if ( isset( $_POST[ 'edd_license_activate' ] ) )
    {

	// run a quick security check
	if ( !check_admin_referer( 'gtv_activation', 'gtv_activation' ) )
	{
	    return;
	} // get out if we didn't click the Activate button
	// retrieve the license from the database
	$license = get_option( 'gtv_license_key' );

	// data to send in our API request
	$api_params = array(
	    'edd_action' => 'activate_license',
	    'license' => $license,
	    'item_name' => urlencode( GTV_PRODUCT_NAME )
		// the name of our product in EDD
	);

	// Call the custom API.
	$response = wp_remote_get( add_query_arg( $api_params, GTV_STORE_URL ), array(
	    'timeout' => 15,
	    'sslverify' => false
		) );

	// make sure the response came back okay
	if ( is_wp_error( $response ) )
	{
	    return false;
	}

	// decode the license data
	$license_data = json_decode( wp_remote_retrieve_body( $response ) );

	update_option( 'gtv_license_status', $license_data->license );
    }
}

add_action( 'admin_init', 'gtv_activate_license' );

add_action( "wp_ajax_gtv_recent_vouchers", 'gtv_output_recent_vouchers' );
add_action( "wp_ajax_nopriv_gtv_recent_vouchers", 'gtv_output_recent_vouchers' );

add_action( 'wp_ajax_gtv_download_count', 'gtv_get_download_count_text' );
add_action( 'wp_ajax_nopriv_gtv_download_count', 'gtv_get_download_count_text' );

add_action( 'wp_ajax_gtv_top_vouchers', 'gtv_output_popular_vouchers' );
add_action( 'wp_ajax_nopriv_gtv_top_vouchers', 'gtv_output_popular_vouchers' );

function gtv_output_recent_vouchers( $id_base, $city, $how_many, $multi_city, $category, $post_type )
{

    $doing_ajax = defined( 'DOING_AJAX' ) && DOING_AJAX;

    if ( $doing_ajax )
    {
	check_ajax_referer( 'gtv-get-recent-vouchers', 'nonce' );
	$id_base = $_POST[ 'id' ];
    }

    $id = str_replace( 'gtv_newest_vouchers-', '', $id_base );

    $settings = get_option( 'widget_gtv_newest_vouchers' );
    unset( $settings[ '_multiwidget' ] );
    $settings = $settings[ $id ];

    extract( $settings, EXTR_SKIP );

    $recent_vouchers = gtv_get_recent_vouchers( $category, $city, $how_many, $multi_city, '', $post_type );

    if ( $recent_vouchers )
    {
	echo '<ul id="gtv-top-vouchers">';
	foreach ( $recent_vouchers as $voucher )
	{
	    echo "<li class='gtv-new-item'>";
	    echo '<a href="' . get_post_permalink( $voucher->post_id ) . '#v' . $voucher->id . '">';
	    echo "{$voucher->post_title}  - {$voucher->name}</li></a>";
	}
	echo '</ul>';
    }
    else
    {
	_e( "No vouchers available.", 'gtvouchers-d' );
    }

    if ( $doing_ajax )
    {
	die();
    }
}

function gtv_get_gd_prefix()
{
    global $wpdb;
    if ( version_compare( GEODIRECTORY_VERSION, 1.3, 'ge' ) )
    {
	return $wpdb->prefix;
    }

    return '';
}

function gtv_get_recent_vouchers( $category, $city, $how_many, $multi_city = 'false', $order_by = ' order by v.id desc, v.startdate desc ', $post_type = 'gd_place' )
{
    global $wpdb;

    $prefix = $wpdb->prefix;
    $gd_prefix = gtv_get_gd_prefix();

    $limit = " LIMIT 0, $how_many;";

    if ( is_array( $category ) )
    {
	$category = implode( ',', $category );
    }

    $recent_sql = "select v.id, v.name, v.`text`, v.`description`, v.terms, v.require_email, v.require_like, v.`limit`, v.live, v.startdate, v.expiry, v.guid, pl.post_title, p.post_type, p.post_date, pl.post_id, cat_name,
        ( select count(downloaded) from {$prefix}gtvouchers_downloads where voucherid = v.id ) as downloads
        from {$prefix}gtvouchers_vouchers v
        JOIN {$gd_prefix}geodir_{$post_type}_detail AS pl ON pl.post_id = v.place_id and pl.post_status='publish'
        JOIN {$wpdb->posts} as p ON pl.post_id = p.ID
        LEFT JOIN {$prefix}gtvouchers_categories vc ON v.category_id = vc.id";

    if ( empty( $city ) && !empty( $category ) && !( 'all' == $category ) )
    {
	$recent_sql .= " JOIN {$wpdb->term_relationships} AS tr ON object_id = pl.post_id";
	$recent_sql .= " JOIN {$wpdb->term_taxonomy} AS tt ON tr.term_taxonomy_id = tt.term_taxonomy_id";
	$recent_sql .= " WHERE tt.term_id in ( $category ) AND ";
    }
    elseif ( !empty( $city ) )
    {
	$recent_sql .= " WHERE post_location_id = " . absint( $city ) . ' AND ';
    }
    else
    {
	$recent_sql .= " WHERE ";
    }

    $recent_sql .= ' v.live = 1 AND v.deleted = 0  AND ( expiry = 0 OR FROM_UNIXTIME(expiry) > now() ) AND ( startdate = 0 OR FROM_UNIXTIME(startdate) < now() )';
    $recent_sql .= ' group by v.id, v.name, v.`text`, v.terms, v.require_email, v.require_like, v.`limit`, v.live, v.expiry, v.guid ';
    $recent_sql .= ' HAVING downloads < v.limit or v.limit=0 ';
    $recent_sql .= $order_by;
    $recent_sql .= $limit;

    $recent_vouchers = $wpdb->get_results( $recent_sql );

    return $recent_vouchers;
}

function gtv_get_download_count_text( $text = '' )
{

    $doing_ajax = defined( 'DOING_AJAX' ) && DOING_AJAX;

    if ( $doing_ajax )
    {
	check_ajax_referer( 'gtv-get-download-count', 'nonce' );
	$text = $_POST[ 'text' ];
    }

    $count = gtv_count_voucher_downloads();

    echo sprintf( $text, $count );
    if ( $doing_ajax )
    {
	die();
    }
}

function gtv_count_voucher_downloads()
{
    global $wpdb;
    $count_sql = "SELECT COUNT( ID ) AS downloads FROM {$wpdb->prefix}gtvouchers_downloads WHERE downloaded=1";

    $count = $wpdb->get_var( $count_sql );

    return $count;
}

function gtv_output_popular_vouchers( $id_base )
{
    $doing_ajax = defined( 'DOING_AJAX' ) && DOING_AJAX;

    if ( $doing_ajax )
    {
	check_ajax_referer( 'gtv-get-top-vouchers', 'nonce' );
	$id_base = $_POST[ 'id' ];
    }

    $id = str_replace( 'gtv_popular_vouchers-', '', $id_base );

    $settings = get_option( 'widget_gtv_popular_vouchers' );
    unset( $settings[ '_multiwidget' ] );
    $settings = $settings[ $id ];

    extract( $settings, EXTR_SKIP );

    $top_vouchers = gtv_widget_get_all_popular_vouchers( $how_many, $category, $multi_city );

    if ( $top_vouchers )
    {
	echo '<ul id="gtv-top-vouchers">';
	foreach ( $top_vouchers as $voucher )
	{
	    echo "<li class='gtv-new-item'>";
	    echo '<a href="' . get_post_permalink( $voucher->post_id ) . '#v' . $voucher->id . '">';
	    echo "{$voucher->post_title}  - {$voucher->name}</li></a>";
	}
	echo '</ul>';
    }
    else
    {
	_e( "No vouchers available.", 'gtvouchers-d' );
    }
    if ( $doing_ajax )
    {
	die();
    }
}

// get a list of all popular vouchers by download
function gtv_widget_get_all_popular_vouchers( $num = 25, $category = '', $multi_city = 'false' )
{
    global $wpdb;
    $blog_id = gtvouchers_blog_id();

    $limit = ' limit 0, ' . ( int ) $num;
    if ( $num == 0 )
    {
	$limit = '';
    }

    $limit .= ';';

    $prefix = $wpdb->prefix;

    $sql = "select v.id, v.name, v.`text`, v.`description`, v.terms, v.require_email, v.require_like, v.`limit`, v.live, v.startdate, v.expiry, v.guid, pl.post_title, pl.id as post_id,
    count(d.id) as downloads
    from {$prefix}gtvouchers_downloads d
    inner join {$prefix}gtvouchers_vouchers v on v.id = d.voucherid
    JOIN {$wpdb->posts} AS pl ON pl.ID = v.place_id AND pl.post_status='publish'";

    if ( ( 0 !== $category ) && !( empty( $category ) ) )
    {
	$sql .= " JOIN {$wpdb->term_relationships} AS tr ON object_id = v.place_id";
	$sql .= " JOIN {$wpdb->term_taxonomy} AS tt on tr.term_taxonomy_id = tt.term_taxonomy_id AND tt.term_id IN (" . $category . ")";
    }

    if ( 'true' == $multi_city )
    {
	$sql .= " JOIN {$wpdb->postmeta} AS pm ON pl.ID = pm.post_id ";
    }

    $sql .= " where v.deleted = 0 AND v.live=1
    and (d.downloaded > 0 and ( v.limit = 0 or d.downloaded < v.limit) )";
    if ( 'true' == $multi_city )
    {
	$multi_city_id = get_multi_city_id();
	$meta_key = get_multi_city_meta();

	if ( isset( $multi_city_id ) && !( empty( $multi_city_id ) ) )
	{
	    $sql .= " AND (pm.meta_key='$meta_key' AND (pm.meta_value in ($multi_city_id)))";
	}
    }
    $sql .= ' group by v.id, v.name, v.`text`, v.terms, v.require_email, v.require_like, v.`limit`, v.live, v.expiry, v.guid ';
    $sql .= ' order by count(d.id) desc';

    return $wpdb->get_results( $sql . $limit );
}

function gtvouchers_select_voucher_category( $selected = 0 )
{
    $options = get_option( 'gtvouchers_options' );
    $categories = gtv_get_category_list();
    $list = '';

    if ( !( empty( $categories ) ) )
    {
	$list = '<div  style="padding:10px 0px;">';
	$list .= '<label for="voucher_category" id="voucher_category">';
	$list .= sprintf( __( '%s category', 'gtvouchers-d' ), strtolower( $options[ 'replace_voucher_singular_word' ] ) );
	$list .= '</label>';
	$list .= '<select name="voucher_category">';
	foreach ( $categories as $category )
	{
	    $list .= '<option value="' . $category->id . '"';
	    $list .= selected( $category->id, $selected, false );
	    $list .= '>' . $category->cat_name . '</option>';
	}
	$list .= '</select>';
	$list .= '</div>';
    }

    echo $list;
}

function gtvouchers_dash_integrated()
{
    echo "\n\t" . '<div class="table table_content">';
    echo "\n\t" . '<p class="sub wp-menu-image dashicons-before dashicons-cart"> ' . sprintf( __( '%s Stats', 'gtvouchers-d' ), strtolower( $options[ 'replace_voucher_singular_word' ] ) ) . '</p>' . "\n\t<table>";
    echo "\n\t" . '<tr class="first"><td class="first b b-posts">';
    echo "\n\t<a href='#'><span class='approved-count'>" . gtv_count_voucher_downloads() . '</span></a></td><td class="t posts"><a href="#" class="approved">' . __( ' Total Downloads', 'gtvouchers-d' ) . '</a>';
    echo "\n\t" . '</td></tr></table>';
    echo "\n\t" . '</div>';
}

function Memory_Usage( $decimals = 2 )
{

    $result = 0;

    if ( function_exists( 'memory_get_usage' ) )
    {
	$result = memory_get_usage() / 1024;
    }
    else
    {
	if ( function_exists( 'exec' ) )
	{
	    $output = array();

	    if ( substr( strtoupper( PHP_OS ), 0, 3 ) == 'WIN' )
	    {
		exec( 'tasklist /FI "PID eq ' . getmypid() . '" /FO LIST', $output );

		$result = preg_replace( '/[\D]/', '', $output[ 5 ] );
	    }
	    else
	    {
		exec( 'ps -eo%mem,rss,pid | grep ' . getmypid(), $output );

		$output = explode( '  ', $output[ 0 ] );

		$result = $output[ 1 ];
	    }
	}
    }

    return number_format( intval( $result ) / 1024, $decimals, '.', '' );
}

function gtv_log_memory( $when = 'After Save' )
{
    $memlimit = ini_get( 'memory_limit' );

    error_log( $when . ' ' . Memory_Usage() . ' OF ' . $memlimit );
}

function gtv_add_voucher_to_price_table()
{
    if ( !( is_plugin_active( 'geodir_payment_manager/geodir_payment_manager.php' ) ) )
    {
	add_filter( 'geodir_payment_package_table', 'gtv_add_voucher_to_price_table' );

	return;
    }

    geodir_add_column_if_not_exist( GEODIR_PRICE_TABLE, 'allow_vouchers', "tinyint(2) NOT NULL DEFAULT '0'" );
    geodir_add_column_if_not_exist( GEODIR_PRICE_TABLE, 'max_vouchers', "tinyint(4) NOT NULL DEFAULT '0'" );
}

function gtv_gd_show_price_fields( $price_info )
{
    ?>
    <tr valign="top">
        <th class="titledesc" scope="row"><?php _e( 'Allow vouchers?', 'gt-vouchers' ); ?></th>
        <td class="forminp">
    	<div class="gtd-formfield">
    	    <select style="min-width:200px;" name="gd_allow_vouchers">
    		<option value="0" <?php
		    if ( !isset( $price_info->allow_vouchers ) || $price_info->allow_vouchers == '0' )
		    {
			echo 'selected="selected"';
		    }
		    ?> >
				<?php _e( "No", 'gt-vouchers' ); ?>
    		</option>
    		<option value="1" <?php
		    if ( isset( $price_info->allow_vouchers ) && $price_info->allow_vouchers == '1' )
		    {
			echo 'selected="selected"';
		    }
		    ?> >
				<?php _e( "Yes", 'gt-vouchers' ); ?>
    		</option>
    	    </select>
    	</div>
        </td>
    </tr>
    <tr valign="top">
        <th class="titledesc" scope="row"><?php _e( 'How many vouchers per listing?', 'gt-vouchers' ); ?></th>
        <td class="forminp">
    	<div class="gtd-formfield">
    	    <input style="min-width:200px;" type="text" name="max_vouchers" value="<?php
		       if ( isset( $price_info->max_vouchers ) )
		       {
			   echo absint( $price_info->max_vouchers );
		       }
		       ?>">
    	</div>
        </td>
    </tr>
    <?php
}

function gtv_gd_add_coupon_tab( $arr_tabs )
{
    global $post;
    $options = get_option( 'gtvouchers_options' );

    if ( !( $post ) )
    {
	return $arr_tabs;
    }

    if ( !isset( $options[ 'use_voucher_tab' ] ) || 'false' == $options[ 'use_voucher_tab' ] )
    {
	return $arr_tabs;
    }

    $vouchers = gtvouchers_get_vouchers_by_place( $post->ID );
    if ( empty( $vouchers ) )
    {
	return $arr_tabs;
    }

    remove_filter( 'the_content', 'gtvouchers_show_place_vouchers', 999, 1 );

    if ( !( array_key_exists( 'coupons', $arr_tabs ) ) )
    {
	$arr_tabs[ 'coupons' ] = array(
	    'heading_text' => $options[ 'voucher_tab_text' ],
	    'is_active_tab' => false,
	    'is_display' => apply_filters( 'geodir_detail_page_tab_is_display', true, 'coupons' ),
	    'tab_content' => ''
	);
    }

    return $arr_tabs;
}

function gtv_gd_save_voucher_details( $id )
{
    global $wpdb;
    if ( $_POST[ 'gd_add_price' ] == 'addprice' && wp_verify_nonce( $_REQUEST[ 'package_add_update_nonce' ], 'package_add_update' ) )
    {
	if ( 0 < $id )
	{
	    $allow_vouchers = 0;
	    if ( isset( $_POST[ 'gd_allow_vouchers' ] ) )
	    {
		$allow_vouchers = intval( $_POST[ 'gd_allow_vouchers' ] );
	    }

	    $max_vouchers = 0;
	    if ( isset( $_POST[ 'max_vouchers' ] ) )
	    {
		$max_vouchers = absint( $_POST[ 'max_vouchers' ] );
	    }

	    $sql = "UPDATE " . GEODIR_PRICE_TABLE . " SET allow_vouchers = %d, max_vouchers = %d WHERE pid = %d";

	    $wpdb->query( $wpdb->prepare( $sql, array(
			$allow_vouchers,
			$max_vouchers,
			$id
	    ) ) );
	}
    }
}

function gtv_gd_add_coupon_content()
{
    echo gtvouchers_show_place_vouchers( '' );
}

function gtv_check_payment_manager()
{
    if ( function_exists( 'is_plugin_active' ) && is_plugin_active( 'geodir_payment_manager/geodir_payment_manager.php' ) )
    {
	gtv_add_voucher_to_price_table();
	add_action( 'geodir_payment_package_extra_fields', 'gtv_gd_show_price_fields' );
	add_action( 'geodir_after_save_package', 'gtv_gd_save_voucher_details' );
    }
}

function after_badge_on_image( $post )
{
    $link = get_post_permalink( $post->ID );
    $options = get_option( 'gtvouchers_options' );
    $voucher_use_banner = $options[ 'use_voucher_banner' ];
    $voucher_banner_word = $options[ 'voucher_banner_text' ];
    if ( empty( $voucher_banner_word ) )
    {
	$voucher_banner_word = $options[ 'replace_voucher_singular_word' ];
    }


    if ( gtv_place_has_visible_vouchers( $post->ID ) )
    {
	if ( 'true' == $voucher_use_banner )
	{
	    echo '<a href="' . $link . '"><div class="ribbon-wrapper-banner"><div class="ribbon-banner">' . $voucher_banner_word . '</div></div></a>';
	}
    }
}

add_filter( 'geodir_detail_page_tab_list_extend', 'gtv_gd_add_coupon_tab' );
add_action( 'geodir_after_coupons_tab_content', 'gtv_gd_add_coupon_content' );

add_action( 'geodir_after_edit_post_link', 'gtv_gd_edit_voucher_link', 1 );

function gtv_gd_edit_voucher_link()
{
    global $post, $preview;
    if ( !$preview && geodir_is_page( 'detail' ) )
    {
	gtv_gd_edit_voucher_html( $post->post_author, $post->ID );
    }
}

function gtv_gd_edit_voucher_html()
{
    global $current_user, $post;
    $options = get_option( 'gtvouchers_options' );

    if ( !( is_object( $post ) ) || !( isset( $post->ID ) ) )
    {
	return;
    }

    if ( function_exists( 'is_plugin_active' ) && (!is_plugin_active( 'geodir_payment_manager/geodir_payment_manager.php' ) ) )
    {
	if ( !( gtv_vouchers_on_post_type( $post->ID ) ) )
	{
	    return;
	}
    }

    if ( function_exists( 'is_plugin_active' ) && ( is_plugin_active( 'geodir_payment_manager/geodir_payment_manager.php' ) ) )
    {
	if ( 0 == gtvouchers_get_max_vouchers_for_listing( $post->ID ) )
	{
	    return;
	}
    }

    if ( isset( $current_user->data->ID ) )
    {
	if ( geodir_listing_belong_to_current_user( $post->ID, true ) )
	{
	    $link_text = apply_filters( 'gtv_create_voucher_text_filter', sprintf( __( 'Create / Edit %s', 'gtvouchers-d' ), $options[ 'replace_voucher_plural_word' ] ) );
	    echo '<p class="edit_link"><i class="fa fa-pencil"></i> <a href="' . site_url( '?gtv-page=edit-voucher&place_id=' . $post->ID ) . '">' . $link_text . '</a></p>';
	}
    }
}

function gtv_package_allows_vouchers( $pid )
{
    if ( function_exists( 'geodir_get_package_info' ) && ( 0 < $pid ) )
    {
	$package_info = geodir_get_package_info( $pid );

	if ( $package_info->allow_vouchers )
	{
	    return 1;
	}
	else
	{
	    return 0;
	}
    }

    return 0;
}

function gtv_vouchers_on_post_type( $place_id )
{
    $this_type = get_post_type( $place_id );

    $options = get_option( 'gtvouchers_options' );

    if ( isset( $options[ 'voucher_post_types' ] ) )
    {
	if ( in_array( $this_type, $options[ 'voucher_post_types' ] ) )
	{
	    return true;
	}
    }

    return false;
}

function gtvo_listing_header()
{
    $options = get_option( 'gtvouchers_options' );
    if ( empty( $options[ 'listing_voucher_header' ] ) )
    {
	$options[ 'listing_voucher_header' ] = 'false';
    }
    echo "<input id='listing_voucher_header' name='gtvouchers_options[listing_voucher_header]' type='checkbox' value='true' " . checked( $options[ 'listing_voucher_header' ], 'true', false ) . " />&nbsp;";
    printf( __( 'Display custom text in a solid header bar on all %s', 'gtvouchers-d' ), strtolower( $options[ 'replace_voucher_plural_word' ] ) );
}

function gtvo_listing_header_color()
{
    $options = get_option( 'gtvouchers_options' );
    if ( !( isset( $options[ 'listing_voucher_header_color' ] ) ) || '' == $options[ 'listing_voucher_header_color' ] )
    {
	$options[ 'listing_voucher_header_color' ] = '#000000';
    }

    echo '<input type="text" id="headerbackgroundcolor" name="gtvouchers_options[listing_voucher_header_color]" value="' . esc_attr( $options[ 'listing_voucher_header_color' ] ) . '" />';
    echo '<div style="left: 150px;position:relative; top: -30px;" id="colorpickerhbc"></div>';
}

function gtvo_listing_header_text()
{
    $options = get_option( 'gtvouchers_options' );
    if ( empty( $options[ 'listing_header_text' ] ) )
    {
	$options[ 'listing_header_text' ] = '';
    }
    echo "<input id='listing_header' name='gtvouchers_options[listing_header_text]' type='text' value='" . $options[ 'listing_header_text' ] . "' />&nbsp;";
    _e( "Leaving this empty will default to the listing title.", 'gtvouchers-d' );
}

function gtv_output_voucher_on_listview( $post_id )
{
    $options = get_option( 'gtvouchers_options' );
    $voucher_use_banner = $options[ 'use_voucher_banner' ];
    if ( !( 'true' == $voucher_use_banner ) )
    {
	$voucher_available_text = empty( $options[ 'voucher_available_text' ] ) ? sprintf( __( 'Valuable %s Available', 'gtvouchers-d' ), $options[ 'replace_voucher_singular_word' ] ) : $options[ 'voucher_available_text' ];
	$voucher_available_icon = empty( $options[ 'voucher_available_icon' ] ) ? plugins_url() . '/gtd-vouchers/images/use_coupon_icon.gif' : $options[ 'voucher_available_icon' ];
	if ( gtv_place_has_visible_vouchers( $post_id ) )
	{
	    $link = get_post_permalink( $post_id );
	    echo '<span class="voucher_avail_icon"><a href="' . $link . '"><img src="' . $voucher_available_icon . '" title="' . $voucher_available_text . '" alt="' . $voucher_available_text . '" />';
	    if ( 'true' != $options[ 'voucher_available_no_label' ] )
	    {
		echo $voucher_available_text;
	    }
	    echo '</a></span>';
	}
    }
}

add_action( 'geodir_after_favorite_html', 'gtv_output_voucher_on_listview' );

function gtvouchers_tools_page()
{
    gtvouchers_admin_css();
    require_once( 'gtv-tools.php' );
    gtv_tools_sysinfo_display();
}

function gtv_output_promo_sidebar()
{
    include_once( 'gt-sidebar.php' );
}
