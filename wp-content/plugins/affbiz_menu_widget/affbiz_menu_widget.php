<?php
/*
Plugin Name: affbiz_menu
Plugin URI: http://localhost/
Description: A pugin to show menu on pages.
Version: 1
Author: Usman Ashraf
Author URI: http://localhost/
*/

class affbiz_menu_widget extends WP_Widget {
     
    function __construct() {
    parent::__construct(false, $name=__('affbiz_menu'));
    
    }
     
    // Creating widget front-end
    // This is where the action happens
    public function widget( $args, $instance ) 
    {
        session_start();

        //echo "<pre>";
        //print_r($_SESSION);
        //echo session_id();
        //echo "</pre>";
        static $query_args;
        global $wpdb;
        global $current_user;
        //get_currentuserinfo();
        //echo $current_user->user_login;
        //the_author();
        //echo get_the_author_meta( 'ID' );
        //echo $current_user->ID."<br>";
        //echo $_SESSION['id'];

    if($_SESSION['id'] == 0)
    {
        //not logged in currently!
        //$output = "<h2>Please Log in!</h2>";
        //return $output;
        //echo "test123";
    }
    else
    {
        echo "<style>.side-img img {
                width: 60px;
                float: left;
                margin-right: 13px;
            }
            .side-img span {
                float: left;
                width: 202px;
            }
            .side-img a {
                font-weight: bold;
            }
            .side-img ul {
                float: right;
                width: 202px;
            }
            .side-img ul li {
                margin-bottom: 0px;
            }
            .link_all{
                position:fixed;
                bottom:10px;
                right:0px;
                height:20px;
                width:250px;
                background-color:#cf8;
                } 
            .side-img ul li a {
                font-weight: normal;
                color: #fff;
                background: #DC2F21;
                float: left;
                text-overflow: ellipsis;
                white-space: nowrap;
                overflow: hidden;
                margin-right: 6px;
                margin-bottom: 10px;
                padding: 2px 7px;
                max-width: 92px;
            }</style>";
        $table_users = $wpdb->prefix . 'users';
        $table_posts = $wpdb->prefix . 'posts';
        $table_postmeta = $wpdb->prefix . 'postmeta';
        $table_usermeta = $wpdb->prefix . 'usermeta';
        $table_affiliates = $wpdb->prefix . 'uap_affiliates';
        $table_mlm = $wpdb->prefix . 'uap_mlm_relations';
        $table_uap_cp = $wpdb->prefix . 'uap_coupons_code_affiliates';
        $table_mc_cp = $wpdb->prefix . 'myCRED_log';

        $cu_id=$_SESSION['id'];

        $business_check_query = "SELECT meta_value FROM ".$table_usermeta.
        " WHERE meta_key='user_category' && user_id='$cu_id'";
        $business_check = $wpdb->get_row($business_check_query,ARRAY_N);

        if($business_check[0] == 'bus')
        {
            echo "<section class='widget'>";
            echo "<div class='geodir_list_heading clearfix'>";
            echo "<h2 class='widget-title'>Business Menu</h2>";
            echo "</div>";
            echo "<ul>";
            echo "<li><a href='".site_url()."/view-all/'>Affiliates</a></li>";
            echo "<li><a href='".site_url()."/view-all-coupons/'>Promo Codes</a></li>";
            echo "<li><a href='".site_url()."/code-authentication/'>Authenticate Code</a></li>";
            echo "<li><a href='".site_url()."/view-log/'>Points Log</a></li>";
            echo "<li><a href='".site_url()."/see-vouchers/'>Vouchers</a></li>";
            echo "</ul>";
            echo "</section>";
        }
        else if($business_check[0] == 'ind')
        {
            echo "<section class='widget'>";
            echo "<div class='geodir_list_heading clearfix'>";
            echo "<h2 class='widget-title'>Affiliate Menu</h2>";
            echo "</div>";
            echo "<ul>";
            echo "<li><a href='".site_url()."/view-all-codes/'>Promo Codes</a></li>";
            echo "<li><a href='".site_url()."/view-log/'>Points Log</a></li>";
            echo "<li><a href='".site_url()."/use-incentive/'>Incentives</a></li>";
            echo "</ul>";
            echo "</section>";
        }

    }
}
    

// Widget Backend
    public function form( $instance ) {
    
    }
         
    // Updating widget replacing old instances with new
    public function update( $new_instance, $old_instance ) {
    
    }
    } // Class wpb_widget ends here
// Register and load the widget
    function wpb_load_widget_affbiz_menu() {
        register_widget( 'affbiz_menu_widget' );
    }
    add_action( 'widgets_init', 'wpb_load_widget_affbiz_menu' );

    ?>