v1.1.2
Added check if BuddyPress is active - ADDED

v1.1.1
Reviews integration not working without multi rating addon - FIXED

v1.1.0
Fixed conflicts between bp profile reviews and woocommerce comments - FIXED
Expire date not displayed on bp user dashboard listings page - FIXED
Filter added to modify review avatar size - ADDED

v1.0.9
Buddypress activity tracking having a problem for a reviews - FIXED
New update licence system implemented - CHANGED
Tabs labels are not translated due to missing gdbuddypress textdomain - FIXED

v1.0.8
Comments on listing activity should not taken as a listing reviews - FIXED
case conversion functions replaced with custom function to support unicode languages - CHANGED

v1.0.7
New login page now compatible with buddypress registration form for redirect option - CHANGED
Option added to link blog author page to buddypress profile page - ADDED

v1.0.6
GD auto update script improved efficiency and security(https) - CHANGED
Changed textdomain from defined constant to a string - CHANGED
Show featured image in activity for CPT does not work - FIXED

v1.0.5
Option added to show featured image in activity for new listing submitted - ADDED
Load plugin files only when BuddyPress active - ADDED
Changes made for WPML compatibility - CHANGED

v1.0.4
Some PHP notices resolved - FIXED

v1.0.3
Option for login redirect added - ADDED
Filter added for favourite text 'gdbuddypress_favourites_text' - ADDED
Docblocks - ADDED

v1.0.2
Checked for XSS vulnerabilities as per latest WP security update for add_query_arg(), vulnerability found if using category sort_by options - SECURITY UPDATE


v1.0.1
Buddypress profile menu not translated - FIXED

v1.0.0
Initial release
