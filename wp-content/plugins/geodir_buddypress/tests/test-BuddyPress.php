<?php
class BuddyPressTest extends BP_UnitTestCase
{
    public function setUp()
    {
        parent::setUp();

        wp_set_current_user(1);


        $ptypes = array();
        $gd_ptypes = geodir_buddypress_posttypes();
        foreach ($gd_ptypes as $key => $value) {
            $ptypes[] = $key;
        }
        update_option('geodir_buddypress_tab_listing', $ptypes);
        update_option('geodir_buddypress_tab_review', $ptypes);
        update_option('geodir_buddypress_activity_listing', $ptypes);
        update_option('geodir_buddypress_activity_review', $ptypes);
        update_option('geodir_buddypress_link_listing', '1');
        update_option('geodir_buddypress_link_favorite', '1');
        update_option('geodir_buddypress_link_author', '1');
        update_option('geodir_buddypress_bp_register', '1');
    }

    public function testInstall() {
        update_option('geodir_installed', 1);
        geodir_buddypress_activation();
    }

    public function testBuddyPressFavoriteLink()
    {
        global $current_user;
        $author_link = get_author_posts_url($current_user->ID);
        $key = 'gd_place';

        $listing_link = geodir_getlink($author_link, array('stype' => $key, 'list' => 'favourite'), false);
        $listing_link = apply_filters('geodir_dashboard_link_favorite_listing', $listing_link, $key, $current_user->ID);

        $this->assertContains('admin/favorites/places', $listing_link);
    }

    public function texstBuddyPressListingLink()
    {
        global $current_user;
        $author_link = get_author_posts_url($current_user->ID);
        $key = 'gd_place';

        $listing_link = geodir_getlink($author_link, array('stype' => $key), false);
        $listing_link = apply_filters('geodir_dashboard_link_my_listing', $listing_link, $key, $current_user->ID);

        $this->assertContains('admin/listings/places', $listing_link);
    }

    public function texstBuddyPressAuthorLink() {
        global $current_user;
        $url = get_author_posts_url($current_user->ID);
        $this->assertContains('admin/listings/places', $url);
    }

    public function testUninstall() {

        geodir_buddypress_uninstall();

    }

    public function testTabsArray() {
        $tabs = array();

        $output = geodir_buddypress_tabs_array($tabs);

        $this->assertTrue($output['geodir_buddypress']['label'] == 'BuddyPress Integration');

    }

    public function testBPReviewsTab() {

        $time = current_time('mysql');

        $args = array(
            'listing_type' => 'gd_place',
            'post_title' => 'Test Listing Title',
            'post_desc' => 'Test Desc',
            'post_tags' => 'test1,test2',
            'post_address' => 'New York City Hall',
            'post_zip' => '10007',
            'post_latitude' => '40.7127837',
            'post_longitude' => '-74.00594130000002',
            'post_mapview' => 'ROADMAP',
            'post_mapzoom' => '10',
            'geodir_timing' => '10.00 am to 6 pm every day',
            'geodir_contact' => '1234567890',
            'geodir_email' => 'test@test.com',
            'geodir_website' => 'http://test.com',
            'geodir_twitter' => 'http://twitter.com/test',
            'geodir_facebook' => 'http://facebook.com/test',
            'geodir_special_offers' => 'Test offer'
        );
        $post_id = geodir_save_listing($args, true);

        $data = array(
            'comment_post_ID' => $post_id,
            'comment_author' => 'admin',
            'comment_author_email' => 'admin@admin.com',
            'comment_author_url' => 'http://wpgeodirectory.com',
            'comment_content' => 'content here',
            'comment_type' => '',
            'comment_parent' => 0,
            'user_id' => 1,
            'comment_author_IP' => '127.0.0.1',
            'comment_agent' => 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.0.10) Gecko/2009042316 Firefox/3.0.10 (.NET CLR 3.5.30729)',
            'comment_date' => $time,
            'comment_approved' => 1,
        );

        $comment_id = wp_insert_comment($data);

        $_REQUEST['geodir_overallrating'] = 5.0;
        geodir_save_rating($comment_id);

        $this->assertTrue(is_int($comment_id));

        update_option( 'geodir_buddypress_tab_review', array('gd_place') );
        add_filter('bp_current_action', 'bp_current_action_places_filter');
        geodir_buddypress_screen_reviews();

        ob_start();
        geodir_buddypress_reviews_title();
        $output = ob_get_contents();
        ob_end_clean();
        $this->assertContains('Reviews on Places', $output);

        ob_start();
        geodir_buddypress_reviews_content();
        $output = ob_get_contents();
        ob_end_clean();
        $this->assertContains('Overall Rating', $output);


        remove_filter('bp_current_action', 'bp_current_action_places_filter');
    }

    public function testBPListingsTab() {

        $time = current_time('mysql');

        $args = array(
            'listing_type' => 'gd_place',
            'post_title' => 'Test Listing Title',
            'post_desc' => 'Test Desc',
            'post_tags' => 'test1,test2',
            'post_address' => 'New York City Hall',
            'post_zip' => '10007',
            'post_latitude' => '40.7127837',
            'post_longitude' => '-74.00594130000002',
            'post_mapview' => 'ROADMAP',
            'post_mapzoom' => '10',
            'geodir_timing' => '10.00 am to 6 pm every day',
            'geodir_contact' => '1234567890',
            'geodir_email' => 'test@test.com',
            'geodir_website' => 'http://test.com',
            'geodir_twitter' => 'http://twitter.com/test',
            'geodir_facebook' => 'http://facebook.com/test',
            'geodir_special_offers' => 'Test offer'
        );
        $post_id = geodir_save_listing($args, true);

        $data = array(
            'comment_post_ID' => $post_id,
            'comment_author' => 'admin',
            'comment_author_email' => 'admin@admin.com',
            'comment_author_url' => 'http://wpgeodirectory.com',
            'comment_content' => 'content here',
            'comment_type' => '',
            'comment_parent' => 0,
            'user_id' => 1,
            'comment_author_IP' => '127.0.0.1',
            'comment_agent' => 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.0.10) Gecko/2009042316 Firefox/3.0.10 (.NET CLR 3.5.30729)',
            'comment_date' => $time,
            'comment_approved' => 1,
        );

        $comment_id = wp_insert_comment($data);

        $_REQUEST['geodir_overallrating'] = 5.0;
        geodir_save_rating($comment_id);

        $this->assertTrue(is_int($comment_id));

        update_option( 'geodir_buddypress_tab_listing', array('gd_place') );
        add_filter('bp_current_action', 'bp_current_action_places_filter');
        geodir_buddypress_screen_listings();

        ob_start();
        geodir_buddypress_listings_title();
        $output = ob_get_contents();
        ob_end_clean();
        $this->assertContains('Places', $output);

        ob_start();
        geodir_buddypress_listings_content();
        $output = ob_get_contents();
        ob_end_clean();
        $this->assertContains('gdbp-listings-page', $output);


        remove_filter('bp_current_action', 'bp_current_action_places_filter');
    }

    public function testAdminOptions() {

        ob_start();
        $_REQUEST['subtab'] = 'gdbuddypress_settings';
        geodir_buddypress_option_form('');
        $output = ob_get_contents();
        ob_end_clean();
        $this->assertContains('gdbuddypress_settings', $output);

    }

    public function testAuthorLink() {
        global $current_user;
        $author_link = get_author_posts_url($current_user->ID);
        $default_author_link = apply_filters('geodir_dashboard_author_link', $author_link, $current_user->ID);
        $this->assertContains('members', $default_author_link);
    }

    public function testBPNav() {
        geodir_buddypress_setup_nav();
    }

    public function testLoadProfile() {
        global $current_user;
        $user_id = $current_user->ID;
        ob_start();
        $url = bp_core_get_user_domain( $user_id );
        $this->go_to( $url );
        $this->load_template();
        $output = ob_get_contents();
        ob_end_clean();
        $this->assertContains('admin', $output);
    }

    public function testDashboardLinks() {

        ob_start();
        the_widget( 'geodir_loginwidget' );
        $output = ob_get_contents();
        ob_end_clean();
        $this->assertContains('Account Settings', $output);


    }

    private function load_template() {
        do_action( 'template_redirect' );
        $template = false;
        if	 ( is_404()			&& $template = get_404_template()			) :
        elseif ( is_search()		 && $template = get_search_template()		 ) :
        elseif ( is_front_page()	 && $template = get_front_page_template()	 ) :
        elseif ( is_home()		   && $template = get_home_template()		   ) :
        elseif ( is_post_type_archive() && $template = get_post_type_archive_template() ) :
        elseif ( is_tax()			&& $template = get_taxonomy_template()	   ) :
        elseif ( is_attachment()	 && $template = get_attachment_template()	 ) :
            remove_filter('the_content', 'prepend_attachment');
        elseif ( is_single()		 && $template = get_single_template()		 ) :
        elseif ( is_page()		   && $template = get_page_template()		   ) :
        elseif ( is_category()	   && $template = get_category_template()	   ) :
        elseif ( is_tag()			&& $template = get_tag_template()			) :
        elseif ( is_author()		 && $template = get_author_template()		 ) :
        elseif ( is_date()		   && $template = get_date_template()		   ) :
        elseif ( is_archive()		&& $template = get_archive_template()		) :
        elseif ( is_paged()		  && $template = get_paged_template()		  ) :
        else :
            $template = get_index_template();
        endif;
        /**
         * Filter the path of the current template before including it.
         *
         * @since 3.0.0
         *
         * @param string $template The path of the template to include.
         */
        if ( $template = apply_filters( 'template_include', $template ) ) {
            $template_contents = file_get_contents( $template );
            $included_header = $included_footer = false;
            if ( false !== stripos( $template_contents, 'get_header();' ) ) {
                do_action( 'get_header', null );
                locate_template( 'header.php', true, false );
                $included_header = true;
            }
            include( $template );
            if ( false !== stripos( $template_contents, 'get_footer();' ) ) {
                do_action( 'get_footer', null );
                locate_template( 'footer.php', true, false );
                $included_footer = true;
            }
            if ( $included_header && $included_footer ) {
                global $wp_scripts;
                $wp_scripts->done = array();
            }
        }
        return;
    }

    public static function setPermalinkStructure( $struc = '/%postname%/' ) {
        global $wp_rewrite;
        $wp_rewrite->set_permalink_structure( $struc );
        $wp_rewrite->flush_rules();
        update_option( 'permalink_structure', $struc );
        flush_rewrite_rules( true );
    }

    function the_widget_form( $widget, $instance = array() ) {
        global $wp_widget_factory;

        $widget_obj = $wp_widget_factory->widgets[$widget];
        if ( ! ( $widget_obj instanceof WP_Widget ) ) {
            return;
        }

        $widget_obj->_set(-1);
        $widget_obj->form($instance);
    }

    function the_widget_form_update( $widget, $new_instance = array(), $old_instance = array() ) {
        global $wp_widget_factory;

        $widget_obj = $wp_widget_factory->widgets[$widget];
        if ( ! ( $widget_obj instanceof WP_Widget ) ) {
            return;
        }

        $widget_obj->_set(-1);
        return $widget_obj->update($new_instance, $old_instance);
    }



    public function tearDown()
    {
        parent::tearDown();
    }
}
?>