<?php

/*
Plugin Name: View Admin Stats
Plugin URI:  http://localhost/
Description: Plugin made for website "Circleofbiz" to view Admin Stats
Version:     1.0
Author:      Usman Ashraf
Author URI:  http://localhost/
License:     GPL2
License URI: http://localhost/
Text Domain: wporg
Domain Path: /languages
*/

/** Step 2 (from text above). */
add_action( 'admin_menu', 'my_plugin_menu1' );

/** Step 1. */
function my_plugin_menu1() {
	add_options_page( 'My Plugin Options', 'View Admin Stats', 'manage_options', 'my-unique-identifier', 'my_plugin_options1' );
}

/** Step 3. */
function my_plugin_options1() {
	if ( !current_user_can( 'manage_options' ) )  {
		wp_die( __( 'You do not have sufficient permissions to access this page.' ) );
	}
      
	$html = '<ul class="nav nav-tabs">
  <li class="active">
    <a href="/wp-admin/options-general.php?page=my-unique-identifier&action=1" style="float:left;margin-left:10px">View Bussinesses Stats</a>
  </li>
  <li>
    <a href="/wp-admin/options-general.php?page=my-unique-identifier&action=2" style="float:left;margin-left:10px">View Bussiness Affiliates</a>
  </li>
</ul>';
        echo $html;
      $action = $_GET['action'];
    switch ($action) {
        case 1:
           fetchBussinessStats();
            break;
        case 2:
           viewAffliates();
            break;
    }
    
    

}

function fetchBussinessStats()
{
     
         
         
       //DB Tables
         global $wpdb;
        $table_users = $wpdb->prefix . 'users';
        $table_posts = $wpdb->prefix . 'posts';
        $table_postmeta = $wpdb->prefix . 'postmeta';
        $table_usermeta = $wpdb->prefix . 'usermeta';
        $table_affiliates = $wpdb->prefix . 'uap_affiliates';
        $table_mlm = $wpdb->prefix . 'uap_mlm_relations';
        $table_uap_cp = $wpdb->prefix . 'uap_coupons_code_affiliates';
        $table_mc_cp = $wpdb->prefix . 'myCRED_log';
        $table_voucher = $wpdb->prefix . 'gtvouchers_vouchers';
        $table_downloaded_vouchers = $wpdb->prefix . 'gtvouchers_downloads';
        
        $business_check_query = "SELECT * FROM ".$table_usermeta.
        " um , ".$table_users. " u WHERE um.meta_key='user_category' AND um.meta_value='bus' AND u.id = um.user_id";
        $business_check = $wpdb->get_results($business_check_query,ARRAY_N);
        
        
         $No_business_check_query = "SELECT count(user_id) FROM ".$table_usermeta.
        " um , ".$table_users." u WHERE um.meta_key='user_category' AND um.meta_value='bus' AND u.id = um.user_id";
        $No_business_check = $wpdb->get_results($No_business_check_query,ARRAY_N);
        
        
        $No_mc_coupon_query = "SELECT count(p.ID) FROM ".$table_posts.
        " p , ".$table_usermeta." um , " .$table_users. " u WHERE  um.meta_key='user_category' AND um.meta_value='bus' AND p.post_author = um.user_id and um.user_id = u.id and p.post_type = 'mycred_coupon'";
        $No_mc_coupons = $wpdb->get_results($No_mc_coupon_query,ARRAY_N);

        $No_voucher_id_query = "SELECT count(v.id)  FROM ".$table_voucher.
        "  v , ".$table_usermeta." um , ".$table_users." u WHERE um.meta_key='user_category' AND um.meta_value='bus' AND u.id=um.user_id AND v.author_id = u.id ";
        $No_voucher_count = $wpdb->get_results($No_voucher_id_query,ARRAY_N);
      
        $total_coupon_redeemed_query = "SELECT count(ref_id) FROM ". $table_mc_cp . " where entry = 'Coupon redemption'";
        $total_vouchers_downloaded_query = "SELECT count(voucherid) FROM ".$table_downloaded_vouchers . " where downloaded = '1'";
        $total_vouchers_redeemed_query = "SELECT count(voucherid) FROM ".$table_downloaded_vouchers . " where redeemed = '1'";
        
        $total_redeemed_coupons = $wpdb->get_results($total_coupon_redeemed_query,ARRAY_N);
        $total_downloded_vouchers = $wpdb->get_results($total_vouchers_downloaded_query,ARRAY_N);
        $total_redeemed_vouchers = $wpdb->get_results($total_vouchers_redeemed_query,ARRAY_N);
        
        $output = '<br><table border = "1" style="margin-top:40px;width:680px">
    <thead>
        <tr>
            <th>Total No of Business</th>
            <th>Total No of coupons created</th>
            <th>No of Times Coupons Redeemed</th>
            <th>Total No of vouchers created</th>            
            <th>No of Times vouchers Downloaded</th> 
            <th>No of Times vouchers Redeemed</th>
            
        </tr>
    </thead>
    <tbody> <tr>
                <td>'.$No_business_check[0][0].'</td>
                <td>'.$No_mc_coupons[0][0].'</td>
                <td>'.$total_redeemed_coupons[0][0].'</td>
                <td>'.$No_voucher_count[0][0].'</td>
                <td>'.$total_downloded_vouchers[0][0].'</td>
                <td>'.$total_redeemed_vouchers[0][0].'</td>
                
            </tr>';
        
        echo $output;
        
        
      $html = '<table border = "1" style="margin-top:30px">
    <thead>
        <tr>
            <th>Bussiness Name</th>
            <th>No of coupons created by business</th>
            <th>No of vouchers created by business</th>
            <th>No of Affliates</th>
        </tr>
    </thead>
    <tbody>';
        
        if (count($business_check)> 0)
         {
            foreach($business_check as $user)
            {
                
                $userId = $user[1];
                $userQuery = "SELECT display_name FROM ".$table_users . " where ID='$userId'";
                $userName = $wpdb->get_row($userQuery,ARRAY_N);
                
                $mc_coupon_query = "SELECT count(ID) FROM ".$table_posts.
                " WHERE post_author = '$userId' AND post_type = 'mycred_coupon'";
                $mc_coupons = $wpdb->get_results($mc_coupon_query,ARRAY_N);
                $noOfCoupons = $mc_coupons[0];
                
                 $voucher_id_query = "SELECT count(id) FROM ".$table_voucher.
                 " WHERE author_id='$userId'";
                 $voucher_count = $wpdb->get_results($voucher_id_query,ARRAY_N);
                 $noOfVouchers = $voucher_count[0];
                 
                $p_aff_id=$wpdb->get_row("SELECT id FROM " .$table_affiliates.
                 " WHERE uid = '$userId' ",ARRAY_N);
                $c_aff_id=$wpdb->get_results("SELECT count(affiliate_id) FROM " .$table_mlm.
                " WHERE parent_affiliate_id = '$p_aff_id[0]' ",ARRAY_N);
                 $noOfAffiliates=$c_aff_id[0];
                if($userName[0] != '')
                {
                    $html .= '<tr>

                        <td>'.$userName[0].'</td>
                        <td>'.$noOfCoupons[0].'</td>
                        <td>'.$noOfVouchers[0].'</td>
                        <td>'.$noOfAffiliates[0].'</td>

                    </tr>';
                }
            }
       
        }
        
    $html .= '</tbody>
</table>';
         
         echo $html;
}

function viewAffliates()
{
    //DB Tables
    global $wpdb;
    $table_users = $wpdb->prefix . 'users';
    $table_posts = $wpdb->prefix . 'posts';
    $table_postmeta = $wpdb->prefix . 'postmeta';
    $table_usermeta = $wpdb->prefix . 'usermeta';
    $table_affiliates = $wpdb->prefix . 'uap_affiliates';
    $table_mlm = $wpdb->prefix . 'uap_mlm_relations';
    $table_uap_cp = $wpdb->prefix . 'uap_coupons_code_affiliates';
    $table_mc_cp = $wpdb->prefix . 'myCRED_log';
    $table_voucher = $wpdb->prefix . 'gtvouchers_vouchers';
    $table_downloaded_vouchers = $wpdb->prefix . 'gtvouchers_downloads';
    
    
    $business_check_query = "SELECT * FROM ".$table_usermeta.
    " WHERE meta_key='user_category' AND meta_value='bus'";
    $business_check = $wpdb->get_results($business_check_query,ARRAY_N);
    
    
    $html = '<br><table border = "1" style="margin-top:40px">
    <thead>
        <tr>
            <th>Bussiness Name</th>
            <th>Bussiness Affiliates</th>
            <th>No of Affiliates</th>
            
        </tr>
    </thead>
    <tbody>';
    
    if (count($business_check)> 0)
         {
            foreach($business_check as $user)
            {
                
                $userId = $user[1];
                $userQuery = "SELECT display_name FROM ".$table_users . " where ID='$userId'";
                $userName = $wpdb->get_row($userQuery,ARRAY_N);
                 
                 
                if($userName[0] != '')
                {
                    $html .= '<tr>

                        <td>'.$userName[0].'</td>';
                        

                        $p_aff_id=$wpdb->get_row("SELECT id FROM " .$table_affiliates.
                         " WHERE uid = '$userId' ",ARRAY_N);
                        $c_aff_id=$wpdb->get_results("SELECT affiliate_id FROM " .$table_mlm.
                        " WHERE parent_affiliate_id = '$p_aff_id[0]' ",ARRAY_N);
                        
                        $countAffiliate=$wpdb->get_results("SELECT count(affiliate_id) as count FROM " .$table_mlm.
                        " WHERE parent_affiliate_id = '$p_aff_id[0]' ",ARRAY_N);
                                $affliate_string = '';
                        if($c_aff_id [0] == '')
                        {
                                $affliate_string = '';
                        }
                        else
                        {
                            $i=0;
                            foreach($c_aff_id as $child)
                            {
                               
                                $c_uid=$wpdb->get_row("SELECT uid FROM " .$table_affiliates.
                                " WHERE id = '$child[0]' ",ARRAY_N);
                                $affiliate=$wpdb->get_row("SELECT display_name FROM " .$table_users. " WHERE ID = '$c_uid[0]'",ARRAY_N);
                                $affliate_string .= "<a href='https://circleofbiz.com/members/".$affiliate[0]."' target='_blank' id='".$i."'>" .$affiliate[0]. "</a>" . " , ";
                                   $i++; 
                                
                            }
                            
                            
                        }
                       
                    $html.= '<td>'.$affliate_string.'</td>
                             <td>'.$countAffiliate[0][0].'</td>

                            </tr>';
                }
            }
       
        }
        
    $html .= 
     '</tbody>
</table>';
         
    echo $html;
    
    
}
?>