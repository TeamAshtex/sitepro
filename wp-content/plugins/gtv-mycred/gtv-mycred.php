<?php
session_start();
$_SESSION[ 'counter' ] = 0;
/**
 * @package GT-Vouchers myCRED integration
 * @author Jeff Rose
 * @version 1.2
 */
/*
  Plugin Name: GT-Vouchers myCRED
  Plugin URI: http://www.InstalledForYou.com/gtv-mycred/
  Description: Integrate MyCred gamification - deduct creds from registered users when downloading vouchers
  Author: Jeff Rose
  Version: 1.2
  Author URI: http://www.InstalledForYou.com/
 */

require 'plugin-updates/plugin-update-checker.php';
$MyUpdateChecker = PucFactory::buildUpdateChecker( 'http://www.installedforyou.com/updates/?action=get_metadata&slug=gtv-mycred', __FILE__ );

add_action( 'init', 'gtvmycred_prepare' );

function gtvmycred_prepare()
{
    gtvmycred_activate();

    $domain = 'gtv-mycred';
    // The "plugin_locale" filter is also used in load_plugin_textdomain()
    $locale = apply_filters( 'plugin_locale', get_locale(), $domain );

    load_textdomain( $domain, WP_LANG_DIR . '/gtd-vouchers/' . $domain . '-' . $locale . '.mo' );
    load_plugin_textdomain( $domain, false, dirname( plugin_basename( __FILE__ ) ) . '/languages/' );
}

register_activation_hook( __FILE__, "gtvmycred_activation" );

function gtvmycred_activation()
{
    global $wpdb;
    $prefix = $wpdb->prefix;
    geodir_add_column_if_not_exist( $prefix . 'gtvouchers_vouchers', 'cred_to_download', $column_attr = "MEDIUMINT NOT NULL DEFAULT 0" );
}

function gtvmycred_activate()
{
    if ( !( did_action( 'gtv_loaded' ) ) )
    {
	// Check our custom GT Vouchers action
	add_action( 'admin_notices', 'gtvmycred_admin_notice_gtv_missing' );
    }

    if ( !( did_action( 'mycred_init' ) ) )
    {
	// Check for MyCred's init action
	add_action( 'admin_notices', 'gtvmycred_admin_notice_mycred_missing' );
    }
}

function gtvmycred_admin_notice_gtv_missing()
{
    ?>
    <div class="error">
        <p><?php _e( 'GTV myCRED is active but GT-Vouchers is not.', 'gtv-mycred' ); ?></p>
    </div>
    <?php
}

function gtvmycred_admin_notice_mycred_missing()
{
    ?>
    <div class="error">
        <p><?php _e( 'GTV myCRED is active but REQUIRES myCRED to work.', 'gtv-mycred' ); ?></p>
    </div>
    <?php
}

//add_action( 'gtv_after_voucher_settings', 'gtvmycred_display_settings' );
function gtvmycred_display_settings()
{
    $options = get_option( 'gtvouchers_options' );
    $singular = $options[ 'replace_voucher_singular_word' ];

    $mycred = mycred();

    add_settings_field( 'creds_per_vouchers', sprintf( __( 'Number of %s to download each %s.', 'gtv-mycred' ), $mycred->name[ 'plural' ], $singular ), 'gtvmycred_creds_base', 'gtvo-mycred', 'gtvo-mycred' );
    add_settings_field( 'creds_override_per', sprintf( __( 'Settings per %s override this base.', 'gtv-mycred' ), $singular ), 'gtvmycred_creds_override', 'gtvo-mycred', 'gtvo-mycred' );
    add_settings_field( 'creds_required_points', sprintf( __( 'Tell the user how many %s they need.', 'gtv-mycred' ), $mycred->name[ 'plural' ] ), 'gtvmycred_creds_required_points_text', 'gtvo-mycred', 'gtvo-mycred' );
    add_settings_field( 'creds_not_enough_points', sprintf( __( 'Message to show the user if they need more %s.', 'gtv-mycred' ), $mycred->name[ 'plural' ] ), 'gtvmycred_creds_not_enough_text', 'gtvo-mycred', 'gtvo-mycred' );
}

function gtvmycred_creds_base()
{
    $options = get_option( 'gtvouchers_options' );

    echo '<input type="text" name="gtvouchers_options[creds_per_voucher]" value="';
    if ( isset( $options[ 'creds_per_voucher' ] ) )
    {
	echo $options[ 'creds_per_voucher' ];
    }
    echo '" style="margin-right:10px;width:35px;" />';
    //_e( 'You can override this per voucher.', 'gtv-mycred' );
}

function gtvmycred_creds_required_points_text()
{
    $options = get_option( 'gtvouchers_options' );
    $mycred = mycred();

    if ( !( isset( $options[ 'creds_required_points' ] ) ) )
    {
	$options[ 'creds_required_points' ] = '';
    }

    echo '<input type="hidden" name="tab" value="mycred_settings" />';
    echo '<textarea name="gtvouchers_options[creds_required_points]" id="creds-required_points" style="width:400px;height:55px;" >';
    echo esc_textarea( $options[ 'creds_required_points' ] );
    echo '</textarea><br />';
    printf( __( 'Enter <strong>&#37;s</strong> to display the number of %s required (formatted to your settings).<br /> eg: <em>"This coupon will cost &#37;s %s to download."</em>', 'gtv-mycred' ), $mycred->name[ 'plural' ], $mycred->name[ 'plural' ] );
    echo '<br />';
    printf( __( 'Could become <em>"This coupon will cost 15 %s to download."</em>', 'gtv-mycred' ), $mycred->name[ 'plural' ] );
}

function gtvmycred_creds_not_enough_text()
{
    $options = get_option( 'gtvouchers_options' );
    $mycred = mycred();

    if ( !( isset( $options[ 'creds_not_enough_points' ] ) ) )
    {
	$options[ 'creds_not_enough_points' ] = '';
    }

    echo '<textarea name="gtvouchers_options[creds_not_enough_points]" id="creds-not-enough" style="width:400px;height:85px;" >';
    echo esc_textarea( $options[ 'creds_not_enough_points' ] );
    echo '</textarea><br />';
    printf( __( 'Enter <strong>&#37;s</strong> to display the users current %s balance (formatted to your settings).<br /> eg: <em>"Your current balance of &#37;s %s is not enough to download this voucher."</em>', 'gtv-mycred' ), $mycred->name[ 'plural' ], $mycred->name[ 'plural' ] );
    echo '<br />';
    printf( __( 'Could become <em>"Your current balance of 37 %s is not enough to download this voucher."</em>', 'gtv-mycred' ), $mycred->name[ 'plural' ] );
    echo '<br />';
    _e( 'If you use the PayPal Top-up module, you can paste your "buy" form in here.', 'gtv-mycred' );
}

function gtvmycred_creds_override()
{
    $options = get_option( 'gtvouchers_options' );
    $mycred = mycred();

    if ( !( isset( $options[ 'creds_override_per' ] ) ) )
    {
	$options[ 'creds_override_per' ] = false;
    }

    echo '<input type="checkbox" name="gtvouchers_options[creds_override_per]" id="creds-override-per"' . checked( $options[ 'creds_override_per' ], 'true', false ) . ' value="true" /> ';
    printf( __( 'Allow each %s to require a different number of %s.', 'gtv-mycred' ), $options[ 'replace_voucher_singular_word' ], $mycred->name[ 'plural' ] );
}

function gtvmycred_save_creds_settings( $new_input, $post_options, $tab )
{
    if ( !( empty( $tab ) ) && 'mycred_settings' == $tab )
    {
	$new_input[ 'creds_per_voucher' ] = intval( $post_options[ 'creds_per_voucher' ] );
	$new_input[ 'creds_required_points' ] = esc_textarea( $post_options[ 'creds_required_points' ] );
	$new_input[ 'creds_not_enough_points' ] = $post_options[ 'creds_not_enough_points' ];
	if ( isset( $post_options[ 'creds_override_per' ] ) )
	{
	    $new_input[ 'creds_override_per' ] = 'true';
	}
	else
	{
	    $new_input[ 'creds_override_per' ] = 'false';
	}
    }

    return $new_input;
}

add_filter( 'gtv_more_settings', 'gtvmycred_save_creds_settings', 10, 3 );

add_filter( 'gtv_before_download_buttons', 'gtvmycred_output_voucher_point_cost', 10, 2 );

function gtvmycred_output_voucher_point_cost( $points, $voucher_id )
{
    $options = get_option( 'gtvouchers_options' );

    $mycred = mycred();
    $creds = $options[ 'creds_per_voucher' ];

    $override_creds = gtvmycred_get_required_points( $voucher_id );

    if ( $override_creds != $creds )
    {
	$creds = $override_creds;
    }

    $points = sprintf( $options[ 'creds_required_points' ], $mycred->format_creds( $creds ) );

    return $points . '<br /><br />';
}

add_action( 'gtvouchers_download', 'gtvmycred_decrement_points', 10, 3 );

function gtvmycred_decrement_points( $voucher_id, $voucher_name, $code)
{
    if ( !( function_exists( 'mycred_subtract' ) ) )
    {
	return false;
    }
    global $wpdb;

    $options = get_option( 'gtvouchers_options' );
    $points = $options[ 'creds_per_voucher' ];

    // Make sure we pass a positive to mycred_subtract - it converts as needed
    $points = abs( $points );

    $user_id_sql = "SELECT IFNULL(user_id, 0) as user_id FROM {$wpdb->base_prefix}gtvouchers_downloads WHERE voucherid=%d AND code = %s AND downloaded=0";
    $user_id = $wpdb->get_var( $wpdb->prepare( $user_id_sql, $voucher_id, $code ) );

    $override_points = gtvmycred_get_required_points( $voucher_id );
    $available_points = $wpdb->get_var( $wpdb->prepare( "SELECT sum(creds) FROM {$wpdb->base_prefix}myCRED_log WHERE user_id = %d", $user_id ) );
	
   
    if ( $override_points != $points )
    {
	$points = $override_points;
    }
  if ( $points > $available_points || $available_points <= 0)
   {

	global $wp_query;
    	$wp_query->set_404();
    	wp_die( __( 'Sorry !  You have isufficient Points to download this voucher.', 'gtvouchers-d' ) );
    	
   }
    $voucher = gtvouchers_get_voucher( intval( $voucher_id ), 0 );
    $post = get_post( $voucher->place_id );

    $log = sprintf( __( 'Downloaded the voucher: %s (%s)', 'gtv-mycred' ), $voucher->name, $post->post_title );

    mycred_subtract( 'voucher_download', $user_id, $points, $log );

    //$message = $user_id;
    //echo "<script type='text/javascript'>alert('$message');</script>";
}

function gtvmycred_get_required_points( $voucher_id )
{
    global $wpdb;
    $creds_sql = "SELECT cred_to_download FROM {$wpdb->base_prefix}gtvouchers_vouchers WHERE id=%d";

    $creds = $wpdb->get_var( $wpdb->prepare( $creds_sql, $voucher_id ) );

    return $creds;
}

add_filter( 'gtv_filter_download_buttons', 'gtvmycred_check_point_balance', 10, 2 );

function gtvmycred_check_point_balance( $download_buttons, $voucher_id )
{
    global $wpdb;
    global $current_user;
    $options = get_option( 'gtvouchers_options' );
    $creds = $options[ 'creds_per_voucher' ];

    $override_creds = gtvmycred_get_required_points( $voucher_id );

    if ( $override_creds != $creds )
    {
	$creds = $override_creds;
    }
    $current_user = $current_user->ID;
    ///////////////////////////////////////////////////////////////////////////////
    $table_users = $wpdb->prefix . 'users';
    $table_posts = $wpdb->prefix . 'posts';
    $table_postmeta = $wpdb->prefix . 'postmeta';
    $table_usermeta = $wpdb->prefix . 'usermeta';
    $table_affiliates = $wpdb->prefix . 'uap_affiliates';
    $table_mlm = $wpdb->prefix . 'uap_mlm_relations';
    $table_uap_cp = $wpdb->prefix . 'uap_coupons_code_affiliates';
    $table_mc_cp = $wpdb->prefix . 'myCRED_log';
    $table_voucher = $wpdb->prefix . 'gtvouchers_vouchers';

    $business_check_query = "SELECT meta_value FROM " . $table_usermeta .
	    " WHERE meta_key='user_category' && user_id='$current_user'";
    $business_check = $wpdb->get_row( $business_check_query, ARRAY_N );
    ////////////////////////////////////////////////////////////////////////////
    if ( $business_check[ 0 ] == 'bus' )
    {
	$download_buttons = false;
    }
    else if ( $business_check[ 0 ] == 'ind' )
    {
	$v_auth_query = "SELECT author_id FROM " . $table_voucher .
		" WHERE id='$voucher_id'";
	$v_auth = $wpdb->get_row( $v_auth_query, ARRAY_N );

	$points_query = "SELECT creds,ref_id,entry FROM " . $table_mc_cp .
		" WHERE user_id='$current_user'";
	$points = $wpdb->get_results( $points_query, ARRAY_N );
	$counter = 0;
	foreach ( $points as $point )
	{
	    if ( $point[ 1 ] != 0 )
	    {
		$post_author_query = "SELECT post_author FROM " . $table_posts . " WHERE ID='{$point[ 1 ]}'";
		$post_author = $wpdb->get_row( $post_author_query, ARRAY_N );
		if ( $post_author[ 0 ] == $v_auth[ 0 ] )
		{
		    $counter+=$point[ 0 ];
		}
	    }
	    else
	    {
		//$v_name_query = "SELECT SUBSTRING($point[2],25,31)";
		//$v_name = $wpdb->get_row($v_name_query,ARRAY_N);
		//$v_name_query = "SELECT SUBSTRING(entry,25,31),creds FROM ".$table_mc_cp.
		//" WHERE user_id='$current_user' AND ref_id='0'";
		//$v_name = $wpdb->get_row($v_name_query,ARRAY_N);
		//$v_name = substr($point[2], start)
		$voucher_name = substr( $point[ 2 ], 24, -2 );
		$v_id_query = "SELECT author_id FROM " . $table_voucher .
			" WHERE name='$voucher_name'";
		$v_id = $wpdb->get_row( $v_id_query, ARRAY_N );
		if ( $v_id[ 0 ] == $v_auth[ 0 ] )
		{
		    $counter+=$point[ 0 ];
		}
		//$message = "wrong answer";
		//echo "<script type='text/javascript'>alert('$voucher_name');</script>";
	    }
	}
	//echo "<script type='text/javascript'>alert('$counter');</script>";
	//mycred_get_users_cred()
	//mycred_get_users_fcred( $current_user->ID )
	//echo "<script type='text/javascript'>alert('$counter');</script>";
	if ( $counter < $creds )
	{
	    $flag = 'true';
	    $download_buttons = sprintf( $options[ 'creds_not_enough_points' ], $counter );
	    //$message = $current_user->ID;
	    //$temp=$_SESSION['overlay'];
	    //echo "<script type='text/javascript'>alert('$flag');</script>";
	}
	else
	{

	    $flag = 'false';
	    //echo "<script type='text/javascript'>alert('$flag');</script>";
	}
    }
    //$message = "hello";
    //echo "<script type='text/javascript'>alert('$message');</script>";
    $_SESSION[ 'overlay' . $_SESSION[ 'counter' ] ] = $flag;
    $_SESSION[ 'counter' ]+=1;

    return $download_buttons;
}

add_action( 'after_settings_tab_list', 'gtvmycred_add_mycred_tab' );

function gtvmycred_add_mycred_tab( $active_tab )
{
    ?>
    <a href="?page=gtv-settings&tab=mycred_settings"
       class="nav-tab <?php echo $active_tab == 'mycred_settings' ? 'nav-tab-active' : ''; ?>"><?php _e( 'MyCred Settings', 'gtv-mycred' ); ?></a>
    <?php
}

add_action( 'after_settings_tab_functions', 'gtvmycred_settings_area' );

function gtvmycred_settings_area()
{
    settings_fields( 'gtvouchers-options' );
    do_settings_sections( 'gtvo-mycred' );
}

add_action( 'gtv_after_voucher_settings', 'gtvmycred_add_settings' );

function gtvmycred_add_settings()
{
    add_settings_section( 'gtvo-mycred', __( 'MyCred Settings', 'gtv-mycred' ), 'gtvmycred_options_text', 'gtvo-mycred' );
    gtvmycred_display_settings();
}

function gtvmycred_options_text()
{
    
}

add_action( 'gtv_post_voucher_description', 'gtvmycred_show_credit_on_voucher_create' );

function gtvmycred_show_credit_on_voucher_create()
{
    $options = get_option( 'gtvouchers_options' );

    if ( !( isset( $options[ 'creds_override_per' ] ) ) || ( 'false' == $options[ 'creds_override_per' ] ) )
    {
	return;
    }

    $value = 0;
    if ( isset( $options[ 'creds_per_voucher' ] ) )
    {
	$value = $options[ 'creds_per_voucher' ];
    }

    $mycred = mycred();

    echo '<div class="geodir_form_row clearfix">';
    printf( __( 'How many %s are required to download this %s?', 'gtv-mycred' ), $mycred->name[ 'plural' ], $options[ 'replace_voucher_singular_word' ] );
    echo ' <input type="number" step="1" name="require_creds" min="' . $value . '" value="' . $value . '" style="width:90px;" />';
    echo '</div>';
    echo '<div class="geodir_form_row clearfix"><br /></div>';
}
