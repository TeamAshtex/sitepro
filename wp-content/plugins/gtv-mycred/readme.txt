== Changelog ==
= 1.2 =
    NEW: Default number of credits/points for new vouchers
    NEW: Allow each voucher to have a custom credit/point value (not lower than default)
    Update: Moved settings to a new tab
    Update: Updated language files

