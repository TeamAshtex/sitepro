<?php 

/**
 * Geo Directory Event Database Install *
 * 
 * Plugin install database tables
 *
 * @author 		Vikas Sharma
 * @category 	Admin
 * @package 	GeoDirectory Events
 *
 */


function geodir_event_tables_install() {
	
	global $wpdb;

	$wpdb->hide_errors();

    $collate = '';
    if($wpdb->has_cap( 'collation' )) {
        if(!empty($wpdb->charset)) $collate = "DEFAULT CHARACTER SET $wpdb->charset";
        if(!empty($wpdb->collate)) $collate .= " COLLATE $wpdb->collate";
    }

    require_once(ABSPATH . 'wp-admin/includes/upgrade.php');

    // Check schedule_id exists and make it auto increment if it does not so we can use dbDelta in the future
    if(@$wpdb->query("SHOW TABLES LIKE '".$wpdb->prefix."geodir_event_schedule'")>0){geodir_add_column_if_not_exist($wpdb->prefix."geodir_event_schedule", "schedule_id", "int(11) PRIMARY KEY AUTO_INCREMENT NOT NULL");}


	$event_detail = "CREATE TABLE ".EVENT_DETAIL_TABLE." (
					post_id int(11) NOT NULL,
					post_title text NULL DEFAULT NULL,
					post_status varchar(20) NULL DEFAULT NULL,
					default_category INT NULL DEFAULT NULL,
					post_tags text NULL DEFAULT NULL,
					geodir_link_business varchar(10) NULL DEFAULT NULL,
					post_location_id int(11) NOT NULL,
					marker_json text NULL DEFAULT NULL,
					claimed ENUM( '1', '0' ) NULL DEFAULT '0',
					businesses ENUM( '1', '0' ) NULL DEFAULT '0',
					is_featured ENUM( '1', '0' ) NULL DEFAULT '0',
					featured_image VARCHAR( 254 ) NULL DEFAULT NULL,
					paid_amount DOUBLE NOT NULL DEFAULT '0',
					package_id INT(11) NOT NULL DEFAULT '1',
					alive_days INT(11) NOT NULL DEFAULT '0',
					paymentmethod varchar(30) NULL DEFAULT NULL,
					expire_date VARCHAR( 25 ) NULL DEFAULT NULL,
					is_recurring TINYINT( 1 ) NOT NULL DEFAULT '0',
					recurring_dates TEXT NOT NULL,
					event_reg_desc text NULL DEFAULT NULL,
					event_reg_fees varchar(200) NULL DEFAULT NULL,
					submit_time varchar(15) NULL DEFAULT NULL,
					submit_ip varchar(254) NULL DEFAULT NULL,
					overall_rating float(11) DEFAULT NULL,
					rating_count INT(11) DEFAULT '0',
					rsvp_count INT(11) DEFAULT '0',
					post_locations VARCHAR( 254 ) NULL DEFAULT NULL,
					post_latitude varchar(20) NULL,
					post_longitude varchar(20) NULL,
					post_dummy ENUM( '1', '0' ) NULL DEFAULT '0',
					PRIMARY KEY  (post_id)
					) $collate ";


    $event_detail = apply_filters('geodir_before_event_detail_table_create', $event_detail);
    dbDelta($event_detail);
	
	do_action('geodir_after_custom_detail_table_create', 'gd_event', EVENT_DETAIL_TABLE);

    $event_schedule = "CREATE TABLE ".EVENT_SCHEDULE." (
        schedule_id int(11) AUTO_INCREMENT NOT NULL,
		event_id int(11) NOT NULL,
		event_date datetime NOT NULL,
		event_enddate DATE NOT NULL,
		event_starttime time,
		event_endtime time,
		all_day TINYINT( 1 ) NOT NULL DEFAULT '0',
		recurring TINYINT( 1 ) NOT NULL DEFAULT '0',
		PRIMARY KEY  (schedule_id)
		) $collate ";

    $event_schedule = apply_filters('geodir_before_event_schedule_table_create', $event_schedule);
    dbDelta($event_schedule);
	
	update_option( 'geodir_event_recurring_feature', '1' );

}	
		

function geodir_event_create_default_fields(){
	
	$package_info = array() ;
	$package_info = geodir_post_package_info($package_info , '', 'gd_event');
	$package_id = $package_info->pid;
	
	$fields = array();
	
	$fields[]	= array('listing_type' 	=> 'gd_event', 
						'data_type' 	=> 'VARCHAR', 
						'field_type' 	=> 'taxonomy', 
						'admin_title' 	=> __('Category', 'geodirevents'),
						'admin_desc' 	=> __('Select listing category from here. Select at least one category', 'geodirevents'),
						'site_title' 	=> __('Category', 'geodirevents'),
						'htmlvar_name' 	=> 'gd_eventcategory', 
						'default_value'	=> '', 
						'is_default'  	=> '1',
						'is_admin'			=> '1',
						'show_on_pkg' => array($package_id),
						'is_required'	=> '1', 
						'clabels'		=> __('Category', 'geodirevents'));
	
	$fields[]	= array(	'listing_type'	=> 'gd_event', 
							'data_type' 	=> 'VARCHAR', 
							'field_type' 	=> 'address', 
							'admin_title' 	=> __('Address', 'geodirevents'),
							'admin_desc' 	=> ADDRESS_MSG, 
							'site_title' 	=> __('Address', 'geodirevents'),
							'htmlvar_name' 	=> 'post', 
							'default_value'	=> '', 
							'option_values' => '', 
							'is_default'  	=> '1',
							'is_admin'			=> '1',
							'show_on_pkg' => array($package_id),
							'is_required'	=> '1',
							'required_msg'	=> __('Address fields are required', 'geodirevents'),
							'clabels'		=> __('Address', 'geodirevents'),
							'extra'	=> array(	'show_city'=> 1 , 'city_lable' => __('City', 'geodirevents'),
												'show_region' => 1, 'region_lable' => __('Region', 'geodirevents'),
												'show_country' => 1, 'country_lable' => __('Country', 'geodirevents'),
												'show_zip' => 1, 'zip_lable' => __('Zip/Post Code', 'geodirevents'),
												'show_map' => 1, 'map_lable' => __('Set Address On Map', 'geodirevents'),
												'show_mapview' => 1, 'mapview_lable' => __('Select Map View', 'geodirevents'),
												'show_latlng' => 1));
							
	$fields[]	= array(	'listing_type'	=> 'gd_event', 
							'data_type' 	=> 'VARCHAR', 
							'field_type' 	=> 'text', 
							'admin_title' 	=> __('Time', 'geodirevents'),
							'admin_desc' 	=> __('Enter Business or Listing Timing Information.<br/>eg. : 10.00 am to 6 pm every day', 'geodirevents'),
							'site_title' 	=> __('Time', 'geodirevents'),
							'htmlvar_name' 	=> 'timing', 
							'default_value'	=> '', 
							'option_values' => '',
							'is_default'  	=> '1',
							'is_admin'			=> '1',
							'show_on_pkg' => array($package_id), 
							'clabels'		=> __('Time', 'geodirevents'));
	
	$fields[]	= array(	'listing_type'	=> 'gd_event', 
							'data_type' 	=> 'VARCHAR', 
							'field_type' 	=> 'phone', 
							'admin_title' 	=> __('Phone', 'geodirevents'),
							'admin_desc' 	=> __('You can enter phone number,cell phone number etc.', 'geodirevents'),
							'site_title' 	=> __('Phone', 'geodirevents'),
							'htmlvar_name' 	=> 'contact', 
							'default_value'	=> '', 
							'option_values' => '',
							'is_default'  	=> '1',
							'is_admin'			=> '1',
							'show_on_pkg' => array($package_id),
							'clabels'		=> __('Phone', 'geodirevents'));
	
	$fields[]	= array(	'listing_type'	=> 'gd_event', 
							'data_type' 	=> 'VARCHAR', 
							'field_type' 	=> 'email', 
							'admin_title' 	=> __('Email', 'geodirevents'),
							'admin_desc' 	=> __('You can enter your business or listing email.', 'geodirevents'),
							'site_title' 	=> __('Email', 'geodirevents'),
							'htmlvar_name' 	=> 'email', 
							'default_value'	=> '', 
							'option_values' => '',
							'is_default'  	=> '1',
							'is_admin'			=> '1',
							'show_on_pkg' => array($package_id),
							'clabels'		=> __('Email', 'geodirevents'));
							
	$fields[]	= array(	'listing_type'	=> 'gd_event', 
							'data_type' 	=> 'VARCHAR', 
							'field_type' 	=> 'url', 
							'admin_title' 	=> __('Website', 'geodirevents'),
							'admin_desc' 	=> __('You can enter your business or listing website.', 'geodirevents'),
							'site_title' 	=> __('Website', 'geodirevents'),
							'htmlvar_name' 	=> 'website', 
							'default_value'	=> '', 
							'option_values' => '',
							'is_default'  	=> '1',
							'is_admin'			=> '1',
							'show_on_pkg' => array($package_id), 
							'clabels'		=> __('Website', 'geodirevents'));
	
	$fields[]	= array(	'listing_type'	=> 'gd_event', 
							'data_type' 	=> 'VARCHAR', 
							'field_type' 	=> 'url', 
							'admin_title' 	=> __('Twitter', 'geodirevents'),
							'admin_desc' 	=> __('You can enter your business or listing twitter url.', 'geodirevents'),
							'site_title' 	=> __('Twitter', 'geodirevents'),
							'htmlvar_name' 	=> 'twitter', 
							'default_value'	=> '', 
							'option_values' => '',
							'is_default'  	=> '1',
							'is_admin'			=> '1',
							'show_on_pkg' => array($package_id),
							'clabels'		=> __('Twitter', 'geodirevents'));
	
	$fields[]	= array(	'listing_type'	=> 'gd_event', 
							'data_type' 	=> 'VARCHAR', 
							'field_type' 	=> 'url', 
							'admin_title' 	=> __('Facebook', 'geodirevents'),
							'admin_desc' 	=> __('You can enter your business or listing facebook url.', 'geodirevents'),
							'site_title' 	=> __('Facebook', 'geodirevents'),
							'htmlvar_name' 	=> 'facebook', 
							'default_value'	=> '', 
							'option_values' => '',
							'is_default'  	=> '1',
							'is_admin'			=> '1',
							'show_on_pkg' => array($package_id),
							'clabels'		=> __('Facebook', 'geodirevents'));
							
	$fields[]	= array(	'listing_type'	=> 'gd_event', 
							'data_type' 	=> 'TEXT', 
							'field_type' 	=> 'textarea', 
							'admin_title' 	=> __('Video', 'geodirevents'),
							'admin_desc' 	=> __('Add video code here, YouTube etc.', 'geodirevents'),
							'site_title' 	=> __('Video', 'geodirevents'),
							'htmlvar_name' 	=> 'video', 
							'default_value'	=> '', 
							'option_values' => '',
							'is_default'  	=> '0',
							'is_admin'			=> '1',
							'show_on_pkg' => array($package_id),
							'clabels'		=> __('Video', 'geodirevents'));
	
	$fields[]	= array(	'listing_type'	=> 'gd_event', 
							'data_type' 	=> 'TEXT', 
							'field_type' 	=> 'textarea', 
							'admin_title' 	=> __('Special Offers', 'geodirevents'),
							'admin_desc' 	=> __('Note: List out any special offers (optional)', 'geodirevents'),
							'site_title' 	=> __('Special Offers', 'geodirevents'),
							'htmlvar_name' 	=> 'special_offers', 
							'default_value'	=> '', 
							'option_values' => '',
							'is_default'  	=> '0',
							'is_admin'			=> '1',
							'show_on_pkg' => array($package_id),
							'clabels'		=> __('Special Offers', 'geodirevents'));
				
	foreach($fields as $field_index => $field )
	{ 
		geodir_custom_field_save( $field ); 
	}							
}