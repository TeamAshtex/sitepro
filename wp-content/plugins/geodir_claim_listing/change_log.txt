v1.2.6
Misspells fixed in claim listing notification messages - FIXED

v1.2.5
WPML compatibility change for popup - CHANGED

v1.2.4
Validating and sanitizing changes  - FIXED

v1.2.3
New update licence system implemented - CHANGED
Claim emails sending to address in headers twice - FIXED

v1.2.2
New actions added to manage claim request status change - ADDED
Show message on login page when guest user click on claim link and after login claim form should auto popup - CHANGED

v1.2.1
Email address with .coop TLD not validated - FIXED
case conversion functions replaced with custom function to support unicode languages - CHANGED
Use get_site_url() instead of home_url() - CHANGED

v1.2.0
Login link updated to use geodir_login_url() function - FIXED

v1.1.9
Some urls updated to use the new gd-info page - CHANGED

v1.1.8
GD auto update script improved efficiency and security(https) - CHANGED
Option to force payment added during claim process if prices and payments installed - ADDED
Changed textdomain from defined constant to a string - CHANGED

v1.1.7
Undefined variable in language.php - FIXED

v1.1.6
Changes made for WPML compatibility - CHANGED

V1.1.5
Added CSS class to author link on details sidebar - ADDED

V1.1.4
dbDelta function used for db tables creation - CHANGED

V1.1.3
Slashes not stripped from message of claim listing email - FIXED

V1.1.2
New hook added for author link of verified listings page - ADDED

V1.1.1
New hook actions added in claim listing form - ADDED

V1.1.0
WordPres multisite compatability - ADDED

V1.0.6
Blank index.php added to each directory of plugin - ADDED
v1.0.5
When CPT addon installed the dropdown select can be hide some PT becasue of overflow - FIXED

A Notice in backend when claim manager is on and try to Edit a wordpress comment on any geodirectory post type - Fixed
