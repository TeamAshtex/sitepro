<?php
/*
Plugin Name: see_bookmarks
Plugin URI: http://localhost/
Description: A pugin that will enable you to see your bookmarked places!!
Version: 1
Author: Usman Ashraf
Author URI: http://localhost/
*/

class bookmarks_widget extends WP_Widget {
     
    function __construct() {
    parent::__construct(false, $name=__('See Bookmarks'));
    
    }
     
    // Creating widget front-end
    // This is where the action happens
    public function widget( $args, $instance ) 
    {
        static $query_args;
        global $wpdb;
        global $post_name,$event_code,$place_code;
        $current_user = get_current_user_id();
        if ($current_user == 0) 
        {
            //echo 'You are currently not logged in!!';
        }
         else 
        {
            
            $table_usermeta = $wpdb->prefix . 'usermeta';

            $business_check_query = "SELECT meta_value FROM ".$table_usermeta.
            " WHERE meta_key='user_category' && user_id='$current_user'";
            $business_check = $wpdb->get_row($business_check_query,ARRAY_N);

            if($business_check[0] == 'bus')
            {
            echo "<style>.side-img img {
                width: 60px;
                float: left;
                margin-right: 13px;
            }
            .side-img span {
                float: left;
                width: 202px;
            }
            .side-img a {
                font-weight: bold;
            }
            .side-img ul {
                float: right;
                width: 202px;
            }
            .side-img ul li {
                margin-bottom: 0px;
            }
            .link_all{
                position:fixed;
                bottom:10px;
                right:0px;
                height:20px;
                width:250px;
                background-color:#cf8;
                } 
            .side-img ul li a {
                font-weight: normal;
                color: #fff;
                background: #DC2F21;
                float: left;
                text-overflow: ellipsis;
                white-space: nowrap;
                overflow: hidden;
                margin-right: 6px;
                margin-bottom: 10px;
                padding: 2px 7px;
                max-width: 92px;
            }</style>";
            
            echo "<section class='widget'>";
            echo "<div class='geodir_list_heading clearfix'>";
            echo "<h3 class='widget-title'>Bookmarks Here!</h3>";
            echo "<a href='http://link1n1.com/DEV/view-all/' class='geodir-viewall'>View all</a>";
            echo "</div>";
            //echo '<h3 class="widget-title">Bookmarks Here!</h3>';
            static $counter=0,$bm_person_count=0,$limit_flag=true;
            //get all the posts bookmarked by all the users
            $result=$this->get_all_bookmarked_posts($wpdb,$current_user);
            //get all logged in user's posts
            $result1 = $this->get_current_user_posts($wpdb,$current_user);

            //echo(exec("whoami"));
            
            

            
            //while ($row = mysql_fetch_array($result, MYSQL_NUM)) 
            foreach ($result as $row)
            {
                $arr=unserialize($row[0]);
                
                //while ($row1 = mysql_fetch_array($result1, MYSQL_NUM)) 
                foreach ($result1 as $row1)
                {
                    if(!empty($arr))
                    {
                    foreach ($arr as $value) 
                    {
                        
                        if($row1[0]==$value)
                        {
                            $username=$this->get_user_nicename($wpdb,$row[1]);
                            
                            if($post_name!=$username[0] || $post_name=="")
                            {
                                if($post_name!="")
                                {
                                    if($counter!=0)
                                    {
                                        echo "</ul>";
                                        echo "<br>";
                                        echo "<br>";
                                    }
                                    
                                }
                                $post_name=$username[0];
                                $bm_person_count++;
                                if($bm_person_count>=6)
                                {
                                    $limit_flag=false;
                            
                                }
                                else
                                {
                                    if($username!="")
                                    {
                                        echo "<div class='side-img'>".get_avatar( $row[1], 64 )."</div><span class='img-info'>
                                        <a href='http://link1n1.com/DEV/members/".$username[0]."'>".$username[0].
                                        "</a> has bookmarked one or more of your posts.</span>";
                                        echo "<ul>";
                                        $sub_counter=0;
                                        $counter++;
                                    }
                                }
                            }
                            
                            $p_details=$this->get_post_cat($wpdb,$row1[0]);
                            $post_details=unserialize($p_details[0]);
                            
                            $place=$post_details['gd_placecategory'];
                            $event=$post_details['gd_eventcategory'];
                            if($event=="" && $place!="")
                            {
                                $place_code=$place[0].$place[1];
                                
                            }
                            else if($event!="" && $place=="")
                            {
                                $event_code=$event[0].$event[1];
                                
                            }

                            $place_id=$this->get_post_loc_id($wpdb,$row1[0]);
                            
                            $place_details_row=$this->get_post_loc_details($wpdb,$place_id[0]);
                            
                            if($event=="" && $place!="")
                            {
                                if($place_code=='20')
                                {
                                
                                    $sub_counter++;
                                    $counter++;
                                    $cat="hotels";
                                    if($sub_counter<=5)
                                    {
                                        //$this->print_link($cat,$place_details_row,$row1);
                                    }
                                }
                                else if($place_code=='21')
                                {
                                
                                    $sub_counter++;
                                    $counter++;
                                    $cat="restaurants";
                                    if($sub_counter<=5)
                                    {
                                        //$this->print_link($cat,$place_details_row,$row1);
                                    }   
                                }
                                else if($place_code=='19')
                                {
                                
                                    $sub_counter++;
                                    $counter++;
                                    $cat="attractions";
                                    if($sub_counter<=5)
                                    {
                                        //$this->print_link($cat,$place_details_row,$row1);
                                    }
                                }
                            }
                            else if($event!="" && $place=="")
                            {
                                
                                $sub_counter++;
                                $counter++;
                                $cat="events";
                                if($sub_counter<=5)
                                {
                                    //$this->print_link($cat,$place_details_row,$row1);
                                }
                            }
                            if($limit_flag==false)
                            {
                                break;
                            }
                        }
                        if($limit_flag==false)
                            {
                                break;
                            }
                    }
                }
                    if($limit_flag==false)
                            {
                                break;
                            }
                    
                }
                if($limit_flag==false)
                            {
                                break;
                            }
            }
            echo "</ul>";
            if($counter!=0)
            {
                echo "<br>";
                echo "<br>";
                echo "</section>";
            }
            
            
            if($counter==0)
            {
                echo "<p>No one has bookmarked your posts!!</p>";
                echo "</section>";
                
            }
            

        }
        }

    }
    //get all the posts bookmarked by all the users
    public function get_all_bookmarked_posts($wpdb,$current_user)
    {
        $table_usermeta = $wpdb->prefix . 'usermeta';
        $query="SELECT meta_value,user_id FROM ".$table_usermeta." 
        WHERE meta_key='gd_user_favourite_post' && user_id != '$current_user'";
            $result = $wpdb->get_results($query,ARRAY_N);
            //echo print_r($result);
            return $result;
    }
    //get all the current user posts
    public function get_current_user_posts($wpdb,$current_user)
    {
        $table_posts = $wpdb->prefix . 'posts';
        $current_user_posts="SELECT ID, post_content, post_title,post_name 
        FROM ".$table_posts." WHERE post_author='$current_user'";
        $result1 = $wpdb->get_results($current_user_posts,ARRAY_N);
        return $result1;
    }
    //get user_nicename
    public function get_user_nicename($wpdb,$userID)
    {
        $table_users = $wpdb->prefix . 'users';
        $name_query="SELECT display_name FROM ".$table_users." WHERE ID='$userID'";
                            //$result_name = mysql_query($name_query) or die (mysql_error());
                            $username = $wpdb->get_row($name_query,ARRAY_N);
                            return $username;
    }
    //get post category
    public function get_post_cat($wpdb,$postID)
    {
        $table_postmeta = $wpdb->prefix . 'postmeta';
        $detail_query="SELECT meta_value FROM ".$table_postmeta." WHERE post_id='$postID'
                             && meta_key='post_categories'";
                            //$result_detail = mysql_query($detail_query) or die (mysql_error());
                             $p_details = $wpdb->get_row($detail_query,ARRAY_N);
                             return $p_details;
    }
    //get post location id
    public function get_post_loc_id($wpdb,$postID)
    {
        $table_name = $wpdb->prefix . 'geodir_gd_place_detail';
        $place_query="SELECT post_location_id FROM ".$table_name." WHERE post_id='$postID'";
                            //$result_place = mysql_query($place_query) or die (mysql_error());
                            $place_id = $wpdb->get_row($place_query,ARRAY_N);
                            return $place_id;
    }
    //get post location details
    public function get_post_loc_details($wpdb,$placeID)
    {
        $table_name = $wpdb->prefix . 'geodir_post_locations';
        $place_details="SELECT region,city,country_slug FROM ".$table_name."
         WHERE location_id='$placeID'";
                            //$result_place_details = mysql_query($place_details) or die (mysql_error());
                            $place_details_row = $wpdb->get_row($place_details,ARRAY_N);
                            return $place_details_row;
    }
    //print link
    public function print_link($cat,$place_details_row,$row1)
    {
        echo "<li><a href='http://link1n1.com/places/".$place_details_row[2]
                                    ."/".$place_details_row[0]."/"
                                    .$place_details_row[1]."/".$cat."/".$row1[3]."/'>".$row1[2]."</a></li>";
                                    
    }

// Widget Backend
    public function form( $instance ) {
    
    }
         
    // Updating widget replacing old instances with new
    public function update( $new_instance, $old_instance ) {
    
    }
    } // Class wpb_widget ends here
// Register and load the widget
    function wpb_load_widget() {
        register_widget( 'bookmarks_widget' );
    }
    add_action( 'widgets_init', 'wpb_load_widget' );

    ?>