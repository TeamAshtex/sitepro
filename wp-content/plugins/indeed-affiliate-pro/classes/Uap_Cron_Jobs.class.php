<?php 
if (!class_exists('Uap_Cron_Jobs')) : 

class Uap_Cron_Jobs{
	
	public function __construct(){
		/*
		 * @param none
		 * @return none
		 */
		
		/////////// RANKS
		$repeat = get_option('uap_update_ranks_interval');
		if (empty($repeat)){
			$repeat = 'daily';
		}
		$schedule = wp_next_scheduled('uap_cron_job');
		if (empty($schedule)){
			//create cron
			wp_schedule_event( time(), $repeat, 'uap_cron_job');//modify time 
		}		
		add_action( 'uap_cron_job', array($this, 'update_affiliates_rank') );
		
		///////// PAYMENTS
		$repeat = get_option('uap_update_payments_status');
		if (empty($repeat)){
			$repeat = 'daily';
		}
		$schedule = wp_next_scheduled('uap_cron_job_payments');
		if (empty($schedule)){
			//create cron
			wp_schedule_event( time(), $repeat, 'uap_cron_job_payments');//modify time
		}
		add_action( 'uap_cron_job_payments', array($this, 'update_affiliates_payments_status') );
		
		///////// RANKS
		$repeat = 'daily';
		$schedule = wp_next_scheduled('uap_cron_error_notifications');
		if (empty($schedule)){
			//create cron
			wp_schedule_event( time(), $repeat, 'uap_cron_error_notifications');//modify time
		}		
		add_action( 'uap_cron_error_notifications', array($this, 'send_email_notification_for_slug_username_errors') );
	}
	
	public function update_affiliates_rank(){
		/*
		 * @param none
		 * @return none
		 */
		require_once UAP_PATH . 'public/Uap_Change_Ranks.class.php';
		$object = new Uap_Change_Ranks();
	}
	
	public function update_affiliates_payments_status(){
		/*
		 * @param none
		 * @return none
		 */
		global $indeed_db;
		$indeed_db->update_paypal_transactions();
	}
	
	public function update_cron_time($new=''){
		/*
		 * @param string
		 * @return none
		 */
		wp_clear_scheduled_hook('uap_cron_job');
		wp_schedule_event( time(), $new, 'uap_cron_job');	
	}
	
	
	public function send_email_notification_for_slug_username_errors(){
		/*
		 * @param none
		 * @return none
		 */
		global $indeed_db;
		$data = $indeed_db->select_all_same_slugs_with_usernames();
		if ($data){
			$output = '';
			foreach ($data as $arr){
				$owner_of_username = $indeed_db->get_username_by_wpuid($arr['user']);
				$owner_of_slug = $indeed_db->get_username_by_wpuid($arr['slug']);		
				$output .= __('User ', 'uap') . $owner_of_slug . __(' has a custom slug that match with nickname of ', 'uap') . $owner_of_username . '. <br/>';
			}
			if ($output){
				$output = __('We inform You that: ', 'uap') . '<br/>' . $output . '<br/>' . __('This could cause some errors into Ultimate Affiliate Pro.', 'uap');
				$admin_email = get_option('admin_email');
				$output = "<html><head></head><body>" . $output . "</body></html>";
				$subject = __('Hello', 'uap');
				$headers[] = 'Content-Type: text/html; charset=UTF-8';
				if ($admin_email){
					wp_mail($admin_email, $subject, $output, $headers);
				}
			}
		}
	}
}

endif;