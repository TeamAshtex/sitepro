<div class="uap-stuffbox">
	<h3 class="uap-h3"><?php echo __('Display MLM children');?></h3>
	<div class="inside">
		
		<?php if (!empty($data['items'])):?>
			<table class="uap-dashboard-inside-table">
				<tbody>
					<tr>
						<th><?php _e('Subaffiliate', 'uap');?></th>
						<th><?php _e('Level', 'uap');?></th>
						<th><?php _e('Amount', 'uap');?></th>
					</tr>		
					<?php foreach ($data['items'] as $item):?>
					<tr>
						<td><?php echo $item['username'];?></td>
						<td><?php echo $item['level'];?></td>
						<td><?php echo $item['amount_value'];?></td>
					</tr>							
					<?php endforeach;?>
				</tbody>
			</table>			
		<?php else : ?>
			<?php _e('No Children Yet!', 'uap');?>
		<?php endif;?>
		
	</div>
</div>
