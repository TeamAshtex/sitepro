<?php
require_once '../../../../wp-load.php';
require_once '../classes/ResetPassword.class.php';

if (!empty($_GET['do_reset_pass']) && !empty($_GET['uid']) && !empty($_GET['c'])){
	/// DO RESET PASSWORD
	$object = new UAP\ResetPassword();
	$object->proceed($_GET['uid'], $_GET['c']);
}


/// AND OUT
$redirect_url = get_home_url();
wp_redirect($redirect_url);
exit();