<div class="uap-banners-wrapp">
	
	<?php if (!empty($data['title'])):?>
		<h3><?php echo $data['title'];?></h3>
	<?php endif;?>
	<?php if (!empty($data['message'])):?>
		<p><?php echo do_shortcode($data['message']);?></p>
	<?php endif;?>
	
		<?php if (!empty($data['items'])):?>
			<table class="uap-account-table">
				<tbody>
					<thead>
						<tr>
							<th><?php _e('Subaffiliate', 'uap');?></th>
							<th><?php _e('Level', 'uap');?></th>
							<th><?php _e('Amount', 'uap');?></th>
						</tr>							
					</thead>
					<tfoot>
						<tr>
							<th><?php _e('Subaffiliate', 'uap');?></th>
							<th><?php _e('Level', 'uap');?></th>
							<th><?php _e('Amount', 'uap');?></th>
						</tr>							
					</tfoot>	
					<?php foreach ($data['items'] as $item):?>
					<tr>
						<td><?php echo $item['username'];?></td>
						<td><?php echo $item['level'];?></td>
						<td><?php echo $item['amount_value'];?></td>
					</tr>							
					<?php endforeach;?>
				</tbody>
			</table>			
		<?php else : ?>
			<?php _e('No Children Yet!', 'uap');?>
		<?php endif;?>

</div>