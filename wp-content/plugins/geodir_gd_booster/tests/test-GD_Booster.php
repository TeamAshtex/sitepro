<?php
use geodir_gd_booster as my_geodir_gd_booster;
class GDBoosterTests extends WP_UnitTestCase
{
    public function setUp()
    {
        parent::setUp();
        wp_set_current_user(1);

        $options = array(
            /* Core/systematic plugin options. */

            'crons_setup'                      => '1', // `0` or timestamp.

            /* Primary switch; enable? */

            'enable'                           => '1', // `0|1`.

            /* Related to debugging. */

            'debugging_enable'                 => '1',
            // `0|1|2` // 2 indicates greater debugging detail.

            /* Related to cache directory. */

            'base_dir'                         => 'cache/geodir-gd-booster', // Relative to `WP_CONTENT_DIR`.
            'cache_max_age'                    => '7 days', // `strtotime()` compatible.

            /* Related to automatic cache clearing. */

            'cache_clear_home_page_enable'     => '1', // `0|1`.
            'cache_clear_posts_page_enable'    => '1', // `0|1`.

            'cache_clear_author_page_enable'   => '1', // `0|1`.

            'cache_clear_term_category_enable' => '1', // `0|1`.
            'cache_clear_term_post_tag_enable' => '1', // `0|1`.
            'cache_clear_term_other_enable'    => '1', // `0|1`.

            /* Misc. cache behaviors. */

            'allow_browser_cache'              => '1', // `0|1`.
            'get_requests'                     => '1', // `0|1`.
            'feeds_enable'                     => '1', // `0|1`.
            'cache_404_requests'               => '1', // `0|1`.
            'exclude_combines'                 => '', // `string`.
            'max_url_length'                   => '2000', // `string`.

            /* Related to uninstallation routines. */

            'uninstall_on_deletion'            => '1', // `0|1`.

        );
        update_option('geodir_gd_booster_options', $options);
    }

    public function testPluginActive() {
        $output = gd_booster_is_plugin_active();
        $this->assertTrue($output);
    }

    public function testMenuPages() {

        $instance = new my_geodir_gd_booster\plugin();
        ob_start();
        $instance->menu_page_options();
        $output = ob_get_contents();
        ob_end_clean();
        $this->assertContains('GD Booster Cache Options', $output);

        $_REQUEST['geodir_gd_booster__updated'] = true;
        ob_start();
        $instance->menu_page_options();
        $output = ob_get_contents();
        ob_end_clean();
        $this->assertContains('Options updated successfully', $output);

        $_REQUEST['geodir_gd_booster__restored'] = true;
        ob_start();
        $instance->menu_page_options();
        $output = ob_get_contents();
        ob_end_clean();
        $this->assertContains('Default options successfully restored', $output);

        $_REQUEST['geodir_gd_booster__cache_wiped'] = true;
        ob_start();
        $instance->menu_page_options();
        $output = ob_get_contents();
        ob_end_clean();
        $this->assertContains('Cache wiped across all sites', $output);

        $_REQUEST['geodir_gd_booster__cache_cleared'] = true;
        ob_start();
        $instance->menu_page_options();
        $output = ob_get_contents();
        ob_end_clean();
        $this->assertContains('Cache cleared for this site', $output);

        $_REQUEST['geodir_gd_booster__wp_config_wp_cache_add_failure'] = true;
        ob_start();
        $instance->menu_page_options();
        $output = ob_get_contents();
        ob_end_clean();
        $this->assertContains('Failed to update your <code>/wp-config.php</code>', $output);

        $_REQUEST['geodir_gd_booster__wp_config_wp_cache_remove_failure'] = true;
        ob_start();
        $instance->menu_page_options();
        $output = ob_get_contents();
        ob_end_clean();
        $this->assertContains('Failed to update your <code>/wp-config.php</code>', $output);

        $_REQUEST['geodir_gd_booster__advanced_cache_add_failure'] = true;
        ob_start();
        $instance->menu_page_options();
        $output = ob_get_contents();
        ob_end_clean();
        $this->assertContains('Failed to update your <code>/wp-content/advanced-cache.php</code>', $output);

        $_REQUEST['geodir_gd_booster__advanced_cache_remove_failure'] = true;
        ob_start();
        $instance->menu_page_options();
        $output = ob_get_contents();
        ob_end_clean();
        $this->assertContains('Failed to remove', $output);

        $_REQUEST['geodir_gd_booster_pro_preview'] = true;
        ob_start();
        $instance->menu_page_options();
        $output = ob_get_contents();
        ob_end_clean();
        $this->assertContains('Pro Features (Preview)', $output);

    }

    public function testSetup() {
        $instance = new my_geodir_gd_booster\plugin();
        $instance->setup();
    }

    public function testActivate() {
        $instance = new my_geodir_gd_booster\plugin();
        $instance->activate();
    }

    public function testDeActivate() {
        $instance = new my_geodir_gd_booster\plugin();
        $instance->deactivate();
    }

    public function testHomePage()
    {
        $this->setPermalinkStructure();

        $homepage = get_page_by_title('GD Home page');
        if ($homepage) {
            update_option('page_on_front', $homepage->ID);
            update_option('show_on_front', 'page');
        }

        ob_start();
        $this->go_to(home_url('/'));
        $this->load_template();
        $output = ob_get_contents();
        ob_end_clean();
        $this->assertContains('body class="home', $output);
    }

    public function testPostCache() {
        $instance = new my_geodir_gd_booster\plugin();
        $instance->setup();

        $args = array(
            'listing_type' => 'gd_place',
            'post_title' => 'Test Listing Title',
            'post_desc' => 'Test Desc',
            'post_tags' => 'test1,test2',
            'post_address' => 'New York City Hall',
            'post_zip' => '10007',
            'post_mapview' => 'ROADMAP',
            'post_mapzoom' => '10',
            'geodir_timing' => '10.00 am to 6 pm every day',
            'geodir_contact' => '1234567890',
            'geodir_email' => 'test@test.com',
            'geodir_website' => 'http://test.com',
            'geodir_twitter' => 'http://twitter.com/test',
            'geodir_facebook' => 'http://facebook.com/test',
            'geodir_special_offers' => 'Test offer'
        );
        $post_id = geodir_save_listing($args, true);

        $this->assertTrue(is_int($post_id));
    }

    public function testPhpVersionNotice() {
        wp_php53_notice('GD Booster');
        ob_start();
        do_action('all_admin_notices');
        $output = ob_get_contents();
        ob_end_clean();
        $this->assertContains('A simple update is necessary', $output);
    }

    public function testPhpVersion() {
        $version = wp_php53();
        $this->assertTrue($version);
    }

    public function texstDetailPage() {
        $time = current_time('mysql');

        $args = array(
            'listing_type' => 'gd_place',
            'post_title' => 'Test Listing Title',
            'post_desc' => 'Test Desc',
            'post_tags' => 'test1,test2',
            'post_address' => 'New York City Hall',
            'post_zip' => '10007',
            'post_latitude' => '40.7127837',
            'post_longitude' => '-74.00594130000002',
            'post_mapview' => 'ROADMAP',
            'post_mapzoom' => '10',
            'geodir_timing' => '10.00 am to 6 pm every day',
            'geodir_contact' => '1234567890',
            'geodir_email' => 'test@test.com',
            'geodir_website' => 'http://test.com',
            'geodir_twitter' => 'http://twitter.com/test',
            'geodir_facebook' => 'http://facebook.com/test',
            'geodir_special_offers' => 'Test offer'
        );
        $post_id = geodir_save_listing($args, true);

        $data = array(
            'comment_post_ID' => $post_id,
            'comment_author' => 'admin',
            'comment_author_email' => 'admin@admin.com',
            'comment_author_url' => 'http://wpgeodirectory.com',
            'comment_content' => 'content here',
            'comment_type' => '',
            'comment_parent' => 0,
            'user_id' => 1,
            'comment_author_IP' => '127.0.0.1',
            'comment_agent' => 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.0.10) Gecko/2009042316 Firefox/3.0.10 (.NET CLR 3.5.30729)',
            'comment_date' => $time,
            'comment_approved' => 1,
        );

        $comment_id = wp_insert_comment($data);

        $_REQUEST['geodir_overallrating'] = 5.0;
        geodir_save_rating($comment_id);

        $this->assertTrue(is_int($comment_id));

        global $preview;
        $preview = false;
        ob_start();
        $this->go_to( get_permalink($post_id) );
        global $wp_query;
        $wp_query->query_vars['post_type'] = 'gd_place';
        $this->load_template();
        $output = ob_get_contents();
        ob_end_clean();
    }

    public function testGDboosterOnFooter() {
        $instance = new my_geodir_gd_booster\plugin();
        $instance->setup();
        add_action('wp_footer', 'gd_booster_wp', 999999);

        $this->setPermalinkStructure();

        $homepage = get_page_by_title('GD Home page');
        if ($homepage) {
            update_option('page_on_front', $homepage->ID);
            update_option('show_on_front', 'page');
        }

        ob_start();
        $this->go_to(home_url('/'));
        $this->load_template();
        $output = ob_get_contents();
        ob_end_clean();
        $this->assertContains('body class="home', $output);
    }

    private function load_template() {
        do_action( 'template_redirect' );
        $template = false;
        if	 ( is_404()			&& $template = get_404_template()			) :
        elseif ( is_search()		 && $template = get_search_template()		 ) :
        elseif ( is_front_page()	 && $template = get_front_page_template()	 ) :
        elseif ( is_home()		   && $template = get_home_template()		   ) :
        elseif ( is_post_type_archive() && $template = get_post_type_archive_template() ) :
        elseif ( is_tax()			&& $template = get_taxonomy_template()	   ) :
        elseif ( is_attachment()	 && $template = get_attachment_template()	 ) :
            remove_filter('the_content', 'prepend_attachment');
        elseif ( is_single()		 && $template = get_single_template()		 ) :
        elseif ( is_page()		   && $template = get_page_template()		   ) :
        elseif ( is_category()	   && $template = get_category_template()	   ) :
        elseif ( is_tag()			&& $template = get_tag_template()			) :
        elseif ( is_author()		 && $template = get_author_template()		 ) :
        elseif ( is_date()		   && $template = get_date_template()		   ) :
        elseif ( is_archive()		&& $template = get_archive_template()		) :
        elseif ( is_paged()		  && $template = get_paged_template()		  ) :
        else :
            $template = get_index_template();
        endif;
        /**
         * Filter the path of the current template before including it.
         *
         * @since 3.0.0
         *
         * @param string $template The path of the template to include.
         */
        if ( $template = apply_filters( 'template_include', $template ) ) {
            $template_contents = file_get_contents( $template );
            $included_header = $included_footer = false;
            if ( false !== stripos( $template_contents, 'get_header();' ) ) {
                do_action( 'get_header', null );
                locate_template( 'header.php', true, false );
                $included_header = true;
            }
            include( $template );
            if ( false !== stripos( $template_contents, 'get_footer();' ) ) {
                do_action( 'get_footer', null );
                locate_template( 'footer.php', true, false );
                $included_footer = true;
            }
            if ( $included_header && $included_footer ) {
                global $wp_scripts;
                $wp_scripts->done = array();
            }
        }
        return;
    }

    public static function setPermalinkStructure( $struc = '/%postname%/' ) {
        global $wp_rewrite;
        $wp_rewrite->set_permalink_structure( $struc );
        $wp_rewrite->flush_rules();
        update_option( 'permalink_structure', $struc );
        flush_rewrite_rules( true );
    }

    function the_widget_form( $widget, $instance = array() ) {
        global $wp_widget_factory;

        $widget_obj = $wp_widget_factory->widgets[$widget];
        if ( ! ( $widget_obj instanceof WP_Widget ) ) {
            return;
        }

        $widget_obj->_set(-1);
        $widget_obj->form($instance);
    }

    function the_widget_form_update( $widget, $new_instance = array(), $old_instance = array() ) {
        global $wp_widget_factory;

        $widget_obj = $wp_widget_factory->widgets[$widget];
        if ( ! ( $widget_obj instanceof WP_Widget ) ) {
            return;
        }

        $widget_obj->_set(-1);
        return $widget_obj->update($new_instance, $old_instance);
    }

    public function tearDown()
    {
        parent::tearDown();
    }
}
?>