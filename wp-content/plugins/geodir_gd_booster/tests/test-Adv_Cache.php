<?php
use geodir_gd_booster as my_geodir_gd_booster;
class GDBoosterAdvCache extends WP_UnitTestCase
{
    public function setUp()
    {
        parent::setUp();
        wp_set_current_user(1);

        $options = array(
            /* Core/systematic plugin options. */

            'crons_setup'                      => '0', // `0` or timestamp.

            /* Primary switch; enable? */

            'enable'                           => '1', // `0|1`.

            /* Related to debugging. */

            'debugging_enable'                 => '1',
            // `0|1|2` // 2 indicates greater debugging detail.

            /* Related to cache directory. */

            'base_dir'                         => 'cache/geodir-gd-booster', // Relative to `WP_CONTENT_DIR`.
            'cache_max_age'                    => '7 days', // `strtotime()` compatible.

            /* Related to automatic cache clearing. */

            'cache_clear_home_page_enable'     => '1', // `0|1`.
            'cache_clear_posts_page_enable'    => '1', // `0|1`.

            'cache_clear_author_page_enable'   => '1', // `0|1`.

            'cache_clear_term_category_enable' => '1', // `0|1`.
            'cache_clear_term_post_tag_enable' => '1', // `0|1`.
            'cache_clear_term_other_enable'    => '0', // `0|1`.

            /* Misc. cache behaviors. */

            'allow_browser_cache'              => '0', // `0|1`.
            'get_requests'                     => '0', // `0|1`.
            'feeds_enable'                     => '0', // `0|1`.
            'cache_404_requests'               => '0', // `0|1`.
            'exclude_combines'                 => '', // `string`.
            'max_url_length'                   => '2000', // `string`.

            /* Related to uninstallation routines. */

            'uninstall_on_deletion'            => '0', // `0|1`.

        );
        update_option('geodir_gd_booster_options', $options);
    }

    public function testAdvCache() {
        $instance = new my_geodir_gd_booster\plugin();
        $instance->setup();
        $instance->add_advanced_cache();
        $instance->delete_advanced_cache();
    }

    private function load_template() {
        do_action( 'template_redirect' );
        $template = false;
        if	 ( is_404()			&& $template = get_404_template()			) :
        elseif ( is_search()		 && $template = get_search_template()		 ) :
        elseif ( is_front_page()	 && $template = get_front_page_template()	 ) :
        elseif ( is_home()		   && $template = get_home_template()		   ) :
        elseif ( is_post_type_archive() && $template = get_post_type_archive_template() ) :
        elseif ( is_tax()			&& $template = get_taxonomy_template()	   ) :
        elseif ( is_attachment()	 && $template = get_attachment_template()	 ) :
            remove_filter('the_content', 'prepend_attachment');
        elseif ( is_single()		 && $template = get_single_template()		 ) :
        elseif ( is_page()		   && $template = get_page_template()		   ) :
        elseif ( is_category()	   && $template = get_category_template()	   ) :
        elseif ( is_tag()			&& $template = get_tag_template()			) :
        elseif ( is_author()		 && $template = get_author_template()		 ) :
        elseif ( is_date()		   && $template = get_date_template()		   ) :
        elseif ( is_archive()		&& $template = get_archive_template()		) :
        elseif ( is_paged()		  && $template = get_paged_template()		  ) :
        else :
            $template = get_index_template();
        endif;
        /**
         * Filter the path of the current template before including it.
         *
         * @since 3.0.0
         *
         * @param string $template The path of the template to include.
         */
        if ( $template = apply_filters( 'template_include', $template ) ) {
            $template_contents = file_get_contents( $template );
            $included_header = $included_footer = false;
            if ( false !== stripos( $template_contents, 'get_header();' ) ) {
                do_action( 'get_header', null );
                locate_template( 'header.php', true, false );
                $included_header = true;
            }
            include( $template );
            if ( false !== stripos( $template_contents, 'get_footer();' ) ) {
                do_action( 'get_footer', null );
                locate_template( 'footer.php', true, false );
                $included_footer = true;
            }
            if ( $included_header && $included_footer ) {
                global $wp_scripts;
                $wp_scripts->done = array();
            }
        }
        return;
    }

    public static function setPermalinkStructure( $struc = '/%postname%/' ) {
        global $wp_rewrite;
        $wp_rewrite->set_permalink_structure( $struc );
        $wp_rewrite->flush_rules();
        update_option( 'permalink_structure', $struc );
        flush_rewrite_rules( true );
    }

    function the_widget_form( $widget, $instance = array() ) {
        global $wp_widget_factory;

        $widget_obj = $wp_widget_factory->widgets[$widget];
        if ( ! ( $widget_obj instanceof WP_Widget ) ) {
            return;
        }

        $widget_obj->_set(-1);
        $widget_obj->form($instance);
    }

    function the_widget_form_update( $widget, $new_instance = array(), $old_instance = array() ) {
        global $wp_widget_factory;

        $widget_obj = $wp_widget_factory->widgets[$widget];
        if ( ! ( $widget_obj instanceof WP_Widget ) ) {
            return;
        }

        $widget_obj->_set(-1);
        return $widget_obj->update($new_instance, $old_instance);
    }

    public function tearDown()
    {
        parent::tearDown();
    }
}
?>