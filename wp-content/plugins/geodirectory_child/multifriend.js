<!---------------------------------------------------------------------------- -->
<script type="text/javascript">
var $=jQuery;
var clicked = false;
var select_all='Select All';
!function(e){
    var t = function(t, i)
    {
        var s, a, d, l = e(t), n = this, f = [], r = 0, o = 0, c = e.extend({max_selected: - 1, max_selected_message:"{0} of {1} selected", pre_selected_friends:[], exclude_friends:[], friend_fields:"id,name", sorter:function(e, t){
            var i = e.name.toLowerCase(), s = t.name.toLowerCase(); return s > i? - 1:i > s?1:0}, app_image:"../images/default_app.gif", user_id:"", labels:{
            selected:"Selected", filter_default:"Start typing a name", filter_title:"Find Friends:", all:"All", max_selected_message:"{0} of {1} selected", heading:"Invite friends", message:"Please accept my invitation"}}, i || {}), m = function(e)
        {
            for (var t = {}, i = 0, s = e.length; s > i; i++)t[e[i]] = ""; return t};
                //console.log(i);
        l.html("<div id='jfmfs-friend-selector'>\n\
          <div class='platform_dialog_header_container'>\n\
          <div class='platform_dialog_icon'>\n\
          </div><div class='platform_dialog_header'>" + c.labels.heading + "</div></div>\n\
          <div class='pam filterBoxAlignment uiBoxGray bottomborder'>\n\
          <div class='clearfix request_preview'>\n\
          <img class='_s0 _29h _29i _rw img' width = '50' height: '50' src='" + c.app_image + "' alt='' />\n\
          <div class='_29j _29k'><div><span class='fwb fbjf'>Keep in mind:</span></div>\n\
          <div class='clearfix mts request_preview'><div class='_29j _29k fbjf'>" + c.labels.message + "</div>\n\
          </div></div></div></div><div id='jfmfs-inner-header' style = 'padding-top: 3px; padding-bottom: 3px;' class =  'pam filterBox uiBoxGray topborder fbjf'>\n\
          <span class='jfmfs-title fbjf'>" + c.labels.filter_title + " </span>\n\
          <input type='text' class = 'fbjf' id='jfmfs-friend-filter-text' value='" + c.labels.filter_default + "'/>\n\
          <a class='filter-link selected fbjf' id='jfmfs-filter-select-all' href='#'>" + select_all + "</a>\n\
          <a class='filter-link selected fbjf' id='jfmfs-filter-all' href='#'>" + c.labels.all + "</a>\n\
          <a class='filter-link fbjf' id='jfmfs-filter-selected' href='#'>" + c.labels.selected + " (<span id='jfmfs-selected-count' class = 'fbjf' >0</span>)</a>" + (c.max_selected > 0?"<div id='jfmfs-max-selected-wrapper' class = 'fbjf' ></div>":"") + "</div>\n\
          <div id='jfmfs-friend-container'></div>\n\
          <div class='platform_dialog_bottom_bar' id='platform_dialog_bottom_bar'>\n\
                <div class='platform_dialog_bottom_bar_border_lr'>\n\
                    <div class='platform_dialog_bottom_bar_border_tb clearfix'>\n\
                        <table class='uiGrid platform_dialog_bottom_bar_table' cellspacing='0' cellpadding='0'>\n\
                            <tbody>\n\
                                  <tr>\n\
                                      <td class='vMid'>\n\
                                         <table class='uiGrid mam platform_dialog_buttons rfloat' cellspacing='0' cellpadding='0'>\n\
                                            <tbody>\n\
                                                 <tr>\n\
                                                     <td class='prs'>\n\
                                                        <label class='uiButton uiButtonConfirm uiButtonLarge fbjf' id='ok_clicked' for='u4deh0e5'>\n\
                                                            <input value='Send Requests' class = 'fbjf' name='ok_clicked'  style = 'background-color: transparent; border: 0px solid #ccc; margin: 0px;' type='submit' id='u4deh0e5'>\n\
                                                        </label>\n\
                                                    </td>\n\
                                                </tr>\n\
                                            </tbody>\n\
                                          </table>\n\
                                       </td>\n\
                                  </tr>\n\
                            </tbody>\n\
                        </table>\n\
                    </div>\n\
                </div>\n\
            </div>\n\
            </div>");
        var u, v = e("#jfmfs-friend-container"), h = e("#jfmfs-friend-selector"), p = m(c.pre_selected_friends), _ = m(c.exclude_friends); FB.api("/me/invitable_friends?limit=5000", function(t)
         {
            //console.log(t.data);
           var i = t.data.sort(c.sorter), s = [], a = ""; e.each(i, function(e, t){
           t.id in _ || (a = t.id in p?"selected":"", s.push("<div class='jfmfs-friend " + a + " ' id='" + t.id + "'><img src='" + t.picture.data.url + "' />\n\
            <div class='friend-name'>"+ t.name +"</div>\n\
             </div>"))}),
            v.append(s.join("")), f = e(".jfmfs-friend", l), f.bind("inview", function(){
                
              //void 0 === e(this).attr("src") && e("img", e(this)).attr("src", "//graph.facebook.com/" + this.id + "/picture"), e(this).unbind("inview")}), b()}), this.getSelectedIds = function(){
              }), b()}), this.getSelectedIds = function(){
               var t = []; return e.each(l.find(".jfmfs-friend.selected"), function(i, s){
                t.push(e(s).attr("id"))}), t},
                this.getSelectedIdsAndNames = function(){
                    var t = []; return e.each(l.find(".jfmfs-friend.selected"), function(i, s){
                    t.push({id:e(s).attr("id"), name:e(s).find(".friend-name").text()})}), t},
                this.clearSelected = function(){
                   u.removeClass("selected")}; var b = function()
               {
                 u = e(".jfmfs-friend", l), a = u.first().offset().top; for (var t = 0, i = u.length; i > t; t++)
                  {
                      if (e(u[t]).offset().top !== a){
                        o = e(u[t]).offset().top - a; break
                       }r++
                    }
            l.delegate(".jfmfs-friend", "click", function(t){
                //alert("check");
             var i = 1 === c.max_selected, s = e(this).hasClass("selected"), a = e(".jfmfs-friend.selected").length >= c.max_selected, f = v.find(".selected").attr("id") === e(this).attr("id"); 
              if (i || s || !j() || !a){if (i && !f && v.find(".selected").removeClass("selected"), e(this).toggleClass("selected"), e(this).removeClass("hover"), e(this).hasClass("selected"))if (d){if (t.shiftKey)for (var r = e(this).index(), o = d.index(), h = Math.max(r, o), p = Math.min(r, o), _ = p; h >= _; _++){var b = e(u[_]); b.hasClass("hide-non-selected") || b.hasClass("hide-filtered") || j() && e(".jfmfs-friend.selected").length < c.max_selected && e(u[_]).addClass("selected")}} else d = e(this); d = e(this), m(), j() && x(), l.trigger("jfmfs.selection.changed", [n.getSelectedIdsAndNames()])}
               }),
                e("#jfmfs-filter-selected").click(function(t){
                    t.preventDefault(), u.not(".selected").addClass("hide-non-selected"),
                    e(".filter-link").removeClass("selected"), e(this).addClass("selected")}),
                    e("#jfmfs-filter-all").click(function(t){
                        t.preventDefault(), u.removeClass("hide-non-selected"),
                        e(".filter-link").removeClass("selected"),
                        e(this).addClass("selected")}),
                    e("#jfmfs-filter-select-all").click(function(t){
                        t.preventDefault()
                        if(clicked==false)
                        {
                            $(".jfmfs-friend.selected").each(function(){
                                //$(this).addClass("selected");
                                var i = 1 === c.max_selected, s = e(this).hasClass("selected"), a = e(".jfmfs-friend.selected").length >= c.max_selected, f = v.find(".selected").attr("id") === e(this).attr("id"); 
                                    if (i || s || !j() || !a){if (i && !f && v.find(".selected").removeClass("selected"), e(this).toggleClass("selected"), e(this).removeClass("hover"), e(this).hasClass("selected"))if (d){if (t.shiftKey)for (var r = e(this).index(), o = d.index(), h = Math.max(r, o), p = Math.min(r, o), _ = p; h >= _; _++){var b = e(u[_]); b.hasClass("hide-non-selected") || b.hasClass("hide-filtered") || j() && e(".jfmfs-friend.selected").length < c.max_selected && e(u[_]).addClass("selected")}} else d = e(this); d = e(this), m(), j() && x(), l.trigger("jfmfs.selection.changed", [n.getSelectedIdsAndNames()])}
                            });
                            $(".jfmfs-friend").each(function(){
                                //$(this).addClass("selected");
                                var i = 1 === c.max_selected, s = e(this).hasClass("selected"), a = e(".jfmfs-friend.selected").length >= c.max_selected, f = v.find(".selected").attr("id") === e(this).attr("id"); 
                                    if (i || s || !j() || !a){if (i && !f && v.find(".selected").removeClass("selected"), e(this).toggleClass("selected"), e(this).removeClass("hover"), e(this).hasClass("selected"))if (d){if (t.shiftKey)for (var r = e(this).index(), o = d.index(), h = Math.max(r, o), p = Math.min(r, o), _ = p; h >= _; _++){var b = e(u[_]); b.hasClass("hide-non-selected") || b.hasClass("hide-filtered") || j() && e(".jfmfs-friend.selected").length < c.max_selected && e(u[_]).addClass("selected")}} else d = e(this); d = e(this), m(), j() && x(), l.trigger("jfmfs.selection.changed", [n.getSelectedIdsAndNames()])}
                            });
                            clicked=true;
                        }
                        else if(clicked==true)
                        {
                            $(".jfmfs-friend").each(function(){
                                
                                //$(this).removeClass("selected");
                                var i = 1 === c.max_selected, s = e(this).hasClass("selected"), a = e(".jfmfs-friend.selected").length >= c.max_selected, f = v.find(".selected").attr("id") === e(this).attr("id"); 
                                    if (i || s || !j() || !a){if (i && !f && v.find(".selected").removeClass("selected"), e(this).toggleClass("selected"), e(this).removeClass("hover"), e(this).hasClass("selected"))if (d){if (t.shiftKey)for (var r = e(this).index(), o = d.index(), h = Math.max(r, o), p = Math.min(r, o), _ = p; h >= _; _++){var b = e(u[_]); b.hasClass("hide-non-selected") || b.hasClass("hide-filtered") || j() && e(".jfmfs-friend.selected").length < c.max_selected && e(u[_]).addClass("selected")}} else d = e(this); d = e(this), m(), j() && x(), l.trigger("jfmfs.selection.changed", [n.getSelectedIdsAndNames()])}
                            });
                            clicked=false;
                        }
                        }),
                    l.find(".jfmfs-friend:not(.selected)").live("hover", function(t){
                        "mouseover" == t.type && e(this).addClass("hover"), "mouseout" == t.type && e(this).removeClass("hover")}),
                    l.find("#jfmfs-friend-filter-text").keyup(function(){
                            var t = e(this).val(); clearTimeout(s), s = setTimeout(function(){
                            "" == t?u.removeClass("hide-filtered"):(h.find(".friend-name:not(:Contains(" + t + "))").parent().addClass("hide-filtered"), h.find(".friend-name:Contains(" + t + ")").parent().removeClass("hide-filtered")),
                    f()}, 400)}).focus(function(){
                        "Start typing a name" == e.trim(e(this).val()) && e(this).val("")
                        }).blur(function(){
                                "" == e.trim(e(this).val()) && e(this).val("Start typing a name")}),
                    l.find(".jfmfs-button").hover(function(){
                        e(this).addClass("jfmfs-button-hover")},
                    function(){e(this).removeClass("jfmfs-button-hover")});
                    var f = function(){
                        var t, i = v.innerHeight(), s = v.scrollTop(), d = v.offset().top,
                        l = 0, n = !1, f = e(".jfmfs-friend:not(.hide-filtered )");
                            e.each(f, function(c, m){
                                    if (l++, null !== m)if (m = e(f[c]), t = a + o * Math.ceil(l / r) - s - d, t + o >= - 10 && i > t - o)m.data("inview", !0), m.trigger("inview", [!0]), n = !0; else if (n)return!1})
                    }, 
                    m = function(){
                        e("#jfmfs-selected-count").html(g())};
                    v.bind("scroll", e.debounce(250, f)), x(), f(), m(), l.trigger("jfmfs.friendload.finished")
                },
                    g = function(){
                        return e(".jfmfs-friend.selected").length},
                    j = function(){
                        return c.max_selected > 0},
                    x = function(){
                        var t = c.labels.max_selected_message.replace("{0}", g()).replace("{1}", c.max_selected); e("#jfmfs-max-selected-wrapper").html(t)}
    };
                    e.fn.jfmfs = function(i){
                        return this.each(function(){
                        var s = e(this); if (!s.data("jfmfs")){var a = new t(this, i); s.data("jfmfs", a)}})},
                        e.expr[":"].Contains = function(t, i, s){
                            return e(t).text().toUpperCase().indexOf(s[3].toUpperCase()) >= 0}
  }(jQuery),
    void 0 === $.debounce && !function(e, t){
     var i, s = e.jQuery || e.Cowboy || (e.Cowboy = {}); s.throttle = i = function(e, i, a, d){
     function l(){
      function s(){f = + new Date, a.apply(r, c)}
      function l(){n = t}
    var r = this, o = + new Date - f, c = arguments; d && !n && s(), n && clearTimeout(n),
        d === t && o > e?s():i !== !0 && (n = setTimeout(d?l:s, d === t?e - o:e))}
    var n, f = 0; return"boolean" != typeof i && (d = a, a = i, i = t),
        s.guid && (l.guid = a.guid = a.guid || s.guid++), l
  },
    s.debounce = function(e, s, a){return a === t?i(e, s, !1):i(e, a, s !== !1)}}(this);

</script>
<!------------------------------------------------------------------------------- -->