<?php
/**
 * Template for the register for box on the register/signin page
 *
 * You can make most changes via hooks or see the link below for info on how to replace the template in your theme.
 *
 * @link http://docs.wpgeodirectory.com/customizing-geodirectory-templates/
 * @since 1.0.0
 * @package GeoDirectory
 */
if (isset($_GET['redirect_to']) && $_GET['redirect_to'] != '') {
    $redirect_to = $_GET['redirect_to'];
} else {
    //echo $_SERVER['HTTP_HOST'] ;
    $redirect_to = isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : '';
    if (strpos($redirect_to, $_SERVER['HTTP_HOST']) === false) {
        $redirect_to = home_url();
    }
}

?>


<!--/////////////////////////like fb//////////////////////////
<script>
  window.fbAsyncInit = function() {
    FB.init({
      appId      : '1701993316716653',
      xfbml      : true,
      version    : 'v2.6'
    });
  };

  (function(d, s, id){
     var js, fjs = d.getElementsByTagName(s)[0];
     if (d.getElementById(id)) {return;}
     js = d.createElement(s); js.id = id;
     js.src = "//connect.facebook.net/en_US/sdk.js";
     fjs.parentNode.insertBefore(js, fjs);
   }(document, 'script', 'facebook-jssdk'));
</script>
<div
  class="fb-like"
  data-share="true"
  data-width="450"
  data-show-faces="true">
</div>
<!--/////////////////////////like fb//////////////////////////-->
<!--/////////////////////////login fb//////////////////////////-->
<script>
  // This is called with the results from from FB.getLoginStatus().
  function statusChangeCallback(response) {
    console.log('statusChangeCallback');
    console.log(response);
    // The response object is returned with a status field that lets the
    // app know the current login status of the person.
    // Full docs on the response object can be found in the documentation
    // for FB.getLoginStatus().
    if (response.status === 'connected') {
      // Logged into your app and Facebook.
      testAPI();
    } else if (response.status === 'not_authorized') {
      // The person is logged into Facebook, but not your app.
      document.getElementById('status').innerHTML = 'Please log ' +
        'into this app.';
    } else {
      // The person is not logged into Facebook, so we're not sure if
      // they are logged into this app or not.
      document.getElementById('status').innerHTML = 'Please log ' +
        'into Facebook.';
    }
  }
  // This function is called when someone finishes with the Login
  // Button.  See the onlogin handler attached to it in the sample
  // code below.
  function checkLoginState() {
    FB.getLoginStatus(function(response) {
      statusChangeCallback(response);
    });
  }

  window.fbAsyncInit = function() {
  FB.init({
    appId      : '1701993316716653',
    cookie     : true,  // enable cookies to allow the server to access 
                        // the session
    xfbml      : true,  // parse social plugins on this page
    version    : 'v2.5' // use graph api version 2.5
  });
  FB.Event.subscribe('auth.login', function () {
          window.location = "http://sitepro360.me/places";
      });
  

  // Now that we've initialized the JavaScript SDK, we call 
  // FB.getLoginStatus().  This function gets the state of the
  // person visiting this page and can return one of three states to
  // the callback you provide.  They can be:
  //
  // 1. Logged into your app ('connected')
  // 2. Logged into Facebook, but not your app ('not_authorized')
  // 3. Not logged into Facebook and can't tell if they are logged into
  //    your app or not.
  //
  // These three cases are handled in the callback function.

  FB.getLoginStatus(function(response) {
    statusChangeCallback(response);
  });


  };

  // Load the SDK asynchronously
  (function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = "//connect.facebook.net/en_US/sdk.js";
    fjs.parentNode.insertBefore(js, fjs);
  }(document, 'script', 'facebook-jssdk'));

  // Here we run a very simple test of the Graph API after login is
  // successful.  See statusChangeCallback() for when this call is made.
  function testAPI() {
    console.log('Welcome!  Fetching your information.... ');
    FB.api('/me/?fields=name,email,friends', function(response) {
      //console.log('Successful login for: ' + response.name);
      document.getElementById('status').innerHTML =
        'Thanks for logging in, ' + response.name + '!';
    });
  }
  function logout() {
            FB.logout(function(response) {
              // user is now logged out
            });
        }
</script>

<!--
  Below we include the Login Button social plugin. This button uses
  the JavaScript SDK to present a graphical Login button that triggers
  the FB.login() function when clicked.
-->
<fb:login-button scope="public_profile,email" onlogin="checkLoginState();">
</fb:login-button>
<button onclick="javascript:logout();">FB Logout</button>
<div id="status">
</div>
<!--/////////////////////////login fb//////////////////////////-->

        
    <script src='http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js'>
        </script>
    <!--<script src="jquery-2.2.4.js"></script>-->

    <script>
 
    
    $(document).ready(function() {
   $('input[type="radio"]').click(function() {
       if($(this).attr('value') == 'bus') {
            $('#bus').show();           
       }

       else if($(this).attr('value') == 'ind') {
            $('#bus').hide();   
       }
   });
});
 
    </script>
<div id="sign_up">
    <div class="login_content">
        <?php echo stripslashes(get_option('ptthemes_reg_page_content')); ?>
    </div>
    <div class="registration_form_box">
        <h4>
            <?php

            /**
             * Filter the `REGISTRATION_NOW_TEXT` title text on the register form template.
             *
             * @since 1.0.0
             */
            echo apply_filters('geodir_registration_page_title', REGISTRATION_NOW_TEXT);

            ?>
        </h4>
        <?php
        global $geodir_signup_error;
        if ($geodir_signup_error != '') {
            echo '<p class="error_msg">' . $geodir_signup_error . '</p>';
            unset($geodir_signup_error);
        } else {
            if (isset($_REQUEST['emsg']) && $_REQUEST['emsg'] == 1) {
                echo '<p class="error_msg">' . EMAIL_USERNAME_EXIST_MSG . '</p>';
            } else if (isset($_REQUEST['emsg']) && $_REQUEST['emsg'] == 'regnewusr') {
                echo '<p class="error_msg">' . REGISTRATION_DESABLED_MSG . '</p>';
            }
        }
        ?>

        <form name="cus_registerform" id="cus_registerform"
              action="<?php echo htmlspecialchars(geodir_curPageURL()); ?>" method="post">
            <input type="hidden" name="action" value="register"/>
            <input type="hidden" name="redirect_to" value="<?php echo htmlspecialchars($redirect_to); ?>"/>

            <div class="form_row clearfix">
                <input placeholder='<?php echo EMAIL_TEXT; ?>' type="text" name="user_email" id="user_email"
                       class="textfield" value="<?php global $user_email;
                if (!isset($user_email)) {
                    $user_email = '';
                }
                echo esc_attr(stripslashes($user_email)); ?>" size="25"/>

                <?php if (!get_option('geodir_allow_cpass')) { ?>
                <div id="reg_passmail">
                    <?php echo REGISTRATION_MESSAGE; ?>
                </div>
                <?php } ?>
                <span id="user_emailInfo"></span>
            </div>

            <div class="row_spacer_registration clearfix">
                <div class="form_row clearfix">
                    <input placeholder='<?php echo FIRST_NAME_TEXT; ?>' type="text" name="user_fname" id="user_fname"
                           class="textfield" value="<?php if (isset($user_fname)) {
                        echo esc_attr(stripslashes($user_fname));
                    } ?>" size="25"/>
                    <span id="user_fnameInfo"></span>
                </div>
            </div>
            <div id="reg_passmail">

                    Select a category from the following:
            </div>
            <div class="form_row_clearfix">
                <input type="radio" id="user_category" name="user_category" value="ind" checked> Individual<br>
                <input type="radio" id="user_category" name="user_category" value="bus"> Businessman<br>
                    <div id="bus" class="desc" style="display: none;">
                        <div id="reg_passmail">

                            Select Business type from the following:
                        </div>
                        <input type="radio" id="business_category" name="business_category" value="pl" checked> Places<br>
                        <input type="radio" id="business_category" name="business_category" value="ev"> Events<br>
                    </div>
            </div>

            <?php if (get_option('geodir_allow_cpass')) { ?>

                <div class="row_spacer_registration clearfix">
                    <div class="form_row clearfix">
                        <input placeholder='<?php echo PASSWORD_TEXT; ?>' type="password" name="user_pass"
                               id="user_pass1" class="textfield input-text" value="" size="25"/>
                        <span id="user_passInfo1"></span>
                    </div>
                </div>

                <div class="row_spacer_registration clearfix">
                    <div class="form_row clearfix">
                        <input placeholder='<?php echo CONFIRM_PASSWORD_TEXT; ?>' type="password" name="user_pass2"
                               id="user_pass2" class="textfield input-text" value="" size="25"/>
                        <span id="user_passInfo2"></span>
                    </div>
                </div>

            <?php } ?>

            <?php
            /**
             * Called just before the register new user button on the register form template.
             *
             * Also used by other plugins to add social connect buttons.
             *
             * @since 1.0.0
             */
            do_action('social_connect_form');
            //remove_action('social_connect_form');
             ?>
            <input type="submit" name="registernow" value="<?php echo REGISTER_NOW_TEXT; ?>" class="geodir_button"/>
        </form>
    </div>

</div>