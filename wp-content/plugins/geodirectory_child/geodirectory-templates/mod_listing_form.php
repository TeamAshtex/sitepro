<form name="propertyform" id="propertyform" action="<?php echo get_page_link(geodir_preview_page_id());?>" method="post" enctype="multipart/form-data" >
        <input type="hidden" name="preview" value="<?php echo sanitize_text_field($listing_type);?>"/>
        <input type="hidden" name="listing_type" value="<?php echo sanitize_text_field($listing_type);?>"/>
        <?php if ($page_id) { ?>
        <input type="hidden" name="add_listing_page_id" value="<?php echo $page_id;?>"/>
        <?php } if (isset($_REQUEST['pid']) && $_REQUEST['pid'] != '') { ?>
            <input type="hidden" name="pid" value="<?php echo sanitize_text_field($_REQUEST['pid']);?>"/>
        <?php } if (isset($_REQUEST['backandedit'])) { ?>
            <input type="hidden" name="backandedit" value="<?php echo sanitize_text_field($_REQUEST['backandedit']);?>"/>
        <?php
        } 
        /**
         * Called at the very top of the add listing page form for frontend.
         *
         * This is called just before the "Enter Listing Details" text.
         *
         * @since 1.0.0
         */
        do_action('geodir_before_detail_fields');
        ?>
        <h5 id="geodir_fieldset_details" class="geodir-fieldset-row" gd-fieldset="details"><?php echo LISTING_DETAILS_TEXT;?></h5>
        <?php
        /**
         * Called at the top of the add listing page form for frontend.
         *
         * This is called after the "Enter Listing Details" text.
         *
         * @since 1.0.0
         */
        do_action('geodir_before_main_form_fields');
        ?>
        <div id="geodir_post_title_row" class="required_field geodir_form_row clearfix gd-fieldset-details">
            <label><?php echo PLACE_TITLE_TEXT;?><span>*</span> </label>
            <input type="text" field_type="text" name="post_title" id="post_title" class="geodir_textfield"
                   value="<?php echo esc_attr(stripslashes($title)); ?>"/>
            <span class="geodir_message_error"><?php _e($required_msg, 'geodirectory');?></span>
        </div>
        <?php
        $show_editor = get_option('geodir_tiny_editor_on_add_listing');

        $desc = $show_editor ? stripslashes($desc) : esc_attr(stripslashes($desc));
        $desc_limit = '';
        /**
         * Filter the add listing description field character limit number.
         *
         * @since 1.0.0
         * @param int $desc_limit The amount of characters to limit the description to.
         */
        $desc_limit = apply_filters('geodir_description_field_desc_limit', $desc_limit);
        /**
         * Filter the add listing description field text.
         *
         * @since 1.0.0
         * @param string $desc The text for the description field.
         * @param int $desc_limit The character limit numer if any.
         */
        $desc = apply_filters('geodir_description_field_desc', $desc, $desc_limit);
        $desc_limit_msg = '';
        /**
         * Filter the add listing description limit message.
         *
         * This is the message shown if there is a limit applied to the amount of characters the description can use.
         *
         * @since 1.0.0
         * @param string $desc_limit_msg The limit message string if any.
         * @param int $desc_limit The character limit numer if any.
         */
        $desc_limit_msg = apply_filters('geodir_description_field_desc_limit_msg', $desc_limit_msg, $desc_limit);
        
        $desc_class = '';
        if ($desc_limit === '' || (int)$desc_limit > 0) {
            /**
             * Called on the add listing page form for frontend just before the description field.
             *
             * @since 1.0.0
             */
            do_action('geodir_before_description_field');
            
            $desc_class = ' required_field';
        } else {
            $desc_class = ' hidden';
        }
        ?>
        <div id="geodir_post_desc_row" class="geodir_form_row clearfix gd-fieldset-details<?php echo $desc_class;?>">
            <label><?php echo PLACE_DESC_TEXT;?><span><?php if ($desc_limit != '0') { echo '*'; } ?></span> </label>
            <?php
            if (!empty($show_editor) && in_array($listing_type, $show_editor)) {
                $editor_settings = array('media_buttons' => false, 'textarea_rows' => 10);
            ?>
                <div class="editor" field_id="post_desc" field_type="editor">
                    <?php wp_editor($desc, "post_desc", $editor_settings); ?>
                </div>
            <?php if ($desc_limit != '') { ?>
                <script type="text/javascript">jQuery('textarea#post_desc').attr('maxlength', "<?php echo $desc_limit;?>");</script>
            <?php } } else { ?>
                <textarea field_type="textarea" name="post_desc" id="post_desc" class="geodir_textarea" maxlength="<?php echo $desc_limit; ?>"><?php echo $desc; ?></textarea>
            <?php } if ($desc_limit_msg != '') { ?>
                <span class="geodir_message_note"><?php echo $desc_limit_msg; ?></span>
            <?php } ?>
            <span class="geodir_message_error"><?php echo _e($required_msg, 'geodirectory');?></span>
        </div>
        <?php
        if ($desc_limit === '' || (int)$desc_limit > 0) {
            /**
             * Called on the add listing page form for frontend just after the description field.
             *
             * @since 1.0.0
             */
            do_action('geodir_after_description_field');
        }
        
        $kw_tags = esc_attr(stripslashes($kw_tags));
        $kw_tags_count = TAGKW_TEXT_COUNT;
        $kw_tags_msg = TAGKW_MSG;
        /**
         * Filter the add listing tags character limit.
         *
         * @since 1.0.0
         * @param int $kw_tags_count The character count limit if any.
         */
        $kw_tags_count = apply_filters('geodir_listing_tags_field_tags_count', $kw_tags_count);
        /**
         * Filter the add listing tags field value.
         *
         * You can use the $_REQUEST values to check if this is a go back and edit value etc.
         *
         * @since 1.0.0
         * @param string $kw_tags The tag field value, usually a comma separated list of tags.
         * @param int $kw_tags_count The character count limit if any.
         */
        $kw_tags = apply_filters('geodir_listing_tags_field_tags', $kw_tags, $kw_tags_count);
        /**
         * Filter the add listing tags field message text.
         *
         * @since 1.0.0
         * @param string $kw_tags_msg The message shown under the field.
         * @param int $kw_tags_count The character count limit if any.
         */
        $kw_tags_msg = apply_filters('geodir_listing_tags_field_tags_msg', $kw_tags_msg, $kw_tags_count);
        
        $tags_class = '';
        if ($kw_tags_count === '' || (int)$kw_tags_count > 0) {
            /**
             * Called on the add listing page form for frontend just before the tags field.
             *
             * @since 1.0.0
             */
            do_action('geodir_before_listing_tags_field');
        } else {
            $tags_class = ' hidden';
        }
        ?>
        <div id="geodir_post_tags_row" class="geodir_form_row clearfix gd-fieldset-details<?php echo $tags_class;?>">
            <label><?php echo TAGKW_TEXT; ?></label>
            <input name="post_tags" id="post_tags" value="<?php echo $kw_tags; ?>" type="text" class="geodir_textfield"
                   maxlength="<?php echo $kw_tags_count;?>"/>
            <span class="geodir_message_note"><?php echo $kw_tags_msg;?></span>
        </div>
        <?php
        if ($kw_tags_count === '' || (int)$kw_tags_count > 0) {
            /**
             * Called on the add listing page form for frontend just after the tags field.
             *
             * @since 1.0.0
             */
            do_action('geodir_after_listing_tags_field');
        }
        
        $package_info = array();
        $package_info = geodir_post_package_info($package_info, $post);
        
        geodir_get_custom_fields_html($package_info->pid, 'all', $listing_type);
        
        // adjust values here
        $id = "post_images"; // this will be the name of form field. Image url(s) will be submitted in $_POST using this key. So if $id == �img1� then $_POST[�img1�] will have all the image urls

        $multiple = true; // allow multiple files upload

        $width = geodir_media_image_large_width(); // If you want to automatically resize all uploaded images then provide width here (in pixels)

        $height = geodir_media_image_large_height(); // If you want to automatically resize all uploaded images then provide height here (in pixels)

        $thumb_img_arr = array();
        $totImg = 0;
        if (isset($_REQUEST['backandedit']) && empty($_REQUEST['pid'])) {
            $post = (object)$gd_session->get('listing');
            if (isset($post->post_images))
                $curImages = trim($post->post_images, ",");


            if ($curImages != '') {
                $curImages_array = explode(',', $curImages);
                $totImg = count($curImages_array);
            }

            $listing_type = $post->listing_type;

        } elseif (isset($_REQUEST['pid']) && $_REQUEST['pid'] != '') {
            $post = geodir_get_post_info((int)$_REQUEST['pid']);
            $listing_type = $post->post_type;
            $thumb_img_arr = geodir_get_images($_REQUEST['pid']);

        } else {
            $listing_type = sanitize_text_field($_REQUEST['listing_type']);
        }


        if (!empty($thumb_img_arr)) {
            foreach ($thumb_img_arr as $img) {
                //$curImages = $img->src.",";
            }

            $totImg = count((array)$thumb_img_arr);
        }

        if ($curImages != '')
            $svalue = $curImages; // this will be initial value of the above form field. Image urls.
        else
            $svalue = '';

        $image_limit = $package_info->image_limit;
        $show_image_input_box = ($image_limit != '0');
        /**
         * Filter to be able to show/hide the image upload section of the add listing form.
         *
         * @since 1.0.0
         * @param bool $show_image_input_box Set true to show. Set false to not show.
         * @param string $listing_type The custom post type slug.
         */

        $show_image_input_box = apply_filters('geodir_image_uploader_on_add_listing', $show_image_input_box, $listing_type);
        if ($show_image_input_box) {
            ?>
<div id='loader' style='display:none' align = 'center'>
       <img src='http://circleofbiz.com/wp-content/uploads/loading.gif' />
</div>
            <h5 id="geodir_form_title_row" class="geodir-form_title"> <?php echo PRO_PHOTO_TEXT;?>
                <?php if ($image_limit == 1) {
                    echo '<br /><small>(' . __('You can upload', 'geodirectory') . ' ' . $image_limit . ' ' . __('image with this package', 'geodirectory') . ')</small>';
                } ?>
                <?php if ($image_limit > 1) {
                    echo '<br /><small>(' . __('You can upload', 'geodirectory') . ' ' . $image_limit . ' ' . __('images with this package', 'geodirectory') . ')</small>';
                } ?>

                <?php if ($image_limit == '') {
                    echo '<br /><small>(' . __('You can upload unlimited images with this package', 'geodirectory') . ')</small>';
                } ?>
            </h5>

            <div class="geodir_form_row clearfix" id="<?php echo $id; ?>dropbox"
                 style="border:1px solid #ccc;min-height:100px;height:auto;padding:10px;text-align:center;">
                <input type="hidden" name="<?php echo $id; ?>" id="<?php echo $id; ?>" value="<?php echo $svalue; ?>"/>
                <input type="hidden" name="<?php echo $id; ?>image_limit" id="<?php echo $id; ?>image_limit"
                       value="<?php echo $image_limit; ?>"/>
                <input type="hidden" name="<?php echo $id; ?>totImg" id="<?php echo $id; ?>totImg"
                       value="<?php echo $totImg; ?>"/>

                <div
                    class="plupload-upload-uic hide-if-no-js <?php if ($multiple): ?>plupload-upload-uic-multiple<?php endif; ?>"
                    id="<?php echo $id; ?>plupload-upload-ui">
                    <h4><?php _e('Drop files to upload', 'geodirectory');?></h4><br/>
                    <input id="<?php echo $id; ?>plupload-browse-button" type="button"
                           value="<?php esc_attr_e('Select Files', 'geodirectory'); ?>" class="geodir_button"/>
                    <span class="ajaxnonceplu"
                          id="ajaxnonceplu<?php echo wp_create_nonce($id . 'pluploadan'); ?>"></span>
                    <?php if ($width && $height): ?>
                        <span class="plupload-resize"></span>
                        <span class="plupload-width" id="plupload-width<?php echo $width; ?>"></span>
                        <span class="plupload-height" id="plupload-height<?php echo $height; ?>"></span>
                    <?php endif; ?>
                    <div class="filelist"></div>
                </div>

                <div class="plupload-thumbs <?php if ($multiple): ?>plupload-thumbs-multiple<?php endif; ?> clearfix"
                     id="<?php echo $id; ?>plupload-thumbs" style="border-top:1px solid #ccc; padding-top:10px;">
                </div>
                <span
                    id="upload-msg"><?php _e('Please drag &amp; drop the images to rearrange the order', 'geodirectory');?></span>
                <span id="<?php echo $id; ?>upload-error" style="display:none"></span>
            </div>

        <?php } ?>

        <?php
        /**
         * Called on the add listing page form for frontend just after the image upload field.
         *
         * @since 1.0.0
         */
        do_action('geodir_after_main_form_fields');?>


        <!-- add captcha code -->

        <script>
 var $ = jQuery;

   

    $(document).ready(function() {

$('#submit').click(function() 
    {
        $('#loader').show();
        $('#loader').delay(10000).fadeOut();
    }) 

});
            <!--<script>-->
            document.write('<inp' + 'ut type="hidden" id="geodir_sp' + 'amblocker_top_form" name="geodir_sp' + 'amblocker" value="64"/>')
        </script>
        <noscript>
            <div>
                <label><?php _e('Type 64 into this box', 'geodirectory');?></label>
                <input type="text" id="geodir_spamblocker_top_form" name="geodir_spamblocker" value="" maxlength="10"/>
            </div>
        </noscript>
        <input type="text" id="geodir_filled_by_spam_bot_top_form" name="geodir_filled_by_spam_bot" value=""/>


        <!-- end captcha code -->

        <div id="geodir-add-listing-submit" class="geodir_form_row clear_both" style="padding:2px;text-align:center;">
            <input type="submit" id ="submit" value="<?php echo PRO_PREVIEW_BUTTON;?>"
                   class="geodir_button" <?php echo $submit_button;?>/>
            <span class="geodir_message_note"
                  style="padding-left:0px;"> <?php _e('Note: You will be able to see a preview in the next page', 'geodirectory');?></span>
        </div>

    </form>