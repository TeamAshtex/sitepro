<?php
/**
 * This is a wrapper function used to add comments to a block of code using ApiGen 2.8.0. 
 * 
 * This block of code represents an action <invite_ajax> of a controller <friends> in the 
 * optimus framework.
 * 
 * The AJAX call handler for the friends controller. Picks up the information of those users
 * who have been invited using the custom facebook friends selector.
 * 
 * @param   mixed $POST All the information is extracted from the POST array which is passed down by the JS AJAX CORE Library
 *
 * @return  void
 * 
 * @since   2012-10-01
 * 
 * @author  M. Usman Ashraf <usman.ashraf@ashtexsolutions.com>
 *
 */
function core_friends_invite_ajax($POST)
{
    return;
}
$uplevel = '../../';
include_once $uplevel.'config.php';
include_once $uplevel.INCLUDE_URL.'error_functions.php';
include_once $uplevel.INCLUDE_URL.'global.php';
include_once $uplevel.INCLUDE_URL.'user_functions.php';


if(isset($_REQUEST['type']))
{
	
	if($_REQUEST['type'] == VALIDATE)
	{
		if( isset($_REQUEST['list']) )
		{
			$list = explode(',',$_REQUEST['list']);
			$fbuser = $fb->getUser();
			$user = getOne($fbuser,1);
			date_default_timezone_set('Europe/London');
			$date = date('Y-m-d H:i:s');
			foreach($list as $li)
			{
				$out = $db->fetch_array($db->query_silent("SELECT count(*) from ".USER_INVITE." WHERE friend_uid = $li and user_id = {$user['user_id']}"),MYSQL_NUM);
				if($out[0] > 0)
				{
					$db->query_silent("update ".USER_INVITE." set last_update_date = '$date' WHERE friend_uid = $li and user_id = {$user['user_id']}");
				}
				else
				{
					insert_table(USER_INVITE, 'user_id,user_uid,friend_uid,last_update_date', $user['user_id'].', '.$user['user_uid'].', '.$li.',"'.$date.'"');
				}
			}		
		}
		else 
		{
			//some seriously shitty problem if we are landing here
		}
	}
	else {}
}
?>