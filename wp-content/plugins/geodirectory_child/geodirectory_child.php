<?php
session_start();

$_SESSION['g_count']=0;
/*
Plugin Name: geodirectory_child
Plugin URI: http://localhost/
Description: A pugin that will customize your plugin's functionality!!
Version: 1
Author: Usman Ashraf
Author URI: http://localhost/
*/
//$P$Bn8qte.TAaqK8C3LAmRFbaOxppW/O00
//$P$Bfw51YEkz/ugw3TAwnY8pstB0hTkuJ1
/**
 * Template functions that affect the output of most GeoDirectory pages
 *
 * @since 1.0.0
 * @package GeoDirectory
 */
###############################################
########### DYNAMIC CONTENT ###################
###############################################

/**
 * Outputs the CSS from the compatibility settings page.
 *
 * @since 1.0.0
 * @package GeoDirectory
 */

add_action( 'the_champ_login_user' , 'social_login_usermeta', 10);
function social_login_usermeta($userId, $profileData = array(), $socialId = '', $update = false)
{
    $category_check = metadata_exists('user', $userId, 'user_category');
    if($category_check===false)
    {
        if($_SESSION['cat']=="ind")
        {
            update_user_meta( $userId, 'user_category', $_SESSION['cat'] );
        }
        else if($_SESSION['cat']=="bus")
        {
            update_user_meta( $userId, 'user_category', $_SESSION['cat'] );
            update_user_meta( $userId, 'business_category', $_SESSION['sub_cat'] );   
        }
    }
    global $wpdb;
    //global $current_user;
    //$current_user = $user_id;
    $child_id = $userId;
    $table_posts = $wpdb->prefix . 'posts';
    $table_usermeta = $wpdb->prefix . 'usermeta';
    $table_affiliates = $wpdb->prefix . 'uap_affiliates';
    $business_check_query = "SELECT meta_value FROM ".$table_usermeta.
    " WHERE meta_key='user_category' AND user_id='$userId'";
    $business_check = $wpdb->get_row($business_check_query,ARRAY_N);

    if($business_check[0] == 'bus')
    {

    $duplicate_query = "SELECT id FROM ".$table_affiliates. " WHERE uid='$userId'";
    $duplicate = $wpdb->get_row($duplicate_query,ARRAY_N);
    if($duplicate[0] == '')
    {
        
    
            //echo "check";
            $count_query = "SELECT MAX(id) FROM ".$table_affiliates;
            $count = $wpdb->get_row($count_query,ARRAY_N);
            $counter = $count[0] + 1;
            //echo $counter;
            $sql = "INSERT INTO ".$table_affiliates." VALUES ('$counter', '$child_id', 1, DEFAULT, 1)";
            $wpdb->query($sql);
        
    }
    }
}
add_shortcode('slider-view','view_slider' );
function view_slider()
{
    ?>
    <!DOCTYPE html>
<html>
<head>
    <title></title>

    <!-- mightySlider basic stylesheet with skins imported -->
    <link rel="stylesheet" type="text/css" href="mightyslider/src/css/mightyslider.css"/>

    <style>
        #example {
            position: relative;
            overflow: hidden;
            margin-top: 61px;
            background: transparent;
        }
        #example .frame {
            position: relative;
            width: 100%;
            padding: 20px 0;
            padding-bottom: 30px;
        }
        #example .frame .slide_element {
            height: 100%;
        }
        #example .frame .slide_element .slide {
            float: left;
            height: 100%;
            margin: 0 -350px;
            opacity: 0;
            background: #000;
            z-index: 5;
            border-radius: 10px;

            -webkit-transform: scale(0.7);
            transform: scale(0.7);

            -webkit-transition: -webkit-transform 1500ms cubic-bezier(0.190, 1.000, 0.220, 1.000);
            -webkit-transition-property: opacity, -webkit-transform;
            transition: transform 1500ms cubic-bezier(0.190, 1.000, 0.220, 1.000);
            transition-property: opacity, transform;
        }
        #example .frame .slide_element .slide.next_1,
        #example .frame .slide_element .slide.prev_1 {
            opacity: 0.7;
            z-index: 9;

            -webkit-transform: scale(0.9);
            transform: scale(0.9);
        }
        #example .frame .slide_element .slide.next_2,
        #example .frame .slide_element .slide.prev_2 {
            opacity: 0.4;
            z-index: 8;

            -webkit-transform: scale(0.8);
            transform: scale(0.8);
        }
        #example .frame .slide_element .slide.active {
            opacity: 1;
            z-index: 10;

            -webkit-transform: scale(1);
            transform: scale(1);
        }
        #example #progress {
            position: absolute;
            border-radius: 50%;
            margin: auto;
            bottom: -40px;
            left: 0;
            right: 0;

            -webkit-transform: scale(0.5);
            transform: scale(0.5);
        }
        #example #thumbnails {
            display: block;
            text-align: center;
            height: 80px;
        }
        #example #thumbnails > div {
            height: 100%;
            width: 100%;
        }
        #example #thumbnails > div > ul {
            list-style: none;
            margin: 0;
            padding: 0;
            height: 100%;
        }
        #example #thumbnails > div > ul > li {
            width: 80px;
            height: 80px;
            margin: 0 5px;
            cursor: pointer;
            padding: 3px;
            border: 4px transparent solid;
            border-radius: 50%;
            opacity: 0.6;
        }
        #example #thumbnails > div > ul > li img {
            width: 100%;
            height: 100%;
            border-radius: 50%;
        }
        #example #thumbnails > div > ul > li.active {
            opacity: 1;
        }
        #example .mSButtons.mSPrev {
            left: 15%;
            right: auto;
            bottom: 30px;
        }
        #example .mSButtons.mSNext {
            right: 15%;
            left: auto;
            bottom: 30px;
        }
        #example.isTouch .mSButtons {
            display: none;
        }
        #example .mSCaption {
            left: 20px;
            bottom: 40px;
            font-size: 18px;
            line-height: normal;
            font-weight: 300;
            color: #000;
            opacity: 0;
            letter-spacing: -1px;
            white-space: nowrap;
            text-transform: uppercase;
            z-index: 1002;

            -webkit-transition: all 0.3s;
            transition: all 0.3s;
        }
        #example .mSCaption.showed {
            bottom: 20px;
            opacity: 1;
        }
    </style>
</head>
<body>

    <!-- PARENT -->
    <div id="example" class="mightyslider_modern_skin black">
        <!-- FRAME -->
        <div class="frame" data-mightyslider="
            width: 1585,
            height: 500
        ">
            <!-- SLIDEELEMENT -->
            <div class="slide_element">
                <!-- SLIDES -->
                <div class="slide prev_2" data-mightyslider="
                    cover: 'MS-test-day-27898.jpg',
                    thumbnail: 'MS-test-day-27898_thumb.jpg'
                "></div>
                <div class="slide prev_1" data-mightyslider="
                    cover: 'MS-test-day-27771.jpg',
                    thumbnail: 'MS-test-day-27771_thumb.jpg'
                "></div>
                <div class="slide active" data-mightyslider="
                    cover: 'MS-test-day-27845.jpg',
                    video: 'http://vimeo.com/32685545',
                    thumbnail: 'MS-test-day-27845_thumb.jpg'
                ">
                    <!-- LAYER -->
                    <div class="mSCaption">
                        You can use direct video url<br />for full-sized videos & covers
                    </div>
                </div>
                <div class="slide next_1" data-mightyslider="
                    cover: 'MS-test-day-27780.jpg',
                    thumbnail: 'MS-test-day-27780_thumb.jpg'
                "></div>
                <div class="slide next_2" data-mightyslider="
                    cover: 'MS-test-day-27749.jpg',
                    thumbnail: 'MS-test-day-27749_thumb.jpg'
                "></div>
                <div class="slide" data-mightyslider="
                    cover: 'MS-test-day-278081.jpg',
                    thumbnail: 'MS-test-day-278081_thumb.jpg'
                "></div>
                <!-- END OF SLIDES -->
            </div>
            <!-- END OF SLIDEELEMENT -->
            <!-- ARROW BUTTONS -->
            <a class="mSButtons mSPrev"></a>
            <a class="mSButtons mSNext"></a>
        </div>
        <!-- END OF FRAME -->
        <!-- SLIDER TIMER -->
        <canvas id="progress" width="160" height="160"></canvas>
        <!-- END OF SLIDER TIMER -->
        <!-- THUMBNAILS -->
        <div id="thumbnails">
            <div>
                <ul></ul>
            </div>
        </div>
        <!-- END OF THUMBNAILS -->
    </div>
    <!-- END OF PARENT -->

    <!-- mightySlider requires jQuery 1.7+  -->
    <!-- If you already have jQuery on your page, you shouldn't include it second time. -->
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
    <!-- Uses the built in easing capabilities added In jQuery 1.1 to offer multiple easing options -->
    <script type="text/javascript" src="assets/js/jquery.easing-1.3.pack.js"></script>
    <!-- Mobile touch events for jQuery -->
    <script type="text/javascript" src="assets/js/jquery.mobile.just-touch.js"></script>
    <!-- mightySlider layers animation engine -->
    <script type="text/javascript" src="mightyslider/src/js/tweenlite.js"></script>
    <!-- Main slider JS script file -->
    <script type="text/javascript" src="mightyslider/src/js/mightyslider.min.js"></script>

    <script type="text/javascript">
        /**
         * Return value from percent of a number.
         *
         * @param {Number} percent
         * @param {Number} total
         *
         * @return {Number}
         */
        function percentToValue(percent, total) {
            return parseInt((total / 100) * percent);
        }

        /**
         * Convert degree to radian
         *
         * @param {Number}   degree
         *
         * @return {Number}
         */
        function degreeToRadian(degree) {
            return ((degree - 90) * Math.PI) / 180;
        }

        jQuery(document).ready(function($) {
            var $win = $(window),
                isTouch = !!('ontouchstart' in window),
                clickEvent = isTouch ? 'tap' : 'click';

            (function(){
                // Global slider's DOM elements
                var $example = $('#example'),
                    $frame = $('.frame', $example),
                    $slides = $('.slide_element', $frame).children(),
                    $thumbnailsBar = $('div#thumbnails ul', $example),
                    $timerEL = $('canvas', $example),
                    ctx = $timerEL[0] && $timerEL[0].getContext("2d"),
                    slideSize = '70%',
                    lastIndex = -1;

                /**
                 * Draw arc on canvas element
                 *
                 * @param {Number}   angle
                 *
                 * @return {Void}
                 */
                function drawArc(angle) {
                    var startingAngle = degreeToRadian(0),
                        endingAngle = degreeToRadian(angle),
                        size = 160,
                        center = size / 2;

                    //360Bar
                    ctx.clearRect(0, 0, size, size);
                    ctx.beginPath();
                    ctx.arc(center, center, center-4, startingAngle, endingAngle, false);
                    ctx.lineWidth = 8;
                    ctx.strokeStyle = "#aaa";
                    ctx.lineCap = "round";
                    ctx.stroke();
                    ctx.closePath();
                }

                // Calling mightySlider via jQuery proxy
                $frame.mightySlider({
                    speed: 1500,
                    startAt: 2,
                    autoScale: 1,
                    easing: 'easeOutExpo',
                    
                    // Navigation options
                    navigation: {
                        slideSize: slideSize,
                        keyboardNavBy: 'slides',
                        activateOn: clickEvent
                    },

                    // Thumbnails options
                    thumbnails: {
                        thumbnailsBar: $thumbnailsBar,
                        thumbnailNav: 'forceCentered',
                        activateOn: clickEvent,
                        scrollBy: 0
                    },

                    // Dragging options
                    dragging: {
                        mouseDragging: 0,
                        onePage: 1
                    },

                    // Buttons options
                    buttons: !isTouch ? {
                        prev: $('a.mSPrev', $frame),
                        next: $('a.mSNext', $frame)
                    } : {},

                    // Cycling options
                    cycling: {
                        cycleBy: 'slides'
                    }
                },

                // Register callbacks to the events
                {
                    // Register mightySlider :active event callback
                    active: function(name, index) {
                        if (lastIndex !== index) {
                            // Hide the timer
                            $timerEL.stop().css({ opacity: 0 });

                            // Remove next and previous classes from the slides
                            $slides.removeClass('next_1 next_2 prev_1 prev_2');

                            // Detect next and prev slides
                            var next1 = this.slides[index + 1],
                                next2 = this.slides[index + 2],
                                prev1 = this.slides[index - 1],
                                prev2 = this.slides[index - 2];

                            // Add next and previous classes to the slides
                            next1 && $(next1.element).addClass('next_1');
                            next2 && $(next2.element).addClass('next_2');
                            prev1 && $(prev1.element).addClass('prev_1');
                            prev2 && $(prev2.element).addClass('prev_2');
                        }

                        lastIndex = index;
                    },

                    // Register mightySlider :moveEnd event callback
                    moveEnd: function() {
                        // Reset cycling progress time elapsed
                        this.progressElapsed = 0;
                        // Fade in the timer
                        $timerEL.animate({ opacity: 1 }, 800);
                    },

                    // Register mightySlider :progress event callback
                    progress: function(name, progress) {
                        // Draw circle bar timer based on progress
                        drawArc(360 - (360 / 1 * progress));
                    },

                    // Register mightySlider :initialize and :resize event callback
                    'initialize resize': function(name) {
                        var self = this,
                            frameSize = self.relative.frameSize,
                            slideSizePixel = percentToValue(slideSize.replace('%', ''), frameSize),
                            remainedSpace = (frameSize - slideSizePixel),
                            margin = (slideSizePixel - remainedSpace / 3) / 2;

                        // Sets slides margin
                        $slides.css('margin', '0 -' + margin + 'px');
                        // Reload immediate
                        self.reload(1);
                    }
                });
            })();
        });
    </script>
</body>
</html>
    <?php
}
add_shortcode('ask_category','ask_category_page' );
function ask_category_page()
{
    //$output = "something..";
    //return $output;
    global $current_user;
    $current_user = get_current_user_id();
    $_SESSION['id'] = $current_user;
    ?>
    <script>
 var $ = jQuery;
    
    $(document).ready(function() {
   $('input[type="radio"]').click(function() {
       if($(this).attr('value') == 'bus') {
            //$('#bus').show();           
       }

       else if($(this).attr('value') == 'ind') {
            $('#bus').hide();   
       }
   });
   $('#form').on('submit', function(e) {
            e.preventDeffault();
            $('input[type="submit"]').prop('disabled', true);
            $("input[type='submit']").unbind("mouseenter mouseleave");
            var bus_cat='';
            var user_cat = $("input[name=user_category]:checked").val();
            if(user_cat=='bus')
            {
                bus_cat = 'pl';
                jQuery.ajax ( 
                data = {
                action: 'submit_category',
                url: '/wp-admin/admin-ajax.php',
                type: 'POST',
                dataType: 'text',
                'user_category' : user_cat,
                'business_category' : bus_cat
                

            });
            //alert("test");
            jQuery.post(ajaxurl, data, function(response) {

                var dataz = response;
                //document.getElementById('friends_list').innerHTML += dataz;
                //alert( dataz );
                console.log (dataz); //show json in console
                window.top.location.href = <?php echo site_url(); ?>;


            });
            }
            else if(user_cat=='ind')
            {
                jQuery.ajax ( 
                data = {
                action: 'submit_category',
                url: '/wp-admin/admin-ajax.php',
                type: 'POST',
                dataType: 'text',
                'user_category' : user_cat,
                'business_category' : bus_cat
                

            });
            //alert("test");
            jQuery.post(ajaxurl, data, function(response) {

                var dataz = response;
                //document.getElementById('friends_list').innerHTML += dataz;
                //alert( dataz );
                console.log (dataz); //show json in console
                window.top.location.href = <?php echo site_url(); ?>;


            });
            }
            });

});
 
    </script>
    <form name="form" method="post" id="form" autocomplete="off">
    <h1>Choose a Category</h1>
    <div id="reg_cat">

                    Select a category from the following:
            </div>
            <div class="form_row_clearfix">
                <input type="radio" id="user_category" name="user_category" value="ind" checked> Individual<br>
                <input type="radio" id="user_category" name="user_category" value="bus"> Businessman<br>
                    <div id="bus" class="bus" style="display: none;">
                        <div id="reg_bus">

                            Select Business type from the following:
                        </div>
                        <input type="radio" id="business_category" name="business_category" value="pl" checked> Places<br>
                        <input type="radio" id="business_category" name="business_category" value="ev"> Events<br>
                    </div>
            </div>
            <div id="publishing-action">
                <span class="spinner"></span>
                <input name="original_publish" type="hidden" id="original_publish" value="Publish">
                <input type="submit" name="publish" id="publish" class="button button-primary button-large" value="Submit">
            </div>
            
        </form>
            <?php
}
add_action("wp_ajax_submit_category", "submit_category");
add_action("wp_ajax_nopriv_submit_category", "submit_category");
function submit_category()
{
    global $current_user;
    $current_user = get_current_user_id();
    $user_cat =  $_POST['user_category'];
    $bus_cat =  $_POST['business_category'];
    if($bus_cat=='')
    {
        update_user_meta($current_user, 'user_category', $user_cat);
        //echo "done";
    }
    else
    {
        update_user_meta($current_user, 'user_category', $user_cat);
        update_user_meta($current_user, 'business_category', $bus_cat);   
        //echo "done";
    }
    
}

add_action( 'bp_member_options_nav' , 'add_menu_options', 10);
function add_menu_options()
{
    //$selected = '';
      //  if ( bp_is_current_component( "http://link1n1.com/DEV/members/ashtex/affiliates/" ) ) {
        //    $selected = ' class="current selected"';
        //}
    //echo apply_filters_ref_array( 'bp_get_displayed_user_nav_affiliates' , array( '<li id="affiliates-personal-li class="current selected""><a id="user-affiliate" href="http://link1n1.com/DEV/view-all/">Affiliates</a></li>') );
    global $wpdb;
    //global $current_user;
    $current_user = get_current_user_id();
    //if(!isset($_SESSION['id']))
    //{
        $_SESSION['id'] = $current_user;
    //}
    $category_check = metadata_exists('user', $current_user, 'user_category');
    if($category_check===true)
    {
        $table_usermeta = $wpdb->prefix . 'usermeta';
        $business_check_query = "SELECT meta_value FROM ".$table_usermeta.
        " WHERE meta_key='user_category' AND user_id='$current_user'";
        $business_check = $wpdb->get_row($business_check_query,ARRAY_N);

        if($business_check[0] == 'bus')
        {
            echo apply_filters_ref_array( 'bp_get_displayed_user_nav_affiliates' , array( '<li id="affiliates-personal-li class="current selected""><a id="user-affiliate" href="' . site_url() . '/view-all/">Affiliates</a></li>') );
            echo apply_filters_ref_array( 'bp_get_displayed_user_nav_promo' , array( '<li id="promo-personal-li class="current selected""><a id="user-promo" href="' . site_url() . '/view-all-coupons/">Promo Codes</a></li>') );
            echo apply_filters_ref_array( 'bp_get_displayed_user_nav_auth' , array( '<li id="auth-personal-li class="current selected""><a id="user-auth" href="' . site_url() . '/code-authentication/">Authenticate Code</a></li>') );
            echo apply_filters_ref_array( 'bp_get_displayed_user_nav_log' , array( '<li id="log-personal-li class="current selected""><a id="user-log" href="' . site_url() . '/view-log/">Points Log</a></li>') );
            echo apply_filters_ref_array( 'bp_get_displayed_user_nav_voucher' , array( '<li id="voucher-personal-li class="current selected""><a id="user-voucher" href="' . site_url() . '/see-vouchers/">Vouchers</a></li>') );
            echo apply_filters_ref_array( 'bp_get_displayed_user_nav_redeem_voucher' , array( '<li id="redeem-voucher-personal-li class="current selected""><a id="redeem-voucher" href="' . site_url() . '/redeem-voucher/">Redeem Voucher</a></li>') );
            
            
        }
        else if($business_check[0] == 'ind')
        {
            echo apply_filters_ref_array( 'bp_get_displayed_user_nav_codes' , array( '<li id="codes-personal-li class="current selected""><a id="user-codes" href="' . site_url() . '/view-all-codes/">Promo Codes</a></li>') );
            echo apply_filters_ref_array( 'bp_get_displayed_user_nav_log' , array( '<li id="log-personal-li class="current selected""><a id="user-log" href="' . site_url() . '/view-log/">Points Log</a></li>') );
            echo apply_filters_ref_array( 'bp_get_displayed_user_nav_incentive' , array( '<li id="incentive-personal-li class="current selected""><a id="user-incentive" href="' . site_url() . '/use-incentive/">Incentives</a></li>') );
            
        }
    }
    else
    {
        echo apply_filters_ref_array( 'bp_get_displayed_user_nav_category' , array( '<li id="category-personal-li class="current selected""><a id="user-category" href="' . site_url() . '/ask-category/">Category</a></li>') );
        //echo "<p>nothing</p>";
    }
}


add_action('geodir_user_register', 'register_business_affiliate', 10);
function register_business_affiliate($user_id)
{
    global $wpdb;
    global $current_user;
    $current_user = $user_id;
    $child_id = $current_user;
    $table_posts = $wpdb->prefix . 'posts';
    $table_usermeta = $wpdb->prefix . 'usermeta';
    $table_affiliates = $wpdb->prefix . 'uap_affiliates';

    $business_check_query = "SELECT meta_value FROM ".$table_usermeta.
    " WHERE meta_key='user_category' AND user_id='$current_user'";
    $business_check = $wpdb->get_row($business_check_query,ARRAY_N);

    if($business_check[0] == 'bus')
    {

    $duplicate_query = "SELECT id FROM ".$table_affiliates. " WHERE uid='$current_user'";
    $duplicate = $wpdb->get_row($duplicate_query,ARRAY_N);
    if($duplicate[0] == '')
    {
        
    
            //echo "check";
            $count_query = "SELECT MAX(id) FROM ".$table_affiliates;
            $count = $wpdb->get_row($count_query,ARRAY_N);
            $counter = $count[0] + 1;
            //echo $counter;
            $sql = "INSERT INTO ".$table_affiliates." VALUES ('$counter', '$child_id', 1, DEFAULT, 1)";
            $wpdb->query($sql);
        
    }
    }   
    
}
add_action( 'geodir_after_add_from_favorite', 'add_favorite', 10 );
function add_favorite($post_id)
{
    global $wpdb;
    global $current_user;
    $current_user = get_current_user_id();
    $child_id = $current_user;
    $table_posts = $wpdb->prefix . 'posts';
    $table_usermeta = $wpdb->prefix . 'usermeta';
    $table_affiliates = $wpdb->prefix . 'uap_affiliates';
    $table_mlm = $wpdb->prefix . 'uap_mlm_relations';

    $business_check_query = "SELECT meta_value FROM ".$table_usermeta.
    " WHERE meta_key='user_category' AND user_id='$current_user'";
    $business_check = $wpdb->get_row($business_check_query,ARRAY_N);

    if($business_check[0] == 'ind')
    {

    $duplicate_query = "SELECT id FROM ".$table_affiliates. " WHERE uid='$current_user'";
    $duplicate = $wpdb->get_row($duplicate_query,ARRAY_N);
    if($duplicate[0] == '')
    {
        $post_author_id_query="SELECT post_author FROM ".$table_posts. " WHERE ID='$post_id'";
        $post_author_id = $wpdb->get_row($post_author_id_query,ARRAY_N);
        $parent_id = $post_author_id[0];

        

        if($parent_id != $child_id)
        {
            
            $count_query = "SELECT MAX(id) FROM ".$table_affiliates;
            $count = $wpdb->get_row($count_query,ARRAY_N);
            $counter = $count[0] + 1;
            //echo $counter;
            $sql = "INSERT INTO ".$table_affiliates." VALUES ('$counter', '$child_id', 1, DEFAULT, 1)";
            $wpdb->query($sql);
            $child_affiliate_id = $counter;
            $parent_affiliate_id_query="SELECT id FROM ".$table_affiliates. " WHERE uid='$parent_id'";
            $parent_affiliate_id = $wpdb->get_row($parent_affiliate_id_query,ARRAY_N);
            $count_query1 = "SELECT MAX(id) FROM ".$table_mlm;
            $count1 = $wpdb->get_row($count_query1,ARRAY_N);
            $counter1 = $count1[0] + 1;
            $duplicate_mlm_query = "SELECT id FROM ".$table_mlm. " WHERE affiliate_id='$child_affiliate_id'
             AND parent_affiliate_id='$parent_affiliate_id[0]'";
            $duplicate_mlm = $wpdb->get_row($duplicate_mlm_query,ARRAY_N);
            if($duplicate_mlm[0] == '')
            {
                $sql1 = "INSERT INTO ".$table_mlm." VALUES ('$counter1', '$child_affiliate_id', '$parent_affiliate_id[0]')";
                $wpdb->query($sql1);
            }
            
        }
    }
    else
    {
        $post_author_id_query="SELECT post_author FROM ".$table_posts. " WHERE ID='$post_id'";
        $post_author_id = $wpdb->get_row($post_author_id_query,ARRAY_N);
        $parent_id = $post_author_id[0];   
        $parent_affiliate_id_query="SELECT id FROM ".$table_affiliates. " WHERE uid='$parent_id'";
            $parent_affiliate_id = $wpdb->get_row($parent_affiliate_id_query,ARRAY_N);
            $count_query1 = "SELECT MAX(id) FROM ".$table_mlm;
            $count1 = $wpdb->get_row($count_query1,ARRAY_N);
            $counter1 = $count1[0] + 1;
            $duplicate_mlm_query = "SELECT id FROM ".$table_mlm. " WHERE affiliate_id='$duplicate[0]'
             AND parent_affiliate_id='$parent_affiliate_id[0]'";
            $duplicate_mlm = $wpdb->get_row($duplicate_mlm_query,ARRAY_N);
            if($duplicate_mlm[0] == '')
            {
                $sql1 = "INSERT INTO ".$table_mlm." VALUES ('$counter1', '$duplicate[0]', '$parent_affiliate_id[0]')";
                $wpdb->query($sql1);
            }
    }
    }
    //else
    //{
      //  echo "false";
    //}

        
    //echo $post_id;$P$Bfw51YEkz/ugw3TAwnY8pstB0hTkuJ1
}
add_action( 'geodir_after_remove_from_favorite', 'rmv_favorite', 10 );
function rmv_favorite($post_id)
{
    global $wpdb;
    global $current_user;
    $current_user = get_current_user_id();
    $table_affiliates = $wpdb->prefix . 'uap_affiliates';
    $duplicate_query = "SELECT id FROM ".$table_affiliates. " WHERE uid='$current_user'";
    $duplicate = $wpdb->get_row($duplicate_query,ARRAY_N);
    if($duplicate[0]!='')
    {
    /*if($duplicate[0] != '')
    {
        $sql="DELETE FROM ".$table_affiliates." WHERE uid='$current_user'";
        $wpdb->query($sql);
    }*/
    $table_mlm = $wpdb->prefix . 'uap_mlm_relations';
    $child_affiliate_id = $duplicate[0];
    //echo $child_affiliate_id." ";
    $table_posts = $wpdb->prefix . 'posts';
    $post_author_id_query="SELECT post_author FROM ".$table_posts. " WHERE ID='$post_id'";
    $post_author_id = $wpdb->get_row($post_author_id_query,ARRAY_N);
    $parent_id = $post_author_id[0];
    $table_affiliates = $wpdb->prefix . 'uap_affiliates';
    $parent_affiliate_id_query="SELECT id FROM ".$table_affiliates. " WHERE uid='$parent_id'";
    $parent_affiliate_id = $wpdb->get_row($parent_affiliate_id_query,ARRAY_N);    
    //echo $parent_affiliate_id[0];
    $table_mlm = $wpdb->prefix . 'uap_mlm_relations';
    $duplicate_mlm_query = "SELECT id FROM ".$table_mlm. " WHERE affiliate_id='$child_affiliate_id'
     AND parent_affiliate_id='$parent_affiliate_id[0]'";
    $duplicate_mlm = $wpdb->get_row($duplicate_mlm_query,ARRAY_N);
    if($duplicate_mlm[0] != '')
    {
        $sql="DELETE FROM ".$table_mlm." WHERE affiliate_id='$child_affiliate_id'
         AND parent_affiliate_id='$parent_affiliate_id[0]'";
         $wpdb->query($sql);
    }
    }

    //echo $post_id;
}
remove_action('geodir_signup_forms', 'geodir_action_signup_forms', 10);
add_action( 'geodir_signup_forms', 'modified_signup_form', 20 );
function modified_signup_form()
 {

    // Contents for your function here.
    global $user_login;

    ?>
    <script type="text/javascript">
        <?php if ( $user_login ) { ?>
        setTimeout(function () {
            try {
                d = document.getElementById('user_pass');
                d.value = '';
                d.focus();
            } catch (e) {
            }
        }, 200);
        <?php } else { ?>
        try {
            document.getElementById('user_login').focus();
        } catch (e) {
        }
        <?php } ?>
    </script>
    <script type="text/javascript">
        <?php if ( $user_login ) { ?>
        setTimeout(function () {
            try {
                d = document.getElementById('user_pass');
                d.value = '';
                d.focus();
            } catch (e) {
            }
        }, 200);
        <?php } else { ?>
        try {
            document.getElementById('user_login').focus();
        } catch (e) {
        }
        <?php } ?>
    </script><?php

    global $errors;
    if (isset($_REQUEST['msg']) && $_REQUEST['msg'] == 'claim')
        $errors->add('claim_login', LOGIN_CLAIM);

    if (!empty($errors)) {
        foreach ($errors as $errorsObj) {
            foreach ($errorsObj as $key => $val) {
                for ($i = 0; $i < count($val); $i++) {
                    echo "<div class=sucess_msg>" . $val[$i] . '</div>';
                    $registration_error_msg = 1;
                }
            }
        }
    }

    if (isset($_REQUEST['page']) && $_REQUEST['page'] == 'login' && isset($_REQUEST['page1']) && $_REQUEST['page1'] == 'sign_in') {
      
        ?>


        <div class="login_form">
            <?php
            /**
             * Contains login form template.
             *
             * @since 1.0.0
             */
            // include("/var/www/wp0/wp-content/plugins/geodirectory_child/geodirectory-templates/mod_login_frm.php");
            include( __DIR__ . "/geodirectory-templates/mod_login_frm.php"); ?>
        </div>

    <?php } elseif (isset($_REQUEST['page']) && $_REQUEST['page'] == 'login' && isset($_REQUEST['page1']) && $_REQUEST['page1'] == 'sign_up') { ?>

        <div class="registration_form">
            <?php
            /**
             * Contains registration form template.
             *
             * @since 1.0.0
             */

            include( __DIR__ . "/geodirectory-templates/mod_reg_frm.php"); ?>
        </div>

    <?php } else { ?>

        <div class="login_form_l">
            <?php
            /**
             * Contains login form template.
             *
             * @since 1.0.0
             */

            include( __DIR__ . "/geodirectory-templates/mod_login_frm.php"); ?>
        </div>
        <div class="registration_form_r">
            <?php
            /**
             * Contains registration form template.
             *
             * @since 1.0.0
             */
            include( __DIR__ . "/geodirectory-templates/mod_reg_frm.php"); ?>
        </div>
        <!--<div class="facebook_reg_f">
            <?php
            /**
             * Contains registration form template.
             *
             * @since 1.0.0
             */
            //include("/var/www/wp0/wp-content/plugins/geodirectory_child/geodirectory-templates/fb_reg_frm.php"); ?>
        </div>-->

    <?php }?>
    <script type="text/javascript">
        try {
            document.getElementById('user_login').focus();
        } catch (e) {
        }
    </script>


    <?php if ((isset($errors->errors['invalidcombo']) && $errors->errors['invalidcombo'] != '') || (isset($errors->errors['empty_username']) && $errors->errors['empty_username'] != '')) { ?>
    <script type="text/javascript">document.getElementById('lostpassword_form').style.display = '';</script>
<?php }
}

remove_action('geodir_add_listing_form','geodir_action_add_listing_form');
add_action('geodir_add_listing_form', 'modified_add_listing_form', 10);
/**
 * Outputs the add listing form HTML content.
 *
 * Other things are needed to output a working add listing form, you should use the add listing shortcode if needed.
 *
 * @since 1.0.0
 * @package GeoDirectory
 * @global object $current_user Current user object.
 * @global object $post The current post object.
 * @global object $post_images Image objects of current post if available.
 * @global object $gd_session GeoDirectory Session object.
 */
function modified_add_listing_form()
{
    
    global $cat_display, $post_cat, $current_user, $gd_session;
    $page_id = get_the_ID();
    $post = '';
    $title = '';
    $desc = '';
    $kw_tags = '';
    $required_msg = '';
    $submit_button = '';

    $ajax_action = isset($_REQUEST['ajax_action']) ? $_REQUEST['ajax_action'] : 'add';

    $thumb_img_arr = array();
    $curImages = '';

    if (isset($_REQUEST['backandedit'])) {
        global $post;
        $post = (object)$gd_session->get('listing');
        $listing_type = $post->listing_type;
        $title = $post->post_title;
        $desc = $post->post_desc;
        $post_cat = $post->post_category;

        $kw_tags = $post->post_tags;
        $curImages = isset($post->post_images) ? $post->post_images : '';
    } elseif (isset($_REQUEST['pid']) && $_REQUEST['pid'] != '') {
        global $post, $post_images;

        $post = geodir_get_post_info($_REQUEST['pid']);
        $thumb_img_arr = geodir_get_images($post->ID);
        if ($thumb_img_arr) {
            foreach ($thumb_img_arr as $post_img) {
                $curImages .= $post_img->src . ',';
            }
        }

        $listing_type = $post->post_type;
        $title = $post->post_title;
        $desc = $post->post_content;
        $kw_tags = $post->post_tags;
        $kw_tags = implode(",", wp_get_object_terms($post->ID, $listing_type . '_tags', array('fields' => 'names')));
    } else {
        $listing_type = sanitize_text_field($_REQUEST['listing_type']);
        //echo $listing_type;
    }

    if ($current_user->ID != '0') {
        $user_login = true;
    }
    $post_type_info = geodir_get_posttype_info($listing_type);

    $cpt_singular_name = (isset($post_type_info['labels']['singular_name']) && $post_type_info['labels']['singular_name']) ? $post_type_info['labels']['singular_name'] : __('Listing','geodirectory');

    global $wpdb;
    $current_users = get_current_user_id();
    $table_usermeta = $wpdb->prefix . 'usermeta';
    //echo $current_user;
    $query1="SELECT meta_value FROM ".$table_usermeta." 
                WHERE meta_key='business_category' AND user_id = '$current_users'";
                $result1 = $wpdb->get_row($query1,ARRAY_N);
                if(($result1[0]=='pl' ||$result1[0] == 'ev') && $listing_type=='gd_place')
                {
                    //echo $result1[0]."  ".$listing_type;    
                   include( __DIR__ . "/geodirectory-templates/mod_listing_form.php");
    
    
                }
                else if(($result1[0]=='pl' ||$result1[0] == 'ev') && $listing_type=='gd_event')
                {
                    //echo $result1[0]."  ".$listing_type;    
                   include( __DIR__ . "/geodirectory-templates/mod_listing_form.php");                
                }
                else
                {
                    
                 //include("/var/www/wp0/wp-content/themes/GeoDirectory_whoop/404.php");
                 get_header(); ?>
<div id="geodir_wrapper" class="geodir-404">
  <div class="clearfix geodir-common">
    <div id="geodir_content" class="" role="main">
      <article id="post-not-found" class="hentry cf">
        <header class="article-header">
          <h1>
            <?php _e( 'Not Authorized', GEODIRECTORY_FRAMEWORK ); ?>
          </h1>
        </header>
        <section class="entry-content">
          <p>
            <?php _e( 'You are not authorized to do the action above!', GEODIRECTORY_FRAMEWORK ); ?>
          </p>
        </section>
        <section class="search">
          <p>
            <?php get_search_form(); ?>
          </p>
        </section>
        <footer class="article-footer">
        </footer>
      </article>
    </div>
  </div>
</div>
<?php get_footer();   
                    //echo "You are not authorized to do this action!";
                }
                //else if()
    wp_reset_query();
}



add_filter('geodir_manage_user_meta','modified_user_info',20,2);
function modified_user_info($arr,$user_id)
{
    $user_category=sanitize_user($_POST['user_category']);
    $user_category = str_replace(",", "", $user_category);
    $fb_name = sanitize_user($_POST['fb_username']);
    $fb_name = str_replace(",", "", $fb_name);
    $user_fname = sanitize_user($_POST['user_fname']);
    $user_fname = str_replace(",", "", $user_fname);
    if($user_category=='bus')
    {
        $business_category=sanitize_user($_POST['business_category']);
        $business_category = str_replace(",", "", $business_category);

  $arr=array(
        "user_add1" => '',
        "user_add2" => '',
        "user_city" => '',
        "user_state" => '',
        "user_country" => '',
        "user_postalcode" => '',
        "user_phone" => '',
        "user_twitter" => '',
        "first_name" => $user_fname,
        "last_name" => '',
        "user_category" => $user_category,
        "business_category" => $business_category,
        "fb_username" => $fb_name,
        
    );
    }
    else
    {
        $arr=array(
        "user_add1" => '',
        "user_add2" => '',
        "user_city" => '',
        "user_state" => '',
        "user_country" => '',
        "user_postalcode" => '',
        "user_phone" => '',
        "user_twitter" => '',
        "first_name" => $user_fname,
        "last_name" => '',
        "user_category" => $user_category,
        "fb_username" => $fb_name,
        
    );   
    }
    return $arr;
}

add_filter('geodir_menu_li_class','make_menu_null',20,1);
function make_menu_null($menu_class)
{
    global $wpdb;
    $current_user = get_current_user_id();
    //echo $current_user;
    $table_usermeta = $wpdb->prefix . 'usermeta';
    $query="SELECT meta_value FROM ".$table_usermeta." 
        WHERE meta_key='user_category' AND user_id = '$current_user'";
            $result = $wpdb->get_row($query,ARRAY_N);
            if($result[0]=='ind')
            {    
                if($menu_class=='menu-item menu-item-has-children menu-gd-add-listing ')
                {
                    $menu_class.='"'.' ';
                    $menu_class.='style="display:none;"';
                }
            }
            
    return $menu_class;
}

//a shortcode
function view_all_bookmarks( $atts, $content = null )
{
    global $current_user;
    $current_user = get_current_user_id();
        if ($current_user == 0) 
        {
            //echo 'You are currently not logged in!!';
        }
         else 
        {

            $output = "<style>.side-img img {
                width: 60px;
                float: left;
                margin-right: 13px;
            }
            .side-img span {
                float: left;
                width: 202px;
            }
            .side-img a {
                font-weight: bold;
            }
            .side-img ul {
                float: right;
                width: 202px;
            }
            .side-img ul li {
                margin-bottom: 0px;
            }
            .side-img ul li a {
                font-weight: normal;
                color: #fff;
                background: #DC2F21;
                float: left;
                text-overflow: ellipsis;
                white-space: nowrap;
                overflow: hidden;
                margin-right: 6px;
                margin-bottom: 10px;
                padding: 2px 7px;
                max-width: 92px;
            }</style>";
            
            $output.= "<section class='widget'>";
            $output.= "<div class='geodir_list_heading clearfix'>";
            $output.= '<h3 class="widget-title">Affiliates Here!</h3>';
            $output.= "</div>";
            static $counter=0,$post_name="";
            global $wpdb;
            //$table = $wpdb->prefix . '';
            global $event_code,$place_code;
            //get all the posts bookmarked by all the users
            
            $table_usermeta = $wpdb->prefix . 'usermeta';
            $table_posts = $wpdb->prefix . 'posts';
            $table_users = $wpdb->prefix . 'users';
            $table_postmeta = $wpdb->prefix . 'postmeta';
            $table_geodir_gd_place_detail = $wpdb->prefix . 'geodir_gd_place_detail';
            $table_geodir_gd_event_detail = $wpdb->prefix . 'geodir_gd_event_detail';
            $table_geodir_post_locations = $wpdb->prefix . 'geodir_post_locations';

            $sql = "SELECT meta_value,user_id
             FROM ".$table_usermeta." WHERE user_id != {$current_user} AND meta_key='gd_user_favourite_post'";
            $result = $wpdb->get_results($sql,ARRAY_N);
            //foreach ( $result as $res )
            //{
              //  $output.= $res[1]."<br>";
            //}
            $sql = "SELECT ID, post_content, post_title,post_name
             FROM ".$table_posts." WHERE post_author = {$current_user}";
            $result1 = $wpdb->get_results($sql,ARRAY_N);
            //foreach ( $result1 as $res )
            //{
              //  $output.= $res[2]."<br>";
            //}
            //while ($row = mysql_fetch_array($result, MYSQL_NUM)) 
            foreach ($result as $row)
            {
                $arr=unserialize($row[0]);
                
                //while ($row1 = mysql_fetch_array($result1, MYSQL_NUM)) 
                foreach ($result1 as $row1)
                {
                    if(!empty($arr))
                    {
                    foreach ($arr as $value) 
                    {
                        
                        if($row1[0]==$value)
                        {
                            //$counter++;
                            //$output.=$value."<br>";
                            $sql = "SELECT display_name
                            FROM ".$table_users." WHERE ID = {$row[1]}";
                            $username = $wpdb->get_row($sql,ARRAY_N);                            
                            //$output.= $username[0]."<br>";
                            if($post_name!=$username[0] || $post_name=="")
                            {
                                if($post_name!="")
                                {
                                    if($counter!=0)
                                    {
                                        $output .= "</ul>";
                                        $output .="<br>";
                                        $output .= "<br>";
                                    }
                                    
                                    //echo "</div>";
                                }
                                $post_name=$username[0];
                                
                            if($username!="")
                                {
                                    $table_usermeta = $wpdb->prefix . 'usermeta';
                                        $business_check_query = "SELECT meta_value FROM ".$table_usermeta.
                                        " WHERE meta_key='user_category' AND user_id='$row[1]'";
                                        $business_check = $wpdb->get_row($business_check_query,ARRAY_N);
                                    if($business_check[0] != 'bus')
                                        {
                                        $output.= "<div class='side-img'>".get_avatar( $row[1], 64 )."</div><span class='img-info'>
                            <a href='" . site_url() . "/members/".$username[0]."'>".$username[0].
                            "</a></span>";
                            $output.= "<ul>";
                            $counter++;
                                        }
                                }
                            }
                            $sql = "SELECT meta_value
                            FROM ".$table_postmeta." WHERE post_id = {$row1[0]} AND meta_key='post_categories'";
                            $p_details = $wpdb->get_row($sql,ARRAY_N);
                            $post_details=unserialize($p_details[0]);
                            
                            $place=$post_details['gd_placecategory'];
                            $event=$post_details['gd_eventcategory'];
                            if($event=="" && $place!="")
                            {
                                $place_code=$place[0].$place[1];
                                
                            }
                            else if($event!="" && $place=="")
                            {
                                $event_code=$event[0].$event[1];
                                
                            }
                            $sql = "SELECT post_location_id
                            FROM ".$table_geodir_gd_place_detail." WHERE post_id = {$row1[0]}";
                            $place_id = $wpdb->get_row($sql,ARRAY_N);
                            $sql_event = "SELECT post_location_id
                            FROM ".$table_geodir_gd_event_detail." WHERE post_id = {$row1[0]}";
                            $event_id = $wpdb->get_row($sql_event,ARRAY_N);
                            //$place_id=$this->get_post_loc_id($wpdb,$row1[0]);
                            $sql = "SELECT region,city,country_slug
                            FROM ".$table_geodir_post_locations." WHERE location_id = {$place_id[0]}";
                            $place_details_row = $wpdb->get_row($sql,ARRAY_N); 
                            //$place_details_row=$this->get_post_loc_details($wpdb,$place_id[0]);
                            if($event=="" && $place!="")
                            {
                                if($place_code=='40')
                                {
                                
                                    $counter++;
                                    $cat="hotels";
                                    //$output.= "<li><a href='" . site_url() . "/places/".$place_details_row[2]
                                    //."/".$place_details_row[0]."/"
                                    //.$place_details_row[1]."/".$cat."/".$row1[3]."/'>".$row1[2]."</a></li>";
                                    
                                      //  $this->print_link($cat,$place_details_row,$row1);
                                    
                                }
                                else if($place_code=='41')
                                {
                                
                                    
                                    $counter++;
                                    $cat="restaurants";
                                    //$output.= "<li><a href='" . site_url() . "/places/".$place_details_row[2]
                                    //."/".$place_details_row[0]."/"
                                    //.$place_details_row[1]."/".$cat."/".$row1[3]."/'>".$row1[2]."</a></li>";
                                    
                                    //    $this->print_link($cat,$place_details_row,$row1);
                                       
                                }
                                else if($place_code=='39')
                                {
                                
                                    
                                    $counter++;
                                    $cat="attractions";
                                    //$output.= "<li><a href='" . site_url() . "/places/".$place_details_row[2]
                                    //."/".$place_details_row[0]."/"
                                    //.$place_details_row[1]."/".$cat."/".$row1[3]."/'>".$row1[2]."</a></li>";
                                    
                                  //      $this->print_link($cat,$place_details_row,$row1);
                                    
                                }
                                else if($place_code=='45')
                                {
                                
                                    
                                    $counter++;
                                    $cat="features";
                                    //$output.= "<li><a href='" . site_url() . "/places/".$place_details_row[2]
                                    //."/".$place_details_row[0]."/"
                                    //.$place_details_row[1]."/".$cat."/".$row1[3]."/'>".$row1[2]."</a></li>";
                                    
                                  //      $this->print_link($cat,$place_details_row,$row1);
                                    
                                }
                                else if($place_code=='43')
                                {
                                
                                    
                                    $counter++;
                                    $cat="festival";
                                    //$output.= "<li><a href='" . site_url() . "/places/".$place_details_row[2]
                                    //."/".$place_details_row[0]."/"
                                    //.$place_details_row[1]."/".$cat."/".$row1[3]."/'>".$row1[2]."</a></li>";
                                    
                                  //      $this->print_link($cat,$place_details_row,$row1);
                                    
                                }
                                else if($place_code=='42')
                                {
                                
                                    
                                    $counter++;
                                    $cat="foodnightlife";
                                    //$output.= "<li><a href='" . site_url() . "/places/".$place_details_row[2]
                                    //."/".$place_details_row[0]."/"
                                    //.$place_details_row[1]."/".$cat."/".$row1[3]."/'>".$row1[2]."</a></li>";
                                    
                                  //      $this->print_link($cat,$place_details_row,$row1);
                                    
                                }
                                else if($place_code=='44')
                                {
                                
                                    
                                    $counter++;
                                    $cat="videos";
                                    //$output.= "<li><a href='" . site_url() . "/places/".$place_details_row[2]
                                    //."/".$place_details_row[0]."/"
                                    //.$place_details_row[1]."/".$cat."/".$row1[3]."/'>".$row1[2]."</a></li>";
                                    
                                  //      $this->print_link($cat,$place_details_row,$row1);
                                    
                                }
                            }
                            else if($event!="" && $place=="")
                            {
                                $sql = "SELECT region,city,country_slug
                                FROM ".$table_geodir_post_locations." WHERE location_id = {$event_id[0]}";
                                $event_details_row = $wpdb->get_row($sql,ARRAY_N);
                                                                
                                $counter++;
                                $cat="events";
                                //$output.= "<li><a href='" . site_url() . "/events/".$event_details_row[2]
                                  //  ."/".$event_details_row[0]."/"
                                    //.$event_details_row[1]."/".$cat."/".$row1[3]."/'>".$row1[2]."</a></li>";
                                
                                //    $this->print_link($cat,$place_details_row,$row1);
                                
                            }

                        }
                    }
                }
                }
            }
            $output.= "</ul>";
            //$output.= "</div>";
            if($counter!=0)
            {
                $output .= "<br>";
                $output .= "<br>";
                $output .= "</section>";
            }
            if($counter==0)
            {
                $output.= "<p>You have no affiliates!!</p>";
                $output.= "</section>";
            }
            //$output.=$counter;
            return $output;
        }
    
}
add_shortcode('view_all_bm','view_all_bookmarks');
//a shortcode

add_action( 'wp_ajax_auto_search', 'auto_search' );
add_action( 'wp_ajax_nopriv_auto_search', 'auto_search' );
function auto_search() {

    $name=$_POST['affiliate'];
    global $wpdb;
    global $current_user;
    $current_user = get_current_user_id();

    $table_users = $wpdb->prefix . 'users';
    $table_mlm = $wpdb->prefix . 'uap_mlm_relations';
    $table_affiliates = $wpdb->prefix . 'uap_affiliates';

    $p_aff_id=$wpdb->get_row("SELECT id FROM " .$table_affiliates.
     " WHERE uid = '$current_user' ",ARRAY_N);
    $c_aff_id=$wpdb->get_results("SELECT affiliate_id FROM " .$table_mlm.
     " WHERE parent_affiliate_id = '$p_aff_id[0]' ",ARRAY_N);
    $names=array();
    foreach($c_aff_id as $child)
    {
        $c_uid=$wpdb->get_row("SELECT uid FROM " .$table_affiliates.
        " WHERE id = '$child[0]' ",ARRAY_N);
        
        //echo $name;
        $affiliate=$wpdb->get_row("SELECT display_name FROM " .$table_users. " WHERE ID = '$c_uid[0]'",ARRAY_N);
        //$affiliate=$wpdb->get_results("SELECT display_name FROM " .$table_users. " WHERE display_name //LIKE '$name%'");
        array_push($names,$affiliate[0]);    
    }
   // print_r($affiliate);
    //die();
    
    //foreach($affiliate as $key=> $value){
        
        //die($value->display_name);
    //echo json_encode($value->display_name);
    //array_push($names,$value->display_name);
    //}
    //$matches  = preg_grep ('/^TestB (\w+)/i', $names);
    print_r(json_encode($names));
    //print_r($names);
    
    //wp_reset_query();
    die();
} // end theme_custom_handler
add_action("wp_ajax_submit_coupons_data", "submit_coupons_data");
add_action("wp_ajax_nopriv_submit_coupons_data", "submit_coupons_data");
function submit_coupons_data()
{
        
        $value =  $_POST['value'];
        $type =  $_POST['type'];
        $expire =  $_POST['expire'];
        $global =  $_POST['global'];
        $user =  $_POST['user'];
        $code =  $_POST['code'];
        $affiliate = $_POST['affiliate'];
        //echo $affiliate;

        global $wpdb;
        global $current_user;
        $current_user = get_current_user_id();

        $table_users = $wpdb->prefix . 'users';
        $table_posts = $wpdb->prefix . 'posts';
        $table_postmeta = $wpdb->prefix . 'postmeta';
        $table_usermeta = $wpdb->prefix . 'usermeta';
        $table_affiliates = $wpdb->prefix . 'uap_affiliates';
        $table_mlm = $wpdb->prefix . 'uap_mlm_relations';
        $table_uap_cp = $wpdb->prefix . 'uap_coupons_code_affiliates';
        $table_mc_cp = $wpdb->prefix . 'myCRED_log';

        $count_query = "SELECT MAX(ID) FROM ".$table_posts;
        $count = $wpdb->get_row($count_query,ARRAY_N);
        $ID = $count[0] + 1;
        $post_author = $current_user;
        $post_title = $code;
        $post_status = 'publish';
        $post_name = $code;
        $post_type = 'mycred_coupon';
        
        // Create Coupon Post
        $data = array();
        $post_id = wp_insert_post( apply_filters( 'mycred_create_new_coupon_post', array(
            'post_type'   => 'mycred_coupon',
            'post_title'  => $code,
            'post_status' => 'publish',
            'post_author' => $post_author
        ), $data ) );
        // Save Coupon Details
            add_post_meta( $post_id, 'value',  $value );
            add_post_meta( $post_id, 'global', $global );
            add_post_meta( $post_id, 'user',   $user );
            add_post_meta( $post_id, 'min',    0 );
            add_post_meta( $post_id, 'max',    0 );
            add_post_meta( $post_id, 'expires', $expire );

            $id_query = "SELECT ID FROM ".$table_users. " WHERE display_name='$affiliate'";
            $uid = $wpdb->get_row($id_query,ARRAY_N);
            $aff_id_query = "SELECT id FROM ".$table_affiliates. " WHERE uid='$uid[0]'";
            $aff_id = $wpdb->get_row($aff_id_query,ARRAY_N);
            $aff=$aff_id[0];
            $sql="INSERT INTO ".$table_uap_cp." VALUES ('$post_id', '$code', $aff, DEFAULT, DEFAULT, 1)";
            $wpdb->query($sql);

        
}
add_action("wp_ajax_set_session_cat", "set_session_cat");
add_action("wp_ajax_nopriv_set_session_cat", "set_session_cat");
function set_session_cat()
{

    $category=$_POST['category'];
    $sub_category=$_POST['sub_category'];
    if($sub_category=='')
    {
        $_SESSION['cat'] = $category;
    }
    else
    {
        $_SESSION['cat'] = $category;
        $_SESSION['sub_cat'] = $sub_category;   
    }

}
add_action("wp_ajax_check_duplicate_user", "check_duplicate_user");
add_action("wp_ajax_nopriv_check_duplicate_user", "check_duplicate_user");
function check_duplicate_user()
{
    $email =  $_POST['email'];
    global $wpdb;
    $flag=false;
        
    $table_users = $wpdb->prefix . 'users';

    $query = "SELECT ID FROM ".$table_users. " WHERE user_email='$email'";
    $result = $wpdb->get_row($query,ARRAY_N);
    if($result[0]=='')
    {
        echo $flag;
    }
    else
    {
        $flag=true;
        echo $flag;
    }

}
add_action("wp_ajax_check_duplicate_code", "check_duplicate_code");
add_action("wp_ajax_nopriv_check_duplicate_code", "check_duplicate_code");
function check_duplicate_code()
{

        $value =  $_POST['value'];
        $type =  $_POST['type'];
        $expire =  $_POST['expire'];
        $global =  $_POST['global'];
        $user =  $_POST['user'];
        $code =  $_POST['code'];
        $discount =  $_POST['discount'];
        $detail =  $_POST['detail'];
        $affiliate = $_POST['affiliate'];       
        $flag=false;

        global $wpdb;
        global $current_user;
        $current_user = get_current_user_id();

        $table_users = $wpdb->prefix . 'users';
        $table_posts = $wpdb->prefix . 'posts';
        $table_postmeta = $wpdb->prefix . 'postmeta';
        $table_usermeta = $wpdb->prefix . 'usermeta';
        $table_affiliates = $wpdb->prefix . 'uap_affiliates';
        $table_mlm = $wpdb->prefix . 'uap_mlm_relations';
        $table_uap_cp = $wpdb->prefix . 'uap_coupons_code_affiliates';
        $table_mc_cp = $wpdb->prefix . 'myCRED_log';

        
        $title_query = "SELECT post_title FROM ".$table_posts. " WHERE post_author='$current_user'";
        $titles = $wpdb->get_results($title_query,ARRAY_N);
        $counter=0;
        foreach($titles as $title)
        {
            if($title[0]==$code)
            {
                $counter++;
            }
        }
        if($counter>0)
        {
            echo $flag;
        }
        else
        {
            $flag=true;
        
        $count_query = "SELECT MAX(ID) FROM ".$table_posts;
        $count = $wpdb->get_row($count_query,ARRAY_N);
        $ID = $count[0] + 1;
        $post_author = $current_user;
        $post_title = $code;
        $post_status = 'publish';
        $post_name = $code;
        $post_type = 'mycred_coupon';
        
        // Create Coupon Post
        $data = array();
        $post_id = wp_insert_post( apply_filters( 'mycred_create_new_coupon_post', array(
            'post_type'   => 'mycred_coupon',
            'post_title'  => $code,
            'post_status' => 'publish',
            'post_author' => $post_author
        ), $data ) );
        // Save Coupon Details
            add_post_meta( $post_id, 'value',  $value );
            add_post_meta( $post_id, 'global', $global );
            add_post_meta( $post_id, 'user',   $user );
            add_post_meta( $post_id, 'min',    0 );
            add_post_meta( $post_id, 'max',    0 );
            add_post_meta( $post_id, 'expires', $expire );

            $id_query = "SELECT ID FROM ".$table_users. " WHERE user_nicename='$affiliate'";
            $uid = $wpdb->get_row($id_query,ARRAY_N);
            $aff_id_query = "SELECT id FROM ".$table_affiliates. " WHERE uid='$uid[0]'";
            $aff_id = $wpdb->get_row($aff_id_query,ARRAY_N);
            $aff=$aff_id[0];
            $sql="INSERT INTO ".$table_uap_cp." VALUES ('$post_id', '$code', $aff, '$discount', '$detail', 1)";
            $wpdb->query($sql);
            echo $flag;
        }
        
}
function edit_code()
{
    $code = $_GET['code'];
    global $wpdb;
    global $current_user;
    $current_user = get_current_user_id();
    if($current_user==0)
    {
        //not logged in currently!
        $output = "<h2>Please Log in!</h2>";
        return $output;
    }
    else
    {
        $table_users = $wpdb->prefix . 'users';
        $table_posts = $wpdb->prefix . 'posts';
        $table_postmeta = $wpdb->prefix . 'postmeta';
        $table_usermeta = $wpdb->prefix . 'usermeta';
        $table_affiliates = $wpdb->prefix . 'uap_affiliates';
        $table_mlm = $wpdb->prefix . 'uap_mlm_relations';
        $table_uap_cp = $wpdb->prefix . 'uap_coupons_code_affiliates';
        $table_mc_cp = $wpdb->prefix . 'myCRED_log';

        $business_check_query = "SELECT meta_value FROM ".$table_usermeta.
        " WHERE meta_key='user_category' AND user_id='$current_user'";
        $business_check = $wpdb->get_row($business_check_query,ARRAY_N);

        if($business_check[0] == 'bus')
        {
            ?>
            <script>
            var $=jQuery;
            $(document).ready(function() {
     
                ////////////////////////////////////
            
            $('#form').on('submit', function(e) {
            e.preventDefault();

            $('input[type="submit"]').prop('disabled', true);
            //$('input[type="submit"]').unbind('mouseenter mouseleave');
            var code = $('#form').find('input[id=title]').val();
            var value = $('#form').find('input[id=mycred-coupon-value]').val();
            var type = $('#form').find('input[id=mycred-coupon-type]').val();
            var expire = $('#form').find('input[id=mycred-coupon-expire]').val();
            var global = $('#form').find('input[id=mycred-coupon-global]').val();
            var discount = $('#form').find('input[id=mycred-coupon-discount]').val();
            var detail = $('#form').find('input[id=mycred-coupon-detail]').val();
            var user = $('#form').find('input[id=mycred-coupon-user]').val();
            var aff = $('#all_ids').find(":selected").text();

            
            if(code=='')
            {
                
                document.getElementById('title').style.borderColor = "red";
                document.getElementById('error_msg').style.display = "";
                document.getElementById('error_msg').innerHTML = "The coupon code field is mandatory";
                $('input[type="submit"]').prop('disabled', false);
               
                
            }
            else
            {
                jQuery.ajax ( 
                data = {
                action: 'get_post_id',
                url: '/wp-admin/admin-ajax.php',
                type: 'POST',
                dataType: 'text',
                'code' : code
                

            });
                jQuery.post(ajaxurl, data, function(responses) {

                var dataz = responses;
                console.log (dataz);
                if(dataz=='0')
                {
                    document.getElementById('title').style.borderColor = "red";
                    document.getElementById('error_msg').style.display = "";
                    document.getElementById('error_msg').innerHTML = "This coupon code does not exist";
                    $('input[type="submit"]').prop('disabled', false);
                }
                else
                {
                dataz=dataz/10;
                //alert(dataz);
                jQuery.ajax ( 
                data = {
                action: 'update_coupon_code',
                url: '/wp-admin/admin-ajax.php',
                type: 'POST',
                dataType: 'text',
                'value' : value,
                'expire' : expire,
                'type' : type,
                'global' : global,
                'user' : user,
                'code' : code,
                'discount' : discount,
                'detail' : detail,
                'affiliate' : aff,
                'post_id' : dataz
                

            });
                
            //alert(dataz);
            jQuery.post(ajaxurl, data, function(response) {

                var dataz = response;
                //document.getElementById('friends_list').innerHTML += dataz;
                //alert( dataz );
                console.log (dataz);

                 //show json in console
                if(dataz=='0')
                {
                    document.getElementById('title').style.borderColor = "red";
                    document.getElementById('error_msg').style.display = "";
                    document.getElementById('error_msg').innerHTML = "This coupon code does not belong to you!";
                    $('input[type="submit"]').prop('disabled', false);
                }
                else
                {
                    alert('Coupon has been successfully updated.');
                    window.top.location.href = site_url() . '/view-all-coupons/';
                }
                    //form.submit();
                
                
            });
            }
            });
            //alert($counter);
            
            }
            

  });
});
            </script>
            <?php
            $mc_coupon_query = "SELECT ID,post_status FROM ".$table_posts.
            " WHERE post_title = '$code'";
            $mc_coupon = $wpdb->get_row($mc_coupon_query,ARRAY_N);
            //$mc_uid = $mc_coupon[3];
                $mc_id = $mc_coupon[0];
                $uap_coupon_query = "SELECT affiliate_id FROM ".$table_uap_cp.
                " WHERE id='$mc_id'";
                $uap_coupon = $wpdb->get_row($uap_coupon_query,ARRAY_N);
                if($uap_coupon[0] != '')
                {
                    $affiliate_id = $uap_coupon[0];
                    $affiliate_uid_query = "SELECT uid FROM ".$table_affiliates.
                    " WHERE id='$affiliate_id'";
                    $affiliate_uid = $wpdb->get_row($affiliate_uid_query,ARRAY_N);
                    $display_name_query = "SELECT display_name FROM ".$table_users." WHERE ID = '$affiliate_uid[0]'";
                    $display_name = $wpdb->get_row($display_name_query,ARRAY_N);
                    $mc_coupon_value_query = "SELECT meta_value FROM ".$table_postmeta.
                    " WHERE post_id = '$mc_id' AND meta_key = 'value'";
                    $mc_coupons_value = $wpdb->get_row($mc_coupon_value_query,ARRAY_N);
                    $mc_coupon_expire_query = "SELECT meta_value FROM ".$table_postmeta.
                    " WHERE post_id = '$mc_id' AND meta_key = 'expires'";
                    $mc_coupons_expire = $wpdb->get_row($mc_coupon_expire_query,ARRAY_N);
                    $mc_coupon_global_query = "SELECT meta_value FROM ".$table_postmeta.
                    " WHERE post_id = '$mc_id' AND meta_key = 'global'";
                    $mc_coupons_global = $wpdb->get_row($mc_coupon_global_query,ARRAY_N);
                    $mc_coupon_user_query = "SELECT meta_value FROM ".$table_postmeta.
                    " WHERE post_id = '$mc_id' AND meta_key = 'user'";
                    $mc_coupons_user = $wpdb->get_row($mc_coupon_user_query,ARRAY_N);
                    //$mc_coupon_data = $mc_coupons_data[0];
                    
                    //<a href='#'>Edit</a> | <a href='#'>Trash</a>


                }

    ?>
    <form name="form" method="post" id="form" autocomplete="off">
            <!--<input type='hidden' name='action' value='submit-form' />-->
            <h2>Update Coupon</h2>
            
            <h3>Coupon Code</h3>
            <p id="error_msg" style="display:none;"></p>
            <div id="titlewrap">
                                <!--<label class="" id="title-prompt-text" for="title">Unique Coupon Code</label>-->
                    <input type="text" name="post_title" size="30" value="<?php echo $code ?>" id="title" spellcheck="true" autocomplete="off" placeholder="Unique Coupon Code">
            </div>
            <br>

                        <br>
            <h3>Coupon Setup</h3>
            <style type="text/css">
table { width: 100%; }
table th { width: 20%; text-align: right; }
table th label { padding-right: 12px; }
table td { width: 80%; padding-bottom: 6px; }
table td textarea { width: 95%; }
#submitdiv .misc-pub-curtime, #submitdiv #visibility, #submitdiv #misc-publishing-actions { display: none; }
#submitdiv #minor-publishing-actions { padding-bottom: 10px; }
</style>
<input type="hidden" name="mycred-coupon-nonce" value="dca26c2bd0">
<table class="table wide-fat">
    <tbody>
        <tr valign="top">
            <th scope="row"><label for="mycred-coupon-value">Value</label></th>
            <td>
                <input type="text" name="mycred_coupon[value]" id="mycred-coupon-value" value="<?php echo $mc_coupons_value[0]; ?>"><br>
                <span class="description">The amount of Points this coupon is worth.</span>
            </td>
        </tr>
        <tr valign="top">
            <th scope="row"><label for="mycred-coupon-value">Point Type</label></th>
            <td>
                
                    Points
                            </td>
        </tr>
        <tr valign="top">
            <th scope="row"><label for="mycred-coupon-value">Expire</label></th>
            <td>
                <input type="text" name="mycred_coupon[expires]" id="mycred-coupon-expire" value="<?php echo $mc_coupons_expire[0]; ?>" placeholder="YYYY-MM-DD"><br>
                <span class="description">Optional date when this coupon expires. Expired coupons will be trashed.</span>
            </td>
        </tr>
    </tbody>
</table>


            <br>
            <h3>Coupon Limits</h3>
            <table class="table wide-fat">
    <tbody>
        <tr valign="top">
            <th scope="row"><label for="mycred-coupon-global">Global Maximum</label></th>
            <td>
                <input type="text" name="mycred_coupon[global]" id="mycred-coupon-global" value="<?php echo $mc_coupons_global[0]; ?>"><br>
                <span class="description">The maximum number of times this coupon can be used. Note that the coupon will be automatically trashed once this maximum is reached</span>
            </td>
        </tr>
        <tr valign="top">
            <th scope="row"><label for="mycred-coupon-user">User Maximum</label></th>
            <td>
                <input type="text" name="mycred_coupon[user]" id="mycred-coupon-user" value="<?php echo $mc_coupons_user[0]; ?>"><br>
                <span class="description">The maximum number of times this coupon can be used by a user.</span>
            </td>
        </tr>
    </tbody>
</table>


            <br>
            <h2>Affiliate:</h2>
                    <div id="titlewrap">
                <!--<label class="" id="title-prompt-text" for="title">Unique Coupon Code</label>
                    <input type="text" name="affiliate" size="30" value="" id="search_affiliate" placeholder="Affiliate" spellcheck="true" >-->
                    
                    <select id='all_ids' name='all_ids' onchange='load_bookmarks(this)'>
                    <option value='default'>Select an affiliate</option>
                    <?php

                    //echo $current_user;
                    $p_aff_id=$wpdb->get_row("SELECT id FROM " .$table_affiliates.
                     " WHERE uid = '$current_user' ",ARRAY_N);
                    $c_aff_id=$wpdb->get_results("SELECT affiliate_id FROM " .$table_mlm.
                     " WHERE parent_affiliate_id = '$p_aff_id[0]' ",ARRAY_N);
                    //$names=array();
                    foreach($c_aff_id as $child)
                    {
                        $c_uid=$wpdb->get_row("SELECT uid FROM " .$table_affiliates.
                        " WHERE id = '$child[0]' ",ARRAY_N);
                        
                        //echo $name;
                        $affiliate=$wpdb->get_row("SELECT display_name FROM " .$table_users. " WHERE ID = '$c_uid[0]'",ARRAY_N);
                        //$affiliate=$wpdb->get_results("SELECT display_name FROM " .$table_users. " WHERE display_name //LIKE '$name%'");
                        //array_push($names,$affiliate[0]);
                        if($affiliate[0] == $_POST['all_ids']){
                          $isSelected = ' selected="selected"'; // if the option submited in form is as same as this row we add the selected tag
                     } else {
                          $isSelected = ''; // else we remove any tag
                     }
                        if($display_name[0]==$affiliate[0])
                        {
                            if($isSelected=='')
                            {
                                echo "<option value='".$affiliate[0]."' selected='selected'>".$affiliate[0]."</option>";
                            }
                            else
                            {
                                echo "<option value='".$affiliate[0]."'".$isSelected.">".$affiliate[0]."</option>";
                            }
                        }
                        else
                        {
                            echo "<option value='".$affiliate[0]."'".$isSelected.">".$affiliate[0]."</option>";
                        }    
                    }
                    ?>
                    </select>
                    <br>        
            </div>
                <!--<label class="" id="title-prompt-text" for="title">Unique Coupon Code</label>-->
                    
                    <ul id="ul" style="display:none; list-style:none;">
                    </ul>
            <br>
            <div id="publishing-action">
                <span class="spinner"></span>
                <input name="original_publish" type="hidden" id="original_publish" value="Update">
                <input type="submit" name="publish" id="publish" class="button button-primary button-large" value="Update"></div>
            </form>
            <?php
        }
        else
        {
            ?>
            <h2>You don't/can't have coupons..</h2>
            <?php   
        }
    }
}
add_shortcode('edit_coupon_code','edit_code');
//a shortcode
add_action("wp_ajax_update_coupon_code", "update_coupon_code");
add_action("wp_ajax_nopriv_update_coupon_code", "update_coupon_code");
function update_coupon_code()
{
    $post_id = $_POST['post_id'];
    $value =  $_POST['value'];
    $type =  $_POST['type'];
    $expire =  $_POST['expire'];
    $global =  $_POST['global'];
    $user =  $_POST['user'];
    $code =  $_POST['code'];
    $affiliate = $_POST['affiliate'];       
    $flag=false;

    global $wpdb;
        global $current_user;
        $current_user = get_current_user_id();

        $table_users = $wpdb->prefix . 'users';
        $table_posts = $wpdb->prefix . 'posts';
        $table_postmeta = $wpdb->prefix . 'postmeta';
        $table_usermeta = $wpdb->prefix . 'usermeta';
        $table_affiliates = $wpdb->prefix . 'uap_affiliates';
        $table_mlm = $wpdb->prefix . 'uap_mlm_relations';
        $table_uap_cp = $wpdb->prefix . 'uap_coupons_code_affiliates';
        $table_mc_cp = $wpdb->prefix . 'myCRED_log';

        
        $author_query = "SELECT post_author FROM ".$table_posts. " WHERE post_title='$code'";
        $author_id = $wpdb->get_row($author_query,ARRAY_N);
        if($current_user==$author_id[0])
        {
            $flag=true;
    $my_post = array(
      'ID'           => $post_id,
      'post_title'   => $code,
    );

    // Update the post into the database
    wp_update_post( $my_post );    

    update_post_meta( $post_id, 'value',  $value );
    update_post_meta( $post_id, 'global', $global );
    update_post_meta( $post_id, 'user',   $user );
    update_post_meta( $post_id, 'min',    0 );
    update_post_meta( $post_id, 'max',    0 );
    update_post_meta( $post_id, 'expires', $expire );

    $id_query = "SELECT ID FROM ".$table_users. " WHERE display_name='$affiliate'";
    $uid = $wpdb->get_row($id_query,ARRAY_N);
    $aff_id_query = "SELECT id FROM ".$table_affiliates. " WHERE uid='$uid[0]'";
    $aff_id = $wpdb->get_row($aff_id_query,ARRAY_N);
    $aff=$aff_id[0];
    $update_code="UPDATE ".$table_uap_cp." SET affiliate_id='$aff', code='$code' WHERE id='$post_id'";
    //$result_name = mysql_query($name_query) or die (mysql_error());
    $wpdb->query($update_code);
    

    echo $flag;
    }
    else
    {
        $flag=false;
        echo $flag;
    }

}
add_action("wp_ajax_get_post_id", "get_post_id");
add_action("wp_ajax_nopriv_get_post_id", "get_post_id");
function get_post_id()
{
    global $wpdb;
    $code = $_POST['code'];

    $table_posts = $wpdb->prefix . 'posts';
    $post_id_query = "SELECT ID FROM ".$table_posts.
        " WHERE post_title='$code'";
        $post_id = $wpdb->get_row($post_id_query,ARRAY_N);
        if($post_id[0]!='')
        {
            echo $post_id[0];
        }
        else
        {
            $flag=false;
            echo $flag;
        }

    

}
add_action("wp_ajax_delete_coupon", "delete_coupon");
add_action("wp_ajax_nopriv_delete_coupon", "delete_coupon");
function delete_coupon()
{
    global $wpdb;
    $code = $_POST['code'];

    $table_uap_cp = $wpdb->prefix . 'uap_coupons_code_affiliates';
    $table_posts = $wpdb->prefix . 'posts';

    $post_delete="DELETE FROM ".$table_posts." WHERE post_title='$code'";
    $wpdb->query($post_delete);
    $uap_delete="DELETE FROM ".$table_uap_cp." WHERE code='$code'";
    $wpdb->query($uap_delete);

}
function view_all_coupons()
{
    ?>
    <script>
    $(document).ready(function() {
    /*$('.edit').click(function (e) {
        var code = $(this).data('value');
        //alert(code);
        window.top.location.href = '<?php echo site_url(); ?>/edit-coupon/?code='+code;


    });*/
    $('.trash').click(function (e) {
        var code = $(this).data('value');
        //alert(code);
        $('.confirm_model').modal('show');
        $('#delete').click(function () {
        
        
        jQuery.ajax ( 
                        data = {
                        action: 'delete_coupon',
                        url: '/wp-admin/admin-ajax.php',
                        type: 'POST',
                        dataType: 'text',
                        'code' : code
                        

                    });
                    //alert("test");
                    jQuery.post(ajaxurl, data, function(responses) {

                        var dataz = responses;
                        //document.getElementById('friends_list').innerHTML += dataz;
                        alert( 'The promo code has been trashed.' );
                        console.log (dataz);
                        window.top.location.href = '<?php echo site_url(); ?>/view-all-coupons/';

                 
                
                    });
                    });
    });
    });
    </script>
    <?php
    global $wpdb;
    global $current_user;
    $current_user = get_current_user_id();
    if($current_user==0)
    {
        //not logged in currently!
        $output = "<h2>Please Log in!</h2>";
        return $output;
    }
    else
    {
        $table_users = $wpdb->prefix . 'users';
        $table_posts = $wpdb->prefix . 'posts';
        $table_postmeta = $wpdb->prefix . 'postmeta';
        $table_usermeta = $wpdb->prefix . 'usermeta';
        $table_affiliates = $wpdb->prefix . 'uap_affiliates';
        $table_mlm = $wpdb->prefix . 'uap_mlm_relations';
        $table_uap_cp = $wpdb->prefix . 'uap_coupons_code_affiliates';
        $table_mc_cp = $wpdb->prefix . 'myCRED_log';

        $business_check_query = "SELECT meta_value FROM ".$table_usermeta.
        " WHERE meta_key='user_category' AND user_id='$current_user'";
        $business_check = $wpdb->get_row($business_check_query,ARRAY_N);

        if($business_check[0] == 'bus')
        {
            $output = "<style>
                        table {
    font-family: arial, sans-serif;
    border-collapse: collapse;
    width: 100%;
                                }

    td, th {
    border: 1px solid #dddddd;
    text-align: left;
    padding: 8px;
    }

    tr:nth-child(even) {
    background-color: #dddddd;
    }
    </style>
    ";
            $output .= '<div class="modal fade confirm_model" id="confirmModal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Confirmation</h4>
      </div>
      <div class="modal-body">
            Are you sure?
            
      </div>
      <div class="modal-footer">
        <button type="button" data-dismiss="modal" class="btn btn-primary" id="delete">Delete</button>
    <button type="button" data-dismiss="modal" class="btn">Cancel</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->';
            $output .= "<h2>Your Coupons:</h2>";
            $output .= "<table>";
            $output .= "<tr>";
            $output .= "<th>Coupon Code</th>";
            $output .= "<th><div style='text-align:center'>QR Code</div></th>";
            $output .= "<th>Value</th>";
            $output .= "<th>Limit</th>";
            $output .= "<th>Expires</th>";
            $output .= "<th>Affiliate</th>";
            
            $output .= "</tr>";
            $mc_coupon_query = "SELECT ID,post_title,post_status FROM ".$table_posts.
            " WHERE post_author = '$current_user' AND post_type = 'mycred_coupon' ORDER BY post_date ASC";
            $mc_coupons = $wpdb->get_results($mc_coupon_query,ARRAY_N);
            if (count($mc_coupons)> 0)
            {
            foreach ($mc_coupons as $mc_coupon) 
            {
                //$mc_uid = $mc_coupon[3];
                $mc_id = $mc_coupon[0];
                $uap_coupon_query = "SELECT affiliate_id FROM ".$table_uap_cp.
                " WHERE id='$mc_id'";
                $uap_coupon = $wpdb->get_row($uap_coupon_query,ARRAY_N);
                if($uap_coupon[0] != '')
                {
                    $affiliate_id = $uap_coupon[0];
                    $affiliate_uid_query = "SELECT uid FROM ".$table_affiliates.
                    " WHERE id='$affiliate_id'";
                    $affiliate_uid = $wpdb->get_row($affiliate_uid_query,ARRAY_N);
                    $display_name_query = "SELECT display_name FROM ".$table_users." WHERE ID = '$affiliate_uid[0]'";
                    $display_name = $wpdb->get_row($display_name_query,ARRAY_N);
                    $mc_coupon_value_query = "SELECT meta_value FROM ".$table_postmeta.
                    " WHERE post_id = '$mc_id' AND meta_key = 'value'";
                    $mc_coupons_value = $wpdb->get_row($mc_coupon_value_query,ARRAY_N);
                    $mc_coupon_expire_query = "SELECT meta_value FROM ".$table_postmeta.
                    " WHERE post_id = '$mc_id' AND meta_key = 'expires'";
                    $mc_coupons_expire = $wpdb->get_row($mc_coupon_expire_query,ARRAY_N);
                    $mc_coupon_global_query = "SELECT meta_value FROM ".$table_postmeta.
                    " WHERE post_id = '$mc_id' AND meta_key = 'global'";
                    $mc_coupons_global = $wpdb->get_row($mc_coupon_global_query,ARRAY_N);
                    $mc_coupon_user_query = "SELECT meta_value FROM ".$table_postmeta.
                    " WHERE post_id = '$mc_id' AND meta_key = 'user'";
                    $mc_coupons_user = $wpdb->get_row($mc_coupon_user_query,ARRAY_N);
                    //$mc_coupon_data = $mc_coupons_data[0];
                    $output .= "<tr>";
                    $output .= "<td>".$mc_coupon[1]."<br><a href='" . site_url() . "/edit-coupon/?code=".$mc_coupon[1]."' class='edit' data- value=".$mc_coupon[1].">Edit</a> | <a href='#' class='trash' data-value=".$mc_coupon[1].">Trash</a></td>";

 if ( !file_exists( plugin_dir_path( __FILE__ ) . "cache/{$mc_coupon[1]}" ) )
                    QRcode::png( $mc_coupon[1], plugin_dir_path( __FILE__ ) . "cache/{$mc_coupon[1]}" );
                     $output .= "<td><div style='text-align:center'><img src = '" . site_url() . "/wp-content/plugins/geodirectory_child/cache/{$mc_coupon[1]}'/><br/>
                    <a href='" . site_url() . "/wp-content/plugins/geodirectory_child/cache/{$mc_coupon[1]}' download='{$mc_coupon[1]}.png'>Download</a> </div></td>";

                    $output .= "<td>".$mc_coupons_value[0]."</td>";
                    $output .= "<td>Global:".$mc_coupons_global[0]."<br>User:".$mc_coupons_user[0]."</td>";
                    if($mc_coupon[2]=='trash')
                    {
                        $output .= "<td>Expired</td>";    
                    }
                    else if($mc_coupon[2]=='publish')
                    {
                        $output .= "<td>".$mc_coupons_expire[0]."</td>";
                    }
                    $output .= "<td><a href='" . site_url() . "/members/".$display_name[0]."'>"
                    .$display_name[0]."</a></td>";
                    $output .= "</tr>";
                    //<a href='#'>Edit</a> | <a href='#'>Trash</a>


                }
            }
            }
            else
            {
                $output .= "</table>";
                $output .= "<h2>You have generated no coupons!</h2>";
                $output .= "<a href='" . site_url() . "/new-promo/'>
                <button>Create New</button>
                </a>";
                return $output;
            }
            $output .= "</table>";
            $output .= "<a href='" . site_url() . "/new-promo/'>
            <button>Create New</button>
            </a>";            
            return $output;
        }
        else
        {
            $output = "<h2>You don't/can't have coupons..</h2>";
            return $output;
        }   
    }
}
add_shortcode('view_all_coupon','view_all_coupons');
//a shortcode
function view_all_codes()
{
    global $wpdb;
    global $current_user;
    $current_user = get_current_user_id();
    if($current_user==0)
    {
        //not logged in currently!
        $output = "<h2>Please Log in!</h2>";
        return $output;
    }
    else
    {
        $table_users = $wpdb->prefix . 'users';
        $table_posts = $wpdb->prefix . 'posts';
        $table_postmeta = $wpdb->prefix . 'postmeta';
        $table_usermeta = $wpdb->prefix . 'usermeta';
        $table_affiliates = $wpdb->prefix . 'uap_affiliates';
        $table_mlm = $wpdb->prefix . 'uap_mlm_relations';
        $table_uap_cp = $wpdb->prefix . 'uap_coupons_code_affiliates';
        $table_mc_cp = $wpdb->prefix . 'myCRED_log';

        $business_check_query = "SELECT meta_value FROM ".$table_usermeta.
        " WHERE meta_key='user_category' AND user_id='$current_user'";
        $business_check = $wpdb->get_row($business_check_query,ARRAY_N);

        if($business_check[0] == 'ind')
        {
            $output = "<style>
                        table {
    font-family: arial, sans-serif;
    border-collapse: collapse;
    width: 100%;
                                }

    td, th {
    border: 1px solid #dddddd;
    text-align: left;
    padding: 8px;
    }

    tr:nth-child(even) {
    background-color: #dddddd;
    }
    </style>
    ";
            $output .= "<h2>Your Coupons:</h2>";
            $output .= "<table>";
            $output .= "<tr>";
            $output .= "<th>Coupon Code</th>";
            $output .= "<th><div style='text-align:center'>QR Code</div></th>";
            $output .= "<th>Value</th>";
            $output .= "<th>Limit</th>";
            $output .= "<th>Expires</th>";
            $output .= "<th>Business</th>";
            
            $output .= "</tr>";

            $affiliate_id_query = "SELECT id FROM ".$table_affiliates.
            " WHERE uid='$current_user'";
            $affiliate_id = $wpdb->get_row($affiliate_id_query,ARRAY_N);
            if($affiliate_id[0]!='')
            {
            $uap_coupon_query = "SELECT id FROM ".$table_uap_cp.
            " WHERE affiliate_id = '$affiliate_id[0]'";
            //$uap_coupon_query = "SELECT id FROM '".$table_uap_cp.
            //"' aff JOIN '".$table_posts."' post ON aff.code = post.post_title 
            //WHERE aff.affiliate_id = '$affiliate_id[0]'";
            $uap_coupons = $wpdb->get_results($uap_coupon_query,ARRAY_N);
            if (count($uap_coupons)> 0)
            {
                $counter=0;
            foreach ($uap_coupons as $uap_coupon) 
            {
                //$mc_uid = $mc_coupon[3];
                $uap_id = $uap_coupon[0];
                $mc_coupon_query = "SELECT post_title,post_author,post_status FROM ".$table_posts.
                " WHERE ID='$uap_id'";
                $mc_coupon_title = $wpdb->get_row($mc_coupon_query,ARRAY_N);
                
                  
                    $display_name_query = "SELECT display_name FROM ".$table_users." WHERE ID = '$mc_coupon_title[1]'";
                    $display_name = $wpdb->get_row($display_name_query,ARRAY_N);
                    $mc_coupon_value_query = "SELECT meta_value FROM ".$table_postmeta.
                    " WHERE post_id = '$uap_id' AND meta_key = 'value'";
                    $mc_coupons_value = $wpdb->get_row($mc_coupon_value_query,ARRAY_N);
                    $mc_coupon_expire_query = "SELECT meta_value FROM ".$table_postmeta.
                    " WHERE post_id = '$uap_id' AND meta_key = 'expires'";
                    $mc_coupons_expire = $wpdb->get_row($mc_coupon_expire_query,ARRAY_N);
                    $mc_coupon_global_query = "SELECT meta_value FROM ".$table_postmeta.
                    " WHERE post_id = '$uap_id' AND meta_key = 'global'";
                    $mc_coupons_global = $wpdb->get_row($mc_coupon_global_query,ARRAY_N);
                    $mc_coupon_user_query = "SELECT meta_value FROM ".$table_postmeta.
                    " WHERE post_id = '$uap_id' AND meta_key = 'user'";
                    $mc_coupons_user = $wpdb->get_row($mc_coupon_user_query,ARRAY_N);
                    $discount_query = "SELECT type,settings FROM ".$table_uap_cp." WHERE code = '$mc_coupon_title[0]'";
                    $discount1 = $wpdb->get_row($discount_query,ARRAY_N);
                    $meta_value_query = "SELECT meta_value From DEV_usermeta where user_id = '$mc_coupon_title[1]' AND meta_key='business_category'";

if($mc_coupon_title[1])
{
                    $metaValue = $wpdb->get_row($meta_value_query ,ARRAY_N);

if($metaValue[0] == 'pl')
{
$q = "SELECT ID FROM ".$table_posts." where post_author = ".$mc_coupon_title[1]." AND post_type = 'gd_place'";
 $bussiness_post = $wpdb->get_row($q,ARRAY_N);
$add_query = "SELECT post_address,post_city,post_region,post_country FROM DEV_geodir_gd_place_detail where post_id = ".$bussiness_post[0];
$complete_address = $wpdb->get_row($add_query ,ARRAY_N);
$address_string = $complete_address[0]. ", ".$complete_address[1].", ".$complete_address[2].", ".$complete_address[3];

}
else
{
$q = "SELECT ID FROM ".$table_posts." where post_author = ".$mc_coupon_title[1]." AND post_type = 'gd_event'";;
 $bussiness_post = $wpdb->get_row($q,ARRAY_N);
$add_query = "SELECT post_address,post_city,post_region,post_country FROM DEV_geodir_gd_event_detail where post_id = ".$bussiness_post[0];
$complete_address = $wpdb->get_row($add_query ,ARRAY_N);
$address_string = $complete_address[0]. ", ".$complete_address[1].", ".$complete_address[2].", ".$complete_address[3];

}


}


        
if($mc_coupon_title[1])
{
$tweet_text = "Use this code: \"".$mc_coupon_title[0]."\" to avail of ".$discount1[0]."% discount at \"".$display_name[0]."\", ".$address_string." .$discount1[1].";
$text = urlencode($tweet_text);
$url1 = urlencode("https://circleofbiz/");
                    $output .= "<tr>";
                    $output .= "<td>".$mc_coupon_title[0]."</td>";
                    if ( !file_exists( plugin_dir_path( __FILE__ ) . "cache/{$mc_coupon_title[0]}" ) )
                    QRcode::png( $mc_coupon_title[0], plugin_dir_path( __FILE__ ) . "cache/{$mc_coupon_title[0]}" );
                         $output .= "<td><div style='text-align:center'><img src = '" . site_url() . "/wp-content/plugins/geodirectory_child/cache/{$mc_coupon_title[0]}'/><br/>
                    <a href='" . site_url() . "/wp-content/plugins/geodirectory_child/cache/{$mc_coupon_title[0]}'  download='{$mc_coupon_title[0]}.png'>Download</a> <br/><a href = 'https://www.facebook.com/sharer/sharer.php?u=" . site_url() . "/&picture=" . site_url() . "/wp-content/plugins/geodirectory_child/cache/{$mc_coupon_title[0]}&title=CircleOfBiz&description=Use+this+code:+\"{$mc_coupon_title[0]}\"+to+avail+of+{$discount1[0]}%+discount+at+\"{$display_name[0]}\",+{$address_string}+{$discount1[1]}.' target = '_blank'>Share on Facebook</a><br/><a href = 'http://twitter.com/share?url={$url1}&text={$text}.' target = '_blank'>Share on Twitter</a>

<br/><button type='button' id='btnm' name='btnm' >Share with Email</button></div> </td>";
                    $output .= "<td>".$mc_coupons_value[0]."</td>";
                    $output .= "<td>Global:".$mc_coupons_global[0]."<br>User:".$mc_coupons_user[0]."</td>";
                  
?>

<style>
/* The Modal (background) */
.modal1 {
    display: none; /* Hidden by default */
    position: fixed;
    z-index: 1000;
    padding-top: 0px;
    left: 0;
    top: 0;
    width: 100%;
    height: 100%;
    overflow: hidden;
    background-color: rgba(0, 0, 0, 0.61);
}

/* Modal Content */
.modal-content1 {
float: none;
    background-color: #fefefe;
    margin: 50px auto;
    padding: 0;
    border: 1px solid #888;
    width: 50%;
    box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2), 0 6px 20px 0 rgba(0,0,0,0.19);
    -webkit-animation-name: animatetop;
    -webkit-animation-duration: 0.4s;
    animation-name: animatetop;
    animation-duration: 0.4s;
    display: block;
    overflow: hidden;
}
.modal-content1 form#email_popup_form{
    padding: 30px;
    width: 100%;
    float: left;
    background: #fff;
}
.modal-content1 form#email_popup_form div#reg_passmail{
    float: left;
    width: 100%;
    margin-bottom: 20px;
}
.modal-content1 form#email_popup_form > input#e_address,
.modal-content1 form#email_popup_form > textarea#e_message{
float:left;
width:100%;
margin-bottom:20px;
}
.modal-content1 .modal-header1 > span.close{
opacity:1;
}

/* Add Animation */
@-webkit-keyframes animatetop {
    from {top:-300px; opacity:0} 
    to {top:0; opacity:1}
}

@keyframes animatetop {
    from {top:-300px; opacity:0}
    to {top:0; opacity:1}
}

/* The Close Button */
.close {
    color: white;
    float: right;
    font-size: 28px;
    font-weight: bold;
}

.close:hover,
.close:focus {
    color: #000;
    text-decoration: none;
    cursor: pointer;
}

.modal-header1 {
    padding: 2px 16px;
    background-color: #ED6D62;
    color: white;
}

.modal-body1 {padding: 2px 16px;}

.modal-footer1 {
    padding: 2px 16px;
    background-color: #ED6D62;
    color: white;
    height:70px;
}
#submit{
margin-top: 10px;
}
</style>

<div id='myModal' class='modal1'>

  <!-- Modal content -->
  <div class='modal-content1'>
    <div class='modal-header1'>
      <span class='close'>&times;</span>
      <h2>Send Coupon via Email </h2>
    </div>
    <div class='modal-body1'>
      
    </div>
      <form name='email_popup_form' id='email_popup_form'
              action='#' method='post'>
                
            <div id='reg_passmail'>

                    Enter Recipient Email Address(s). Email should be coma(,) separated if recipients are multiple:
            </div>
                <b>To:</b><input type='text' id='e_address' name='e_address' /><br>
                <b>Custom Message:</b><br><textarea rows='5' name='e_message' cols='30' id='e_message'></textarea><br>
            <input type='submit' name='send_email' value='Send' class='geodir_button' id='send_email'/>
        </form>
    <div class='modal-footer1'>
     
    </div>
  </div>

</div>
<?php
echo "<script>
var $ = jQuery;
$(document).ready(function(e){
var span = document.getElementsByClassName('close')[0];
span.onclick = function() {
   $('#myModal').hide();
}
$( '#btnm' ).click(function() {
$('#myModal').show();
  
});

});
</script>
";
if(isset($_POST['send_email'])){
require_once(plugin_dir_path( __FILE__ )."/PHPMailer/class.phpmailer.php");
require_once(plugin_dir_path( __FILE__ )."/PHPMailer/class.smtp.php");


$to = $_POST['e_address'];
$subject = "Invitation";
$message = $_POST['e_message'];
$url = site_url();
$current_user = apply_filters( 'determine_current_user', false );
$u_query = "SELECT user_email,display_name From DEV_users where id = ".$current_user;
$c_user = $wpdb->get_row($u_query ,ARRAY_N);
$from_email = $c_user[0];
$from_name = $c_user[1];
$img = "https://circleofbiz.com/wp-content/plugins/geodirectory_child/cache/".$mc_coupon_title[0];

$mail = new PHPMailer();

$mail->IsSendmail();
//Set who the message is to be sent from
$mail->From = "no-reply@circleofbiz.com";
$mail->setFrom("no-reply@circleofbiz.com","CircleofBiz");
$mail->FromName = "CircleofBiz";

$addr = explode(',',$to);

foreach ($addr as $ad) {
    $mail->AddAddress( trim($ad) );       
}

$mail->Subject = $subject;
//$mail->AddReplyTo($c_user[0],"CircleofBiz");
$mail->msgHTML(true);
//
$mail->Body = '<div class="begining">  
                        <p>
                            Dear <span>User</span> <br/>
                           Please share coupon with your friends and enjoy exciting deals.<br/> <br/>
                        </p>
                    </div>
                    <p> '.$message.' </p><br><br>
                    <table border="1" style="width:80% ;margin: auto;height:50%">

                        <tr>
                            <td><b>QR Code</b> </td>
                            <td><div style="align:center"><b>Description</b></div></td>
                        </tr>
                        <tr>
                   <td> <img src="https://circleofbiz.com/wp-content/plugins/geodirectory_child/cache/'.$mc_coupon_title[0].'" width="150" height ="150" alt="QrCode" /> </td>
                        
                            <td><p style = "font-size:20px"><b>CircleofBiz</b></p><br/>Use this code : <b>"'.$mc_coupon_title[0].'"</b> to avail of '.$discount1[0].'% discount at  '.$display_name[0].', '.$address_string.'   '.$discount1[1].'.</td>
                        </tr>
                  </table>
                        <div class="begining">  
                        <p><br/> <br/>

Thanks
                        Sincerely,<br/><br/>

            '.$url.'<br/><br/><
            <br/>
            </p>
                    </div>';
$mail->AddEmbeddedImage("https://circleofbiz.com/wp-content/plugins/geodirectory_child/cache/'{$mc_coupon_title[0]}'","Qr_Code");

if(!$mail->Send())
{
   echo "<script> alert('Error Sending Email... ".$mail->ErrorInfo."'); 
                   window.location = 'https://circleofbiz.com/view-all-codes/';
        </script>";
  // header('Location: https://circleofbiz.com/view-all-codes/');
}
else
{
   echo "<script> alert('Email sent successfully'); 
                  window.location = 'https://circleofbiz.com/view-all-codes/';
        </script>";
   //header('Location: https://circleofbiz.com/view-all-codes/');
}

}

                    if($mc_coupon_title[2]=='trash')
                    {
                        $output .= "<td>Expired</td>";
                    }
                    else if($mc_coupon_title[2]=='publish')
                    {
                        $output .= "<td>".$mc_coupons_expire[0]."</td>";
                    }
                    $output .= "<td><a href='" . site_url() . "/members/".$display_name[0]."'>"
                    .$display_name[0]."</a></td>";
                    $output .= "</tr>";
                    $counter++;

                
            }
}
            }
            else
            {
                $output .= "</table>";
                $output = "<h2>No Coupons are assigned to you.</h2>";
                return $output;          
            }
            if($counter>0)
            {
                $output .= "</table>";
                
                return $output;
            }            
         }
         else
         {
            $output .= "</table>";
            $output = "<h2>You are not an affiliate of any business!</h2>";
            return $output;   
         }   
        }
        else
        {
            $output = "<h2>You don't/can't have promo codes..</h2>";
            return $output;
        }   
    }
}

add_shortcode('view_all_code','view_all_codes');
//a shortcode

function view_user_log()
{
    global $wpdb;
    global $current_user;
    $current_user = get_current_user_id();
    if($current_user==0)
    {
        //not logged in currently!
        $output = "<h2>Please Log in!</h2>";
        return $output;
    }
    else
    {
        $table_users = $wpdb->prefix . 'users';
        $table_posts = $wpdb->prefix . 'posts';
        $table_postmeta = $wpdb->prefix . 'postmeta';
        $table_usermeta = $wpdb->prefix . 'usermeta';
        $table_affiliates = $wpdb->prefix . 'uap_affiliates';
        $table_mlm = $wpdb->prefix . 'uap_mlm_relations';
        $table_uap_cp = $wpdb->prefix . 'uap_coupons_code_affiliates';
        $table_mc_cp = $wpdb->prefix . 'myCRED_log';
        $table_voucher = $wpdb->prefix . 'gtvouchers_vouchers';

        $business_check_query = "SELECT meta_value FROM ".$table_usermeta.
        " WHERE meta_key='user_category' AND user_id='$current_user'";
        $business_check = $wpdb->get_row($business_check_query,ARRAY_N);

        if($business_check[0] == 'ind')
        {
            $points=0;
            $output = "<style>
                        table {
    font-family: arial, sans-serif;
    border-collapse: collapse;
    width: 100%;
                                }

    td, th {
    border: 1px solid #dddddd;
    text-align: left;
    padding: 8px;
    }

    tr:nth-child(even) {
    background-color: #dddddd;
    }
    </style>
    ";
            $output .= "<h2>Choose a Business to see the points in accordance:</h2>";
            $output .= "<form method='post'>";
            $output .= "<select id='all_ids' name='all_ids' onchange='load_bookmarks(this)'>";
            $output .= "<option value='default' selected='selected'>Select a Business</option>";
            
            
            //$output .= "<option selected='selected'>Select a business</option>";
            //$output .= "<option value='default' selected='selected'>Select a business</option>";
            //$table_users = $wpdb->prefix . 'users';
            $aff_id_query = "SELECT id FROM ".$table_affiliates.
            " WHERE uid='$current_user'";
            $aff_id = $wpdb->get_row($aff_id_query,ARRAY_N);

            $parent_id_query = "SELECT parent_affiliate_id FROM ".$table_mlm.
            " WHERE affiliate_id='$aff_id[0]'";
            $parent_ids = $wpdb->get_results($parent_id_query,ARRAY_N);
            //$query="SELECT user_login FROM ".$table_users;
            //$result = $wpdb->get_results($query,ARRAY_N);
            if(count($parent_ids)> 0)
            {
            foreach ($parent_ids as $parent_id)
            {
                $parent_uid_query = "SELECT uid FROM ".$table_affiliates.
                " WHERE id='$parent_id[0]'";
                $parent_uid = $wpdb->get_row($parent_uid_query,ARRAY_N);
                $dn_query = "SELECT display_name, user_nicename FROM ".$table_users.
                " WHERE ID='$parent_uid[0]'";
                $dn = $wpdb->get_row($dn_query,ARRAY_N);

                if($dn[0] == $_POST['all_ids']){
                  $isSelected = ' selected="selected"'; // if the option submited in form is as same as this row we add the selected tag
             } else {
                  $isSelected = ''; // else we remove any tag
             }
                $output .= "<option value='".$dn[1]."'".$isSelected.">".$dn[0]."</option>";

            //    echo "<p>".$row[0]."</p>";
            }
            $output .= "</select>";
            $output .= "<input type='submit' value='Sumbit my choice'/>";
            $output .= "</form>";
            }
            else
            {
                $output .= "</select>";
                $output = "<h3>No business has you as an affiliate.</h3>";
            }
            
            
            //////////////////////////////////////////////////////////////
            $business=$_POST['all_ids'];
            if($business != '')
            {
            $output .= "<h2>Points:</h2>";
            $output .= "<table>";
            $output .= "<tr>";
            $output .= "<th>Promo Code/Voucher</th>";
            $output .= "<th>Entry</th>";
            $output .= "<th>Points</th>";
            $output .= "<th>Business</th>";
            $output .= "</tr>";

            $log_query = "SELECT creds,entry,data,ref_id FROM ".$table_mc_cp." WHERE user_id = '$current_user'";
            $log_details = $wpdb->get_results($log_query,ARRAY_N);
            if (count($log_details)> 0)
            {
                $counter=0;
            foreach($log_details as $log_row)
            {
                $aff_uid_query = "SELECT post_author FROM ".$table_posts." WHERE ID = '$log_row[3]'";
                $aff_uid = $wpdb->get_row($aff_uid_query,ARRAY_N);
                $dn_query = "SELECT display_name, user_nicename FROM ".$table_users." WHERE ID = '$aff_uid[0]'";
                $dn = $wpdb->get_row($dn_query,ARRAY_N);
                if($dn[1]!="")
                {
                    if($business==$dn[1])
                    {
                        $output .= "<tr>";
                        $output .= "<td>".$log_row[2]."</td>";
                        $output .= "<td>".$log_row[1]."</td>";
                        $output .= "<td>".$log_row[0]."</td>";
                        $output .= "<td>".$dn[0]."</td>";
                        $output .= "</tr>";
                        $points += $log_row[0];
                        $counter++;
                    }
                }
                else
                {
                    $v_name = substr($log_row[1], 24, -2);
                    $v_auth_query = "SELECT author_id FROM ".$table_voucher." WHERE name = '$v_name'";
                    $v_auth_id = $wpdb->get_row($v_auth_query,ARRAY_N);
                    $dn_query = "SELECT display_name, user_nicename FROM ".$table_users." WHERE ID = '$v_auth_id[0]'";
                    $dn = $wpdb->get_row($dn_query,ARRAY_N);
                    if($business==$dn[1])
                    {
                        $output .= "<tr>";
                        $output .= "<td>".$v_name."</td>";
                        $output .= "<td>".$log_row[1]."</td>";
                        $output .= "<td>".$log_row[0]."</td>";
                        $output .= "<td>".$dn[0]."</td>";
                        $output .= "</tr>";
                        $points += $log_row[0];
                        $counter++;
                    }
                }
            }
            }
            else
            {
                $output .= "</table>";
                $output .= "<h2>No one has redeemed any of the promo codes assigned to you.</h2>";
                return $output; 
            }
            if($counter>0)
            {
                $output .= "</table>";
                
                $output .= "<p><b>Total Points : ".$points."</b></p>";
                return $output;
            }
        }
        else
        {
            return $output;
        }
        //return $output;
        }
        else if($business_check[0] == 'bus')
        {
            $points=0;
            $output = "<style>
                        table {
    font-family: arial, sans-serif;
    border-collapse: collapse;
    width: 100%;
                                }

    td, th {
    border: 1px solid #dddddd;
    text-align: left;
    padding: 8px;
    }

    tr:nth-child(even) {
    background-color: #dddddd;
    }
    </style>
    ";
            $output .= "<h2>Choose an affiliate to see the points in accordance:</h2>";
            $output .= "<form method='post'>";
            $output .= "<select id='all_ids' name='all_ids' onchange='load_bookmarks(this)'>";
            $output .= "<option value='default' selected='selected'>Select an affiliate</option>";
            
            //$output .= "<option selected='selected'>Select a business</option>";
            //$output .= "<option value='default' selected='selected'>Select a business</option>";
            //$table_users = $wpdb->prefix . 'users';
            $p_aff_id=$wpdb->get_row("SELECT id FROM " .$table_affiliates.
             " WHERE uid = '$current_user' ",ARRAY_N);
            $c_aff_id=$wpdb->get_results("SELECT affiliate_id FROM " .$table_mlm.
             " WHERE parent_affiliate_id = '$p_aff_id[0]' ",ARRAY_N);
            //$names=array();
            if(count($c_aff_id)> 0)
            {
            foreach($c_aff_id as $child)
            {
                $c_uid=$wpdb->get_row("SELECT uid FROM " .$table_affiliates.
                " WHERE id = '$child[0]' ",ARRAY_N);
                
                //echo $name;
                $affiliate=$wpdb->get_row("SELECT display_name, user_nicename FROM " .$table_users. " WHERE ID = '$c_uid[0]'",ARRAY_N);
                //$affiliate=$wpdb->get_results("SELECT display_name FROM " .$table_users. " WHERE display_name //LIKE '$name%'");
                //array_push($names,$affiliate[0]);
                if($affiliate[0] == $_POST['all_ids']){
                  $isSelected = ' selected="selected"'; // if the option submited in form is as same as this row we add the selected tag
             } else {
                  $isSelected = ''; // else we remove any tag
             }
                $output .= "<option value='".$affiliate[1]."'".$isSelected.">".$affiliate[0]."</option>";    
            }
            $output .= "</select>";
            $output .= "<input type='submit' value='Sumbit my choice'/>";
            $output .= "</form>";
            }
            else
            {
                $output .= "</select>";
                $output = "<h3>You have no afiliates.</h3>";
                //return $output;
            }
            
            
            //////////////////////////////////////////////////////////////
            $business=$_POST['all_ids'];
            if($business!="")
            {
            $output .= "<h2>Points:</h2>";
            $output .= "<table>";
            $output .= "<tr>";
            $output .= "<th>Affiliate</th>";
            $output .= "<th>Promo Code/Voucher</th>";
            $output .= "<th>Entry</th>";
            $output .= "<th>Points</th>";
            $output .= "</tr>";
            $log_query = "SELECT creds,entry,data,ref_id,user_id FROM ".$table_mc_cp;
            $log_details = $wpdb->get_results($log_query,ARRAY_N);
            if (count($log_details)> 0)
            {
                $counter=0;
            foreach($log_details as $log_row)
            {
                $auth_query = "SELECT post_author FROM ".$table_posts." WHERE post_title = '$log_row[2]'";
                $auth_id = $wpdb->get_row($auth_query,ARRAY_N);
                $dn_query = "SELECT display_name, user_nicename FROM ".$table_users." WHERE ID = '$log_row[4]'";
                $dn = $wpdb->get_row($dn_query,ARRAY_N);
                if($auth_id[0]==$current_user)
                {
                    if($dn[1]==$business)
                    {
                        $output .= "<tr>";
                        $output .= "<td>".$dn[0]."</td>";
                        $output .= "<td>".$log_row[2]."</td>";
                        $output .= "<td>".$log_row[1]."</td>";
                        $output .= "<td>".$log_row[0]."</td>";
                        $output .= "</tr>";
                        $points += $log_row[0];
                        $counter++;
                    }
                }
                if($log_row[2]=="")
                {
                    $v_name = substr($log_row[1], 24, -2);
                    $v_auth_query = "SELECT author_id FROM ".$table_voucher." WHERE name = '$v_name'";
                    $v_auth_id = $wpdb->get_row($v_auth_query,ARRAY_N);
                    if($v_auth_id[0]==$current_user)
                    {
                        if($dn[1]==$business)
                        {
                            $output .= "<tr>";
                            $output .= "<td>".$dn[0]."</td>";
                            $output .= "<td>".$v_name."</td>";
                            $output .= "<td>".$log_row[1]."</td>";
                            $output .= "<td>".$log_row[0]."</td>";
                            $output .= "</tr>";
                            $points += $log_row[0];
                            $counter++;
                        }
                    }
                }
            }
            }
            else
            {
                $output .= "</table>";
                $output .= "<h2>No entry!</h2>";
                return $output; 
            }
            if($counter>0)
            {
                $output .= "</table>";
                
                $output .= "<p><b>Total Points : ".$points."</b></p>";
                return $output;
            }
            /*$aff_ids=array();
            $mc_coupon_query = "SELECT ID,post_status FROM ".$table_posts.
            " WHERE post_author = '$current_user' AND post_type = 'mycred_coupon'";
            $mc_coupons = $wpdb->get_results($mc_coupon_query,ARRAY_N);
            if (count($mc_coupons)> 0)
            {
                $counter=0;
                $temp = '';
            foreach ($mc_coupons as $mc_coupon) 
            {
                if($mc_coupon[1]=="trash")
                {
                    $mc_id = $mc_coupon[0];
                    $uap_coupon_query = "SELECT affiliate_id FROM ".$table_uap_cp.
                    " WHERE id='$mc_id'";
                    $uap_coupon = $wpdb->get_row($uap_coupon_query,ARRAY_N);

                    if($uap_coupon[0] != '' AND $uap_coupon[0] != $temp)
                    {
                        array_push($aff_ids, $uap_coupon[0]);
                    }
                    $temp = $uap_coupon[0];
                }
                $counter++;
            }
                    
        }
        else
        {
            $output .= "</table>";
            $output = "<h2>No affiliate of yours has redeemed any promo code.</h2>";
            return $output;
        }
        if($counter>0)
        {
            $counter1=sizeof($aff_ids);
            $counter2=0;
            foreach($aff_ids as $ids)
            {
                $affiliate_uid_query = "SELECT uid FROM ".$table_affiliates.
                    " WHERE id='$ids'";
                    $affiliate_uid = $wpdb->get_row($affiliate_uid_query,ARRAY_N);
                    $display_name_query = "SELECT display_name FROM ".$table_users." WHERE ID = '$affiliate_uid[0]'";
                    $display_name = $wpdb->get_row($display_name_query,ARRAY_N);
                $log_query = "SELECT creds,entry,data FROM ".$table_mc_cp." WHERE user_id = '$affiliate_uid[0]'";
            $log_details = $wpdb->get_results($log_query,ARRAY_N);
            
            foreach($log_details as $log_row)
            {
                $output .= "<tr>";
                $output .= "<td>".$display_name[0]."</td>";
                //$output .= "<td>".sizeof($aff_ids)."</td>";
                $output .= "<td>".$log_row[2]."</td>";
                $output .= "<td>".$log_row[1]."</td>";
                $output .= "<td>".$log_row[0]."</td>";
                $output .= "</tr>";
                
            }
            $counter2++;
            if($counter2==$counter1)
                break;
            }
            $output .= "</table>";
            return $output;
        }*/
    }
    else
        {
            return $output;
        }
    }
}
}
add_shortcode('view_log','view_user_log');
//a shortcode
function create_voucher()
{
    global $wpdb;
    global $current_user;
    $current_user = get_current_user_id();
    if($current_user==0)
    {
        //not logged in currently!
        $output = "<h2>Please Log in!</h2>";
        return $output;
    }
    else
    {
        $table_users = $wpdb->prefix . 'users';
        $table_posts = $wpdb->prefix . 'posts';
        $table_postmeta = $wpdb->prefix . 'postmeta';
        $table_usermeta = $wpdb->prefix . 'usermeta';
        $table_affiliates = $wpdb->prefix . 'uap_affiliates';
        $table_mlm = $wpdb->prefix . 'uap_mlm_relations';
        $table_uap_cp = $wpdb->prefix . 'uap_coupons_code_affiliates';
        $table_mc_cp = $wpdb->prefix . 'myCRED_log';

        $business_check_query = "SELECT meta_value FROM ".$table_usermeta.
        " WHERE meta_key='user_category' AND user_id='$current_user'";
        $business_check = $wpdb->get_row($business_check_query,ARRAY_N);

        if($business_check[0] == 'bus')
        {
    ?>
    <script>
    var $=jQuery;
    function validateDateFormat(dateVal){
 
      var dateVal = dateVal;
 
      if (dateVal == null)
          return false;
 
      var validatePattern = /^(\d{4})(\/|-)(\d{1,2})(\/|-)(\d{1,2})$/;
 
          dateValues = dateVal.match(validatePattern);
 
          if (dateValues == null)
              return false;
 
      var dtYear = dateValues[1];        
          dtMonth = dateValues[3];
          dtDay=  dateValues[5];
 
       if (dtMonth < 1 || dtMonth > 12)
          return false;
       else if (dtDay < 1 || dtDay> 31)
         return false;
       else if ((dtMonth==4 || dtMonth==6 || dtMonth==9 || dtMonth==11) && dtDay ==31)
         return false;
       else if (dtMonth == 2){
         var isleap = (dtYear % 4 == 0 && (dtYear % 100 != 0 || dtYear % 400 == 0));
         if (dtDay> 29 || (dtDay ==29 && !isleap))
                return false;
      }
 
     return true;
   }
    $(document).ready(function() 
    {
        $('#form').on('submit', function(e) {

            e.preventDefault();
            $('input[type="submit"]').prop('disabled', true);
            //alert('Voucher has been successfully submitted.');
            //window.top.location.href = 'http://link1n1.com/DEV/see-vouchers/';
            var req_creds = $('#form').find('input[id=v_cred]').val();
            var name = $('#form').find('input[id=v_name]').val();
            var des = $('#form').find('input[id=v_des]').val();
            var expire = $('#form').find('input[id=v_expire]').val();
            if(req_creds!='' && name!='' && des!='' && expire!='')
            {
                var date_check=validateDateFormat(expire);
                if(date_check==true)
                {
                    if(req_creds>0)
                    {
            //alert(req_creds);
            //alert(name);
            //alert(des);
            //alert(expire);
            jQuery.ajax ( 
                data = {
                action: 'submit_voucher_data',
                url: '/wp-admin/admin-ajax.php',
                type: 'POST',
                dataType: 'text',
                'req_creds' : req_creds,
                'name' : name,
                'description' : des,
                'expire' : expire

            });
            //alert("test");
            jQuery.post(ajaxurl, data, function(response) {

                var dataz = response;
                //document.getElementById('friends_list').innerHTML += dataz;
              //  alert( dataz );
                console.log (dataz); //show json in console
                alert('Voucher has been successfully submitted.');
                window.top.location.href = '<?php echo site_url(); ?>/see-vouchers/';
                    


            });
            //alert("test");
        }
        else
        {
            $('input[type="submit"]').prop('disabled', false);
            alert("Required Credits to Download must be greater then 0.");
        }
        }
        else
        {
            $('input[type="submit"]').prop('disabled', false);
            alert("Invalid Date Format.");
        }
        }
        else
        {
            $('input[type="submit"]').prop('disabled', false);
            alert("One of the fields is missing.");
        }
  });
    });

    </script>
    <form name="form" method="post" id="form" autocomplete="off">
    <h1>Create a Voucher</h1>
    <div class="geodir_form_row clearfix">How many Points are required to download this ? <input type="number" step="1" name="require_creds" min="0" value="0" id="v_cred" style="width:90px;"></div>
    <h2>Voucher Name</h2>
        <div id="titlewrap">
            <!--<label class="" id="title-prompt-text" for="title">Unique Coupon Code</label>-->
                <input type="text" name="v_name" size="30" value="" id="v_name" spellcheck="true" autocomplete="off" placeholder="Voucher Name">
        </div>
    <br>
    <h2>Description</h2>
        <div id="titlewrap">
                <!--<label class="" id="title-prompt-text" for="title">Unique Coupon Code</label>-->
                    <input type="text" name="v_des" size="30" value="" id="v_des" spellcheck="true"   autocomplete="off" placeholder="Description">
        </div>
    <br>
    <h2>Expires</h2>
        <div id="titlewrap">
                <!--<label class="" id="title-prompt-text" for="title">Unique Coupon Code</label>-->
                    <input type="text" name="v_expire" size="30" value="" id="v_expire" spellcheck="true"   autocomplete="off" placeholder="YYYY-MM-DD">
        </div>
    <br>
    <div id="publishing-action">
                <span class="spinner"></span>
                <input name="original_publish" type="hidden" id="original_publish" value="Publish">
                <input type="submit" name="publish" id="publish" class="button button-primary button-large" value="Publish">
    </div>
    </form>
        

    <?php
    }
    else
    {
        $output = "<h2>You can't create vouchers..</h2>";
        return $output;
    }
}
}
add_shortcode('create_v','create_voucher');
//a shortcode
add_action("wp_ajax_submit_voucher_data", "submit_voucher_data");
add_action("wp_ajax_nopriv_submit_voucher_data", "submit_voucher_data");
function submit_voucher_data()
{
    $req_creds =  $_POST['req_creds'];
    $name =  $_POST['name'];
    $expire =  $_POST['expire'];
    $des =  $_POST['description'];
    
    
    global $wpdb;
    global $current_user;
    $current_user = get_current_user_id();

    $table_users = $wpdb->prefix . 'users';
    $table_posts = $wpdb->prefix . 'posts';
    $table_postmeta = $wpdb->prefix . 'postmeta';
    $table_usermeta = $wpdb->prefix . 'usermeta';
    $table_affiliates = $wpdb->prefix . 'uap_affiliates';
    $table_mlm = $wpdb->prefix . 'uap_mlm_relations';
    $table_uap_cp = $wpdb->prefix . 'uap_coupons_code_affiliates';
    $table_mc_cp = $wpdb->prefix . 'myCRED_log';
    $table_voucher = $wpdb->prefix . 'gtvouchers_vouchers';

    $count_query = "SELECT MAX(id) FROM ".$table_voucher;
    $count = $wpdb->get_row($count_query,ARRAY_N);
    $id = $count[0] + 1;
    $author_id = $current_user;
    $expiry = strtotime($expire);
    $success="success";
    $blog_id = gtvouchers_blog_id();
    $guid    = gtvouchers_guid( 36 );
    //echo $id;
    $sql="INSERT INTO ".$table_voucher." VALUES ('$id', '$blog_id', '$author_id', 1, DEFAULT, '$name', '$des', '$des', '$des', 1, 'times', '#000000', 0, '', DEFAULT, '$guid', 1, DEFAULT, '$expiry', DEFAULT, '', '', 6, '', DEFAULT, DEFAULT, DEFAULT, '', DEFAULT, DEFAULT, '$req_creds')";
    $wpdb->query($sql);
    
    //echo $result;


}
function view_all_vouchers()
{
    global $wpdb;
    global $current_user;
    $current_user = get_current_user_id();
    if($current_user==0)
    {
        //not logged in currently!
        $output = "<h2>Please Log in!</h2>";
        return $output;
    }
    else
    {
        $table_users = $wpdb->prefix . 'users';
        $table_posts = $wpdb->prefix . 'posts';
        $table_postmeta = $wpdb->prefix . 'postmeta';
        $table_usermeta = $wpdb->prefix . 'usermeta';
        $table_affiliates = $wpdb->prefix . 'uap_affiliates';
        $table_mlm = $wpdb->prefix . 'uap_mlm_relations';
        $table_uap_cp = $wpdb->prefix . 'uap_coupons_code_affiliates';
        $table_mc_cp = $wpdb->prefix . 'myCRED_log';
        $table_voucher = $wpdb->prefix . 'gtvouchers_vouchers';

        $business_check_query = "SELECT meta_value FROM ".$table_usermeta.
        " WHERE meta_key='user_category' AND user_id='$current_user'";
        $business_check = $wpdb->get_row($business_check_query,ARRAY_N);

        if($business_check[0] == 'bus')
        {
            //header("Refresh:0");
            $output = "";
            $output .= "<h1>Vouchers:</h1>";
            

            $voucher_id_query = "SELECT id FROM ".$table_voucher.
            " WHERE author_id='$current_user'";
            $voucher_ids = $wpdb->get_results($voucher_id_query,ARRAY_N);
            if(count($voucher_ids)>0)
            {
                //$output .= "<script type='text/javascript'>alert(count('$voucher_ids'));</script>";
                foreach($voucher_ids as $voucher_id)
                {
                    $output .= "[voucher id=".$voucher_id[0]."]";
                }
            }
            else
            {
                $output .= "<h3>You have created no vouchers!</h3>";
            }
            $output .= "<a href='" . site_url() . "/create-voucher/'>
                <button>Create New</button>
                </a>";
                //$output .= "<p>".count($voucher_ids)."</p>";
            return $output;
         }
         else if($business_check[0] == 'ind')
         {
            $output = "<h2>You are not authorized to see vouchers!</h2>";
            return $output;
         }
     }
}
add_shortcode('view_v','view_all_vouchers');
//a shortcode
function use_incentives()
{
    global $wpdb;
    //session_start();
    global $current_user;
    $current_user = get_current_user_id();
    if($current_user==0)
    {
        //not logged in currently!
        $output = "<h2>Please Log in!</h2>";
        return $output;
    }
    else
    {
        ?>
        <style>
    body    {  background-color: rgba(0,0,0, 0.9);}
#box    {  
           background-color: rgba(0,0,0, 0.9);
          background:url(http://ianfarb.com/wp-content/uploads/2013/10/nicholas-hodag.jpg);
  background-size:cover;
overflow:hidden;}

#overlay    {   
    background-color: rgba(0,0,0, 0.9);
               text-align:center;
               overflow-x: hidden;
                transition: 0.5s;
               opacity:0.1;
               -webkit-transition: opacity .25s ease;
-moz-transition: opacity .25s ease;}

#box:hover #overlay {
               opacity:1;}

.voucher-list-place       {  font-family:Helvetica;
               font-weight:900;
               color:rgba(255,0,0,0.6);
               font-size:16px;
           font-family: "Times New Roman", Georgia, Serif;}
</style>


</style>
        <?php
        $table_users = $wpdb->prefix . 'users';
        $table_posts = $wpdb->prefix . 'posts';
        $table_postmeta = $wpdb->prefix . 'postmeta';
        $table_usermeta = $wpdb->prefix . 'usermeta';
        $table_affiliates = $wpdb->prefix . 'uap_affiliates';
        $table_mlm = $wpdb->prefix . 'uap_mlm_relations';
        $table_uap_cp = $wpdb->prefix . 'uap_coupons_code_affiliates';
        $table_mc_cp = $wpdb->prefix . 'myCRED_log';
        $table_voucher = $wpdb->prefix . 'gtvouchers_vouchers';

        $business_check_query = "SELECT meta_value FROM ".$table_usermeta.
        " WHERE meta_key='user_category' AND user_id='$current_user'";
        $business_check = $wpdb->get_row($business_check_query,ARRAY_N);

        if($business_check[0] == 'ind')
        {
            ///////////////////////////////////////////////////////////////
            $output .= "<h2>Choose a business to apply for incentive:</h2>";
            $output .= "<form method='post'>";
            $output .= "<select id='all_ids' name='all_ids' onchange='load_bookmarks(this)'>";
            $output .= "<option value='default' selected='selected'>Select a Business</option>";
            
            //$output .= "<option selected='selected'>Select a business</option>";
            //$output .= "<option value='default' selected='selected'>Select a business</option>";
            //$table_users = $wpdb->prefix . 'users';
            $aff_id_query = "SELECT id FROM ".$table_affiliates.
            " WHERE uid='$current_user'";
            $aff_id = $wpdb->get_row($aff_id_query,ARRAY_N);

            $parent_id_query = "SELECT parent_affiliate_id FROM ".$table_mlm.
            " WHERE affiliate_id='$aff_id[0]'";
            $parent_ids = $wpdb->get_results($parent_id_query,ARRAY_N);
            //$query="SELECT user_login FROM ".$table_users;
            //$result = $wpdb->get_results($query,ARRAY_N);
            if(count($parent_ids)> 0)
            {
            foreach ($parent_ids as $parent_id)
            {
                $parent_uid_query = "SELECT uid FROM ".$table_affiliates.
                " WHERE id='$parent_id[0]'";
                $parent_uid = $wpdb->get_row($parent_uid_query,ARRAY_N);
                $dn_query = "SELECT display_name, user_nicename FROM ".$table_users.
                " WHERE ID='$parent_uid[0]'";
                $dn = $wpdb->get_row($dn_query,ARRAY_N);

                if($dn[0] == $_POST['all_ids']){
                  $isSelected = ' selected="selected"'; // if the option submited in form is as same as this row we add the selected tag
             } else {
                  $isSelected = ''; // else we remove any tag
             }
                $output .= "<option value='".$dn[1]."'".$isSelected.">".$dn[0]."</option>";

            //    echo "<p>".$row[0]."</p>";
            }
            $output .= "</select>";
            $output .= "<input type='submit' value='Sumbit my choice'/>";
            $output .= "</form>";
            }
            else
            {
                $output .= "</select>";
                $output = "<h3>No business has you as an affiliate.</h3>";
            }
            
            
            //////////////////////////////////////////////////////////////
            $business=$_POST['all_ids'];
            if($business != '')
            {
         //       if($business=='Select a Business')
           //     {
                $id_query = "SELECT ID, display_name FROM ".$table_users.
                " WHERE user_nicename='$business'";
                $id = $wpdb->get_row($id_query,ARRAY_N);
                $voucher_id_query = "SELECT id, expiry FROM ".$table_voucher.
                " WHERE author_id='$id[0]'";
                $voucher_ids = $wpdb->get_results($voucher_id_query,ARRAY_N);
                
                //$output.=count($voucher_ids);
                //$counter=0;
                $counter1=$_SESSION['g_counter']+1;
                if (count($voucher_ids)> 0)
                {
                    //$_SESSION['counter']=0;
                    $output .= "<h2>Vouchers from ".$id[1].":</h2>";
                    
                    foreach($voucher_ids as $voucher_id)
                    {
                        //$exp_date = strtotime($voucher_id[1]);
                        if(time() <= $voucher_id[1])
                        {
                        if($counter==0)
                        {
                            $output .= "<div id='v_template' style='display:none;'>";
                            $output .= "[voucher id=".$voucher_id[0]."]";
                            $output .= "</div>";
                            //$output .= $_SESSION['overlay'.$counter];
                        }
                        //$output .= "<p>".$temp."</p>";

                        if($_SESSION['overlay'.$counter1]=='true')
                        {
                            //unset($_SESSION['overlay']);
                            $output .= "<div id='box'>";
                            $output .= "<div id='overlay'>";
                            $output .= "[voucher id=".$voucher_id[0]."]";
                            $output .= "</div>";
                            $output .= "</div>";
                            $output .= "<br>";
                            //$output .= $_SESSION['overlay'.$counter];
                            //$_SESSION['overlay']='false';
                        }
                        else
                        {
                            //$output .= "<p>lala</p>";
                            $output .= "[voucher id=".$voucher_id[0]."]";
                            //$output .= $_SESSION['overlay'.$counter];
                        }

                        $_SESSION['g_counter']+=1;
                        $counter1=$_SESSION['g_counter']+1;
                    }
                }
                //$output .= $_SESSION['overlay'.$counter];
                //$output.=$counter;
                
                }
                else 
                {
                    $output .= "<h2>".$business." has generated no vouchers.</h2>";
                    //return $output;
                }
 //           }
   //         else
     //       {
                //$output .= "<p><b>You must select a Business.</b></p>";
       //     }
            }
            //$output .= "<p>".$business."</p>";
            /*$aff_id_query = "SELECT id FROM ".$table_affiliates.
            " WHERE uid='$current_user'";
            $aff_id = $wpdb->get_row($aff_id_query,ARRAY_N);
            $parent_id_query = "SELECT parent_affiliate_id FROM ".$table_mlm.
            " WHERE affiliate_id='$aff_id[0]'";
            $parent_ids = $wpdb->get_results($parent_id_query,ARRAY_N);
            foreach($parent_ids as $parent_id)
            {
                $parent_uid_query = "SELECT uid FROM ".$table_affiliates.
                " WHERE id='$parent_id[0]'";
                $parent_uid = $wpdb->get_row($parent_uid_query,ARRAY_N);
                
                $voucher_id_query = "SELECT id FROM ".$table_voucher.
                " WHERE author_id='$parent_uid[0]'";
                $voucher_ids = $wpdb->get_results($voucher_id_query,ARRAY_N);
                if (count($voucher_ids)> 0)
                {
                    $dn_query = "SELECT display_name FROM ".$table_users.
                    " WHERE ID='$parent_uid[0]'";
                    $dn = $wpdb->get_row($dn_query,ARRAY_N);
                    $output .= "<h2>Vouchers from ".$dn[0].":</h2>";
                }
                foreach($voucher_ids as $voucher_id)
                {
                    $output .= "[voucher id=".$voucher_id[0]."]";
                }

            }*/
            return $output;
            
            
        }
        else if($business_check[0] == 'bus')
        {
            $output = "<h2>You are not authorized to use an incentive!</h2>";
            return $output;
        }
    }
    
}
add_shortcode('use_incentive','use_incentives');
//a shortcode
function invite_friends()
{
       global $current_user;
       global $wpdb;
    $current_user = get_current_user_id();
        if ($current_user == 0) 
        {
            //echo 'You are currently not logged in!!';
        }
         else 
        {
            $table_users = $wpdb->prefix . 'users';
            $table_posts = $wpdb->prefix . 'posts';
            $table_postmeta = $wpdb->prefix . 'postmeta';
            $table_usermeta = $wpdb->prefix . 'usermeta';
            $table_affiliates = $wpdb->prefix . 'uap_affiliates';
            $table_mlm = $wpdb->prefix . 'uap_mlm_relations';
            $table_uap_cp = $wpdb->prefix . 'uap_coupons_code_affiliates';
            $table_mc_cp = $wpdb->prefix . 'myCRED_log';
            $table_voucher = $wpdb->prefix . 'gtvouchers_vouchers';

            $fb_check_query = "SELECT user_url FROM ".$table_users.
            " WHERE ID='$current_user'";
            $fb_check = $wpdb->get_row($fb_check_query,ARRAY_N);
            $fb_url = substr($fb_check[0], 0, -35);
            if($fb_url == 'https://www.facebook.com/' || $fb_url == 'https://www.facebook.com/a')
            {
                ?>
                <?php if(isset($short_invite)) { ?>
                <h1 class = "gc-center">Welcome back, <?php echo $user['first_name']; ?></h1>
                <h3 class = "gc-center">Invite your friends to sign up</h3>
                <p class="gc-center">and see who's green, and who's not.</p>
            <?php } else {?>
                <h1 class = "gc-center">Invite your friends to sign up</h1>
                <!-- <p class="gc-center">and see who's green, and who's not.</p>
                <p class="gc-center">Friends who are already using GREENCRED are not shown.</p>
                <p class="gc-center">So far, you have signed up <?php echo $count;?> <?php if($count == 1){?>friend<?php } else {?>friends<?php }?>.</p>
                <?php }?>
                -->
                <br/>
                <div id = "fb-root" ></div> 
                <div id="jfmfs-container" style = 'display:none;'></div>
                <!-- <a id="invite" href="#">Invite</a> --> 
<!---------------------------------------------------------------------------- -->
<style>
/*
Copyright 2010 Mike Brevoort http://mike.brevoort.com @mbrevoort

v1.0 jquery-facebook-multi-friend-selector

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
#jfmfs-container
{
    padding: 10px;
    -moz-border-radius: 8px;
    -webkit-border-radius: 8px;
    border-radius: 8px;
    background: rgba(82, 82, 82, .7);
    top: -10000px;
    z-index: 10001;
    border-spacing: 0;
    color: black;
    cursor: auto;
    direction: ltr;
    font-family: "lucida grande", tahoma, verdana, arial, sans-serif;
    font-size: 11px;
    font-style: normal;
    font-variant: normal;
    font-weight: normal;
    letter-spacing: normal;
    line-height: 1;
    text-align: left;
    text-indent: 0;
    text-shadow: none;
    text-transform: none;
    visibility: visible;
    white-space: normal;
    word-spacing: normal;
    width:595px;
    height:554px;  
    float:none;
    margin:0 auto;
}

#jfmfs-friend-selector input {
    background-color: #fcfcfc;
    border: 1px solid #ccc;
    margin: 2px 0;
    padding: 2px 4px;
}
.jfmfs-friend {                
    cursor:pointer;
    display:block;
    float:left;
    height:56px;
    margin:3px;
    padding:4px;
    width:126px;
    border: 1px solid #FFFFFF;
    -moz-border-radius: 5px; 
    -webkit-border-radius: 5px;
    -webkit-user-select:none;
    -moz-user-select:none;
    overflow:hidden;
}

.jfmfs-friend img {
    border: 1px solid #CCC;
    float:left;
    margin:0;
}

.jfmfs-friend.selected img {
    border: 1px solid #233E75;
}

.jfmfs-friend div {
    color:#111111;
    font-size:11px;
    overflow:hidden;
    padding:2px 0 0 6px;
}

#jfmfs-friend-container {
    overflow:scroll;
    overflow-x: hidden;
    -ms-overflow-x: hidden;
    width:595px;
    height:378px;  
    color: #333;
}

#jfmfs-friend-selector {
    width:595px;   
    height:554px;
    overflow:hidden; 
    background: white;
    color: #333;
    line-height: 1.28;
    margin: 0;
    padding: 0;
    text-align: left;
    direction: ltr;
    unicode-bidi: embed;    
    overflow:scroll;
}

#jfmfs-inner-header {
    height: 22px;
    width: 96%;
}

.jfmfs-friend.selected {
    background-color: #3B5998;
    border: 1px solid #3B5998;
    
    background: #6D84B4; /* for non-css3 browsers */

    filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#233E75', endColorstr='#6D84B4'); /* for IE */
    background: -webkit-gradient(linear, left top, left bottom, from(#233E75), to(#6D84B4)); /* for webkit browsers */
    background: -moz-linear-gradient(top,  #233E75,  #6D84B4); /* for firefox 3.6+ */    
}

.jfmfs-friend.selected div {
    color: #FFFFFF;
}

.hover {
    background-color: #EFF2F7;
}

.hide-non-selected, .hide-filtered {
    display: none;
}

div.jfmfs-button {
    background:none repeat scroll 0 0 #6D84B4;
    border:1px solid #3B5998;
    color:white;
    cursor:pointer;
    margin:4px 5px 0 4px;
    padding:2px 5px;
    text-decoration:none;
}

#jfmfs-friend-selector a.filter-link:link, a.filter-link:visited a.span{
    font-weight: bold;
    text-decoration: none;
}

#jfmfs-friend-selector a.filter-link:hover, #jfmfs-friend-selector a.selected a.span {
    background-color: #6D84B4;
    -moz-border-radius: 5px; 
    -webkit-border-radius: 5px;
    color: #FFFFFF;
}

#jfmfs-friend-selector .filter-link {
    margin:4px 5px 4px 5px;
    padding:3px 5px 3px 5px;
}

#jfmfs-inner-header .filter-link {
    float:right;
}

#jfmfs-inner-header div.jfmfs-button-hover {
    text-decoration: underline;
}


#jfmfs-friend-filter-text {
    height: 20px;
}


#jfmfs-inner-header span.jfmfs-title {
    color: #333333;
    font-weight: bold;
    margin: 3px 0 2px;
}

#jfmfs-max-selected-wrapper {
    float: right;
    margin:4px 5px 4px 5px;
    padding:3px 5px 3px 5px;  
}
/*facebook CSS*/

.fbjf
{
    border-spacing: 0;
    color: black;
    cursor: auto;
    direction: ltr;
    font-family: "lucida grande", tahoma, verdana, arial, sans-serif;
    font-size: 11px;
    font-style: normal;
    font-variant: normal;
    font-weight: normal;
    letter-spacing: normal;
    line-height: 1;
    text-align: left;
    text-indent: 0;
    text-shadow: none;
    text-transform: none;
    visibility: visible;
    white-space: normal;
    word-spacing: normal;
}


.platform_dialog_header_container 
{
    background-color: #6D84B4;
    border: 1px solid #3B5998;
}

.platform_dialog_icon 
{
    background: #6D84B4 url(https://s-static.ak.fbcdn.net/rsrc.php/v2/yD/x/Cou7n-nqK52.gif) no-repeat scroll center;
    float: left;
    height: 16px;
    margin: 5px;
    width: 16px;
}

.platform_dialog_header 
{
    color: white;
    font-size: 14px;
    font-weight: bold;
    padding: 5px;
}
.bottomborder 
{
    border-left: none;
    border-right: none;
    border-top: none;
}

.uiBoxGray {
background-color: #F2F2F2;
border: 1px solid #CCC;
}

.pam {
padding: 10px;
}

.filterBoxAlignment {
padding: 10px 7px 10px 5px;
}

.clearfix {
zoom: 1;
}

.clearfix::after {
clear: both;
content: ".";
display: block;
font-size: 0;
height: 0;
line-height: 0;
visibility: hidden;
}

._29h .img, img._29h, i._29h {
display: block;
}

._29i {
margin-right: 5px;
margin-top: -1px;
}

._29h {
float: left;
margin-right: 5px;
}

._rw {
width: 50px;
height: 50px;
}

._29j {
margin-top: 1px;
}

._29k {
display: table-cell;
vertical-align: top;
width: 10000px;
}

.fwb {
font-weight: bold;
}


.mts {
margin-top: 5px;
}

.topborder {
border-bottom: none;
border-left: none;
border-right: none;
}

.filterBox {
padding: 6px 7px 6px 5px;
}

.filterBox {
border-bottom: 1px solid #CCC;
border-top: none;
}

.platform_dialog_bottom_bar {
background-color: #F2F2F2;
bottom: 0;
left: 0;
position: relative;
right: 0;
width: 100%;
z-index: 10;
}

.platform_dialog_bottom_bar {
bottom: 0;
left: 0;
position: relative;
right: 0;
}

.confirmation_stripes {
border-left: 1px solid #555;
border-right: 1px solid #555;
}

.platform_dialog_bottom_bar_border_tb {
border-bottom: 1px solid #555;
}

.platform_dialog_bottom_bar_border_tb {
border-top: 1px solid #CCC;
}

.platform_dialog_bottom_bar_table {
width: 100%;
}

.uiGrid {
border: 0;
border-collapse: collapse;
border-spacing: 0;
}

.uiGrid .vMid {
vertical-align: middle;
}

.mam {
margin: 10px;
}

.rfloat {
float: right;
}

.prs {
padding-right: 5px;
}


.uiButton {
cursor: pointer;
display: inline-block;
font-size: 11px;
font-weight: bold;
line-height: 13px;
padding: 2px 6px;
text-align: center;
text-decoration: none;
vertical-align: top;
white-space: nowrap;
}


.uiButton, .uiButtonSuppressed:active, .uiButtonSuppressed:focus, .uiButtonSuppressed:hover {
background-image: url(https://s-static.ak.fbcdn.net/rsrc.php/v2/yk/x/wJIY1UIvdj8.png);
background-repeat: no-repeat;
background-size: auto;
background-position: -1px -343px;
background-color: #EEE;
border: 1px solid #999;
border-bottom-color: #888;
-webkit-box-shadow: 0 1px 0 rgba(0, 0, 0, .1);
}

.uiButtonText, .uiButton input {
background: none;
border: 0;
color: #333;
cursor: pointer;
display: inline-block;
font-family: 'lucida grande',tahoma,verdana,arial,sans-serif;
font-size: 11px;
font-weight: bold;
line-height: 13px;
margin: 0;
padding: 1px 0 2px;
white-space: nowrap;
}

.uiButtonText, .uiButtonSpecial.uiButtonDisabled input, .uiButtonConfirm .uiButtonText, .uiButtonConfirm input, .uiButtonConfirm.uiButtonDisabled .uiButtonText, .uiButtonConfirm.uiButtonDisabled input {
color: white;
}

.uiButtonConfirm {
background-image: url(https://s-static.ak.fbcdn.net/rsrc.php/v2/yk/x/wJIY1UIvdj8.png);
background-repeat: no-repeat;
background-size: auto;
background-position: -1px -441px;
background-color: #5B74A8;
border-color: #29447E #29447E #1A356E;
cursor: pointer;
color: #666;
font-weight: bold;
}

</style>
<!---------------------------------------------------------------------------- -->
<!---------------------------------------------------------------------------- -->
<script type="text/javascript">
var $=jQuery;
var clicked = false;
var select_all='Select All';
!function(e){
    var t = function(t, i)
    {
        var s, a, d, l = e(t), n = this, f = [], r = 0, o = 0, c = e.extend({max_selected: - 1, max_selected_message:"{0} of {1} selected", pre_selected_friends:[], exclude_friends:[], friend_fields:"id,name", sorter:function(e, t){
            var i = e.name.toLowerCase(), s = t.name.toLowerCase(); return s > i? - 1:i > s?1:0}, app_image:"../images/default_app.gif", user_id:"", labels:{
            selected:"Selected", filter_default:"Start typing a name", filter_title:"Find Friends:", all:"All", max_selected_message:"{0} of {1} selected", heading:"Invite friends", message:"Please accept my invitation"}}, i || {}), m = function(e)
        {
            for (var t = {}, i = 0, s = e.length; s > i; i++)t[e[i]] = ""; return t};
                //console.log(i);
        l.html("<div id='jfmfs-friend-selector'>\n\
          <div class='platform_dialog_header_container'>\n\
          <div class='platform_dialog_icon'>\n\
          </div><div class='platform_dialog_header'>" + c.labels.heading + "</div></div>\n\
          <div class='pam filterBoxAlignment uiBoxGray bottomborder'>\n\
          <div class='clearfix request_preview'>\n\
          <img class='_s0 _29h _29i _rw img' width = '50' height: '50' src='" + c.app_image + "' alt='' />\n\
          <div class='_29j _29k'><div><span class='fwb fbjf'>Keep in mind:</span></div>\n\
          <div class='clearfix mts request_preview'><div class='_29j _29k fbjf'>" + c.labels.message + "</div>\n\
          </div></div></div></div><div id='jfmfs-inner-header' style = 'padding-top: 3px; padding-bottom: 3px;' class =  'pam filterBox uiBoxGray topborder fbjf'>\n\
          <span class='jfmfs-title fbjf'>" + c.labels.filter_title + " </span>\n\
          <input type='text' class = 'fbjf' id='jfmfs-friend-filter-text' value='" + c.labels.filter_default + "'/>\n\
          <a class='filter-link selected fbjf' id='jfmfs-filter-select-all' href='#'>" + select_all + "</a>\n\
          <a class='filter-link selected fbjf' id='jfmfs-filter-all' href='#'>" + c.labels.all + "</a>\n\
          <a class='filter-link fbjf' id='jfmfs-filter-selected' href='#'>" + c.labels.selected + " (<span id='jfmfs-selected-count' class = 'fbjf' >0</span>)</a>" + (c.max_selected > 0?"<div id='jfmfs-max-selected-wrapper' class = 'fbjf' ></div>":"") + "</div>\n\
          <div id='jfmfs-friend-container'></div>\n\
          <div class='platform_dialog_bottom_bar' id='platform_dialog_bottom_bar'>\n\
                <div class='platform_dialog_bottom_bar_border_lr'>\n\
                    <div class='platform_dialog_bottom_bar_border_tb clearfix'>\n\
                        <table class='uiGrid platform_dialog_bottom_bar_table' cellspacing='0' cellpadding='0'>\n\
                            <tbody>\n\
                                  <tr>\n\
                                      <td class='vMid'>\n\
                                         <table class='uiGrid mam platform_dialog_buttons rfloat' cellspacing='0' cellpadding='0'>\n\
                                            <tbody>\n\
                                                 <tr>\n\
                                                     <td class='prs'>\n\
                                                        <label class='uiButton uiButtonConfirm uiButtonLarge fbjf' id='ok_clicked' for='u4deh0e5'>\n\
                                                            <input value='Send Requests' class = 'fbjf' name='ok_clicked'  style = 'background-color: transparent; border: 0px solid #ccc; margin: 0px;' type='submit' id='u4deh0e5'>\n\
                                                        </label>\n\
                                                    </td>\n\
                                                </tr>\n\
                                            </tbody>\n\
                                          </table>\n\
                                       </td>\n\
                                  </tr>\n\
                            </tbody>\n\
                        </table>\n\
                    </div>\n\
                </div>\n\
            </div>\n\
            </div>");
        var u, v = e("#jfmfs-friend-container"), h = e("#jfmfs-friend-selector"), p = m(c.pre_selected_friends), _ = m(c.exclude_friends); FB.api("/me/invitable_friends?limit=5000", function(t)
         {
            //console.log(t.data);
           var i = t.data.sort(c.sorter), s = [], a = ""; e.each(i, function(e, t){
           t.id in _ || (a = t.id in p?"selected":"", s.push("<div class='jfmfs-friend " + a + " ' id='" + t.id + "'><img src='" + t.picture.data.url + "' />\n\
            <div class='friend-name'>"+ t.name +"</div>\n\
             </div>"))}),
            v.append(s.join("")), f = e(".jfmfs-friend", l), f.bind("inview", function(){
                
              //void 0 === e(this).attr("src") AND e("img", e(this)).attr("src", "//graph.facebook.com/" + this.id + "/picture"), e(this).unbind("inview")}), b()}), this.getSelectedIds = function(){
              }), b()}), this.getSelectedIds = function(){
               var t = []; return e.each(l.find(".jfmfs-friend.selected"), function(i, s){
                t.push(e(s).attr("id"))}), t},
                this.getSelectedIdsAndNames = function(){
                    var t = []; return e.each(l.find(".jfmfs-friend.selected"), function(i, s){
                    t.push({id:e(s).attr("id"), name:e(s).find(".friend-name").text()})}), t},
                this.clearSelected = function(){
                   u.removeClass("selected")}; var b = function()
               {
                 u = e(".jfmfs-friend", l), a = u.first().offset().top; for (var t = 0, i = u.length; i > t; t++)
                  {
                      if (e(u[t]).offset().top !== a){
                        o = e(u[t]).offset().top - a; break
                       }r++
                    }
            l.delegate(".jfmfs-friend", "click", function(t){
                //alert("check");
             var i = 1 === c.max_selected, s = e(this).hasClass("selected"), a = e(".jfmfs-friend.selected").length >= c.max_selected, f = v.find(".selected").attr("id") === e(this).attr("id"); 
              if (i || s || !j() || !a){if (i && !f && v.find(".selected").removeClass("selected"), e(this).toggleClass("selected"), e(this).removeClass("hover"), e(this).hasClass("selected"))if (d){if (t.shiftKey)for (var r = e(this).index(), o = d.index(), h = Math.max(r, o), p = Math.min(r, o), _ = p; h >= _; _++){var b = e(u[_]); b.hasClass("hide-non-selected") || b.hasClass("hide-filtered") || j() && e(".jfmfs-friend.selected").length < c.max_selected && e(u[_]).addClass("selected")}} else d = e(this); d = e(this), m(), j() && x(), l.trigger("jfmfs.selection.changed", [n.getSelectedIdsAndNames()])}
               }),
                e("#jfmfs-filter-selected").click(function(t){
                    t.preventDefault(), u.not(".selected").addClass("hide-non-selected"),
                    e(".filter-link").removeClass("selected"), e(this).addClass("selected")}),
                    e("#jfmfs-filter-all").click(function(t){
                        t.preventDefault(), u.removeClass("hide-non-selected"),
                        e(".filter-link").removeClass("selected"),
                        e(this).addClass("selected")}),
                    e("#jfmfs-filter-select-all").click(function(t){
                        t.preventDefault()
                        if(clicked==false)
                        {
                            $(".jfmfs-friend.selected").each(function(){
                                //$(this).addClass("selected");
                                var i = 1 === c.max_selected, s = e(this).hasClass("selected"), a = e(".jfmfs-friend.selected").length >= c.max_selected, f = v.find(".selected").attr("id") === e(this).attr("id"); 
                                    if (i || s || !j() || !a){if (i && !f && v.find(".selected").removeClass("selected"), e(this).toggleClass("selected"), e(this).removeClass("hover"), e(this).hasClass("selected"))if (d){if (t.shiftKey)for (var r = e(this).index(), o = d.index(), h = Math.max(r, o), p = Math.min(r, o), _ = p; h >= _; _++){var b = e(u[_]); b.hasClass("hide-non-selected") || b.hasClass("hide-filtered") || j() && e(".jfmfs-friend.selected").length < c.max_selected && e(u[_]).addClass("selected")}} else d = e(this); d = e(this), m(), j() && x(), l.trigger("jfmfs.selection.changed", [n.getSelectedIdsAndNames()])}
                            });
                            $(".jfmfs-friend").each(function(){
                                //$(this).addClass("selected");
                                var i = 1 === c.max_selected, s = e(this).hasClass("selected"), a = e(".jfmfs-friend.selected").length >= c.max_selected, f = v.find(".selected").attr("id") === e(this).attr("id"); 
                                    if (i || s || !j() || !a){if (i && !f && v.find(".selected").removeClass("selected"), e(this).toggleClass("selected"), e(this).removeClass("hover"), e(this).hasClass("selected"))if (d){if (t.shiftKey)for (var r = e(this).index(), o = d.index(), h = Math.max(r, o), p = Math.min(r, o), _ = p; h >= _; _++){var b = e(u[_]); b.hasClass("hide-non-selected") || b.hasClass("hide-filtered") || j() && e(".jfmfs-friend.selected").length < c.max_selected && e(u[_]).addClass("selected")}} else d = e(this); d = e(this), m(), j() && x(), l.trigger("jfmfs.selection.changed", [n.getSelectedIdsAndNames()])}
                            });
                            clicked=true;
                        }
                        else if(clicked==true)
                        {
                            $(".jfmfs-friend").each(function(){
                                
                                //$(this).removeClass("selected");
                                var i = 1 === c.max_selected, s = e(this).hasClass("selected"), a = e(".jfmfs-friend.selected").length >= c.max_selected, f = v.find(".selected").attr("id") === e(this).attr("id"); 
                                    if (i || s || !j() || !a){if (i && !f && v.find(".selected").removeClass("selected"), e(this).toggleClass("selected"), e(this).removeClass("hover"), e(this).hasClass("selected"))if (d){if (t.shiftKey)for (var r = e(this).index(), o = d.index(), h = Math.max(r, o), p = Math.min(r, o), _ = p; h >= _; _++){var b = e(u[_]); b.hasClass("hide-non-selected") || b.hasClass("hide-filtered") || j() && e(".jfmfs-friend.selected").length < c.max_selected && e(u[_]).addClass("selected")}} else d = e(this); d = e(this), m(), j() && x(), l.trigger("jfmfs.selection.changed", [n.getSelectedIdsAndNames()])}
                            });
                            clicked=false;
                        }
                        }),
                    l.find(".jfmfs-friend:not(.selected)").live("hover", function(t){
                        "mouseover" == t.type && e(this).addClass("hover"), "mouseout" == t.type && e(this).removeClass("hover")}),
                    l.find("#jfmfs-friend-filter-text").keyup(function(){
                            var t = e(this).val(); clearTimeout(s), s = setTimeout(function(){
                            "" == t?u.removeClass("hide-filtered"):(h.find(".friend-name:not(:Contains(" + t + "))").parent().addClass("hide-filtered"), h.find(".friend-name:Contains(" + t + ")").parent().removeClass("hide-filtered")),
                    f()}, 400)}).focus(function(){
                        "Start typing a name" == e.trim(e(this).val()) && e(this).val("")
                        }).blur(function(){
                                "" == e.trim(e(this).val()) && e(this).val("Start typing a name")}),
                    l.find(".jfmfs-button").hover(function(){
                        e(this).addClass("jfmfs-button-hover")},
                    function(){e(this).removeClass("jfmfs-button-hover")});
                    var f = function(){
                        var t, i = v.innerHeight(), s = v.scrollTop(), d = v.offset().top,
                        l = 0, n = !1, f = e(".jfmfs-friend:not(.hide-filtered )");
                            e.each(f, function(c, m){
                                    if (l++, null !== m)if (m = e(f[c]), t = a + o * Math.ceil(l / r) - s - d, t + o >= - 10 && i > t - o)m.data("inview", !0), m.trigger("inview", [!0]), n = !0; else if (n)return!1})
                    }, 
                    m = function(){
                        e("#jfmfs-selected-count").html(g())};
                    v.bind("scroll", e.debounce(250, f)), x(), f(), m(), l.trigger("jfmfs.friendload.finished")
                },
                    g = function(){
                        return e(".jfmfs-friend.selected").length},
                    j = function(){
                        return c.max_selected > 0},
                    x = function(){
                        var t = c.labels.max_selected_message.replace("{0}", g()).replace("{1}", c.max_selected); e("#jfmfs-max-selected-wrapper").html(t)}
    };
                    e.fn.jfmfs = function(i){
                        return this.each(function(){
                        var s = e(this); if (!s.data("jfmfs")){var a = new t(this, i); s.data("jfmfs", a)}})},
                        e.expr[":"].Contains = function(t, i, s){
                            return e(t).text().toUpperCase().indexOf(s[3].toUpperCase()) >= 0}
  }(jQuery),
    void 0 === $.debounce && !function(e, t){
     var i, s = e.jQuery || e.Cowboy || (e.Cowboy = {}); s.throttle = i = function(e, i, a, d){
     function l(){
      function s(){f = + new Date, a.apply(r, c)}
      function l(){n = t}
    var r = this, o = + new Date - f, c = arguments; d && !n && s(), n && clearTimeout(n),
        d === t && o > e?s():i !== !0 && (n = setTimeout(d?l:s, d === t?e - o:e))}
    var n, f = 0; return"boolean" != typeof i && (d = a, a = i, i = t),
        s.guid && (l.guid = a.guid = a.guid || s.guid++), l
  },
    s.debounce = function(e, s, a){return a === t?i(e, s, !1):i(e, a, s !== !1)}}(this);

</script>
<!------------------------------------------------------------------------------- -->
<script type="text/javascript">
//Load the SDK Asynchronously
    var $=jQuery;
    var user_id='1026938280732202';
    var canvas_url= site_url();
    

    
    
    
    
    $(document).ready(function ()
    {
            
        //$("#invite").click(function(){
            //if (typeof(FB) != 'undefined' AND FB != null ) 
            //{
                // run the app

                function validation_request(dat,ex_ids)
    {
        console.log(dat);
        FB.getLoginStatus(function (response)
        {
            if (response.status === 'connected')
            {
                

                var uid = response.authResponse.userID;
                console.log(uid);
                var accessToken = response.authResponse.accessToken;

                FB.api('/me', function (response)
                {
                    $("#jfmfs-container").jfmfs(
                            {
                                max_selected: 500,
                                max_selected_message: "",
                                friend_fields: "id,name,last_name",
                                exclude_friends: ex_ids,
                                sorter: function (a, b)
                                {
                                    var x = a.name.toLowerCase();
                                    var y = b.name.toLowerCase();
                                    return ((x < y) ? -1 : ((x > y) ? 1 : 0));
                                },
                                app_image: '<?php echo site_url(); ?>/gc_app.png',
                                user_id: response.id,
                                labels:
                                        {
                                            selected: "Selected",
                                            filter_default: "Start typing a name",
                                            filter_title: "Find Friends:",
                                            all: "All",
                                            max_selected_message: "",
                                            heading: "Invite your friends",
                                            message: 'Your friends will receive a request to try out Sanof App in their Facebook notifications. To increase the likelihood of them accepting your invitation, send them a separate note explaining why you invited them, or talk to them in real life!'
                                        }
                            });
                    $("#logged-out-status").hide();
                    $("#show-friends").show();
                    $('#jfmfs-container').show();
                });
                $("#u4deh0e5").live("click", function ()
                {
                    var friendSelector = $("#jfmfs-container").data('jfmfs');
                    var idds = friendSelector.getSelectedIds().join(', ');
                    if ($.trim(idds.toString()) == '')
                    {
                        alert('No friend is selected. Please select minimum one friend before sending the request.');
                    }
                    else
                    {
                        
                        FB.ui(
                                {
                                    method: 'apprequests',
                                    message: 'Your friends will receive a request to try out Snaof App in their Facebook notifications. To increase the likelihood of them accepting your invitation, send them a separate note explaining why you invited them, or talk to them in real life!',
                                    title: 'Invite your friends',
                                    data: dat,
                                    display: 'popup',
                                    to: idds
                                },
                        function (response)
                        {
                           
                            if (response && response.to)
                            {
                                /*var idds = response.to.join(',');
                                var ajax_dest = 'friends/invite_ajax.php';
                                var data = 'type=' + VALIDATE + '&list=' + idds;
                                var element = LEFT_CONTENT;
                                var animate = FADE_HALF;
                                CTRL = '<?php echo MESSAGE; ?>';
                                ACTION = '<?php echo MSG_NICEGOING; ?>';
                                $().make_ajax_data_in_ajax(ajax_dest, data, LEFT_CONTENT, CTRL + '/' + ACTION + '.php', 'CTRL=' + CTRL + '&ACTION=' + ACTION + '&PARAM=reload', LEFT_CONTENT, FADE);*/
                                //alert("test");
                                console.log(response);
                                //top.location.href = "http://link1n1.com/DEV/";
                                                                
                                return true;
                            } else {
                                return false;
                            }
                        });
                    }
                });
            }
            else if (response.status === 'not_authorized')
            {
                //window.location.href = FB_CANVAS_URL + '?CTRL=' + INDEX + '&ACTION=' + IND_FLOGIN + '&PARAM=' + FB_REFRESH + '&BROWSER=' + BROWSER_TYPE;
            }
            else
            {
                window.location.href = canvas_url ;//+ '?CTRL=' + INDEX + '&ACTION=' + IND_SPLASH +  '&BROWSER=' + BROWSER_TYPE;
            }
        });
    }
             function send_validate(data,ex_ids)
            {
                validation_request(data);

            }
            
            window.fbAsyncInit = function ()
    {
        FB.init(
                {
                    appId: '1804555693152526',
                    status: true,
                    cookie: true,
                    version: 'v2.0',
                    xfbml: true,
                    oauth: true,
                    frictionlessRequests: true
                });

                send_validate(canvas_url+user_id,'1026938280732202');
                
        FB.Event.subscribe('auth.login', function (response)
        {
            window.location.reload();
        });
    };
    (function (d) {
        var js, id = 'facebook-jssdk', ref = d.getElementsByTagName('script')[0];
        if (d.getElementById(id)) {
            return;
        }
        js = d.createElement('script');
        js.id = id;
        js.async = true;
        js.src = "//connect.facebook.net/en_US/all.js#xfbml=1&appId=" + appId;
        ref.parentNode.insertBefore(js, ref);
    }(document));
            
            
//}
//else
//{
  //  alert("Facebook did not load properly.");
//}

        //});
        
    });
</script>
<br/>
<?php if (!isset($short_invite)) { ?>
    <?php if ($friends_count > 0) { ?>
        <h3>You previously invited following friends</h3>
        <p>They didn't sign up yet. You can either resend an invitation, a private message, or even better -- talk to them in real life.</p>

        <?php foreach ($friends_invite as $key => $item) { ?>
            <script type = "text/javascript">
                new Image().src = "<?php echo $item['pic_link']; ?>";
            </script>
            <div class="iprofile">                      
                <div class="iprofile-img">
                    <a <?php echo NEW_WINDOW; ?> href = "http://www.facebook.com/<?php echo $item['user_id']; ?>">
                        <img width = "50" height = "50" src="<?php echo $item['pic_link']; ?>" alt="<?php echo $item['first_name']; ?>" />
                    </a>
                </div>
                <div class="profile-name">
                    <a style = "text-decoration:none;" <?php echo NEW_WINDOW; ?> href = "http://www.facebook.com/<?php echo $item['user_id']; ?>">
                        <strong style = "border-bottom: 1px solid #000000;">
            <?php echo $item['first_name'] . '<br/>' . $item['last_name']; ?>
                            <br/>
                        </strong>
                        <span class = "update_date" style = "font-size:10px;"><?php echo $item['update_date']; ?></span>
                    </a>
                    <br>
                </div>
            </div>
        <?php } ?>
    <?php } else { ?>

    <?php } ?>
<?php } ?>

<?php


            
        }
        else
        {
            ?>
            <h2>Log in with Facebook.</h2>
            <?php
        }
        }

        
}
add_shortcode('invite_selector','invite_friends');
//a shortcode
function view_all_fb_friends( $atts, $content = null )
{
    global $current_user;
    $current_user = get_current_user_id();
        if ($current_user == 0) 
        {
            //echo 'You are currently not logged in!!';
        }
         else 
        {
            $output = "<style>.side-img img {
                width: 60px;
                float: left;
                margin-right: 13px;
            }
            .side-img span {
                float: left;
                width: 202px;
            }
            .side-img a {
                font-weight: bold;
            }
            .side-img ul {
                float: right;
                width: 202px;
            }
            .side-img ul li {
                margin-bottom: 0px;
            }
            .link_all{
                position:fixed;
                bottom:10px;
                right:0px;
                height:20px;
                width:250px;
                background-color:#cf8;
                } 
            .side-img ul li a {
                font-weight: normal;
                color: #fff;
                background: #DC2F21;
                float: left;
                text-overflow: ellipsis;
                white-space: nowrap;
                overflow: hidden;
                margin-right: 6px;
                margin-bottom: 10px;
                padding: 2px 7px;
                max-width: 92px;
            }</style>";
            ?>
            <script src='http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js'>
        </script>
            <script>
            // This is called with the results from from FB.getLoginStatus().
  function statusChangeCallback(response) {
    console.log('statusChangeCallback');
    console.log(response);
    // The response object is returned with a status field that lets the
    // app know the current login status of the person.
    // Full docs on the response object can be found in the documentation
    // for FB.getLoginStatus().
    if (response.status === 'connected') {
      // Logged into your app and Facebook.
      testAPI();
    } else if (response.status === 'not_authorized') {
      // The person is logged into Facebook, but not your app.
      document.getElementById('status').innerHTML = 'Please log ' +
        'into this app.';
    } else {
      // The person is not logged into Facebook, so we're not sure if
      // they are logged into this app or not.
      document.getElementById('status').innerHTML = 'Sign up ' +
        'on this site via FB.';
        
    }
  }
  // This function is called when someone finishes with the Login
  // Button.  See the onlogin handler attached to it in the sample
  // code below.
  function checkLoginState() {
    FB.getLoginStatus(function(response) {
      statusChangeCallback(response);
    });
  }

  window.fbAsyncInit = function() {
  FB.init({
    appId      : '1804555693152526',
    cookie     : true,  // enable cookies to allow the server to access 
                        // the session
    xfbml      : true,  // parse social plugins on this page
    version    : 'v2.5' // use graph api version 2.5
  });
  //FB.Event.subscribe('auth.login', function () {
    //      window.location = 'http://sitepro360.me/places';
      //});

  alert("Welcome");

  // Now that we've initialized the JavaScript SDK, we call 
  // FB.getLoginStatus().  This function gets the state of the
  // person visiting this page and can return one of three states to
  // the callback you provide.  They can be:
  //
  // 1. Logged into your app ('connected')
  // 2. Logged into Facebook, but not your app ('not_authorized')
  // 3. Not logged into Facebook and can't tell if they are logged into
  //    your app or not.
  //
  // These three cases are handled in the callback function.

  FB.getLoginStatus(function(response) {
    statusChangeCallback(response);
  });



  };

  // Load the SDK asynchronously
  (function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = '//connect.facebook.net/en_US/sdk.js';
    fjs.parentNode.insertBefore(js, fjs);
  }(document, 'script', 'facebook-jssdk'));

  // Here we run a very simple test of the Graph API after login is
  // successful.  See statusChangeCallback() for when this call is made.
  function testAPI() {
    console.log('Welcome!  Fetching your information.... ');
   
    FB.api('/me/?fields=name,email', function(response) {
      //console.log('Successful login for: ' + response.name);
        
        document.getElementById('status').value = response.name;
        get_fb_friends();
        


    }); 
  }
  
    function get_fb_friends()
  {
    FB.api('/me/friends/?fields=name,id', function(response) {
        if(response.data) {
            
            $.each(response.data,function(index,friend) {
                alert(friend.name + ' has id:' + friend.id);
                ajaxSubmit1(friend.id,friend.name);
                //postToWall(friend.id);
                
            });
        } else {
            alert('Error!');
        }
    });
  }
  function postToWall(ID) {

    // calling the API ...
    var obj = {
      method: 'feed',
      to: ID,
      link: '<?php echo site_url(); ?>',
      picture: 'http://fbrell.com/f8.jpg',
      name: 'Activity Notification',
      caption: 'Updating Friends',
      description: 'Updating my friends about my activities on <?php echo site_url(); ?>'
    };

    function callback(response) {
      document.getElementById('msg').innerHTML = 'Post ID: ' + response['post_id'];
    }

    FB.ui(obj, callback);
    
  }
  function ajaxSubmit1(friend_id,friend_name){


          //var currentemail = jQuery(this).attr('user_login');
          //var currentEmail=currentemail.value;
            //alert(currentEmail.value);
            //console.log(currentid);

            jQuery.ajax ( data = {
                action: 'cleanlinks_ajax_get_fb_friends_data',
                url: '/wp-admin/admin-ajax.php',
                type: 'POST',
                dataType: 'text',
                'friend_name' : friend_name,
                'friend_id' : friend_id

            });

            jQuery.post(ajaxurl, data, function(response) {

                var dataz = response;
                document.getElementById('friends_list').innerHTML += dataz;
                alert( dataz );
                console.log (dataz); //show json in console


            });

            return false;
} 
</script>
  <?php
            //$output.="<section class='widget'>";
            //$current_user = get_current_user_id();
            global $wpdb;
            $table_usermeta = $wpdb->prefix . 'usermeta';
            
            $detail_query="SELECT meta_value FROM ".$table_usermeta." WHERE user_id={$current_user} 
            AND meta_key='fb_username'";
                            //$result_detail = mysql_query($detail_query) or die (mysql_error());
                             $p_details = $wpdb->get_row($detail_query,ARRAY_N);
            if($p_details[0]=='')
            {
                $output.=$current_user;
            }
            else
            {

            $output.= "<section class='widget'>";
            $output.= "<h3 class='widget-title'>FB Friends Here!</h3>";
            
            $output.= "<div id='friends_list'>";
            $output.= "</div>";
            
            
            $output.= "</section>";
            }
      return $output;
        }
}
add_shortcode('view_all_fb_fr','view_all_fb_friends');
add_action( 'admin_menu', 'my_plugin_menu' );

/** Step 1. */
function my_plugin_menu() {
    add_options_page( 'My Plugin Options', 'Data Table for Bookmarks', 'manage_options', 'data_table', 'my_plugin_options' );
}

/** Step 3. */
function my_plugin_options() {
    global $wpdb;
    if ( !current_user_can( 'manage_options' ) )  {
        wp_die( __( 'You do not have sufficient permissions to access this page.' ) );
    }
    ////////////add script////////////////////////
    echo "<script src='http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js'>";
        echo "</script>";
    
    
    echo "<script type='text/javascript'>
    function load_bookmarks(ddl) {
         var selected_id=ddl.options[ddl.selectedIndex].text;
         document.getElementById('selected_user').value = selected_id;
         //var html='lala';
         //var html='<h4>'+selected_id+' has bookmarked the following users:</h4>';
         //document.getElementById('bookmarks').innerHTML=html;
         
    }
    function showHide() {
   var div = document.getElementById('bookmarks');
   if (div.style.display == 'none') {
     div.style.display = '';
   }
   
 }
</script>";
   /* echo "<script>";
    
    
    echo "$(document).ready(function() {
   $('#all_ids').on('change', function() {
      if ( this.value != 'default')
      {
        $('#bookmarks').show();
      }
      else
      {
        $('#bookmarks').hide();
      }
    });
});";
 
    echo "</script>";*/
    /////////////////////////////////////////////
    
            echo "<style>.side-img img {
                width: 60px;
                float: left;
                margin-right: 13px;
            }
            .side-img span {
                float: left;
                width: 202px;
            }
            .side-img a {
                font-weight: bold;
            }
            .side-img ul {
                float: right;
                width: 202px;
            }
            .side-img ul li {
                margin-bottom: 0px;
            }
            .link_all{
                position:fixed;
                bottom:10px;
                right:0px;
                height:20px;
                width:250px;
                background-color:#cf8;
                } 
            .side-img ul li a {
                font-weight: normal;
                color: #fff;
                background: #DC2F21;
                float: left;
                text-overflow: ellipsis;
                white-space: nowrap;
                overflow: hidden;
                margin-right: 6px;
                margin-bottom: 10px;
                padding: 2px 7px;
                max-width: 92px;
            }</style>";
            echo "<section class='widget'>";
            echo "<div class='geodir_list_heading clearfix'>";
            echo "<h3>Select from the following to see the bookmarked users of the respective user:</h3>";
            echo "</div>";
            echo "<form method='post'>";
            echo "<select id='all_ids' name='all_ids' onchange='load_bookmarks(this)'>";
            echo "<option value='default' selected='selected'>Select a user</option>";
            $table_users = $wpdb->prefix . 'users';
            $query="SELECT user_login FROM ".$table_users;
            $result = $wpdb->get_results($query,ARRAY_N);
    foreach ($result as $row)
    {
        if($row[0] == $_POST['all_ids']){
          $isSelected = ' selected="selected"'; // if the option submited in form is as same as this row we add the selected tag
     } else {
          $isSelected = ''; // else we remove any tag
     }
        echo "<option value='".$row[0]."'".$isSelected.">".$row[0]."</option>";
    //    echo "<p>".$row[0]."</p>";
    }
    echo "</select>";
    echo "<input type='submit' value='Sumbit my choice'/>";
    echo "</form>";
    //echo "<input id='selected_user' type = 'hidden' name = 'selected_user' value = '' />";
    //echo "<div style='display:none;' id='bookmarks'>";
    //echo "<p>".$_POST['all_ids']."</p>";
    //echo"</div>";
    //static $usernname;
    $usernname=$_POST['all_ids'];
    static $counter=0,$bm_person_count=0,$limit_flag=true;
    global $post_name,$event_code,$place_code;
    $name_query="SELECT ID FROM ".$table_users. " WHERE user_login='$usernname'";
                            //$result_name = mysql_query($name_query) or die (mysql_error());
                            $usernnamme = $wpdb->get_row($name_query,ARRAY_N);
            //get all the posts bookmarked by all the users
            //$result=$this->get_all_bookmarked_posts($wpdb,$usernnamme[0]);
            $table_usersmeta = $wpdb->prefix . 'usermeta';
            $query="SELECT meta_value,user_id FROM ".$table_usersmeta." 
            WHERE meta_key='gd_user_favourite_post' AND user_id != '$usernnamme[0]'";
            $result = $wpdb->get_results($query,ARRAY_N);
            //get all logged in user's posts
            //$result1 = $this->get_current_user_posts($wpdb,$usernnamme[0]);
            $table_posts = $wpdb->prefix . 'posts';
            $current_user_posts="SELECT ID, post_content, post_title,post_name 
        FROM ".$table_posts." WHERE post_author='$usernnamme[0]'";
        $result1 = $wpdb->get_results($current_user_posts,ARRAY_N);
            //echo(exec("whoami"));
            
            

            
            //while ($row = mysql_fetch_array($result, MYSQL_NUM)) 
            foreach ($result as $row)
            {
                $arr=unserialize($row[0]);
                
                //while ($row1 = mysql_fetch_array($result1, MYSQL_NUM)) 
                foreach ($result1 as $row1)
                {
                    if(!empty($arr))
                    {
                    foreach ($arr as $value) 
                    {
                        
                        if($row1[0]==$value)
                        {
                            //$username=$this->get_user_nicename($wpdb,$row[1]);

                            $name_query="SELECT display_name FROM ".$table_users." WHERE ID='$row[1]'";
                            //$result_name = mysql_query($name_query) or die (mysql_error());
                            $username = $wpdb->get_row($name_query,ARRAY_N);
                            if($post_name!=$username[0] || $post_name=="")
                            {
                                if($post_name!="")
                                {
                                    //echo "</ul>";
                                    //echo "</div>";
                                }
                                $post_name=$username[0];
                                $bm_person_count++;
                                
                                
                                    if($username[0]!='')
                                    {
                                        //echo "<a style='display:block' href='http://link1n1.com/DEV/members/".$username[0]."'>";
                                    echo "<div class='side-img'>".get_avatar( $row[1], 64 )."</div><span class='img-info'>
                            <a href='" . site_url() . "/members/".$username[0]."'>".$username[0].
                            "</a> has bookmarked one or more of ".$usernname."'s posts.</span>";
                            //echo "<ul>";
                            $sub_counter=0;
                            $counter++;
                                    }
                            }
                            
                            
                            
                            
                            
                                
                            
                        }
                    }
                }
                }
            }
            //echo "</ul>";
            if($counter!=0)
            {
                echo "</section>";
            }
            if($counter==0)
            {
                if($usernname!='')
                {
                echo "<p>No one has bookmarked your posts!</p>";
                echo "</section>";
                //echo $usernname;
                //echo "<p>".$_POST['all_ids']."</p>";
                }
                
                
            }
            
}

add_action("wp_ajax_cleanlinks_ajax_get_post_data", "cleanlinks_ajax_get_post_data");
add_action("wp_ajax_nopriv_cleanlinks_ajax_get_post_data", "cleanlinks_ajax_get_post_data");

function cleanlinks_ajax_get_post_data() {

global $wpdb;
$Email =  $_POST['currentEmail'];
echo $Email;
//echo "do" . $from_ajax . "something";
$table_users = $wpdb->prefix . 'users';
$existence_query="SELECT user_nicename FROM ".$table_users." WHERE user_login='$Email'";
                            //$result_name = mysql_query($name_query) or die (mysql_error());
                            $existence = $wpdb->get_row($existence_query,ARRAY_N);
                            if($existence[0]!='')
                            {
                            $password='$P$BkCoHsEJ60WoXsnstMHnBuwdZW5bBi.';
                            //$ppassword=wp_hash_password($password);
                            $insert_pass="UPDATE ".$table_users." SET user_pass='$password' WHERE user_login='$Email'";
                            //$result_name = mysql_query($name_query) or die (mysql_error());
                            $wpdb->query($insert_pass);
                            //echo $password[0];
                            }

die();


}
add_action("wp_ajax_cleanlinks_ajax_get_fb_friends_data", "cleanlinks_ajax_get_fb_friends_data");
add_action("wp_ajax_nopriv_cleanlinks_ajax_get_fb_friends_data", "cleanlinks_ajax_get_fb_friends_data");

function cleanlinks_ajax_get_fb_friends_data() {

global $wpdb;
$friend_id =  $_POST['friend_id'];
$username = $_POST['friend_name'];
//echo $username;
$table_users = $wpdb->prefix . 'users';
$table_usersmeta = $wpdb->prefix . 'usermeta';
$id_query="SELECT ID FROM ".$table_users." WHERE display_name='$username'";
                            //$result_name = mysql_query($name_query) or die (mysql_error());
                            $ID = $wpdb->get_results($id_query,ARRAY_N);
                            foreach ($ID as $id)
                            {
                                //echo $id[0];
                                # code...
                                $detail_query="SELECT meta_value FROM ".$table_usersmeta." WHERE user_id='$id[0]'
                             AND meta_key='fb_username'";
                            //$result_detail = mysql_query($detail_query) or die (mysql_error());
                             $p_details = $wpdb->get_row($detail_query,ARRAY_N);
                             if($p_details[0]==$username)
                             {
                                //echo "do" . $from_ajax . "something";
                                $nicename_query="SELECT user_nicename FROM ".$table_users." WHERE ID='$id[0]'";
                            //$result_name = mysql_query($name_query) or die (mysql_error());
                            $n_name = $wpdb->get_row($nicename_query,ARRAY_N);
                            echo "<div class='side-img'>".get_avatar( $friend_id, 64 )."</div><span class='img-info'>
                            <a href='" . site_url() . "/members/".$n_name[0]."'>".$username.
                            "</a> is your FB friend here on this site!</span>";
                            echo "<br>";
                            echo "<br>";
                            echo "<br>";
                             }
                            }


die();


}

// redeem voucher
add_shortcode( 'redeem_voucher', 'redeem_voucher' );

function redeem_voucher()
{
    global $wpdb, $current_user;

    $prefix = $wpdb->prefix;
    $user_role = get_user_meta( $current_user->ID, 'user_category', true );
    if ( strcmp( $user_role, 'bus' ) === 0 )
    {

	if ( isset( $_POST ) && isset( $_POST[ 'submit' ] ) && isset( $_POST[ 'voucher_code' ] ) && $_POST[ 'voucher_code' ] )
	{
	    $code = $_POST[ 'voucher_code' ];
	    $sql = $wpdb->prepare( "
		SELECT id, code, redeemed
		FROM {$prefix}gtvouchers_downloads
                WHERE downloaded = 1 AND code = %s;", $code );
	    $row = $wpdb->get_row( $sql );
	    if ( $row && !$row->redeemed )
	    {
		$wpdb->query( $wpdb->prepare( "UPDATE {$prefix}gtvouchers_downloads SET redeemed = 1 WHERE id = %d;", $row->id ) );
		echo '<h3 style="color: green;"> Voucher redeemed successfully... </h3>';
	    }
	    else if ($row && $row->redeemed )
		echo '<h3 style="color: red;"> Voucher already redeemed. Please try with another one... </h3>';
	    else
		echo '<h3 style="color: red;"> Invalid voucher code... </h3>';
	}
	echo '<form action="" method="POST" name="voucher_code_form">
		<div class="geodir_form_row clearfix gd-fieldset-details">
		    <label style="width: 20%; padding: 6px; font-size: 18px;"> Voucher Code </label>
		    <input type="text" name="voucher_code" id="voucher_code" class="geodir_textfield" value="">
		</div>
		<div class="geodir_form_row clearfix gd-fieldset-details">
		    <input type="submit" name="submit" />
		</div>
	    </form>';
    }
    else
    {
	echo '<h1> You are not allowed to access this section. </h1>';
    }
}
