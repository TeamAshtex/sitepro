<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'cirbiz_main');

/** MySQL database username */
define('DB_USER', 'cirbiz_main');

/** MySQL database password */
define('DB_PASSWORD', 'MAIN**main**');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'bXk50,w^!cnsgI-Ev:8-Ix[M-oMRl`7zN]QZsU<Qu[FEYRx-A8y}`(48LSLSx%7W');
define('SECURE_AUTH_KEY',  'n/5Pq/Xs!PML==TFj$$tXDY-/tSyjx0:;r>@fr%f@6v91-%#W4 ko;9FF+WGSaEO');
define('LOGGED_IN_KEY',    '0>6PeA+mn(ks W%d_h:Ms!9SwcmWwOG|4agO0Qb!QNGDq_j/:Q[.KR*Xx|!(ycz%');
define('NONCE_KEY',        'hm1,P 8;|6D!Pj-RAfLGZBKxcZRGj{FTrZri^h{@nIMBL|yKODmoY&Rk^GricKwp');
define('AUTH_SALT',        'oh}gS}%Ty%gc?P+GLG~LxC1Y,<`LxUczu&>Aey@3[;cYK`RLKWLi>N.Ncll^R,1_');
define('SECURE_AUTH_SALT', '22tLL?naxy3udJLnux(#z=b,|Rnai1mfBzp)T]Yoi0Y_fxB}%^X0,ZCjDD?+ny 2');
define('LOGGED_IN_SALT',   '=^+XsWMMzz{=[TrUM$Y8Oj.5q%X3O>py1.7h?/Y){<HT(Dk5m>}H]qzOeJ(vRVVP');
define('NONCE_SALT',       '-v<nBH`2*sOh1xgab7Mgnjk<(wGBTh4EzhwUiT--@2k-Ga-LMP_s|11[`zOb{5|b');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'DEV_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
